@component('helpdesk.components.modal', [
    'id'    => 'assign_company_to_gear',
    'title' => 'Asignar Compañia A Equipo'
])

    @slot('body')
        <form :action="form_url" method="post" id="assign_gear">
            {{ csrf_field() }}
            @include('helpdesk.stock.partials.select_company_sites_form')
        </form>
    @endslot


    @slot('footer')
        <button type="submit" form="assign_gear" class="btn btn-success">
            <i class="fa fa-check"></i>
            Asignar Compañia
        </button>
    @endslot


@endcomponent