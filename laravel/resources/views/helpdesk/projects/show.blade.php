@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')

	
	<div v-cloak>


		<div class="card-header d-flex align-items-center">
			<p class="lead">{{ str_limit($project->title, 50) }}</p>
			<a href="{{ route('projects.edit', $project) }}" class="ml-auto btn btn-primary mr-2"><i class="fa fa-pencil-alt"></i> Editar</a>
			@include('helpdesk.projects.partials.active_cancel_project_button')
		</div>
		
		
		@include('helpdesk.projects.partials.wizard')


		<table v-if="project.condition" class="table table-striped m-0" style="border-top: 1px solid #f2f2f2;">
			
			@include('helpdesk.projects.steps.one')

			@include('helpdesk.projects.steps.two')

		</table>

		<div class="card-body text-center mt-3" v-else>
			
			<i class="fa fa-archive fa-3x" style="color: #ddd;"></i>

			<p class="lead mt-3">

				El proyecto se encuentra cancelado.

			</p>


		</div>


		@include('helpdesk.projects.partials.require-revision')

		@include('helpdesk.projects.partials.generate-quotation')

		@include('helpdesk.projects.partials.new-ticket')	

		<div v-if="quotation && quotation.accepted == 0">

			@include('helpdesk.projects.partials.validate-quotation')

		</div>


	</div>

	

@stop



@section('extra_panels')

	
	@include('helpdesk.projects.partials.tickets')
	

@stop



@section('scripts')


	<script>
		
		var project_app = new Vue({


			el: '#companyApp',

			data: {

				project: @json($project),
				step: {{ $project->status }},
				need_revision: true,
				revision_ticket: @json($revision_ticket),
				revision_ticket_status: "{{ $revision_ticket_status }}",
				quotation: @json($quotation),
				quotation_resources: @json($quotation_resources),
				tickets: @json($tickets),
				newticket:{
					department: {
						id: null,
						agents: null
					},
					categories: {

						b: [],
						c: []

					}
				}


			},

			mounted: function(){

				var t = this;

				t.go_to_step(t.step);

				if( ! t.revision_ticket && t.step > 1){


					$('#withOutRevision').attr('checked', 'checked');

					t.need_revision = false;

					$('#requireRevisionBtn').attr('disabled', 'disabled');


				}

			},

			methods: {

				
				@include('helpdesk.projects.scripts.alert')
				@include('helpdesk.projects.scripts.project-status')
				@include('helpdesk.projects.scripts.ticket-route')
				@include('helpdesk.projects.scripts.go-to-step')
				@include('helpdesk.projects.scripts.validate-steps')
				@include('helpdesk.projects.scripts.require-revision')
				@include('helpdesk.projects.scripts.no-require-revision')
				@include('helpdesk.projects.scripts.quotation')
				@include('helpdesk.projects.scripts.new-ticket')



			}


		});

	</script>


@stop