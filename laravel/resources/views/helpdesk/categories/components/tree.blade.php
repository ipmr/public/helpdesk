@component('helpdesk.categories.components.folder_link', ['category' => $category])@endcomponent
<a href="{{ route('category.show', [ 'category' => $category->id ]) }}">{{ $category->name }}</a> <br>

@if($category->childs->count() > 0)
<div class="collapse company_tree" id="category_{{ $category->id }}">
    @foreach($category->childs as $child)
            <div class="pl-4 tree_link">
                @component('helpdesk.categories.components.tree', ['category' => $child])@endcomponent
            </div>
    @endforeach
</div>
@endif
