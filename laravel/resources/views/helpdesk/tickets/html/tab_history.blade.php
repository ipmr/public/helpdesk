<div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
	@if(count($ticket->histories) > 0)
	<table class="table table-sm table-bordered table-striped m-0">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Evento</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ticket->histories()->latest()->get() as $history)
			<tr>
				<td class="no-wrap">{{ $history->created_at->format('d M, Y h:i a') }}</td>
				<td>{!! $history->description !!}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p class="m-0">No han ocurrido eventos</p>
	@endif
</div>