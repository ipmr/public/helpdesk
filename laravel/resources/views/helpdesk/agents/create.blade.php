@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card" id="agentApp">
		<div class="card-header">
			<a class="btn btn-link p-0" href="{{ route('agents.index') }}/">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<div class="card-body">
			<form action="{{ route('agents.store') }}" method="post" id="newAgentForm">
				
				<div class="form-row">
					<div class="col-sm-4 mx-auto">
						<label><b>Información del agente</b></label>
						<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
					</div>
				</div>

				{{ csrf_field() }}
				
				@if(request('department'))
				<input type="hidden" name="rel" value="{{ request('department') }}">
				@endif
				@include('helpdesk.agents.partials.form')
			</form>
		</div>
		<div class="card-footer">
			<div class="row form-row">
				<div class="col-sm-8 ml-auto">
					<button class="btn btn-success" form="newAgentForm"> <i class="fa fa-check"></i> Guardar Agente</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
	

	@include('helpdesk.agents.scripts.general')


@stop