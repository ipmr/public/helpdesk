<?php

namespace App\Console\Commands;

use App\Agent;
use App\Department;
use App\User;
use App\Company;
use App\Contract;
use App\SlaSeverity;
use App\Status;
use App\Ticket;
use Illuminate\Console\Command;
use App\Notifications\NoActivityTicketSla;
use App\Notifications\ChangeTicketStatus;
use App\Notifications\NotResolveTicket;
use Notification;

class SLANoActivityTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:sla_no_activity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Script para avisar dependiendo del status del ticket el tiempo de inactividad por sla';

    public $periods = [
        'h' => 'hour',
        'd' => 'day',
        'm' => 'month',
        'y' => 'year',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses          = Status::whereNotIn('name', ['Resuelto', 'Cerrado'])->get();
        $send_notification = [
            'response' => [],
            'resolve'  => [],
        ];

        $tickets = Ticket::whereIn('status_id', $statuses);

        if ($tickets->count() > 0) {

            foreach ($tickets->get() as $ticket) {

                $SLA = $ticket->company->Sla;

                if ($SLA) {

                    $sla_severity = SlaSeverity::where('sla_id', $SLA->id)->where('severity_id', $ticket->severity_id)->first();

                    if (isset($sla_severity->id)) {

                        $time_to_respond = date('Y-m-d H:i:s', strtotime('+' . $sla_severity->respond_number . ' ' . $this->periods[$sla_severity->respond_format] . $ticket->created_at));

                        // echo "response tickets ";

                        if ($this->ticket_validation($ticket, $time_to_respond)) {

                            if ($ticket->agent_id) {

                                $send_notification['response']['agents'][$ticket->agent_id][] = $ticket;
                            } else if ($ticket->department_id) {

                                $send_notification['response']['departments'][$ticket->department_id][] = $ticket;
                            } else {

                                $send_notification['response']['not_asigned'][] = $ticket;
                            }
                        } 

                        $time_to_resolve = date('Y-m-d H:i:s', strtotime('+' . $sla_severity->resolve_number . ' ' . $this->periods[$sla_severity->resolve_format] . $ticket->created_at));

                        // echo "resolve tickets ";
                        if ($this->ticket_validation($ticket, $time_to_resolve)) {

                            if ($ticket->agent_id) {

                                $send_notification['resolve']['agents'][$ticket->agent_id][] = $ticket;
                            } else if ($ticket->department_id) {

                                $send_notification['resolve']['departments'][$ticket->department_id][] = $ticket;
                            } else {

                                $send_notification['resolve']['not_asigned'][] = $ticket;
                            }
                        } 
                    }
                }
            }
            ### response ###
            #notificando al agente
            if (isset($send_notification['response']['agents'])) {
                foreach ($send_notification['response']['agents'] as $agent => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $agent = Agent::where('id', $agent)->first();

                    #notificacion
                    Notification::send($agent->user, new NoActivityTicketSla($notification_data));
                    echo "Enviando a agente " . $agent->user->FullName . " las notificaciones\n";
                }
            }

            #notificando al supervisor
            if (isset($send_notification['response']['departmens'])) {
                foreach ($send_notification['response']['departmens'] as $department => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $department = Department::where('id', $department)->first();

                    #notificacion
                    if (isset($department->supervisor)) {
                        
                        Notification::send($department->supervisor->user, new NoActivityTicketSla($notification_data));
                        echo "Enviando a supervisores " . $agent->user->FullName . " las notificaciones\n";
                    }
                }
            }

            #notificando al manager
            if (isset($send_notification['response']['not_asigned'])) {
                $notification_data = [
                    'tickets' => $send_notification['response']['not_asigned'],
                ];

                $managers = User::where('role', 'manager')->get();

                Notification::send($managers, new NoActivityTicketSla($notification_data));
                echo "Enviando a managers las notificaciones\n";
            }

            ### resolve ###
            #notificando al agente
            if (isset($send_notification['response']['agents'])) {
                foreach ($send_notification['response']['agents'] as $agent => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $agent = Agent::where('id', $agent)->first();

                    #notificacion
                    Notification::send($agent->user, new NotResolveTicket($notification_data));
                    echo "Enviando a agente " . $agent->user->FullName . " las notificaciones\n";
                }
            }

            #notificando al supervisor
            if (isset($send_notification['response']['departmens'])) {
                foreach ($send_notification['response']['departmens'] as $department => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $department = Department::where('id', $department)->first();

                    #notificacion
                    if (isset($department->supervisor)) {
                        
                        Notification::send($department->supervisor->user, new NotResolveTicket($notification_data));
                        echo "Enviando a supervisores " . $agent->user->FullName . " las notificaciones\n";
                    }
                }
            }

            #notificando al manager
            if (isset($send_notification['response']['not_asigned'])) {
                $notification_data = [
                    'tickets' => $send_notification['response']['not_asigned'],
                ];

                $managers = User::where('role', 'manager')->get();

                Notification::send($managers, new NotResolveTicket($notification_data));
                echo "Enviando a managers las notificaciones\n";
            }

        }
    }

    public function ticket_validation($ticket, $date_search)
    {
        $result = true;
        $today  = date('Y-m-d');
        $hour   = date('H');
        $day    = (int) date('N');

        #si no hay comentarios en el ticket desde que se creo notificar
        $comments = $ticket->comments();
        if ( date( 'Y-m-d', strtotime( $date_search ) ) < $today ) {

            $comments->whereDate( 'created_at', '>=', $date_search);
        }
        $comments->whereDate( 'created_at', '<=', date( 'Y-m-d H:i:s', strtotime( $today ) ) );
        if ($comments->count() > 0) {
            // echo 'tiene comentarios vigentes #' . $ticket->ticket_id . "\n";
            return false;
        }


        #si no hay historial en el ticket desde que se creo notificar
        $histories = $ticket->histories();
        if ( date( 'Y-m-d', strtotime( $date_search ) ) < $today ) {

            $histories->whereDate( 'created_at', '>=', $date_search);
        }
        $histories->whereDate( 'created_at', '<=', date('Y-m-d H:i:s', strtotime( $today ) ) );
        if ($histories->count() > 0) {
            // echo 'tiene historial vigente #' . $ticket->ticket_id . "\n";
            return false;
        }


        #si no hay tareas en el ticket desde que se creo notificar
        $tasks = $ticket->tasks();
        if ( date( 'Y-m-d', strtotime( $date_search ) ) < $today ) {

            $tasks->whereDate( 'created_at', '>=', $date_search);
        }
        $tasks->whereDate( 'created_at', '<=', date('Y-m-d H:i:s', strtotime( $today ) ) );
        if ($tasks->count() > 0) {
            // echo 'no tiene tareas pendientes #' . $ticket->ticket_id . "\n";
            return false;
        }


        #verificar que el contracto est'e en vigencia
        $contract = $ticket->Contract;
        $coverage = $ticket->Coverage;
        $gear     = $ticket->gear;

        if (isset($contract->id)) {
            if(isset($gear->id)){

                $vigency = date( 'Y-m-d', strtotime( '+'.$contract->period_number.' '.$this->periods[ $contract->period_format ].' '.$gear->contract_start_at ) );
            } else {
                
                $vigency = date( 'Y-m-d', strtotime( '+'.$contract->period_number.' '.$this->periods[ $contract->period_format ].' '.$ticket->company->contract_start_at ) );
            }

            #si al dia de hoy el contrato es menor al dia de vigencia, se notifica
            if ($today > $vigency) {
                // echo 'el ticket no tiene vigencia '.$vigency."\n";
                $result = false;
            }
        }

        #se debe validar la cobertura
        if ( isset( $coverage->id ) ) {
            
            if ($coverage->from_day < $day && $day > $coverage->to_day ) {
                // echo "la cobertura del ticket #".$ticket->ticket_id." no cubre el dia ".date('l')."\n";
                $result = false;
            }
            
            if ($coverage->from_hour < $hour && $hour > $coverage->to_hour ) {
                // echo "la cobertura no cubre la hora ".$hour."\n";
                $result = false;
            }
        }

        if ( $today >= date('Y-m-d', strtotime( $date_search ) ) ) {
            
            // echo "ticket ".$ticket->ticket_id." pasa a notificacion por inactividad mayor al sla\n";
            if ($ticket->get_status->name == 'Abierto') {

                $new_status = Status::where('name', 'En proceso')->first();

                $ticket->update([
                    'status_id' => $new_status->id,
                ]);

                $notification_data = [
                    'ticket' => $ticket,
                    'status' => $new_status,
                    'user'   => $ticket->author
                ];

                Notification::send($ticket->author, new ChangeTicketStatus($notification_data));

            }
            $result = true;
        }

        // echo "ticket ".$ticket->ticket_id." pasa a notificacion por inactividad";
        // echo "\n";
        return $result;
    }
}
