<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_data_status($request);

        $status = new Status();

        $status->name        = $request->name;
        $status->description = $request->description;
        $status->color       = '#8e8e92';
        $status->static      = 0;

        $status->save();

        return redirect()
            ->route('ticket-options.index')
            ->with('msg', 'Se ha creado el estado ' . $status->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show(Status $status)
    {
        // return view('helpdesk.status.show', compact('status') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit(Status $status)
    {
        $view = 'helpdesk.status.edit';

        return view($view, compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Status $status)
    {
        $this->validate_data_status($request, $status);

        if($request->has('name') && !$status->static){
            $status->name        = $request->name;
        }
        $status->description = $request->description;
        $status->save();

        return redirect()
            ->route('ticket-options.index')
            ->with('msg', 'Se ha actualizado el estado ' . $status->name . ' exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        //
    }

    public function validate_data_status($request, $status = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required' => 'El nombre del estado es requerido',
            'name.unique'   => 'El nombre del estado no esta disponible',
        ];

        $validations = [

            'name' => ['sometimes', 'required', 'string', Rule::unique('statuses')->ignore($status ? $status->id : '')],
            'description' => 'string|nullable'

        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}
