<table style="font-family: 'Nunito', sans-serif;font-size: 9px;" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Tipo</th>
            <th># Serie</th>
            <th>Módulos</th>
            <th>Contrato</th>
            <th>Covertura</th>
            <th>Estatus</th>
            <th>Sitio</th>
        </tr>
    </thead>
    <tbody>
        @if ($gears->count() > 0)
            @foreach ($gears as $gear)
                <tr style="background-color:{{ ($loop->index % 2 == 0)?'#F2F2F2':'#FFF' }};">
                    <td>{{ $gear->brand }}</td>
                    <td>{{ $gear->model }}</td>
                    <td>{{ $gear->type }}</td>
                    <td>{{ $gear->serial_number }}</td>
                    <td>{{ $gear->modules->count() }}</td>
                    <td>{{ ($gear->contract)?$gear->contract:'Sin Asignar' }}</td>
                    <td>{{ ($gear->coverage)?$gear->coverage:'Sin Asignar' }}</td>
                    <td>{{ $gear->status }}</td>
                    <td>{{ $gear->company }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>