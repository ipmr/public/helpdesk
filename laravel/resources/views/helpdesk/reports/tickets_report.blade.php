<style>
    
    
    html,body{

        padding: 0;
        margin: 0;

    }

    body{

        font-family: 'Helvetica', sans-serif;
        font-size: 9px;
        position: relative;
        padding-top: 50px;

    }
    

    h2.header_title{

        font-weight: lighter;
        font-size: 22px !important;
        color: #fff;
        position: fixed;
        top: 10px;
        right: 20px;
        z-index: 1;
        text-align: right;
        display: inline-block;
        width: 100%;

    }


    h3{

        font-weight: lighter;
        font-size: 18px !important;
        text-transform: uppercase;
        color: #13395f;

    }


    h4{

        font-weight: lighter;
        font-size: 14px !important;
        text-transform: uppercase;
        color: #13395f;
        margin: 0 0 15px 0;

    }

    .header_img{

    
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        z-index: -1;
        

    }

    .full_content{
        padding: 40px;
    }
    
    .page-break {
        
        page-break-after: always;

    }

</style>


{{-- <img src="{{ asset('img/pdf_header.jpg') }}" class="header_img" alt=""> --}}

<div class="full_content">
    

    <h1 style="text-align: center;">
        Reporte Actividad Tickets del {{ $params['start_date'] }} a {{ $params['end_date'] }}
    </h1>

    {{-- fast data --}}
    <table width="100%" cellpadding="5" cellspacing="0" border="1">
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center;">
                    <label style="font-size: 18px;"><b>Tiempos de atención al cliente:</b></label>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <p style="font-weight: bold; font-size: 14px; color: #007bff;">Atención Remota</p>
                    <p style="font-size: 24px;">{{ $complements['total_remote_time'] }}</p>
                </td>
                <td style="text-align: center;">
                    <p style="font-weight: bold; font-size: 14px; color: #e83e8c;">Atención En Sitio</p>
                    <p style="font-size: 24px;">{{ $complements['total_site_time'] }}</p>
                </td>
                <td style="text-align: center;">
                    <p style="font-weight: bold; font-size: 14px; color: #28a745;">Total de tiempo</p>
                    <p style="font-size: 24px;">{{ $complements['total_service_time'] }}</p>
                </td>
            </tr>
        </tbody>
    </table>

    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="padding-top: 30px;">
        <tbody>
            <tr>
                <td colspan="{{ count($complements['status']) }}" style="text-align: center;">
                    <label style="font-size: 18px;"><b>Tickets por status:</b></label>
                </td>
            </tr>
            <tr>
                @foreach ($complements['status'] as $status => $status_data)
                    <td style="text-align: center;">
                        <p style="font-weight: bold; font-size: 18px; color: {{ $status_data['color'] }};">({{ $status_data['total_tickets_status'] }}) Tickets</p>
                        <p style="font-size: 14px;">{{ $status }}</p>
                    </td>
                @endforeach
            </tr>
        </tbody>
    </table>

    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="padding-top: 30px;">
        <tbody>
            <tr>
                <td colspan="{{ count($complements['severity']) }}" style="text-align: center;">
                    <label style="font-size: 18px;"><b>Tickets por severidad:</b></label>
                </td>
            </tr>
            <tr>
                @foreach ($complements['severity'] as $severity => $severity_data)
                    <td style="text-align: center;">
                        <p style="font-weight: bold; font-size: 18px; color: {{ $severity_data['color'] }};">({{ $severity_data['total_tickets_severity'] }}) Tickets</p>
                        <p style="font-size: 14px;">{{ $severity }}</p>
                    </td>
                @endforeach
            </tr>
        </tbody>
    </table>

    <div class="page-break"></div>

    <table width="100%" cellpadding="5" cellspacing="0" border="1">
        <thead>
            <tr>
                <th>#</th>
                <th># Ticket</th>
                <th>Asunto</th>
                <th>Compañia</th>
                <th>Severidad</th>
                <th>Estado</th>
                <th>Categoría</th>
                <th>Agente</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        <a href="{{ route('tickets.show', $ticket->ticket_id) }}" title="">
                            #{{ $ticket->ticket_id }}
                        </a>
                    </td>
                    <td>{{ $ticket->subject }}</td>
                    <td>{{ $ticket->company }}</td>
                    <td><span style="color: {{ $ticket->severity_color }}">{{ $ticket->severity }}</span></td>
                    <td><span style="color: {{ $ticket->status_color }}">{{ $ticket->status }}</span></td>
                    <td>{{ $ticket->category }}</td>
                    <td>{{ $ticket->first_name.' '.$ticket->last_name }}</td>
                    <td>{{ $ticket->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>