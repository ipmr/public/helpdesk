<?php

namespace App;

use App\DepartmentGroupAgent;
use App\DepartmentGroup;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table   = 'agents';
    public $timestamps = true;
    protected $guarded = [];

	public function user(){

		return $this->belongsTo('App\User', 'user_id', 'id');

	}

    public function tickets(){

    	return $this->hasMany('App\Ticket', 'agent_id', 'id');

    }


    public function getFullNameAttribute(){

        return "{$this->user->full_name}";

    }


    public function department(){

    	return $this->belongsTo('App\Department', 'department_id', 'id');

    }


    public function get_groups(){


        return $this->hasMany('App\DepartmentGroupAgent', 'agent_id', 'id');


    }


    public function getGroupsAttribute(){

        $groups = collect([]);

        foreach($this->get_groups as $group){

            $groups->push($group->group);

        }

        return $groups;

    }


    public function getSupervisorInGroupsAttribute(){


        $groups = DepartmentGroup::where('supervisor_id', $this->id)->get();

        return $groups;


    }



    public function supervisor(){

        return $this->belongsTo('App\Department', 'id', 'supervisor_id');

    }

    public function tasks(){

    	return $this->hasMany('App\Task', 'agent_id', 'id');

    }

}
