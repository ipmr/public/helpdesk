@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <p class="lead">Crear Nuevo Proyecto</p>
            </div>
            <div class="card-body" id="project_app">
                <form action="{{ route('projects.store') }}" method="post" id="newProjectForm">
                    
                    {{ csrf_field() }}

                    @include('helpdesk.projects.partials.form', ['global' => true])
                    
                </form>
            </div>
            <div class="card-footer">
                <div class="row form-row">
                    <div class="col-sm-8 ml-auto">
                        <button type="submit" form="newProjectForm" id="department_form_btn" class="btn btn-success">
                            <i class="fa fa-check"></i>
                            Crear Proyecto
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    
    <script src="{{ asset('js/vue.js') }}"></script>
    <script>
        var ticketApp = new Vue({

            el: '#project_app',

            data: {
                zones : null,
            },

            methods: {

                get_company_gears_models: function(e){

                    var t = this;

                    var select = $(e.target);

                    var company = select.val();

                    var url = '{{ route('companies.gears.index', '#company_id') }}';

                    url = url.replace('#company_id', company);

                    t.zones = null;

                    $.get(url, function(response){

                        t.zones = response.zones;

                    });

                },
            }
        });
    </script>

@stop