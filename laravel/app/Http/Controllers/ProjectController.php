<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Category;
use App\Company;
use App\CompanyProject;
use App\Project;
use App\ProjectTicket;
use App\ProjectMember;
use App\ProjectHistory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;


class ProjectController extends Controller
{

    public function __construct()
    {

        // $this->middleware('under-construction');
        $this->middleware('project_canceled')->except(['index','create','store','active_project','project_details']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $companies = Company::get();

        return view('helpdesk.project.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::latest()->where('master', 1)->get();

        return view('helpdesk.project.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_project($request);

        $project = new Project();

        $project->title       = $request->title;
        $project->description = $request->description;
        $project->fup         = $request->fup;
        if (isset($request->start_date)) {
            $project->start_date = $request->start_date;
        }
        if (isset($request->end_date)) {
            $project->end_date = $request->end_date;
        }
        $project->status      = 1;

        $project->save();

        $company_project             = new CompanyProject();
        $company_project->project_id = $project->id;
        $company_project->company_id = $request->company_id;

        $company_project->save();


        $project_member = new ProjectMember();
        
        $project_member->project_id = $project->id;
        $project_member->user_id    = auth()->user()->id;
        $project_member->type       = 'owner';

        $project_member->save();

        $this->load_history($project, "Se creó el proyecto");


        return redirect()
            ->route('companies.projects.show', [$company_project->company, $project->fup])
            ->with('msg', 'El proyecto "' . $project->title . '" ha sido creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('helpdesk.project.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {

        $project->update($request->except(["_token", "_method"]));

        $this->load_history($project, "Se actualizó el proyecto");

        if ($request->ajax()) {

            return response()->json($project);

        }

        $company = optional($project->company_project)->company;

        return redirect()
            ->route('companies.projects.show', ['company' => $company->id, 'project' => $project->fup])
            ->with('msg', 'El proyecto "' . $project->title . '" fue actualizado correctamente');

    }

    public function complete_revision_ticket($ticket)
    {
        $project_ticket = ProjectTicket::where('ticket_id', $ticket->ticket_id)->first();

        if ($project_ticket && $project_ticket->phase == 1) {

            $project = $project_ticket->project;

            $project->update([

                'status' => 2,

            ]);

            $this->load_history($project, "Se ha resuelto ticket  #{$ticket->ticket_id}, ha completado el levantamiento");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    public function validate_project($request, $project = null)
    {

        #se especifican los mensajes para las validaciones
        $validations = [
            'fup'             => ['required', 'string', Rule::unique('projects')->ignore($project ? $project->id : 'unique:projects,fup')],
            'title'           => 'required|string|max:190',
            'description'     => 'required|string',
            'company_id'      => 'required|exists:companies,id',
        ];

        $msgs = [
            'company_id.required'    => 'La Compañia o el Sitio es requerido',
            'company_id.exists'      => 'La Compañia o el Sitio no existen en la base de datos',
            'fup.required'           => 'El número de FUP es requerido',
            'fup.unique'             => 'El número de FUP no esta disponible',
            'title.required'         => 'El título es requerido',
            'title.string'           => 'El título debe ser alfanumérico',
            'title.max'              => 'El título debe contener 190 carácteres',
            'description.required'   => 'La descripción es requerido',
            'description.string'     => 'La descripción debe ser alfanumérico',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();
    }

    public function load_history(Project $project, $description)
    {
        $project_history = new ProjectHistory();

        $project_history->project_id  = $project->id;
        $project_history->description = $description;
        $project_history->status      = $project->status;
        $project_history->condition   = $project->condition;       

        $project_history->save(); 
    }

    public function project_ticket(Request $request, Company $company, $project)
    {

        $user = auth()->user();

        $project = Project::where('fup', $project)->first();

        $request['company_id'] = $company->id;

        if($request->has('subcategory_c')){
            $request['category_id'] = $request->subcategory_c;
        }elseif($request->has('subcategory_b')){
            $request['category_id'] = $request->subcategory_b;
        }


        $new_ticket = app('App\Http\Controllers\TicketController')->store($request);

        
        app('App\Http\Controllers\TicketController')->update_department($request, $new_ticket, $user);

        app('App\Http\Controllers\TicketController')->update_agent($request, $new_ticket, $user);


        if($request->has('revision')){

            $add_ticket_to_project = new ProjectTicket();

            $add_ticket_to_project->phase      = 1;
            $add_ticket_to_project->project_id = $project->id;
            $add_ticket_to_project->ticket_id  = $new_ticket->ticket_id;

            $add_ticket_to_project->save();

            $response = [

                'msg'           => "Se ha solicitado el levantamiento a través del ticket #{$new_ticket->ticket_id}",
                'ticket'        => $new_ticket,
                'ticket_status' => $new_ticket->get_status->name,

            ];

        }else{


            $add_ticket_to_project = new ProjectTicket();

            $add_ticket_to_project->phase      = 3;
            $add_ticket_to_project->project_id = $project->id;
            $add_ticket_to_project->ticket_id  = $new_ticket->ticket_id;

            $add_ticket_to_project->save();

            $response = [

                'msg'           => "Se ha creado el ticket #{$new_ticket->ticket_id} exitosamente.",
                'tickets'       => $project->tickets

            ];

        }

        $this->load_history($project, $response['msg']);

        return response()->json($response);
    }

    public function cancel_project($project)
    {
        $project = Project::where('fup', $project)->first();
        $project->condition = 0;
        $project->save();

        $this->load_history($project, "Se ha cancelado el proyecto");

        return redirect()
            ->back()
            ->with('msg', 'El proyecto "' . $project->title . '" ha sido cancelado');
    }

    public function active_project($project)
    {
        $project = Project::where('fup', $project)->first();
        $project->condition = 1;
        $project->save();

        $this->load_history($project, "Se ha activado el proyecto");

        return redirect()
            ->back()
            ->with('msg', 'El proyecto "' . $project->title . '" ha sido activado');
    }

    public function project_details(Request $request)
    {
        $project = Project::where('fup', $request->fup)->first();
        $data = [
            'histories' => $project->history()->where('status', $request->status)->orderBy('id', 'DESC')->get(),
        ];

        return response()->json( $data );
    }
}
