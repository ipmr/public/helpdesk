<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyUserType extends Model
{
    public function zone_users(){

        return $this->hasMany('App\ZoneUser', 'role', 'id');
    }
}
