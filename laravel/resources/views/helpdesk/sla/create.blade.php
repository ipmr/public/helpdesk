@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<a href="{{ route('sla.index') }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<div class="card-body">
			<form action="{{ route('sla.store') }}" method="post" id="newSLAForm">
				
				@include('helpdesk.sla.partials.form')

			</form>
		</div>
		<div class="card-footer">
			<div class="row form-row">
				<div class="col-sm-9 ml-auto">
					<button type="submit" class="btn btn-success" form="newSLAForm">
						<i class="fa fa-check"></i>
						Guardar Acuerdo de Servicio
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop