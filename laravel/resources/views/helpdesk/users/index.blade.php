@extends('layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Listado de usuarios
            </p>
            <form action="{{ route('users.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
            <a class="btn btn-primary ml-2" href="{{ route('users.create') }}/">
                <i class="fa fa-plus"></i>
                Nuevo Usuario
            </a>
        </div>
        @if(count($users) > 0)
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Nombre de Usuario</th>
                        <th>Correo</th>
                        <th>Tipo Usuario</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td><a href="{{ route('users.show', $user) }}" title="">{{ $user->full_name }}</a></td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ ($user->role == 'admin')?'Administrador':'Manager' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-footer">
                {{ $users->links() }}
            </div>
        @else
            <div class="card-body">No se han agregado tipos de contacto</div>
        @endif
    </div>
</div>
@stop
