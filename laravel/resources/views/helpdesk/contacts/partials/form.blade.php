

<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Nombre Completo:</label>
	<div class="col-sm-6">
		<div class="row form-row">
			<div class="col-sm-6">
				<input type="text" name="contact_first_name" placeholder="Nombre(s)" value="{{ $user->first_name or old('contact_first_name') }}" class="form-control" required>
			</div>
			<div class="col-sm-6">
				<input type="text" name="contact_last_name" placeholder="Apellido(s)" value="{{ $user->last_name or old('contact_last_name') }}" class="form-control" required>
			</div>
		</div>
	</div>
</div>
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> E-mail:</label>
	<div class="col-sm-3">
		<input type="email" name="email" value="{{ $user->email or old('email') }}" class="form-control" required>
	</div>
</div>
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Puesto:</label>
	<div class="col-sm-3">
		<input type="text" name="jobtitle" value="{{ $user->contact->jobtitle or old('jobtitle') }}" class="form-control" required>
	</div>
</div>
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">Teléfono:</label>
	<div class="col-sm-3">
		<input type="text" name="contact_phone" value="{{ $user->contact->phone or old('contact_phone') }}" class="form-control phone">
	</div>
	<div class="col-sm-2">
		<input type="text" name="contact_phone_ext" value="{{ $user->contact->ext or old('contact_phone_ext') }}" class="form-control" placeholder="Ext.">
	</div>
</div>
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">Movil:</label>
	<div class="col-sm-3">
		<input type="text" name="contact_mobile" value="{{ $user->contact->mobile or old('contact_mobile') }}" class="form-control phone">
	</div>
</div>
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">
		@isset($user)
			Restaurar Contraseña:
		@else
			<span class="text-danger">*</span>  Contraseña:
		@endif
	</label>
	<div class="col-sm-3">
		<input type="password" name="password" class="form-control" @empty($company) required @endempty>
	</div>
</div>