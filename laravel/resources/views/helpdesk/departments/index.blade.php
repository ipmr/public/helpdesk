@extends('layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Listado de departamentos
            </p>
            <form action="{{ route('departments.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
            <a class="btn btn-primary ml-2" href="{{ route('departments.create') }}/">
                <i class="fa fa-plus"></i>
                Nuevo Departamento
            </a>
        </div>
    	@if(count($departments) > 0)
        	<table class="table table-striped m-0">
        		<thead>
        			<tr>
        				<th>Nombre</th>
        				<th>Descripción</th>
                        <th class="text-center">Grupos</th>
        				<th>Supervisor</th>
        				<th class="text-center">Agentes</th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach($departments as $department)
					<tr>
						<td><a href="{{ route('departments.show', $department) }}/">{{ $department->name }}</a></td>
						<td>{{ $department->description }}</td>
                        <td class="text-center">{{ $department->groups->count() }}</td>
						<td>
							@if($department->supervisor)
								<a href="{{ route('agents.show', $department->supervisor) }}/">
									{{ $department->supervisor->user->first_name .' '. $department->supervisor->user->last_name }}
								</a>
							@else
								Sin supervisor
							@endif
						</td>
						<td class="text-center">{{ $department->agents()->count() }}</td>
					</tr>
        			@endforeach
        		</tbody>
        	</table>
            <div class="card-footer">
                {{ $departments->links() }}
            </div>
    	@else
			<div class="card-body">No se han agregado departamentos</div>
    	@endif
    </div>
</div>
@stop
