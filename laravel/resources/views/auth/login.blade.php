@extends('layouts.master')

@section('content')

    
    <div class="container">
        <div class="row">
            <div class="col-sm-5 mx-auto">
                <div class="card">
                    
                    <div class="card-header">
                        <p class="lead text-center">Servicio de tickets</p>
                    </div>

                    <div class="card-body">

                        <p class="text-info text-center mb-4">Introduce tu usuario y contraseña para continuar</p>

                        <form class="form-horizontal" method="POST" action="{{ route('login') }}" id="loginForm">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-mail / Usuario</label>
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Contraseña</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>

                            <div class="form-group m-0">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="remember_account" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                  <label class="custom-control-label" for="remember_account">Recordar Cuenta</label>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer d-block d-sm-flex">
                        <button type="submit" class="btn btn-primary btn-block btn-sm-inline-block mb-2 mb-sm-0" form="loginForm">
                            <i class="fa fa-check"></i>
                            Ingresar
                        </button>

                        <a class="btn btn-link d-block d-sm-inline-block m-0 ml-sm-auto text-center text-sm-left" href="{{ route('password.request') }}">
                            ¿Olvidaste tu contraseña?
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
