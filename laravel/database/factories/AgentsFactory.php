<?php

use Faker\Generator as Faker;

$factory->define(App\Agent::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'department_id' => 1,
        'jobtitle' => $faker->jobTitle
    ];
});
