<?php

namespace App\Http\Controllers;

use App\Notifications\AsignTask;
use App\Notifications\RequestTaskReAssign;
use App\Notifications\EndTask;
use App\Task;
use App\Ticket;
use Illuminate\Http\Request;
use Notification;
use Validator;

class TaskController extends Controller
{
    public function __construct()
    {

        #middleware para evitar modificaciones una vez que el ticket
        #se encuentra cerrado
        $this->middleware('ticket_closed')->only([
            'store',
            'update',
            'destroy',
            'reassign',
            'start_end_task',
        ]);

    }

    public function index()
    {

        $user = auth()->user();

        $tasks = $user->agent->tasks()->latest()->get();

        return view('helpdesk.tasks.index', compact('tasks'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request, $ticket)
    {

        $validate = Validator::make($request->all(), [
            'description' => 'required|string',
        ])->validate();

        $ticket = Ticket::where('ticket_Id', $ticket)->firstOrFail();

        $task = new Task();

        $task->ticket_id   = $ticket->ticket_id;
        $task->description = $request->description;

        if ($request->agent_id != 'null') {

            $task->agent_id = $request->agent_id;

        }

        $task->save();

        if ( isset( $task->agent ) ) {

            $notification_data = [
                'ticket' => $ticket,
                'task'   => $task,
                'agent'  => $task->agent,
            ];

            Notification::send( $task->agent->user, new AsignTask( $notification_data ) );
        }

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha creado una nueva tarea exitosamente.',
            'tab' => 'tasks',
        ]);
    }

    public function show($ticket, Task $task)
    {
        $ticket = Ticket::where('ticket_id', $ticket)->firstOrFail();

        if (\Request::ajax()) {

            return response()->json($task);

        }

        return view('helpdesk.tasks.show', compact('task'));
    }

    public function edit(Task $task)
    {
        //
    }

    public function update(Request $request, $ticket, Task $task)
    {
        $ticket = Ticket::where('ticket_id', $ticket)->firstOrFail();

        $validate = Validator::make($request->all(), [
            'description' => 'sometimes|string',
            'agent_id'    => 'sometimes|exists:agents,id',
            'status'      => 'sometimes|boolean',
        ]);

        $msg = null;

        if ($request->description) {
            $task->update([
                'description' => $request->description,
            ]);

            $msg = 'La tarea ha sido actualizada correctamente';
        }

        if ($request->agent_id) {

            $task->update([
                'agent_id' => $request->agent_id,
            ]);

            $msg = 'El agente de la tarea ' . $task->description . ' fue actualizado por ' . optional(optional($task->agent)->user)->FullName;

            $notification_data = [
                'ticket' => $ticket,
                'task'   => $task,
                'agent'  => $task->agent,
            ];

            Notification::send( $task->agent->user, new AsignTask( $notification_data ) );
        }

        if ($request->status) {
            $task->update([
                'status' => $request->status,
            ]);

            $msg = 'Se ha actualizado el estado de la tarea';
        }

        app('App\Http\Controllers\TicketController')->history($ticket, $msg);

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => $msg,
            'tab' => 'tasks',
        ]);
    }

    public function destroy($ticket, Task $task)
    {

        $task->events()->delete();

        $task->delete();

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha eliminado la tarea',
            'tab' => 'tasks',
        ]);
    }

    public function reassign(Request $request, $ticket, $task)
    {

        $user = auth()->user();


        $task = Task::find($task);


        $notification_data = [

            'agent'   => $user,
            'task'    => $task,
            'reasson' => $request->reasson,

        ];

        if ($task->ticket->department) {

            if ($task->ticket->department->supervisor_id) {

                $supervisor = $task->ticket->department->supervisor->user;

                $supervisor->notify(new RequestTaskReAssign($notification_data));

            }

            if ($task->ticket->agent && $task->ticket->agent_id != $task->ticket->department->supervisor_id) {

                $agent_assigned = $task->ticket->agent->user;

                $agent_assigned->notify(new RequestTaskReAssign($notification_data));

            }

            app('App\Http\Controllers\TicketController')->history($task->ticket, 'Solicitud de reasignación ' . $user->FullName . ' con el motivo ' . $request->reasson);
        }

        return redirect()->back()->with('msg', 'Tu solicitud de reasignación de tarea ha sido enviada');
    }

    public function start_end_task(Request $request, $ticket, $task)
    {

        $task = Task::where('id', $task)->first();

        /*$accion = '';

        if ($task->start_task == null && $task->end_task == null) {
        $task->update([
        'start_task' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $accion = 'Iniciado';
        } elseif ($task->start_task !== null && $task->end_task == null) {
        $task->update([
        'end_task'     => Carbon::now()->format('Y-m-d H:i:s'),
        'average_time' => Carbon::parse($task->start_task)->diffInMinutes(Carbon::parse($task->end_task)),
        'status'       => 1,
        ]);
        $accion = 'Terminado';
        }*/

        $task->update([
            'status' => 1,
        ]);

        if ( isset( $task->agent ) ) {
           
            $data_notification = [
                'ticket' => $task->ticket,
                'task'   => $task,
            ];

            if ( $task->agent_id != $task->ticket->agent_id ) {

                Notification::send( $task->ticket->agent->user, new EndTask( $data_notification ) );
            }

            if ($task->ticket->department->supervisor_id) {

                $supervisor = $task->ticket->department->supervisor->user;
                Notification::send( $supervisor, new EndTask( $data_notification ) );
            }
        }

        app('App\Http\Controllers\TicketController')->history($task->ticket, 'La tarea ' . $task->description . ' ha sido finalizada por ' . optional(optional($task->agent)->user)->FullName);

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha terminado la tarea',
            'tab' => 'tasks',
        ]);
    }
}
