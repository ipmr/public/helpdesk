<?php

use Faker\Generator as Faker;

$factory->define(App\SlaSeverity::class, function (Faker $faker) {
    return [
        'sla_id' => 1,
        'severity_id' => 1,
        'respond_number' => 1,
        'respond_format' => 'h',
        'resolve_number' => 1,
        'resolve_format' => 'h'
    ];
});
