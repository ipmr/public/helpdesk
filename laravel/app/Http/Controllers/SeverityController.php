<?php

namespace App\Http\Controllers;

use App\Severity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class SeverityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_data_severity($request);

        $severity = new Severity();

        $severity->name   = $request->name;
        $severity->color  = '#8e8e92';
        $severity->static = 0;

        $severity->save();

        return redirect()
            ->route('ticket-options.index')
            ->with('msg', 'Se ha creado la severidad ' . $severity->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Severity  $severity
     * @return \Illuminate\Http\Response
     */
    public function show(Severity $severity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Severity  $severity
     * @return \Illuminate\Http\Response
     */
    public function edit(Severity $severity)
    {
        $view = 'helpdesk.severity.edit';

        return view( $view, compact('severity') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Severity  $severity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Severity $severity)
    {
        $this->validate_data_severity($request, $severity);

        $severity->name = $request->name;

        $severity->save();

        return redirect()
            ->route('ticket-options.index')
            ->with('msg', 'Se ha actualizado la severidad ' . $severity->name . ' exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Severity  $severity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Severity $severity)
    {
        //
    }

    public function validate_data_severity($request, $severity = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required' => 'El nombre de la severidad es requerido',
            'name.unique'   => 'El nombre de la severidad no esta disponible',
        ];

        $validations = [

            'name' => ['required', 'string', Rule::unique('severities')->ignore($severity ? $severity->id : '')],

        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}
