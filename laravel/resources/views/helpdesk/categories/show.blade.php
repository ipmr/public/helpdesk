@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <a class="btn btn-link p-0" class="p-0" href="{{ route('ticket-options.index') }}">
                    <i class="fa fa-angle-left fa-sm mr-1"></i>
                    Regresar
                </a>
                <a class="btn btn-primary ml-auto" href="{{ route('category.edit', $category) }}/">
                    <i class="fa fa-pencil-alt fa-xs"></i>
                    Editar
                </a>
            </div>
            <div class="card-body">
                <label><b>Categoría: <span class="text-dark">{{ $category->name }}</span></b></label>
                <p class="text-muted">Departamentos o e-mails a quienes debe notificarse en base a la severidad del ticket</p>
                <table class="table table-striped table-bordered mb-0">
                    <thead>
                        <tr>
                            <th>Severidad</th>
                            <th>Departamento</th>
                            <th>Correos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($severities as $severity)
                        @php
                            $category_email = null;
                            if (isset($category)) {
                                $category_email = $category->mails()
                                        ->where('severity_id', $severity->id )
                                    ->first();
                            }
                        @endphp
                        <tr>
                            <td style="color: {{ $severity->color  }}">
                                {{ $severity->name }}
                            </td>
                            <td>
                                @isset ($category_email->department)
                                    <a href="{{ route('departments.show', ['department'=>$category_email->department_id]) }}">
                                    {{ $category_email->department->name }}
                                    </a>
                                @else
                                No asignado
                                @endisset
                            </td>
                            <td>
                                @if ( !empty($category_email->mails) )
                                    {{ $category_email->mails }}
                                @else
                                No asignados
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>    
            </div>
            <div class="card-header d-flex align-items-center">
                <b>Sub Categorías: <span class="text-dark">{{ $category->name }}</span></b>
                <a class="btn btn-primary ml-auto" href="{{ route('category.subcategory.create', ['category' => $category->id]) }}">
                    <i class="fa fa-plus"></i> Nueva
                </a>
            </div>
            @if (count($category->childs) > 0)
                <div class="card-body">
                    <table class="table table-striped table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Departamentos</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($category->childs as $sub_category)
                                    @if ($sub_category->id != $category->id)
                                        <tr>
                                            <td>
                                                @component('helpdesk.categories.components.folder_link', ['category' => $sub_category])@endcomponent
                                                <a href="{{ route('category.show', $sub_category) }}">
                                                    {{ $sub_category->name }}
                                                </a>
                                                @if($sub_category->childs->count() > 0)
                                                    <div class="collapse company_tree" id="category_{{ $sub_category->id }}">
                                                        <div class="pl-4 tree_link">
                                                            @foreach($sub_category->childs as $sub_category)
                                                                @component('helpdesk.categories.components.tree', ['category' => $sub_category]) 
                                                                @endcomponent
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                @if (count($sub_category->department_names()))
                                                    @foreach ($sub_category->department_names() as $id => $department)
                                                        <a href="{{ route('departments.show', ['department'=>$id]) }}" title="">
                                                        {{ $department }}
                                                        </a><br>
                                                    @endforeach
                                                @else
                                                No asignados
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                    </table>
                </div>
            @else
                <div class="card-body">
                    No se han agregado Sub Categorías
                </div>
            @endif
        </div>
    </div>
@endsection