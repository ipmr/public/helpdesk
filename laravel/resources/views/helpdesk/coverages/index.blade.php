@extends('layouts.master')
@section('content')
<div class="container" id="coverages_app">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Listado de coberturas</p>
			<form action="{{ route('coverages.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
			<a href="#new_contract_collapse" data-toggle="collapse" @if($errors->any()) aria-expanded="true" @endif class="btn btn-primary ml-2">
				<i class="fa fa-plus"></i>
				Nueva Cobertura
			</a>
		</div>
		<div class="card-body py-0">
			<div id="new_contract_collapse" class="collapse @if($errors->any()) show @endif">
				<form action="{{ route('coverages.store') }}" class="py-3" method="post" id="newContractForm">
					@include('helpdesk.coverages.partials.form', ['btn' => 'Crear'])
					<div class="row d-flex form-row">
						<div class="col-sm-8 ml-auto mt-3">
							<button type="submit" class="btn btn-success">
							<i class="fa fa-check"></i>
							Guardar Cobertura
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@if(count($coverages) > 0)
		<table class="table table-striped mb-0">
			<thead>
				<tr>
					<th>Cobertura</th>
					<th>Rango de dias y horas</th>
				</tr>
			</thead>
			<tbody>
				@foreach($coverages as $contract)
				<tr>
					<td><a href="{{ route('coverages.show', $contract) }}/">{{ $contract->name }}</a></td>
					<td>
						@if($contract->all_week)
						Toda la semana
						@else
						{{ $contract->range }}
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
		<div class="card-body">
			No se han agregado coberturas
		</div>
		@endif
	</div>
</div>
@stop
@section('scripts')
@include('helpdesk.coverages.partials.scripts')
@stop