<?php

use Faker\Generator as Faker;

$factory->define(App\SLA::class, function (Faker $faker) {
    return [
        'name'        => $faker->word,
        'description' => $faker->text($maxNbChars = 70),
    ];
});
