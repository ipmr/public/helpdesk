<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function tickets(){

    	return $this->hasMany('App\Ticket', 'category_id', 'id');

    }

    public function department(){

    	return $this->hasOne('App\Department', 'id', 'department_id');

    }


    public function mails(){

    	return $this->hasMany('App\CategoryMail', 'category_id', 'id');

    }

    public function department_names()
    {   
        $department_names = [];

        if ( count( $this->all_childs() ) > 0 ) {

            foreach ($this->all_childs() as $category) {

                if ($category->mails->count() > 0){

                    foreach ($category->mails as $category_mails){
                        if ($category_mails->department_id !== null) {

                            $department_names[optional($category_mails->department)->id] = optional($category_mails->department)->name;

                        }

                    }

                }
            }
        }
        

        return $department_names;
    }

    public function parent()
    {

        return $this->belongsTo('App\Category', 'parent_id', 'id');

    }

    public function childs()
    {

        return $this->hasMany('App\Category', 'parent_id', 'id');

    }

    public function getChildsAttribute()
    {
        return Self::where('parent_id', $this->id)->get();
    }

    public function all_parents()
    {

        $parents = collect([$this]);

        $parent = $this->parent;

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents;

    }

    public function all_childs()
    {

        $all_childs = collect([$this]);
        $next_childs = collect($this->childs);
        
        while (!is_null($next_childs)) {

            $add_to_next_childs = collect([]);

            $next_childs->each(function ($child) use ($all_childs, $add_to_next_childs) {
                $all_childs->push($child);
                $child->childs()->each(function ($c) use ($add_to_next_childs) {
                    $add_to_next_childs->push($c);
                });
            });

            if ($add_to_next_childs->count() > 0) {
                $next_childs = $add_to_next_childs;
            } else {
                $next_childs = null;
            }
        }
        return $all_childs;

    }
}
