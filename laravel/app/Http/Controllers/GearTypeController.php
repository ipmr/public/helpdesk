<?php

namespace App\Http\Controllers;

use App\GearType;
use Illuminate\Http\Request;
use Validator;

class GearTypeController extends Controller
{

    public function index()
    {

    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {

    }

    public function show(GearType $type)
    {
        
    }

    public function edit(GearType $type)
    {

    }

    public function update(Request $request, GearType $type)
    {

        $this->validate_type_data($request);

        $old_name = $type->name;
        $type->name = $request->name;
        $type->save();

        return redirect()->back()->with("msg", "Se modificó el tipo de equipo '{$old_name}' a {$type->name} exitosamente");

    }

    public function destroy(GearType $type)
    {
        //
    }

    public function validate_type_data($request){

        $validate = Validator::make($request->all(), [
            'name' => 'required|unique:gear_types'
        ], [
            'name.required' => 'El nombre del tipo de equipo es requerido.',
            'name.unique' => 'El nombre del tipo de equipo no esta disponible.'
        ])->validate();

    }
}
