<div class="row form-row">
	<div class="col-sm-7">
		<label><b>Datos de la compañia</b></label>
		<p>	
			Compañia: 
			<a href="{{ route('companies.show', $ticket->company) }}/" data-toggle="tooltip" data-placement="right" title="Ver información de la compañia">
				{{ $ticket->company->name }}
			</a>
			<br>
			@if($ticket->author)
				Contacto: <b>{{ $ticket->author->full_name }}</b>
				<br>
			@endif
			Domicilio: {{ $ticket->company->address }} <br>
			Teléfono: {{ $ticket->company->phone }} <br>
			RFC: {{ $ticket->company->rfc or $ticket->company->master_parent->rfc }}
		</p>
	</div>
	@if($ticket->contract || $ticket->gear)
	<div class="col-sm-5">
		<label><b>Contrato</b></label>
		
		@if($ticket->gear)
		<p class="m-0">	
			Equipo:
			<a href="{{ route('companies.gears.show', [$ticket->company, $ticket->gear->id]) }}" class="text-uppercase" data-toggle="tooltip" data-placement="top" title="{{ $ticket->gear->serial_number }}">
				{{ $ticket->gear->model->name }}
			</a>
		</p>
		@endif
		<p class="m-0">
			Contrato:
			<a href="{{ route('companies.show', $ticket->company) }}/">
				{{ $ticket->contract ? $ticket->contract->name : ' Sin contrato' }}
			</a>
			@if($ticket->contract)
			({!! $ticket->company->validate_contract !!})
			@endif
		</p>
		<p class="m-0">
			Cobertura: 
			<a href="{{ route('companies.show', $ticket->company) }}">
				{{ $ticket->coverage ? $ticket->coverage->name : 'Sin cobertura' }}
			</a>
		</p>
		<p class="m-0">
			SLA: 
			<a href="{{ route('companies.show', $ticket->company) }}">
				{{ $ticket->company->sla ? $ticket->company->sla->name : 'Sin SLA' }}
			</a>
		</p>

	</div>
	@endif
</div>