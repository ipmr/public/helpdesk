<!doctype html>
<html lang="en">
    
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        
        @yield('css')
        
        <title>{{ config('app.name') }}</title>

    </head>


    <body>
        

        <nav class="navbar navbar-expand-lg navbar-dark header_master">
            

            <a class="navbar-brand @guest mx-auto @endguest" href="{{ route('home') }}">
                <img src="{{ asset('img/logo.svg') }}" class="logo d-none d-sm-block" alt="{{ config('app.name') }}">
                <img src="{{ asset('img/icono.svg') }}" class="logo d-block d-sm-none" alt="{{ config('app.name') }}">
            </a>
            

            @auth
                

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto d-flex align-items-center">
                        
                        @role('admin', 'manager')
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}/">
                                    <i class="fa fa-desktop d-none d-sm-inline-block"></i>
                                    Escritorio <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="projectsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-folder-open"></i>
                                    Proyectos
                                </a>
                                <div class="dropdown-menu" aria-labelledby="projectsDropdown">
                                    <a class="dropdown-item" href="{{ route('projects.create') }}/">Nuevo proyecto <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('projects.index') }}/">Listado de proyectos <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="ticketsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-ticket-alt"></i>
                                    Tickets
                                </a>
                                <div class="dropdown-menu" aria-labelledby="ticketsDropdown">
                                    <a class="dropdown-item" href="{{ route('tickets.create') }}/">Nuevo ticket <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('tickets.index') }}/">Listado de tickets <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="companiesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-building"></i>
                                    Compañias
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="companiesDropdown">
                                    <a class="dropdown-item" href="{{ route('companies.create') }}/">Nueva compañia <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('companies.index') }}/">Listado de compañias <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                        @endrole

                        @role('company')
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="ticketsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-ticket-alt"></i>
                                    Tickets
                                </a>
                                <div class="dropdown-menu" aria-labelledby="ticketsDropdown">
                                    <a class="dropdown-item" href="{{ route('tickets.create') }}/">Nuevo ticket <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('tickets.index') }}/">Listado de tickets <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('companies.index') }}">
                                    <i class="d-none d-sm-inline-block fa fa-building"></i>
                                    Compañias
                                </a>
                            </li>
                        @endrole

                        @role('agent')
                            
                
                            @if( ! auth()->user()->agent->as_manager)

                                @if( ! auth()->user()->agent->generate_tickets)

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('tickets.index') }}">
                                            <i class="d-none d-sm-inline-block fa fa-ticket-alt"></i>
                                            Tickets
                                        </a>
                                    </li>

                                @else
                                
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="ticketsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="d-none d-sm-inline-block fa fa-ticket-alt"></i>
                                            Tickets
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="ticketsDropdown">
                                            <a class="dropdown-item" href="{{ route('tickets.create') }}/">Nuevo ticket <span class="sr-only">(current)</span></a>
                                            <a class="dropdown-item" href="{{ route('tickets.index') }}/">Listado de tickets <span class="sr-only">(current)</span></a>
                                        </div>
                                    </li>

                                @endif

                            @endif


                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('agent-tasks') }}">
                                    <i class="d-none d-sm-inline-block fa fa-thumbtack"></i>
                                    @if(count(auth()->user()->agent->tasks()->where('status', 0)->get()) > 0)
                                        (<b class="text-danger">{{ count(auth()->user()->agent->tasks()->where('status', 0)->get()) }}</b>)
                                    @endif
                                    Mis Tareas <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            
                            @if( ! auth()->user()->agent->as_manager )
            
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('companies.index') }}">
                                        <i class="d-none d-sm-inline-block fa fa-building"></i>
                                        Compañias
                                    </a>
                                </li>

                            @endif

                        @endrole                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('notifications') }}/">
                                <i class="d-none d-sm-inline-block fa fa-flag"></i>                            
                                @if(count(auth()->user()->unreadNotifications) > 0)
                                    (<b class="text-danger">{{ count(auth()->user()->unreadNotifications) }}</b>)
                                @endif
                                {{ count(auth()->user()->unreadNotifications) == 1 ? 'Notificación' : 'Notificaciones' }} <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        @role('admin')
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="configDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-cog"></i>
                                    Configuración
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="configDropdown">
                                    <a class="dropdown-item" href="{{ route('users.index') }}/">Usuarios <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('agents.index') }}/">Agentes <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('departments.index') }}/">Departamentos <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('contact-types.index') }}/">Tipos de contacto <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('brands.index') }}/">Equipos <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('inventories.index') }}/">Inventarios <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('contracts.index') }}/">Contratos <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('coverages.index') }}/">Coberturas <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('sla.index') }}/">Acuerdos de servicio (SLA) <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('ticket-options.index') }}">Opciones de ticket <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('reports') }}">Reportes <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                        @endrole
                        @role('manager')
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="configDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="d-none d-sm-inline-block fa fa-cog"></i>
                                    Configuración
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="configDropdown">
                                    <a class="dropdown-item" href="{{ route('agents.index') }}/">Agentes <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('brands.index') }}/">Equipos <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('contracts.index') }}/">Contratos <span class="sr-only">(current)</span></a>
                                    <a class="dropdown-item" href="{{ route('coverages.index') }}/">Coberturas <span class="sr-only">(current)</span></a>
                                </div>
                            </li>
                        @endrole
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle nav-link-avatar" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="line-height: 0">
                                {{-- <i class="d-none d-sm-inline-block fa fa-user"></i>
                                {{ auth()->user()->first_name }} --}}

                                
                                <div class="avatar">
                                    <span>{{ auth()->user()->first_name[0] . auth()->user()->last_name[0] }}</span>
                                </div>


                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a href="{{ route('profile.index') }}" class="dropdown-item">
                                    Mi cuenta
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Salir
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>


            @endauth


        </nav>


        <div class="py-4" @auth style="min-height: 80vh;" @endauth>
            

            <div class="container" id="alerts_container">
                @include('helpdesk.components.alerts')
            </div>
            

            @yield('content')


        </div>
        


        @auth
            
            <footer class="footer_master">
                <div class="container py-5">
                    
                
                    <div class="row">
                        
                        <div class="col-sm-4">
                            <p class="m-0">

                                <b>© {{ \Carbon\Carbon::now()->format('Y') .', '. config('app.name') }}</b>

                            </p>
                        </div>

                        <div class="col-sm-6 ml-auto text-right">
                            
                        
                            <ul class="list-inline mb-0">
                                
                                <li class="list-inline-item"><a href="{{ route('home') }}">Escritorio</a></li>
                                <li class="list-inline-item"><a href="{{ route('directory.index') }}">Directorio</a></li>
                                <li class="list-inline-item"><a href="mailto:jvelazquez@ipmediariver.com?subject=Soporte Técnico | {{ config('app.name') }}">Soporte Técnico</a></li>

                            </ul>


                        </div>

                    </div>


                </div>
            </footer>

        @endauth
        


        <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
        <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
        <script src="{{ asset('js/general.js') }}"></script>
        <script src="{{ asset('js/vue.js') }}"></script>
        <script src="{{ asset('js/vue-resource.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue2-filters/dist/vue2-filters.min.js"></script>


        @yield('scripts')
    </body>
</html> 