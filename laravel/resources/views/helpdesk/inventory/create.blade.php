@extends('layouts.master')

@section('content')
<div class="container" id="gears_app">
    <div class="card">
        <div class="card-header">
            <a class="btn btn-link p-0" href="{{ route('inventories.index') }}/">
                <i class="fa fa-angle-left fa-sm mr-1"></i>
                Regresar
            </a>
        </div>
        <div class="card-body">
            <form action="{{ route('inventories.store') }}" method="post" id="form_create">
                
                @include('helpdesk.inventory.partials.form', [
                    'inventory' => true,
                ])
                
            </form>
        </div>
        <div class="card-footer">
            <div class="form-row form-group m-0">
                <div class="col-sm-8 ml-auto">
                    <button type="submit" class="btn btn-success" form="form_create">
                        <i class="fa fa-check"></i> Crear Almacen
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection