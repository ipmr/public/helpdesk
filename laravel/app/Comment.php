<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    
    public function ticket(){

    	return $this->belongsTo('App\Ticket', 'ticket_id', 'ticket_id');

    }

    public function user(){

    	return $this->belongsTo('App\User', 'user_id', 'id');

    }

    public function files(){

    	return $this->hasMany('App\CommentFile', 'comment_id', 'id');

    }


    public function getTimeAttribute(){


		$minutes = $this->event_time;	

    	$total_days = number_format(floor ($minutes / 1440));
    	
    	$total_hours = number_format(floor(($minutes - ($total_days * 1440)) / 60));

    	$total_minutes = number_format(floor(($minutes - ($total_days * 1440)) - ($total_hours * 60)));


    	$format_days = $total_days > 0 ? $total_days . 'd' : '';

    	$format_hours = $total_hours > 0 ? $total_hours . 'hrs' : '';

    	$format_minutes = $total_minutes > 0 ? $total_minutes . 'min' : '';

    	if($format_days || $format_hours || $format_minutes){

    		return "{$format_days} {$format_hours} {$format_minutes}";

    	}else{
    		
    		return null;

    	}


    }

}
