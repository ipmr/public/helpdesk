<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketClose extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->user   = $request['user'];
        $this->ticket = $request['ticket'];
        $this->status = $request['status'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text  = $this->status->description . '. Para dudas y/o comentarios, contacta a nuestras oficinas para gestionar una solicitud de reapertura de ticket.';

        if (in_array($this->user->role, ['agent','manager'])) {
            $text = $this->user->FullName . ' modificó el status del ticket a ' . $this->ticket->get_status->name.'. Si el cliente solicita modificación o reapertura del ticket, puedes levantar solicitud.';
        }

        return (new MailMessage)
            ->subject('Ticket cerrado')
            ->greeting('Ticket #' . $this->ticket->ticket_id)
            ->line($text)
            ->action('Ver ticket', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text  = $this->status->description . '. Para dudas y/o comentarios, contacta a nuestras oficinas para gestionar una solicitud de reapertura de ticket.';

        if (in_array($this->user->role, ['agent','manager'])) {
            $text = $this->user->FullName . ' modificó el status del ticket a ' . $this->ticket->get_status->name.'. Si el cliente solicita modificación o reapertura del ticket, puedes levantar solicitud.';
        }

        return [
            'msg'    => $text,
            'ticket' => $this->ticket,
        ];
    }
}
