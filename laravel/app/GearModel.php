<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GearModel extends Model
{
	protected $table   = 'gear_models';
    public $timestamps = true;
    protected $guarded = [];
}
