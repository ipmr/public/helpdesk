@component('helpdesk.components.modal', [
	'id' 	=> 'new_comment_to_company',
	'title' => 'Nuevo evento'
])

	@slot('body')
		
		<form 
			action="{{ route('public-comment', $ticket->ticket_id) }}" 
			method="post" 
			id="new_comment_to_company_form" 
			enctype="multipart/form-data"
			
			@role('admin', 'manager', 'agent')

				@submit.prevent="submit_comment"

			@endrole

			>

			{{ csrf_field() }}

			
			<div class="form-group">
				<label>Comentario:</label>
				<textarea name="comment" class="form-control" placeholder="Escribe aquí..." rows="5" required></textarea>
			</div>

			@role('admin', 'manager', 'agent')
			
			<div class="form-group">
				<label>Tiempo invertido:</label>
				<div class="form-row">
					<div class="col-sm-4">
						<input type="number" name="days" class="form-control" value="0" required>
						<small class="mt-1 d-block">(Días)</small>
					</div>
					<div class="col-sm-4">
						<input type="number" name="hours" class="form-control" value="0" required>
						<small class="mt-1 d-block">(Horas)</small>
					</div>
					<div class="col-sm-4">
						<input type="number" name="minutes" class="form-control" value="1" min="1" @keyup="minute_value" required>
						<small class="mt-1 d-block">(Minutos)</small>
					</div>
				</div>
			</div>
			
			<hr>

			<div class="form-row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Tipo de atención:</label>
						<select name="atention" class="form-control" required>
							<option disabled selected value="">Elige una opción</option>
							<option value="Remota">Remota</option>
							<option value="En sitio">En sitio</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Publicar como:</label>
						<select name="type" class="form-control" required id="commentPublicCheckbox">
							<option disabled selected value="">Elige una opción</option>
							<option value="1">Evento Público (Visible para el cliente)</option>
							<option value="0">Evento Privado (No visible para el cliente)</option>
						</select>
					</div>
				</div>
			</div>

			@endrole

			<hr>

			<div class="form-group m-0">
				<label for="">Adjuntar imágenes</label> <br>
				<input type="file" name="files[]" multiple>
			</div>


			{{-- <div class="container pl-4 mt-3" @role('company')  hidden="true" @endrole>
				<label class="form-check-label">
					<input type="checkbox" 
						   class="form-check-input"
						   name="type" 
						   value="is_public" 
						   @role('company') 
						   checked="true" 
						   @endrole
						   id="commentPublicCheckbox">

						   Evento público
				</label>
			</div> --}}

		</form>

	@endslot
	
	@slot('footer')
		
		<button type="submit" form="new_comment_to_company_form" class="btn btn-success">
			<i class="fa fa-paper-plane"></i>
			Publicar evento
		</button>

	@endslot

@endcomponent