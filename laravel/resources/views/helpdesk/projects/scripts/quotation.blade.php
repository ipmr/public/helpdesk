addResourceToQuotation: function(e){

	var t = this;

	var form = $(e.target);

	var btn = $('button', form);


	var qty = $('[name="qty"]', form);
	var name = $('[name="name"]', form);
	var subtotal = $('[name="subtotal"]', form);


	var new_resource = {


		qty		: qty.val(),
		name: name.val(),
		subtotal: subtotal.val(),


	}


	t.quotation_resources.push(new_resource);


	form[0].reset();

	qty.focus();

	setTimeout(function(){
		btn.removeAttr('disabled');
	}, 200);


},


remove_resource: function(index){

	var t = this;


	var ask = confirm('¿Estas seguro que deseas eliminar este recurso de la cotización?');

	if(ask){


		t.quotation_resources.splice(index, 1);

		
	}



},


quotation_total: function(){


	var t = this;

	var total = 0;

	$.each(t.quotation_resources, function(i,o){


		total += parseFloat(o.subtotal);


	});


	return total;


},


save_quotation: function(e){


	var t = this;

	var form = $(e.target);

	var btn = $('button', form);

	var url = form.attr('action');

	var data = form.serialize();


	if(t.quotation_resources.length > 0){


		$.post(url, data, function(response){

			btn.removeAttr('disabled');

		}).done(function(response){


			t.quotation = response.quotation;


			t.quotation_resources = response.quotation_resources;



			$('#generateQuotationModal').modal('hide');


			setTimeout(function(){

				t.show_notification(response.msg);


			}, 300);


		});


	}else{


		alert("Agrega 1 o más recursos para continuar.");


		setTimeout(function(){


			btn.removeAttr('disabled');


		}, 200);

	}


},


resources_list: function(){


	var t = this;

	var resources = t.quotation_resources;

	resources = JSON.stringify(resources);

	return resources;


},


validate_quotation: function(e){


	var t = this;

	var form = $(e.target);

	var btn = $("button", form);

	var data = form.serialize();

	var url = form.attr('action');

	$.post(url, data, function(response){

		btn.removeAttr('disabled');

	}).done(function(response){



		t.quotation = response.quotation;


		$('#validateQuotationModal').modal('hide');


		setTimeout(function(){

			t.go_to_step(3);

			t.show_notification(response.msg);


		}, 300);


	});


},