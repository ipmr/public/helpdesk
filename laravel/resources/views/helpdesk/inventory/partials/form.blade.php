{{ csrf_field() }}


<div class="form-row">
    <div class="col-sm-4 mx-auto">
        <label><b>Datos del Almacen</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

<div class="form-row form-group">
    <label class="col-form-label col-sm-4 text-right">
        <span class="text-danger">*</span>
        Nombre
    </label>
    <div class="col-sm-4">
        <input type="text" name="name" value="{{ $inventory->name or old('name') }}" class="form-control">
    </div>
</div>