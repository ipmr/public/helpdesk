@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')
	<table class="table table-striped m-0" id="gears_app">
		<thead>
			
			<tr>
				<td colspan="8">
					<div class="d-flex align-items-center py-2">
						
						<label class="m-0"><b>Listado De Equipos</b></label>
						
						<form action="{{ route('search_gear', $company) }}" method="post" class="form-group mb-0 ml-auto d-flex align-items-center">
							{{ csrf_field() }}
							<input type="text" name="q" placeholder="Buscar #serie..." class="form-control" style="border-radius: 3px 0 0 3px">
							<button class="btn btn-success" style="border-radius: 0 3px 3px 0; padding-left: 1rem;">
								<i class="fa fa-search" style="margin: 0"></i>
							</button>
						</form>

						@role('admin', 'manager')
						<a class="btn btn-primary ml-3" href="#addNewGear" data-toggle="collapse">
							<i class="fa fa-plus fa-sm mr-1"></i>
							Nuevo Equipo
						</a>
						<form action="{{ route('download_company_gear', $company) }}" method="post">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-light ml-3">
								<i class="fa fa-download fa-md mr-1"></i>
								Descargar
							</button>
						</form>
						@endrole

					</div>
					@role('admin','manager')
						<div v-cloak id="addNewGear" class="collapse @if($errors->any() || request()->module_from) show @endif">
							<form action="{{ route('companies.gears.store', $company) }}" method="post" class="pb-3" id="new_gear_form">
								@include('helpdesk.gears.partials.form', [
									'btn' => 'Guardar Equipo'
								])
							</form>
						</div>
					@endrole
				</td>
			</tr>

			@isset($search)
	
			<tr>
				<td colspan="8" class="text-center p-0">
					<hr class="mt-0">
					<div class="mb-3">
						[<a href="{{ route('companies.gears.index', $company) }}/">Restaurar listado de equipos</a>]
					</div>

					<p class="mb-0">Resultados relacionados con: {{ $q }}</p>
					<hr class="mb-0">
				</td>
			</tr>

			@endisset
	
			<tr style="border-top: 1px solid #f2f2f2">
				<th>Marca</th>
				<th>Modelo</th>
				<th># Serie</th>
				<th>Módulos</th>
				<th>Contrato</th>
				<th>Status</th>
				<th>Sitio</th>
			</tr>
		</thead>

		<tbody>
			
			
			@if($gears->count() > 0)

				@foreach($gears as $g)
					

					<tr class="{{ $g->active ? '' : 'bg-danger-light' }}">

						<td style="vertical-align: middle">

							
							@if($g->modules->count() > 0)

								<a href="#modules_box{{ $g->id }}" data-toggle="collapse" class="no_decoration folder_link mr-1">
									<i class="fa fa-chevron-circle-right fa-md"></i>
								</a>

							@endif
		
								
							{{ $g->brand->name }}

						</td>
						

						<td style="vertical-align: middle">{{ $g->model->name }}</td>
						

						<td style="vertical-align: middle" class="droppable_tr" data-gear="{{ $g->serial_number }}">
							@role('admin','manager')
							<a href="{{ route('companies.gears.show', [$company, $g]) }}/" class="draggable_tr" data-gear="{{ $g->serial_number }}">
								{{ $g->serial_number }}
							</a>
							@else
								{{ $g->serial_number }}
							@endrole
						</td>
						

						<td style="vertical-align: middle">{{ $g->modules->count() }} Módulos</td>
						

						<td style="vertical-align: middle">{{ $g->contract ? $g->contract->name : 'Sin Contrato' }}</td>
						

						<td style="vertical-align: middle">{{ $g->status }}</td>
						

						<td style="vertical-align: middle">
							@role('admin','manager')
								@include('helpdesk.gears.partials.assign_gear_to_site')	
							@else
								{{ optional($g->company)->name }}
							@endrole
						</td>

					</tr>

					
					@if($g->modules->count() > 0)

					
						<tr class="p-0">
							
							<td colspan="8" class="p-0"  style="background: #f1f1ff;">
								
								@component('helpdesk.gears.components.modules-box', [
									'g' => $g,
									'company' => $company
								])

								@endcomponent

							</td>

						</tr>


					@endif


				@endforeach


			@else


				<tr>
					
					<td colspan="8">
						No se ha agregado equipo
					</td>

				</tr>


			@endif


		</tbody>

	</table>
@stop

@section('scripts')
	
	@include('helpdesk.gears.partials.scripts')

@stop