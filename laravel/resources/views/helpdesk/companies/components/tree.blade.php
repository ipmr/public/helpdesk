@component('helpdesk.companies.components.folder_link', ['company' => $zone])@endcomponent
<a href="{{ route('companies.show', $zone) }}/">{{ $zone->name }}</a> <br>

@if($zone->childs->count() > 0)
<div class="collapse company_tree" id="zone_{{ $zone->id }}">
	@foreach($zone->childs as $child)
			<div class="pl-4 tree_link">
				@component('helpdesk.companies.components.tree', ['zone' => $child])@endcomponent
			</div>
	@endforeach
</div>
@endif
