<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectTicket;
use App\Ticket;
use Illuminate\Http\Request;
use Validator;

class ProjectTicketController extends Controller
{


    public function __construct(){


        // $this->middleware('under-construction');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectTicket  $projectTicket
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectTicket $projectTicket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectTicket  $projectTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectTicket $projectTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectTicket  $projectTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectTicket $projectTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectTicket  $projectTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectTicket $projectTicket)
    {
        //
    }

    public function assign_ticket_to_project(Request $request, $ticket)
    {
        $cond = [
            'project_id' => [
                'required',
                'numeric',
                'exists:projects,id',
            ],
        ];

        $msgs = [
            'project_id.required' => 'El proyecto es requerido',
            'project_id.numeric'  => 'El proyecto debe ser un id',
            'project_id.exists'   => 'El proyecto no existe en la base de datos',
        ];

        $validate = Validator::make($request->all(), $cond, $msgs)->validate();

        $ticket = Ticket::where('ticket_id', $ticket)->first();

        $project_ticket = ProjectTicket::where('ticket_id', $ticket->ticket_id)->first();

        if (!isset($project_ticket->id)) {
            $project_ticket = new ProjectTicket();
        }

        $project_ticket->project_id = $request->project_id;
        $project_ticket->ticket_id  = $ticket->ticket_id;
        $project_ticket->phase      = 0;

        $project_ticket->save();

        return redirect()
            ->back()
            ->with('msg', 'El ticket fue agregado al proyecto "' . optional($project_ticket->project)->title . '" exitosamente');
    }
}
