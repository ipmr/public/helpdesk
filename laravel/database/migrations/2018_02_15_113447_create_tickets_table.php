<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket_id')->nullable();
            $table->string('category_id')->nullable();
            $table->string('subject');
            $table->text('description');
            $table->string('severity_id')->nullable();
            $table->string('status_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('department_id')->nullable();
            $table->string('agent_id')->nullable();
            $table->string('contact_id')->nullable();
            $table->string('gear_id')->nullable();
            $table->string('deadline')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
