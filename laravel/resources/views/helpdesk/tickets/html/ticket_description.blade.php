<label class="d-block d-sm-inline-block">
	<b>
		Descripción del ticket
		@if($ticket->overdue())
		(<span class="text-danger">Retrasado</span>)
		@endif
	</b> 
</label>

<div class="row form-row mt-2">
	<div class="col-sm-7">
		<table class="table table-sm table-striped table-bordered m-0">
			<tbody>
				<tr>
					<th class="d-none d-sm-table-cell">Asunto:</th>
					<td>
				
						<p class="d-block d-sm-none m-0">
							<b>Asunto:</b>
						</p>

						{{ $ticket->subject }}


					</td>
				</tr>
				<tr>
					<th class="d-none d-sm-table-cell">Descripción:</th>
					<td class="bg-secondary">

						
						<p class="d-block d-sm-none m-0">
							<b>Descripción:</b>
						</p>

						{{ $ticket->description }}


					</td>
				</tr>
				<tr>
					<th class="no-wrap d-none d-sm-table-cell">Fecha de apertura:</th>
					<td>
						

						<p class="d-block d-sm-none m-0">
							<b>Fecha de apertura:</b>
						</p>

						{{ $ticket->created_at->format('d/m/Y - h:i a') }}

					</td>
				</tr>
				@role('admin', 'manager', 'agent')

				@if($ticket->contract && $ticket->company->sla)
					
					<tr>
						<th class="d-none d-sm-table-cell">Responder antes de:</th>
						<td>{{ $ticket->sla_respond->format('d/m/Y - h:i a') }}</td>
					</tr>

				@endif

				@endrole
			</tbody>
		</table>
	</div>
	<div class="col-sm-5 mt-3 mt-sm-0">
		<table class="table table-sm table-striped table-bordered m-0">
			<tbody>
				<tr>
					<th class="d-none d-sm-table-cell">
						<div class="th_border_color" style="background: {{ $ticket->get_status->color }}"></div>
						Estado:
					</th>
					<td>

						<p class="m-0 d-block d-sm-none">
							<b>Estado:</b>
						</p>

						@role('company')
							<span style="color: {{ $ticket->get_status->color }};">
								{!! $ticket->get_status->description !!} 
							</span>
						@endrole
						
						@role('agent')

							@if( ! auth()->user()->agent->as_manager)

								@if(optional($ticket->department->supervisor)->user->id == auth()->user()->id)
									@if ($ticket->get_status->name != 'Cerrado')
										@include('helpdesk.tickets.components.ticket_description_status_select')
									@else
										{!! $ticket->status() !!} 1
									@endif
								@elseif($ticket->agent->user->id !== auth()->user()->id)

									{!! $ticket->status() !!}

								@else
									@if ( !in_array($ticket->get_status->name, ['Resuelto', 'Cerrado']) )
										@include('helpdesk.tickets.components.ticket_description_status_select')
									@else
										{!! $ticket->status() !!} 
										@if ($ticket->reopen_tickets()->where('status',0)->count() == 0)
		                                    <br>
		                                    <a href="#" class="ml-auto" data-toggle="modal" data-target="#reopen_ticket">[Solicitar reapertura]</a>
		                                @else
		                                    <br>
		                                    <span>[Solicitud de apertura enviada]</span>
		                                @endif
									@endif
								@endif

							@endif

						@endrole

						@role('admin','manager')
							@include('helpdesk.tickets.components.ticket_description_status_select')	
						@endrole

					</td>
				</tr>

				<tr>
					<th class="d-none d-sm-table-cell">
						<div class="th_border_color" style="background: {{ $ticket->get_severity->color }}"></div>
						Severidad:
					</th>
					<td>

						<p class="m-0 d-block d-sm-none">
							<b>Severidad:</b>
						</p>

						<div class="d-flex">
							@role('company')
								{{ $ticket->get_severity->name }}
							@endrole
							@role('agent')
								@if(! auth()->user()->agent->as_manager)
									<span>{{ $ticket->get_severity->name }}</span>
								@endif
							@endrole
							@role('admin','manager')
								@include('helpdesk.tickets.components.ticket_description_severity')	
							@endrole
						</div>
					</td>
				</tr>

				{{-- <tr>
					<th class="d-none d-sm-table-cell">Categoría:</th>
					<td>

						<p class="m-0 d-block d-sm-none"><b>Categoría:</b></p>

						<div class="d-flex">
							@role('company')
								{{ $ticket->category->name }}
							@endrole
							@role('agent')
								@if(! auth()->user()->agent->as_manager)
									<span>{{ $ticket->category->name }}</span>
								@endif
							@endrole
							@role('admin','manager')
								@include('helpdesk.tickets.components.ticket_description_category')	
							@endrole
						</div>
					</td>
				</tr> --}}

				@role('admin', 'manager')
					<tr>
						<th  class="d-none d-sm-table-cell">Departamento:</th>
						<td>

							<p class="m-0 d-block d-sm-none">
								<b>Departamento:</b>
							</p>

							<div class="d-flex">
								@include('helpdesk.tickets.components.ticket_description_department')
							</div>
						</td>
					</tr>
				@endrole
				
				@role('agent')
					@if(! auth()->user()->agent->as_manager)
						
						<tr>
							<th class="d-none d-sm-table-cell">Departamento:</th>
							<td>

								<p class="m-0 d-block d-sm-none">
									<b>Departamento:</b>
								</p>

								<div class="d-flex">
									<span>{{ $ticket->department->name }}</span>
								</div>
							</td>
						</tr>

					@endif
				@endrole

				@if($ticket->department)
					<tr>
						<th  class="d-none d-sm-table-cell">Ingeniero:</th>
						<td>

							<p class="m-0 d-block d-sm-none">
								<b>Ingeniero:</b>
							</p>

							@role('company')
								@if($ticket->agent)
									{{ $ticket->agent->full_name }}<br>(<small>{{ $ticket->agent->jobtitle }}</small>)
								@else
									Sin asignar
								@endif
							@endrole
							@role('agent')
								
								@if( ! auth()->user()->agent->as_manager )
			
									@if(optional($ticket->department->supervisor)->user->id == auth()->user()->id)
										<div class="d-flex">
											@include('helpdesk.tickets.components.ticket_description_agents')	
										</div>
									@else
										@if($ticket->agent)
											{{ $ticket->agent->full_name }}
										@else
											Sin asignar
										@endif
									@endif

								@endif

							@endrole
							@role('admin','manager')
								<div class="d-flex">
									@include('helpdesk.tickets.components.ticket_description_agents')	
								</div>
							@endrole							
						</td>
					</tr>
				@endif

				@role('admin', 'manager')
					{{-- <tr>
						<th>
							Proyecto:
						</th>
						<td>

							<p class="m-0 d-block d-sm-none">
								<b>Proyecto:</b>
							</p>

							<div class="d-flex">
								@include('helpdesk.tickets.components.ticket_description_project')
							</div>
						</td>
					</tr> --}}
				@endrole
			</tbody>
		</table>
	</div>
</div>