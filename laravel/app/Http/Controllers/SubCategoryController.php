<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryMail;
use App\Severity;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Category $category )
    {
        if (\Request::ajax()) {

            return response()->json($category->childs);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Category $category )
    {
        $severities = Severity::get();

        $departments = Department::get();

        $parent = $category;

        return view('helpdesk.categories.create', compact( 'parent', 'severities', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->validate_data_category($request);

        $category_son = new Category();

        $category_son->name = $request->name;

        $category_son->parent_id = $category->id;

        $category_son->save();

        foreach ($request->severity_id as $index => $severity_id) {

            $this->insert_mails(
                $category_son,
                $severity_id,
                $request->department_id[$index],
                $request->mails[$index]
            );
        }

        return redirect()
            ->route('category.show', [ 'category' => $category->id ])
            ->with('msg', 'Se ha creado la categoría ' . $category_son->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function insert_mails($category, $severity_id, $department_id, $mails)
    {
        $category_mail = $category->mails()->where('severity_id', $severity_id)->first();

        if (empty($category_mail)) {

            $category_mail = new CategoryMail();

        }

        $category_mail->category_id   = $category->id;
        $category_mail->severity_id   = $severity_id;
        $category_mail->department_id = $department_id;
        $category_mail->mails         = $mails;

        $category_mail->save();
    }

    public function validate_data_category($request, $category = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required'            => 'El nombre de la categoría es requerido',
            'name.unique'              => 'El nombre de la categoría no esta disponible',
            'severity_id.required'     => 'La severidad de la categoría es requerida',
            'severity_id.exists'       => 'La severidad de la categoría no existe',
            'department_id.*.exists'   => 'El departamento seleccionado no es válido',
            'mails.*.string'           => 'Los correos deben ser caracteres alfanuméricos',
        ];

        $validations = [
            'name'            => ['required', 'string', Rule::unique('categories')->ignore($category ? $category->id : '')],
            'severity_id.*'   => 'required|exists:severities,id',
            'department_id.*' => 'nullable|sometimes|exists:departments,id',
            'mails.*'         => 'nullable|string',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}