<?php

namespace App\Http\Controllers;

use App\ReopenTicket;
use App\Ticket;
use App\User;
use App\Notifications\ReopenTicketNotification;
use Illuminate\Http\Request;
use Notification;
use Validator;

class ReopenTicketController extends Controller
{

    public function reopen_ticket_action(Request $request, $ticket)
    {
        $validate = Validator::make($request->all(), [
            'subject' => 'required|string',
            'message' => 'required|string',
        ], [
            'subject.required' => 'El asunto es requerido',
            'message.required' => 'La solicitud es requerida',
        ])->validate();

        $ticket = Ticket::where('ticket_id', $ticket)->first();

        $reopen_ticket = new ReopenTicket();

        $reopen_ticket->ticket_id = $ticket->ticket_id;
        $reopen_ticket->author_id = auth()->user()->id;
        $reopen_ticket->subject   = $request->subject;
        $reopen_ticket->message   = $request->message;
        $reopen_ticket->status    = 0;

        $reopen_ticket->save();

        #send notification to admins
        $admins = User::where('role', 'admin')->get();

        $notification_data = [
            'ticket'        => $ticket,
            'reopen_ticket' => $reopen_ticket,
        ];
        Notification::send( $admins, new ReopenTicketNotification( $notification_data ) );

        return redirect()
            ->route('tickets.show', ['ticket' => $ticket->ticket_id])
            ->with('msg', 'La solicitud de reapertura fue enviada correctamente');
    }

    public function mark_reopen_ticket_as_read( $reopen_ticket ){
        $reopen_ticket = ReopenTicket::where('id', $reopen_ticket)->first();
        if (empty($reopen_ticket->id)) {
            return redirect()
                    ->back()
                ->with('warning', 'No es posible acceder al ticket');

        }

        $reopen_ticket->status = 1;

        $reopen_ticket->save();

        return redirect()
            ->route('tickets.show', ['ticket' => $reopen_ticket->ticket_id ]);
    }
}
