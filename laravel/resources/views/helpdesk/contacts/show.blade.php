@extends('helpdesk.companies.layouts.tabs')

@section('tab_content')
	<div class="card-body">
		<div class="row form-row">
			<div class="col-sm-8 ml-auto">
				<div class="mb-3">
					<a href="{{ route('companies.contacts.index', $company) }}/" class="btn btn-link p-0">
						<i class="fa fa-angle-left"></i>
						Regresar
					</a>
				</div>
			</div>
		</div>

		<div class="form-row">
			<div class="col-sm-4 mx-auto">
				<label><b>Datos del contacto</b></label>
				<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
			</div>
		</div>

		<form action="{{ route('companies.contacts.update', ['company' => $company, 'contact' => $user->contact]) }}" method="post" id="updateContactForm">
			{{ method_field('PATCH') }}
			{{ csrf_field() }}
			
			@php
				$type = $company->contacts()->where('user_id', $user->id)->first();
			@endphp
			
			@if($company->contact->id !== $user->id)

			<div class="form-group row form-row">
				<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Tipo de contacto:</label>
				<div class="col-sm-3">
					<select name="type" class="form-control" required>
						<option disabled selected value>Elige una opción</option>
						@foreach (App\CompanyUserType::where('id','>',1)->get() as $type_users)
							<option value="{{ $type_users->id }}" @isset ($type)
							    {{ ($type->role == $type_users->id)?'selected':'' }}
							@endisset>{{ $type_users->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			
			@else

			<div class="form-row d-flex">
				<div class="col-sm-8 ml-auto">
					<p class="mb-3 text-info">[{{ $user->full_name }} es el contacto principal en {{ $company->name }}]</p>
				</div>
			</div>

			@endif

			@include('helpdesk.contacts.partials.form')
		</form>


		
		<div class="form-row">
			<div class="col-sm-4 mx-auto">
				<label><b>Zonas en las que el contacto se encuentra asigando</b></label>
			</div>
		</div>


		<div class="form-row">
			<div class="col-sm-6 offset-sm-4">
				
		
				<ul class="list-group">
					
				
					@foreach($user->zones as $zone)

	
	
						<li class="list-group-item d-flex align-items-center">
							

							<a href="{{ route('companies.show', $zone->company) }}/">

								{{ $zone->company->name }}

							</a>

							@php
								$zone_user = App\ZoneUser::where( 'user_id', $user->id )->where('zone_id', $zone->company->id)->first();
							@endphp
							@if (isset($zone_user))
								@if ($company->contact_id == $user->id)
									<a class="btn btn-link text-danger ml-auto" title="Remover usuario de la zona" data-toggle="modal" data-target="#chose_contact">
										<i class="fa fa-times"></i>
									</a>
								@else
									<button class="btn btn-link text-danger ml-auto" title="Remover usuario de la zona" onclick="return confirm('¿Estas seguro que deseas eliminar al contacto de la zona?')" form="remove_zone">
										<i class="fa fa-times"></i>
									</button>
									<form action="{{ route('zoneusers.destroy', ['zoneuser' => $zone_user->id]) }}" class="form-inline ml-2" method="post" id="remove_zone">
										{{ method_field('DELETE') }}
										{{ csrf_field() }}
									</form>
								@endif
							@endif

						</li>
					@endforeach


				</ul>


			</div>
		</div>



	</div>
	<div class="card-footer" id="contact_app">
		<div class="row form-row">
			<div class="col-sm-8 ml-auto">
				@role('admin', 'manager')
				<button type="submit" form="updateContactForm" class="btn btn-success">
					<i class="fa fa-check"></i>
					Actualizar Contacto
				</button>
				@endrole
			</div>
		</div>
	</div>

	@role('admin', 'manager')
		@isset ($zone_user)
			@include('helpdesk.contact_catalog.components.modal-assign-principal')
		@endisset
	@endrole
@stop