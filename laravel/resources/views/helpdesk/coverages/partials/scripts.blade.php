<script src="{{ asset('js/vue.js') }}"></script>
<script>
	var coveragesApp = new Vue({

		el: '#coverages_app',
		
		methods: {
			
			full_week: function(e){

				var checkbox = $(e.target);

				var from_input = $('#fromDaySelect');
				var to_input = $('#toDaySelect');

				from_input.val($('option:first', from_input).val());
				to_input.val($('option:last', from_input).val());

				if(checkbox.is(':checked')){
					from_input.attr('disabled', 'disabled');
					to_input.attr('disabled', 'disabled');
				}else{
					from_input.removeAttr('disabled');
					to_input.removeAttr('disabled');
				}

			},

			full_day: function(e){

				var checkbox = $(e.target);

				var from_input = $('#fromHourSelect');
				var to_input = $('#toHourSelect');

				from_input.val($('option:first', from_input).val());
				to_input.val($('option:last', from_input).val());

				if(checkbox.is(':checked')){
					from_input.attr('disabled', 'disabled');
					to_input.attr('disabled', 'disabled');
				}else{
					from_input.removeAttr('disabled');
					to_input.removeAttr('disabled');
				}

			}

		} 

	});
</script>