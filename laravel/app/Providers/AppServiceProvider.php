<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        \Blade::if('role', function ($role1 = null, $role2 = null, $role3 = null, $role4 = null) {
            
            $user = auth()->user();

            if($user){
                
                if(
                    $user->role == $role1 ||
                    $user->role == $role2 ||
                    $user->role == $role3 ||
                    $user->role == $role4
                ){
                    return true;
                    
                }elseif($user->role == 'agent'){

                    if(
                        $role1 == 'manager' ||
                        $role2 == 'manager' ||
                        $role3 == 'manager' ||
                        $role4 == 'manager'
                    ){

                        if($user->agent->as_manager){

                            return true;
                            
                        }
                    }

                }

            }


        });

        
        \Blade::if('ticket_owner', function ($ticket) {

            $user = auth()->user();

            if($user->role == 'agent' && $ticket->agent_id == $user->agent->id){
                return true;
            }elseif($user->role == 'agent' && optional($ticket->department)->supervisor_id == $user->agent->id){
                return true;
            }elseif($user->role == 'agent' && $user->agent->as_manager){
                return true;
            }elseif($user->role == 'admin' || $user->role == 'manager'){
                return true;
            }else{
                return false;
            }

        });


        \Blade::if('task_owner', function ($task) {

            $user = auth()->user();

            if($user->role == 'agent' && $task->agent_id == $user->agent->id){
                return true;
            }elseif($user->role == 'agent' && $user->agent->as_manager){
                return true;
            }elseif($user->role == 'admin' || $user->role == 'manager'){
                return true;
            }else{
                return false;
            }

        });



        \Blade::if('supervisor', function ($department) {

            $user = auth()->user();

            if($user->role == 'admin' || $user->role == 'manager'){

                return true;

            }elseif($user->role == 'agent' && $user->agent->as_manager){
                return true;
            }elseif($user->role == 'agent'){

                if(optional($user->agent->department)->supervisor_id == $user->agent->id){
                    return true;
                }
                
            }


        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
