<?php

namespace App\Http\Middleware;

use Closure;

class CompanyOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();

        if($user->role == 'company'){

            $contacts_ids = collect([]);

            $parents = $request['company']->all_parents();

            foreach ($parents as $parent) {
                foreach($parent->contacts as $contact){

                    $contacts_ids->push($contact->user->id);

                }
            }

            if(in_array($user->id, $contacts_ids->toArray())){

                return $next($request);
                
            }else{

                return redirect()->back()->withErrors(['Acceso Denegado']);

            }

        }

        return $next($request);

    }
}
