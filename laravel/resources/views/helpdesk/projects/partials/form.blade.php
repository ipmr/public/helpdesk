<div class="row form-row">
    <div class="form-group col-sm-4 mx-auto">
        <label><b>Información de proyecto</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

@isset($global)

    <div class="row form-row">
        <label for="" class="col-sm-4 text-right col-form-label">
            <span class="text-danger">*</span>
            Compañia:
        </label>
        <div class="form-group col-sm-4">
            <select name="company_id" id="company_master_select" class="form-control" @change="get_company_gears_models" required>
                <option disabled selected value>Elige una compañia</option>
                @foreach($companies as $company)
                    
                    <option value="{{ $company->id }}">{{ $company->name }}</option>

                @endforeach
            </select>
        </div>
    </div>
    <div class="row form-row" v-if=" zones ">
        <label for="" class="col-sm-4 text-right col-form-label">
            Sitio:
        </label>
        <div class="form-group col-sm-4">
            <select name="company_id" class="form-control">
                <option selected value>Elige un sitio (opcional)</option>
                <option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>
            </select>
        </div>
    </div>

@endisset

<div class="row form-row">
    <label class="col-sm-4 text-right col-form-label">
        <span class="text-danger">*</span>
        FUP:
    </label>
    <div class="form-group col-sm-2">
        <input type="text" name="fup" class="form-control" value="{{ $project->fup or old('fup') }}" autofocus required>
    </div>
</div>

<div class="row form-row">
    <label class="col-sm-4 text-right col-form-label">
        <span class="text-danger">*</span>
        Título:
    </label>
    <div class="form-group col-sm-4">
        <input type="text" name="title" class="form-control" value="{{ $project->title or old('title') }}" required>
    </div>
</div>

<div class="row form-row">
    <label class="col-sm-4 text-right col-form-label">
        Fecha Inicio:
    </label>
    <div class="form-group col-sm-2">
        <input type="text" name="start_date" class="form-control datepicker" value="{{ $project->start_date or old('start_date') }}">
    </div>
</div>

<div class="row form-row">
    <label class="col-sm-4 text-right col-form-label">
        Fecha Final:
    </label>
    <div class="form-group col-sm-2">
        <input type="text" name="end_date" class="form-control datepicker" value="{{ $project->end_date or old('end_date') }}">
    </div>
</div>


<div class="row form-row">
    <label class="col-sm-4 text-right col-form-label">
        <span class="text-danger">*</span>
        Descripción:
    </label>
    <div class="form-group col-sm-4">
        <textarea name="description" rows="4" class="form-control" required>{{ $project->description or old('description') }}</textarea>
    </div>
</div>