<?php

use Faker\Generator as Faker;

$factory->define(App\GearBrand::class, function (Faker $faker) {
    return [
        'name' => strtoupper($faker->word),
    ];
});
