@extends('layouts.master')
@section('content')
<div class="container" id="brandsApp">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Marcas de equipos</p>
		</div>
		<table class="table table-striped mb-0">
			<thead>
				<tr>
					<th>
						<div class="d-flex align-items-center">
							Marcas
							<form action="{{ route('brands.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
				                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
				                    value="{{ $_GET['q'] }}"
				                @endisset>
				                <button class="btn btn-success" type="submit">
				                    <i class="fa fa-search"></i> Buscar
				                </button>
				            </form>
							<a href="#new_brand_collapse" data-toggle="collapse" class="btn btn-primary ml-2" @if(request('new')) aria-expanded="true" @endif>
								<i class="fa fa-plus"></i>
								Agregar Marcas
							</a>
						</div>
					</th>
				</tr>
				<tr>
					<td class="p-0">
						<div class="collapse @if($errors->any()) show @endif" id="new_brand_collapse">
							
							<div class="pt-3">
								<form method="post" @submit.prevent="save_and_add_more">
									
									{{ csrf_field() }}
									
									<div class="form-row">
										<label class="col-form-label col-sm-3 text-right">Marcas:</label>
										<div class="form-group d-flex align-items-center col-sm-6">
											<input type="text" id="new_brand_input" class="form-control text-uppercase" autocomplete="off" required autofocus>
											<button type="submit" class="btn btn-info ml-2">
												<i class="fa fa-plus" style="margin: 0 !important"></i>
											</button>
										</div>
									</div>
								</form>

								<div class="form-row" v-if="brands.length > 0">
									<div class="col-sm-6 mx-auto">
										<ul class="list-group mb-3">
											<li class="list-group-item d-flex align-items-center" v-for="(brand, index) in brands">
												<span class="text-uppercase">@{{ brand }}</span>
												<a href="#" class="btn btn-link text-danger pr-0 ml-auto" @click.prevent="remove_brand_from_list(index)">
													<i class="fa fa-times"></i> 
												</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="form-row" v-show="brands.length > 0">
									<div class="form-group col-sm-9 ml-auto">
										<form action="{{ route('brands.store') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" :value="brands" name="brands_list">
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check"></i>
												Guardar Marcas
											</button>
										</form>
									</div>
								</div>
							</div>

						</div>
					</td>
				</tr>
			</thead>
			<tbody>
				@if(count($brands) > 0)
					@foreach($brands as $brand)
					<tr>
						<td>
							<a href="{{ route('brands.show', $brand) }}/">{{ $brand->name }}</a>
						</td>
					</tr>
					@endforeach
				@else
					<tr>
						
						<td>
							No se han agregado marcas de equipos
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('scripts')

	<script>
			
		var brands_app = new Vue({

			el: '#brandsApp',
			data: {
				brands: [],
				existing_brands: @json($brands)
			},
			methods: {

				save_and_add_more: function(e){

					var t = this;
					var form = $(e.target);
					var input = $('#new_brand_input');
					var value = input.val().toUpperCase();
					var btn = $('button', form);

					var exists = false;

					$.each(t.brands, function(i,o){

						if(o == value){

							exists = true;

						}

					});

					$.each(t.existing_brands, function(i,o){

						if(o.name == value){

							exists = true;

						}

					});

					if(!exists){
						t.brands.push(value.toUpperCase());
						input.val('').focus();
					}else{
						alert('La marca ya ha sido agregada.');
					}

					setTimeout(function(){
						btn.removeAttr('disabled');
					}, 300);

				},

				remove_brand_from_list: function(index){

					this.brands.splice(index, 1);

				}

			}

		});

	</script>

@stop
