<div class="card-body d-flex align-items-center justify-content-between wizard">
	
	<div class="project_progress_bar" id="projectProgressBar"></div>


	<div class="wizard_step" data-step="1" :class="step == 1 ? 'active' : step > 1 ? 'ready' : ''">
		<b class="text-uppercase">Inicio</b>
	</div>

	<div class="wizard_step" data-step="2" :class="step == 2 ? 'active' : step > 2 ? 'ready' : ''">
		<b class="text-uppercase">Cotización</b>
	</div>

	<div class="wizard_step" data-step="3" :class="step == 3 ? 'active' : step > 3 ? 'ready' : ''">
		<b class="text-uppercase">Producción</b>
	</div>

	<div class="wizard_step" data-step="4" :class="step == 4 ? 'active' : step > 4 ? 'ready' : ''">
		<b class="text-uppercase">Documentación</b>
	</div>

	<div class="wizard_step" data-step="5" :class="step == 5 ? 'active' : step > 5 ? 'ready' : ''">
		<b class="text-uppercase">Facturación</b>
	</div>

</div>