<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AsignTask extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->ticket = $request['ticket'];
        $this->task   = $request['task'];
        $this->agent  = $request['agent'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject( 'Tarea asignada en ticket' )
                    ->greeting( $this->agent->user->FullName )
                    ->line( 'Se te ha asignado la tarea "'.$this->task->description.'" en el ticket #' . $this->ticket->ticket_id )
                    ->action('Ver ticket', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg'    => 'Se te ha asignado la tarea "'.$this->task->description.'" en el ticket #' . $this->ticket->ticket_id,
            'ticket' => $this->ticket,
            'task'   => $this->task,
        ];
    }
}
