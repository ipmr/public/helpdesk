@component('helpdesk.components.modal', [

	'id' => 'requireRevisionModal',
	'title' => 'Solicitud de levantamiento',
	'size' => 'modal-lg'

])

		
	@slot('body')

		
		<form action="{{ route('new-project-ticket', [$company, $project->fup]) }}/" method="post" id="requiredRevisionForm" @submit.prevent="require_revision">
			
			<input type="hidden" name="revision" value="1">

			@include('helpdesk.projects.partials.ticket-form')

		</form>


	@endslot


	@slot('footer')

		<button class="btn btn-success" form="requiredRevisionForm"><i class="fa fa-check"></i> Enviar Ticket</button>

	@endslot


@endcomponent