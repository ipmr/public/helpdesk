@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <a class="btn btn-link p-0" href="{{ route('users.index') }}/">
                <i class="fa fa-angle-left fa-sm mr-1"></i>
                Regresar
            </a>
            <a class="btn btn-primary ml-auto" href="{{ route('users.edit', $user) }}/">
                <i class="fa fa-pencil-alt fa-xs"></i>
                Editar
            </a>
        </div>
        <table class="table table-striped m-0">
            <tr>
                <th width="30%">Nombre</th>
                <td>{{ $user->first_name }}</td>
            </tr>
            <tr>
                <th width="30%">Apellido</th>
                <td>{{ $user->last_name }}</td>
            </tr>
            <tr>
                <th width="30%">Usuario</th>
                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <th width="30%">E-mail</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th width="30%">Rol</th>
                <td>{{ ($user->role == 'admin')?'Administrador':'Manager' }}</td>
            </tr>
        </table>
    </div>
</div>
@stop