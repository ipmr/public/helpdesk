$(document).ready(function(){

	$(window).on('load', function(){
		$('#page_preload').fadeOut();
	});

	$('[href="#"]').on('click', function(e){
		e.preventDefault();
	});

	$('[data-toggle="tooltip"]').tooltip();

	$('.phone').mask('(000) 000-0000', {placeholder: "(___) ___-____"});


	$( ".datepicker" ).datepicker({
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
		dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		dateFormat: 'yy-mm-dd',
	});

	function fixed_on_scroll(item){

		var w = $(window);

		item.each(function(){
			var el = $(this);
			var pos = el.offset().top - 60;
			var el_width = el.parent().width() - 30;
			var header_height = el.height() + 26;

			w.scroll(function(){
				
				var w_scroll = w.scrollTop();
				
				if(w_scroll >= pos){
					el.addClass('fix_item').width(el_width);
					el.parent().css({paddingTop: header_height});
				}else{
					el.removeClass('fix_item').width('auto');
					el.parent().css({paddingTop: 0});
				}

			});

			if(el.hasClass('alert')){

				$('button.close', el).on('click', function(){
					setTimeout(function(){
						el.parent().css({paddingTop: 0});
					}, 150);
				});

			}

		});
		

	}


	var item = $('.card_header_fixed');
	var alerts = $('.fixed_alert');

	fixed_on_scroll(item);
	fixed_on_scroll(alerts);


	$('.folder_link').on('click', function(){
		var t = $(this);
		var id = t.attr('href');
		var close = `
			<i class="fa fa-chevron-circle-right fa-md"></i>
		`;
		var open = `
			<i class="fa fa-chevron-circle-down fa-md"></i>
		`;
		
		setTimeout(function(){
			if($(id).hasClass('show')){
				t.html(open);
			}else{
				t.html(close);
			}
		}, 400);
	});

});