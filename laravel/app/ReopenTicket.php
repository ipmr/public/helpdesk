<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReopenTicket extends Model
{
    public function ticket(){

        return $this->belongsTo('App\Ticket', 'ticket_id', 'ticket_id');

    }

    public function user(){

        return $this->belongsTo('App\User', 'id', 'user_id');

    }
}
