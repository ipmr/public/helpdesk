<?php

use Faker\Generator as Faker;

$factory->define(App\QuestionType::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'description' => $faker->text,
    ];
});
