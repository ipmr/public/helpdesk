<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">
		<span class="text-danger">*</span> 
		@isset($new_contact)
			Elige un contacto existente
		@else
			Contacto
		@endif
	</label>
	<div class="col-sm-3">
		<select name="contact_id" class="form-control" id="select_contact" required>
			<option
				disabled
				value
				selected
				>
				Elige una opción
			</option>
			@foreach($company->all_contacts() as  $contact)
			@isset ($contact->user)
			<option
				value="{{ $contact->id }}"
				@if(isset($intention) && $intention == 'edit_company')
					@if($contact->user->id == $company->contact->id)
					selected
					@endif
				@endif
				>
				{{ $contact->user->full_name }}
			</option>			    
			@endisset
			@endforeach
		</select>
	</div>
	<div class="col-sm-2">
		@if(isset($intention) && $intention == 'new_zone' || $intention == 'add_new_contact_in_zone')
		<a href="#" class="btn btn-link px-0 ml-3" @click.prevent="add_new_contact">
		@else
		<a href="{{ route('companies.contacts.index', $company) }}?new=true" class="btn btn-link px-0 ml-3" data-toggle="tooltip" data-placement="top" title="Crea un nuevo contacto para {{ $company->name }}">
		@endif
			<i class="fa fa-plus"></i> Nuevo Contacto
		</a>
	</div>
</div>