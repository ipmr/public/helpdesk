<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<label><b>Información de la cobertura</b></label>
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>

{{ csrf_field() }}
<div class="form-group row d-flex form-row">
	<label class="col-sm-4 col-form-label text-right">
		<span class="text-danger">*</span>
		Nombre:
	</label>
	<div class="col-sm-5">
		<input type="text" class="form-control" name="name" value="{{ $coverage->name or old('name') }}" autocomplete="off" required {{ isset($coverage) ? '' : 'autofocus' }}>
	</div>
</div>

<div class="form-group row d-flex form-row">
	<label class="col-sm-4 col-form-label text-right">
		<span class="text-danger">*</span>
		Días de cobertura:
	</label>
	<div class="col-sm-5 d-flex align-items-center">
		<div class="form-group m-0" style="width: 100%">
			<select name="from_day" id="fromDaySelect" class="form-control" @isset($coverage) {{ $coverage->all_week ? 'disabled' : '' }}  @endisset>
				<option value="1" @isset($coverage) {{ $coverage->from_day == '1' ? 'selected' : '' }} @endisset>Lunes</option>
				<option value="2" @isset($coverage) {{ $coverage->from_day == '2' ? 'selected' : '' }} @endisset>Martes</option>
				<option value="3" @isset($coverage) {{ $coverage->from_day == '3' ? 'selected' : '' }} @endisset>Miercoles</option>
				<option value="4" @isset($coverage) {{ $coverage->from_day == '4' ? 'selected' : '' }} @endisset>Jueves</option>
				<option value="5" @isset($coverage) {{ $coverage->from_day == '5' ? 'selected' : '' }} @endisset>Viernes</option>
				<option value="6" @isset($coverage) {{ $coverage->from_day == '6' ? 'selected' : '' }} @endisset>Sábado</option>
				<option value="7" @isset($coverage) {{ $coverage->from_day == '7' ? 'selected' : '' }} @endisset>Domingo</option>
			</select>
		</div>
		<span class="m-0 mx-3">a</span>
		<div class="form-group m-0" style="width: 100%">
			<select name="to_day" id="toDaySelect" class="form-control" @isset($coverage) {{ $coverage->all_week ? 'disabled' : '' }}  @endisset>
				<option value="1" @isset($coverage) {{ $coverage->to_day == '1' ? 'selected' : '' }} @endisset>Lunes</option>
				<option value="2" @isset($coverage) {{ $coverage->to_day == '2' ? 'selected' : '' }} @endisset>Martes</option>
				<option value="3" @isset($coverage) {{ $coverage->to_day == '3' ? 'selected' : '' }} @endisset>Miercoles</option>
				<option value="4" @isset($coverage) {{ $coverage->to_day == '4' ? 'selected' : '' }} @endisset>Jueves</option>
				<option value="5" @isset($coverage) {{ $coverage->to_day == '5' ? 'selected' : '' }} @else selected @endisset>Viernes</option>
				<option value="6" @isset($coverage) {{ $coverage->to_day == '6' ? 'selected' : '' }} @endisset>Sábado</option>
				<option value="7" @isset($coverage) {{ $coverage->to_day == '7' ? 'selected' : '' }} @endisset>Domingo</option>
			</select>
		</div>
	</div>
</div>

<div class="form-group row d-flex mt-3 form-row">
	<div class="col-sm-8 ml-auto">
		<div class="custom-control custom-checkbox">
		  <input type="checkbox" name="all_week" @change="full_week" @isset($coverage) {{ $coverage->all_week ? 'checked' : '' }} @endisset class="custom-control-input" id="allWeekCheckbox">
		  <label class="custom-control-label" for="allWeekCheckbox">Toda la semana</label>
		</div>
	</div>
</div>

<div class="form-group row d-flex form-row m-0">
	<label class="col-sm-4 col-form-label text-right">
		<span class="text-danger">*</span>
		Horario:
	</label>
	<div class="col-sm-5 d-flex align-items-center">
		<div class="form-group m-0" style="width: 100%">
			<select name="from_hour" class="form-control" id="fromHourSelect" @isset($coverage) {{ $coverage->all_day ? 'disabled' : '' }}  @endisset>
				@for($i = 1; $i <= 24; $i++)
				<option value="{{ $i }}" 
				@isset($coverage)
					@if($coverage->from_hour == $i) selected @endif
				@else
					@if($i == 9) selected @endif
				@endif
				>
					{{ $i }}:00
				</option>
				@endfor
			</select>
		</div>
		<span class="m-0 mx-3">a</span>
		<div class="form-group m-0" style="width: 100%">
			<select name="to_hour" class="form-control" id="toHourSelect" @isset($coverage) {{ $coverage->all_day ? 'disabled' : '' }}  @endisset>
				@for($i = 1; $i <= 24; $i++)
				<option value="{{ $i }}"
					@isset($coverage)
						@if($coverage->to_hour == $i) selected @endif
					@else
						@if($i == 17) selected @endif
					@endif
				>
					{{ $i }}:00
				</option>
				@endfor
			</select>
		</div>
	</div>
</div>

<div class="form-group row d-flex mt-3 mb-0 form-row">
	<div class="col-sm-8 ml-auto">
		<div class="custom-control custom-checkbox">
		  <input type="checkbox" name="all_day" @change="full_day" @isset($coverage) {{ $coverage->all_day ? 'checked' : '' }} @endisset class="custom-control-input" id="allDayCheckbox">
		  <label class="custom-control-label" for="allDayCheckbox">Todo el día</label>
		</div>
	</div>
</div>