<?php

namespace App\Http\Controllers;

use App\User;
use App\Agent;
use App\Department;
use Illuminate\Http\Request;

class DirectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = [
            "users.id",
            "users.first_name",
            "users.last_name",
            "users.email",
            "users.password",
            "users.username",
            "users.role",
            "agents.id as agent_id",
            "agents.department_id",
            "agents.jobtitle",
            "agents.active",
            "agents.cellphone",
            "agents.short_number",
            "agents.extension",
            "departments.name",
            "departments.description",
            "departments.supervisor_id",
        ];
        
        $users = User::select($columns)
            ->leftJoin('agents', 'users.id', '=', 'agents.user_id')
            ->leftJoin('departments', 'agents.department_id', '=', 'departments.id');

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $users = $users->when('users.first_name', function( $query ) use ($query_string, $request){
                $query->orWhere('users.first_name', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('users.first_name', 'like', '%'.$search.'%');
                }
            })
            ->when('users.last_name', function( $query ) use ($query_string, $request){
                $query->orWhere('users.last_name', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('users.last_name', 'like', '%'.$search.'%');
                }
            })
            ->when('users.email', function( $query ) use ($query_string, $request){
                $query->orWhere('users.email', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('users.email', 'like', '%'.$search.'%');
                }
            })
            ->when('users.username', function( $query ) use ($query_string, $request){
                $query->orWhere('users.username', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('users.username', 'like', '%'.$search.'%');
                }
            })
            ->when('agents.jobtitle', function( $query ) use ($query_string, $request){
                $query->orWhere('agents.jobtitle', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('agents.jobtitle', 'like', '%'.$search.'%');
                }
            })
            ->when('agents.cellphone', function( $query ) use ($query_string, $request){
                $query->orWhere('agents.cellphone', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('agents.cellphone', 'like', '%'.$search.'%');
                }
            })
            ->when('agents.short_number', function( $query ) use ($query_string, $request){
                $query->orWhere('agents.short_number', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('agents.short_number', 'like', '%'.$search.'%');
                }
            })
            ->when('agents.extension', function( $query ) use ($query_string, $request){
                $query->orWhere('agents.extension', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('agents.extension', 'like', '%'.$search.'%');
                }
            })
            ->when('departments.name', function( $query ) use ($query_string, $request){
                $query->orWhere('departments.name', 'like', '%'.$request->q.'%');
                foreach ($query_string as $search) {
                    $query->orWhere('departments.name', 'like', '%'.$search.'%');
                }
            });
        }

        if (isset($request->debug)) {
            echo $users->toSql();

            if ($request->debug == 2) {
                dd($users->get());
            }
        }

        $users = $users->paginate(12);

        return view( 'helpdesk.directory.index', compact( 'users' ) );
    }
}