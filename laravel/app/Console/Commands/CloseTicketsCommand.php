<?php

namespace App\Console\Commands;

use App\History;
use App\Status;
use App\Ticket;
use Illuminate\Console\Command;
use App\Notifications\ChangeTicketStatus;
use Notification;

class CloseTicketsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close:ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cerrar los tickets en status "Resuelto" después de 72 horas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date_search  = date('Y-m-d H:i:s', strtotime('-72 hour ' . date('Y-m-d H:i:s')));
        $status       = Status::select('id', 'name')->where('name', 'Resuelto')->first();
        $status_close = Status::where('name', 'Cerrado')->first();

        $tickets = Ticket::where('status_id', $status->id)->where('updated_at', '<=', $date_search)->get();

        if ($tickets->count() > 0) {
            foreach ($tickets as $ticket) {
                // echo $date_search."\n";
                #validar comentarios de ticket
                $comments = $ticket->comments()->whereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();

                #si los comentarios del ticket son recientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($comments->count() > 0) {
                    echo 'tiene comentarios vigentes #' . $ticket->ticket_id . "\n";
                    continue;
                }

                #validar historial de ticket
                $histories = $ticket->histories()->whereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();
                #si el historial del ticket son recientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($histories->count() > 0) {
                    echo 'tiene historial vigente #' . $ticket->ticket_id . "\n";
                    continue;
                }

                $tasks = $ticket->tasks()->where('status',1)->orWhereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();

                #si el ticket tiene tareas pendientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($tasks->count() > 0) {
                    echo 'tiene tareas pendientes #' . $ticket->ticket_id . "\n";
                    continue;
                }

                echo 'cerrando ticket #' . $ticket->ticket_id . "\n";
                $ticket->update([
                    'status_id' => $status_close->id,
                ]);

                #se guarda el historico del cambio de status
                $history = new History();
                $history->ticket_id   = $ticket->ticket_id;
                $history->description = 'Ticket cerrado por sistema';
                $history->save();

                #notificacion cambio status
                $notification_data = [
                    'user'   => $ticket->author,
                    'ticket' => $ticket,
                    'status' => $status_close,
                ];  

                Notification::send($ticket->author, new ChangeTicketStatus($notification_data));
            }
        } else {

            echo "No hay tickets Resueltos\n";

        }
    }
}
