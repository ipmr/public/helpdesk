<div class="collapse @isset($show) show @endisset p-2" id="modules_box{{ $g->id }}">
	
		<table class="table table-bordered table-striped m-0">
			
			<thead>
				<tr style="background: #fff !important">
					<th>Módulo</th>
					<th>Modelo</th>
					<th># Serie</th>
					<th class="text-center">Módulos</th>
					<th class="text-center" width="12%">Retirar</th>
				</tr>
			</thead>

			<tbody>

				@foreach($g->modules as $module)

					<tr class="{{ $module->module->active ? '' : 'bg-danger-light' }}">
						<td>
				
					
							@if($module->module->modules->count() > 0)

								<a href="#modules_box{{ $module->module->id }}" data-toggle="collapse" class="no_decoration folder_link" style="margin-right: 2px !important">
									<i class="fa fa-chevron-circle-right fa-md"></i>
								</a>

							@endif
					

							{{ $module->slot }}

						</td>
						<td>{{ $module->module->model->name }}</td>
						<td class="droppable_tr" data-gear="{{$module->module->serial_number}}">
							<a href="{{ route('companies.gears.show', [$company, $module->module->id]) }}/" class="draggable_tr" data-gear="{{$module->module->serial_number}}">
								{{ $module->module->serial_number }}
							</a>
						</td>
						<td class="text-center">{{ $module->module->modules->count() }} Módulos</td>
						<td class="text-center" width="12%">
							<form action="{{ route('remove_module', $module) }}" method="post" @submit.prevent="remove_module_form">

								{{ csrf_field() }}
									
								<button type="submit" class="btn btn-link p-0 text-danger">
									<i class="fa fa-times"></i>
								</button>

							</form>
						</td>
					</tr>

					@if($module->module->modules->count() > 0)

						<tr class="p-0">
							<td colspan="5" class="p-0">
								
								@component('helpdesk.gears.components.modules-box', [
									'g' => $module->module,
									'company' => $company
								])

								@endcomponent

							</td>
						</tr>

					@endif

				@endforeach

			</tbody>

		</table>

</div>