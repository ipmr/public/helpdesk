<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<label><b>Información del contrato</b></label>
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>

{{ csrf_field() }}
<div class="form-group row form-row">
	<label for="" class="col-form-label col-sm-4 text-right">
		<span class="text-danger">*</span>
		Nombre:
	</label>
	<div class="col-sm-5">
		<input type="text" value="{{ $contract->name or old('name') }}" name="name" class="form-control" autofocus required>
	</div>
</div>
<div class="form-group row form-row">
	<label for="" class="col-form-label col-sm-4 text-right">Descripción:</label>
	<div class="col-sm-5">
		<textarea name="description" class="form-control" rows="3">{{ $contract->description or old('description') }}</textarea>
	</div>
</div>
<div class="form-group row form-row">
	<label for="" class="col-form-label col-sm-4 text-right">
		<span class="text-danger">*</span>
		Vigencia:
	</label>
	<div class="col-sm-1">
		<input type="text" placeholder="Num." value="{{ $contract->period_number or 1 }}" name="period_number" class="form-control" required>
	</div>
	<div class="col-sm-2">
		<select name="period_format" class="form-control" required>
			<option value="d" @isset($contract) {{ $contract->period_format == 'd' ? 'selected' : '' }} @endisset>Día(s)</option>
			<option value="w" @isset($contract) {{ $contract->period_format == 'w' ? 'selected' : '' }} @endisset>Semana(s)</option>
			<option value="m" @isset($contract) {{ $contract->period_format == 'm' ? 'selected' : '' }} @else selected @endisset>Mes(es)</option>
			<option value="y" @isset($contract) {{ $contract->period_format == 'y' ? 'selected' : '' }} @endisset>Año(s)</option>
		</select>
	</div>
</div>