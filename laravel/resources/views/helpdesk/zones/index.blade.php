@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')
	<div id="zone_app">
		<table class="table table-striped mb-0">
			<thead>
				<tr>
					<td colspan="3">
						<div class="d-flex align-items-center py-2">
							<label class="m-0"><b>Lista de sitios</b></label>
							@role('admin','manager')
							<a class="btn btn-primary ml-auto" href="#add_new_zone_collapse" data-toggle="collapse" @if($errors->any()) aria-expanded="true" @endif>
								<i class="fa fa-plus fa-sm mr-1"></i>
								Nuevo Sitio
							</a>
							@endrole
						</div>
						@role('admin','manager')
						<div class="collapse @if($errors->any()) show @endif" id="add_new_zone_collapse">
							<form action="{{ route('companies.zones.store', $company) }}" method="post">
								
								@include('helpdesk.companies.partials.form', [

									'intention' => 'new_zone'

								])

								<div class="row form-row">
									<div class="col-sm-8 ml-auto">
										<button class="btn btn-success mb-3"><i class="fa fa-check"></i> Guardar Sitio</button>
									</div>
								</div>
							</form>
						</div>
						@endrole
					</td>
				</tr>
				@if(count($zones) > 0)
				<tr style="border-top: 1px solid #f2f2f2">
					<td>Nombre de sitio</td>
					<td>Contacto principal</td>
					<td>Sitios</td>
				</tr>
				@endif
			</thead>
			<tbody>
				@if(count($zones) > 0)
					@foreach($zones as $zone)
					<tr>
						<td><a href="{{ route('companies.show', [$zone]) }}/">{{ $zone->name }}</a></td>
						<td>{{ $zone->contact->first_name .' '. $zone->contact->last_name }}</td>
						<td>{{ $zone->childs->count() }}</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="3">
							<p class="m-0">No se han agregado sitios a {{ $company->name }}</p>
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
@stop

@section('scripts')

	<script src="{{ asset('js/vue.js') }}"></script>
	<script>
		@include('helpdesk.contacts.scripts.create_new_contact', [
			'intention' => 'new_zone'
		])
	</script>
@stop