<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Notifications\AgentAssigned;
use App\Notifications\AssignSupervisorDepartment;
use App\Notifications\AssignAgentDepartment;
use Validator;
use Notification;

class DepartmentController extends Controller
{

    public function __construct()
    {

        $this->middleware('role:admin')->only(['index', 'create', 'update', 'edit']);

    }

    public function index(Request $request)
    {
        $columns = [
            'departments.id',
            'departments.name',
            'departments.description',
            'departments.supervisor_id',
            'users.first_name',
            'users.last_name',
        ];
        #se instancian los departamentos existentes
        $departments = Department::select($columns)
                ->join('agents', 'departments.supervisor_id', 'agents.id')
                ->leftJoin('users', 'agents.user_id', 'users.id');

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $departments = $departments->where(function ($q) use ($query_string, $request) {
                $q = $q->where('departments.name', 'like', '%'.$request->q.'%')
                    ->orWhere('departments.description', 'like', '%'.$request->q.'%')
                    ->orWhere('users.first_name', 'like', '%'.$request->q.'%')
                    ->orWhere('users.last_name', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('departments.name', 'like', '%'.$search.'%')
                        ->orWhere('departments.description', 'like', '%'.$search.'%')
                        ->orWhere('users.first_name', 'like', '%'.$search.'%')
                        ->orWhere('users.last_name', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $departments->toSql() );
            }

            if ($request->debug == 2) {
                dd( $departments->get() );
            }
        }

        $departments = $departments->orderBy('id','DESC')->paginate(12);

        return view('helpdesk.departments.index', compact('departments'));
    }

    public function create()
    {

        if(Agent::count() == 0){

            return redirect()->route('agents.create')->with('warning', 'Es necesario crear a un agente antes de crear un departamento.');

        }

        $agents = Agent::with('supervisor')
            ->has('supervisor', '==', 0)
            ->get();

        return view('helpdesk.departments.create', compact('agents'));
    }

    public function store(Request $request)
    {
        #se valida la informacion obtenida
        $this->validate_department_data($request);

        $department = $this->create_department($request);

        #se asignan al departamento los agentes seleccionados
        $this->assign_agents_to_department($request, $department);

        #se asigna al agente elegido como supervisor al departamento
        $this->assign_agent_as_supervisor($request, $department);

        #se retorna con mensaje de confirmacion
        return redirect()->route('departments.show', $department)->with('msg', 'Se ha creado el departamento ' . $department->name . ' exitosamente');

    }

    public function create_department($request){

        $department = new Department();
        $department->name          = $request->name;
        $department->description   = $request->description;
        $department->supervisor_id = $request->supervisor_id;
        $department->save();

        return $department;

    }

    public function assign_agent_as_supervisor($request, $department){

        if ($request->supervisor_id) {

            $agent = Agent::find($request->supervisor_id);
            $agent->update([
                'department_id' => $department->id,
            ]);

            $department->update([
                'supervisor_id' => $agent->id,
            ]);

            $notification_data = [
                'department' => $department,
                'agent'      => $agent,
            ];

            Notification::send( $agent->user, new AssignSupervisorDepartment( $notification_data ) );
        }

    }

    public function show(Department $department)
    {
        return view('helpdesk.departments.show', compact('department'));
    }

    public function edit(Department $department)
    {

        $agents = Agent::with('supervisor')
            ->has('supervisor', '==', 0)
            ->orWhereHas('supervisor', function ($q) use ($department) {
                $q->where('id', $department->id);
            })
            ->get();

        return view('helpdesk.departments.edit', compact('department', 'agents'));
    }

    public function update(Request $request, Department $department)
    {
        #se valida la información obtenida
        $this->validate_department_data($request, $department);

        #se actualiza la informacion del departamento
        $department->update([

            'name'          => $request->name,
            'description'   => $request->description,
            'supervisor_id' => null,

        ]);

        #se eliminan los agentes relacionados al departamento
        $department->agents()->update([
            'department_id' => null,
        ]);

        #se asignan al departamento los agentes seleccionados
        $this->assign_agents_to_department($request, $department);

        #se asigna como supervisor del departamento al agente seleccionado
        $agent = Agent::find($request->supervisor_id);

        $this->assign_agent_as_supervisor($request, $department);

        $agent->update([
            'department_id' => $department->id,
        ]);

        return redirect()
            ->route('departments.show', $department)
            ->with('msg', 'La información del departamento ha sido actualizada.');
    }

    public function destroy(Department $department)
    {
        //
    }

    public function assign_agents_to_department($request, $department)
    {

        if ($request->agents) {
            if (count($request->agents) > 0) {

                foreach ($request->agents as $agent) {

                    #se valida el id del agente
                    $validate = Validator::make([
                        'id' => $agent,
                    ], [
                        'id' => 'exists:agents',
                    ], [
                        'id.exists' => 'Agente seleccionado no válido',
                    ])->validate();

                    #se busca al agente correspondiente al ID
                    $agent      = Agent::find($agent);
                    $department = Department::find($department)->first();


                    #si el agente es supervisor de otro departamento
                    #se elimina la asignacion
                    if ($agent->id == optional( $agent->department )->supervisor_id) {

                        $agent->department->update([
                            'supervisor_id' => null,
                        ]);

                    }

                    #se asigna al agente al departamento
                    $agent->update([
                        'department_id' => $department->id,
                    ]);

                    $notification_data = [
                        'department' => $department,
                        'agent'      => $agent,
                    ];

                    Notification::send( $agent->user, new AssignAgentDepartment( $notification_data ) );

                }

            }
        }

    }

    public function validate_department_data($request, $department = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required'          => 'El nombre del departamento es requerido',
            'name.unique'            => 'El nombre del departamento no esta disponible',
            'supervisor_id.required' => 'El supervisor del departamento es requerido',
            'supervisor_id.exists'   => 'El supervisor seleccionado no es válido',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), [

            'name'          => ['required', 'string', Rule::unique('departments')->ignore($department ? $department->id : '')],
            'description'   => 'string|nullable',
            'supervisor_id' => 'required|exists:agents,id',

        ], $msgs)->validate();

    }


    public function get_department_agents(Request $request, Department $department){

        $agents = $department->agents()->with('user')->get();

        return response()->json($agents);

    }
}
