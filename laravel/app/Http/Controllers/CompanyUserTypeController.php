<?php

namespace App\Http\Controllers;

use App\CompanyUserType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;


class CompanyUserTypeController extends Controller
{


    public function __construct(){


        $this->middleware('role:admin');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact_types = CompanyUserType::paginate(12);

        return view('helpdesk.contact_types.index', compact('contact_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('helpdesk.contact_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        $contact_type = new CompanyUserType();

        $contact_type->name = $request->name;

        $contact_type->save();

        return redirect()
            ->route('contact-types.show', $contact_type)
            ->with('msg', 'Se ha creado el tipo de contacto ' . $contact_type->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyUserType  $contact_type
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyUserType $contact_type)
    {
        return view('helpdesk.contact_types.show', compact('contact_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyUserType  $contact_type
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyUserType $contact_type)
    {
        return view('helpdesk.contact_types.edit', compact('contact_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyUserType  $contact_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyUserType $contact_type)
    {
        $this->validation($request, $contact_type);

        $contact_type->name = $request->name;

        $contact_type->save();

        return redirect()
            ->route('contact-types.show', $contact_type)
            ->with('msg', 'La información del tipo de contacto ha sido actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyUserType  $contact_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyUserType $contact_type)
    {
        //
    }

    public function validation($request, $contact_type = null)
    {
        $rules = [
            'name'        => [
                'required',
                'string',
                'max:190',
                $contact_type ? Rule::unique('company_user_types')->ignore($contact_type->id) : 'unique:company_user_types',
            ],
        ];

        $msgs = [
            'name.required' => 'El nombre de tipo de contacto es requerido',
            'name.string'   => 'El nombre de tipo de contacto debe ser alfanumérico',
            'name.max'      => 'El nombre de tipo de contacto debe contener menos de 190 caracteres',
            'name.unique'   => 'El nombre de tipo de contacto ya existe',
        ];

        $validate = Validator::make($request->all(), $rules, $msgs)->validate();
    }
}
