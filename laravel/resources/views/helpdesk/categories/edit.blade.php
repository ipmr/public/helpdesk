@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <a class="btn btn-link p-0" href="{{ route('category.show', $category) }}/">
                <i class="fa fa-angle-left fa-sm mr-1"></i>
                Regresar
            </a>
        </div>
        <div class="card-body">
            <div class="row form-row mb-3">
                <div class="col-sm-9 ml-auto">
                    <label><b>Editar categoría</b></label>
                </div>
            </div>
            <form action="{{ route('category.update', $category) }}" method="post" id="newDepartmentForm">                        
                {{ method_field('PATCH') }}
                @include('helpdesk.categories.partials.form', ['btn'=>'Actualizar'])
                
            </form>
        </div>
    </div>
</div>
@stop