<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketTime extends Model
{
    public function ticket(){

        return $this->belongsTo('App\Ticket', 'ticket_id', 'ticket_id');

    }
}
