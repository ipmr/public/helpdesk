@extends('layouts.master')

@section('content')
    
    <div class="container" id="app_notifications">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <p class="lead mt-2 mb-2">
                    Notificaciones Archivadas
                </p>
            </div>
            @include('helpdesk.notifications.partials.notifications_list', [
                'notifications' => $notifications,
                'is_archived'   => true
            ])
        </div>
    </div>

@stop