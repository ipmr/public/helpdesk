<?php

namespace App\Http\Controllers;

use App\Company;
use App\Gear;
use App\GearModule;
use Illuminate\Http\Request;
use Validator;

class GearModuleController extends Controller
{
    public function index(Company $company, Gear $gear)
    {

        $modules = $gear->modules()->with('module.brand', 'module.model', 'module.modules', 'module.company')->get();

        return response()->json($modules);

    }

    public function create()
    {
        //
    }

    public function store(Request $request, Company $company, Gear $gear)
    {


        if(!$request->has('new_gear')){


            $insert_module = $this->insert_module($request, $request->serial_number, $gear->serial_number);



            if($insert_module['error']){


                return redirect()->back()->withErrors([$insert_module['error']]);


            }



        }else{


            $exists_gear = Gear::where('serial_number', $request->serial_number)->first();

            if($exists_gear){


                return redirect()->back()->withErrors([

                    'El número de serie proporcionado ya se encuentra registrado'

                ]);


            }else{



                $new_gear = $gear->replicate();

                $new_gear->brand_id = $request->brand_id;
                $new_gear->type_id = $request->type_id;
                $new_gear->model_id = $request->model_id;
                $new_gear->serial_number = $request->serial_number;


                $new_gear->save();



                $insert_module = $this->insert_module($request, $request->serial_number, $gear->serial_number);



                if($insert_module['error']){


                    return redirect()->back()->withErrors([$insert_module['error']]);


                }



            }


        }


        return redirect()
            ->back()
            ->with("msg", "Se ha instalado un nuevo equipo en el slot {$request->slot} correctamente.");


    }

    public function validate_data($request)
    {

        $validate = Validator::make($request->all(), [

            'slot'        => 'required|string',
            'brand_id'    => 'sometimes|exists:gear_brands,id',
            'type_id'     => 'sometimes|exists:gear_types,id',
            'model_id'    => 'sometimes|exists:gear_models,id',

        ], [

            'slot.required'   => 'El nombre del módulo/slot es requrido.',
            'brand_id.exists' => 'La marca de equipo proporcionada no es válida',
            'type_id.exists'  => 'El tipo de equipo proporcionado no es válido',
            'model_id.exists' => 'El modelo del equipo proporcionado no es válido',

        ])->validate();

    }

    public function insert_module(Request $request, $module, $gear)
    {

        $module = Gear::where('serial_number', $module)->first();
        $gear   = Gear::where('serial_number', $gear)->first();


        if($module->id == $gear->id){


            $response = [

                'error' => 'Un equipo no puede insertarse en si mismo.',

            ];

            

            if($request->ajax()){


                return response()->json($response);


            }else{


                return $response;

            }

        }


        $module_is_parent = $module
            ->all_modules
            ->where('gear_id', $module->id)
            ->first();

        $module_connected = $module
            ->all_modules
            ->where('module_id', $gear->id)
            ->first();

        if ($module_is_parent && $module_connected) {

            $response = [

                'error' => 'Un equipo no puede insertarse en si mismo.',

            ];

            

            if($request->ajax()){


                return response()->json($response);


            }else{


                return $response;

            }



        } else {

            $find_current_module = GearModule::where('module_id', $module->id)->first();

            if ($find_current_module) {

                $find_current_module->delete();

            }

            $module_unavailable = $gear->modules->where('slot', $request->slot)->first();

            if ($module_unavailable) {

                $response = [

                    'error' => 'El slot proporcionado ya se encuentra ocupado',

                ];

                if($request->ajax()){


                    return response()->json($response);


                }else{


                    return $response;


                }

            } else {

                $new_module = new GearModule();

                $new_module->gear_id   = $gear->id;
                $new_module->module_id = $module->id;
                $new_module->slot      = $request->slot;

                $new_module->save();

                $module->company_id        = $gear->company_id;
                $module->coverage_id       = $gear->coverage_id;
                $module->master_coverage   = $gear->master_coverage;
                $module->contract_id       = $gear->contract_id;
                $module->master_contract   = $gear->master_contract;
                $module->contract_start_at = $gear->contract_start_at;

                $module->comments = "Equipo insertado en el módulo {$request->slot} del equipo {$gear->model->name} con número de serie {$gear->serial_number}";

                $module->save();

            }

        }

    }

    public function remove_module(GearModule $module)
    {

        $module->module->update([

            "comments" => null,

        ]);

        $module->delete();

        return redirect()->back()->with('msg', 'El módulo ha sido retirado exitosamente.');

    }

    public function show(Company $company, Gear $gear, GearModule $module)
    {

    }

    public function edit(GearModule $gearModule)
    {
        //
    }

    public function update(Request $request, GearModule $gearModule)
    {
        //
    }

    public function destroy(GearModule $gearModule)
    {
        //
    }
}
