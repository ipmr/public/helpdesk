{{ csrf_field() }}
{{ method_field('PATCH') }}

<div v-if="edit_module">

	<div class="form-row">
		<label class="col-sm-3 col-form-label text-right">Slot:</label>
		<div class="form-group col-sm-4">
			<input type="text" name="slot" :value="edit_module.slot" class="form-control">
		</div>
	</div>

	<div class="form-row">
		<label class="col-sm-3 col-form-label text-right">Descripción:</label>
		<div class="form-group col-sm-6">
			<textarea name="description" rows="4" class="form-control">@{{ edit_module.description }}</textarea>
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-6 mx-auto">
			<div class="form-group">
				<div class="custom-control custom-checkbox">
				  <input type="checkbox" 
				  		 class="custom-control-input" 
				  		 id="slotWithModuleInModal" 
				  		 :checked="edit_module_add_gear"
				  		 @change.prevent="add_gear_to_module_in_modal">

				  <label class="custom-control-label" for="slotWithModuleInModal">Hay equipo instalado en el módulo</label>
				</div>
			</div>
		</div>
	</div>
	
	<div v-if="edit_module_add_gear">
		<div class="form-row">
			<div class="col-sm-6 mx-auto">
				<p class="text-info">
					Adjunta un equipo existente a traves de el número de serie.
				</p>
			</div>
		</div>

		<div class="form-row">
			<label class="col-sm-3 col-form-label text-right"># Serie:</label>
			<div class="form-group col-sm-4">
				<input type="text" 
					   name="existing_serial_number"
					   class="form-control serial_numbers_input_in_modal" 
					   :value="edit_module.module ? edit_module.module.serial_number : ''" 
					   placeholder="Ingresa el número de serie..." 
					   required>
			</div> 	
		</div>

		<div class="form-row">
			<div class="form-group col-sm-6 mx-auto">
				<a href="#" class="btn btn-link p-0">O bien, crea un nuevo equipo</a>
			</div>
		</div>
	</div>

</div>

<div class="form-row" v-else>
	<div class="col-sm-6 mx-auto">
		<p class="m-0"><i class="fa fa-spin fa-spinner mr-2 fa-lg"></i> Cargando Información...</p>
	</div>
</div>