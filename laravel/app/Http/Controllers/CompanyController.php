<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyUser;
use App\CompanyUserType;
use App\ContactCatalog;
use App\Notifications\NewCompanyAccount;
use App\User;
use App\ZoneUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CompanyController extends Controller
{

    public function __construct()
    {

        #se establecen el middleware role
        #para que solo personal interno pueda verificar las compañias
        $this->middleware('role:admin,manager')->except(['index', 'show']);

        #se establece el middleware company_owner para que
        #un cliente pueda visualizar su información
        $this->middleware('company_owner')->only(['show']);
    }

    public function index(Request $request)
    {

        $user = auth()->user();

        #se buscan las compañias relacionadas al tipo de usuario
        $companies = $this->companies_by_user_role($request, $user);

        return view('helpdesk.companies.index', compact('companies'));
    }

    public function companies_by_user_role($request, $user)
    {
        $companies = Company::where('id', '>', 0)->when('id',function($query) use ($user){
            if ( $user->zones_from_contact_type ) {
                $query->whereIn('id', $user->zones_from_contact_type );
            } else {
                $query->where('master',1);
            }
        });

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q);

            $companies = $companies->when('name', function ($query) use ($query_string, $request) {
                $query->where('name', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->orWhere('name', 'like', '%' . $search . '%');
                }
            })
            ->when('rfc', function ($query) use ($query_string, $request) {
                $query->where('rfc', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->orWhere('rfc', 'like', '%' . $search . '%');
                }
            })
            ->when('address', function ($query) use ($query_string, $request) {
                $query->orWhere('address', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->where('address', 'like', '%' . $search . '%');
                }
            })
            ->when('city', function ($query) use ($query_string, $request) {
                $query->orWhere('city', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->where('city', 'like', '%' . $search . '%');
                }
            })
            ->when('state', function ($query) use ($query_string, $request) {
                $query->orWhere('state', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->where('state', 'like', '%' . $search . '%');
                }
            });
        }

        return $companies->paginate(12);
    }

    public function create()
    {

        return view('helpdesk.companies.create');
    }

    public function store(Request $request, $zone = null, $parent = null)
    {

        #se valida la informacion
        $this->validate_company_data($request);

        #se crea el usuario del contacto principal
        $contact = $this->create_contact($request);

        #se crea la compañia
        $company = $this->create_company($request, $contact);

        #se crea el perfil del contacto
        $this->create_contact_profile($request, $contact, $company, $parent);

        #se asocia al contacto con la zona
        $this->add_contact_to_zone($contact, $company, $parent );

        #se notifica al contacto sobre la compañia creada
        $this->notify_to_contact($request, $company);

        #si la compañia que se intenta crear es una zona
        #se retorna el modelo para utilizarlo en el controlador
        #CompanyZoneController
        if (!is_null($zone)) {

            return $company;

        } else {

            #se retorna con notificacion de nueva compañia
            return redirect()
                ->route('companies.show', $company)
                ->with('msg', 'Se ha creado la compañia ' . $company->name . ' exitosamente');
        }
    }

    public function create_contact($request)
    {

        #si se elige a un contacto especifico se busca para retornarlo
        #de lo contrario se crea un nuevo usuario de contacto.

        if ($request->has('contact_id')) {

            $user = CompanyUser::find($request->contact_id)->user;

        } else {

            $user             = new User();
            $user->first_name = $request->contact_first_name;
            $user->last_name  = $request->contact_last_name;
            $user->email      = $request->email;
            $user->role       = 'company';
            $user->password   = \Hash::make($request->password);
            $user->username   = uniqid();
            $user->save();

        }

        return $user;
    }

    public function create_contact_profile($request, $user, $company, $parent)
    {

        #se define el equipo al que pertenece la compañia

        $team_id = null;

        if (!is_null($parent)) {
            $team_id = $parent->all_parents()->reverse()->first()->id;
        } else {
            $team_id = $company->all_parents()->reverse()->first()->id;
        }

        #si se elige a un contacto especifico se busca para retornarlo
        #de lo contrario se crea un nuevo perfil de contacto.

        if ($request->has('contact_id')) {

            $user_profile = User::find($request->contact_id)->contact;

        } else {

            $user_profile           = new CompanyUser();
            $user_profile->user_id  = $user->id;
            $user_profile->team_id  = $team_id;
            $user_profile->phone    = $request->contact_phone;
            $user_profile->jobtitle = $request->jobtitle;
            $user_profile->ext      = $request->contact_phone_ext;
            $user_profile->mobile   = $request->contact_mobile;
            $user_profile->save();

        }

        return $user_profile;
    }

    public function create_company($request, $user)
    {

        $company                 = new Company();
        $company->name           = $request->name;
        $company->address        = $request->address;
        $company->city           = $request->city;
        $company->state          = $request->state;
        $company->zip            = $request->zip;
        $company->phone          = $request->phone;
        $company->contact_id     = $user->id;
        $company->urgent_tickets = $request->urgent_tickets ? 1 : 0;

        $company->save();

        #se asigna el rfc a la compañia
        $this->set_company_rfc($request, $company);

        #si se le asigna un contrato a la compañia
        $this->add_contract_to_company($request, $company);

        return $company;
    }

    public function set_company_rfc($request, $company)
    {

        $company->rfc        = $request->rfc ? strtoupper($request->rfc) : null;
        $company->master_rfc = $request->rfc ? false : true;
        $company->save();
    }

    public function add_contract_to_company($request, $company)
    {

        if ($request->has('contract_id')) {

            if (is_null($request->contract_id)) {

                $company->contract_id     = null;
                $company->sla_id          = null;
                $company->coverage_id     = null;
                $company->master_contract = null;

            } else {

                $contract          = \App\Contract::find($request->contract_id);
                $contract_start_at = Carbon::parse($request->contract_start_at);

                $company->contract_id       = $contract->id;
                $company->contract_start_at = $contract_start_at->format('Y-m-d');
                $company->sla_id            = $request->sla_id;
                $company->coverage_id       = $request->coverage_id;
                $company->master_contract   = null;

            }

        } else {

            $company->contract_id       = null;
            $company->contract_start_at = null;
            $company->sla_id            = null;
            $company->coverage_id       = null;
            $company->master_contract   = 1;

        }

        $company->save();
    }

    public function notify_to_contact($request, $company)
    {

        $notification_data = [
            'password' => $request->password,
            'company'  => $company,
        ];

        $company->contact->notify(new NewCompanyAccount($notification_data));
    }

    public function add_contact_to_zone($contact, $company, $parent = null)
    {


        $contact_type = CompanyUserType::where('name', 'Contacto Principal')->first();

        $zone          = new ZoneUser();
        $zone->user_id = $contact->id;
        $zone->zone_id = $company->id;
        $zone->role    = $contact_type->id;
        $zone->save();
        
        
        if(is_null($parent)){


            $contact_catalog                 = new ContactCatalog();
            $contact_catalog->user_id        = $contact->id;
            $contact_catalog->master_zone_id = $company->id;
            $contact_catalog->save();


        }



        return $zone;
    }

    public function show(Company $company)
    {


        $tab = 'general';


        return view('helpdesk.companies.show', compact('company', 'tab'));


    }

    public function edit(Company $company)
    {
        $tab = 'general';

        return view('helpdesk.companies.edit', compact('company', 'tab'));
    }

    public function update(Request $request, Company $company)
    {
        #se valida la informacion
        $this->validate_company_data($request, $company);

        if (!$request->has('contact_id')) {

            #se actualiza la informacion del contacto principal
            $this->update_contact_user($request, $company);

            #se actualiza el perfil del contacto principal
            $this->update_contact_profile($request, $company);

        }

        #se actualiza la informacion de la compañia
        $this->update_company_information($request, $company);

        #se retorna con mensaje de confirmacion
        return redirect()->route('companies.show', $company)->with('msg', 'La información de la compañia ha sido actualizada.');
    }

    public function update_contact_user($request, $company)
    {

        $user = $company->contact;

        #se modifica el nombre del contacto principal en la compañia
        $user->update([

            'first_name' => $request->contact_first_name,
            'last_name'  => $request->contact_last_name,
            'email'      => $request->email,

        ]);

        #se actualiza la contraseña en caso de ser requerido
        if ($request->password) {

            $user->update([
                'password' => \Hash::make($request->password),
            ]);

        }
    }

    public function update_contact_profile($request, $company)
    {

        $company->contact->contact->update([

            'phone'  => $request->contact_phone,
            'ext'    => $request->contact_phone_ext,
            'mobile' => $request->contact_mobile,

        ]);
    }

    public function update_company_information($request, $company)
    {

        #se actualiza la informacion de la compañia
        $company->update([

            'name'           => $request->name,
            'address'        => $request->address,
            'city'           => $request->city,
            'state'          => $request->state,
            'phone'          => $request->phone,
            'sla_id'         => $request->sla_id or null,
            'coverage_id'    => $request->coverage_id or null,
            'urgent_tickets' => $request->urgent_tickets ? 1 : 0,

        ]);

        $this->set_company_rfc($request, $company);

        if ($request->has('contact_id')) {


            $contact = CompanyUser::find($request->contact_id)->user->id;


            $current_contact            = $company->contact;
            $zone_current_contact       = $company->contacts()->where('user_id', $current_contact->id)->first();
            

            if($zone_current_contact){


                $zone_current_contact->delete();


            }


            $zone_contact = $company->contacts()->where('user_id', $contact)->first();

            if ($zone_contact) {

                $zone_contact->role = 1;
                $zone_contact->save();

            } else {

                $add_contact_to_zone          = new ZoneUser();
                $add_contact_to_zone->user_id = $contact;
                $add_contact_to_zone->zone_id = $company->id;
                $add_contact_to_zone->role    = 1;
                $add_contact_to_zone->save();

            }

            $company->update([

                'contact_id' => $contact,

            ]);
        }

        $this->add_contract_to_company($request, $company);
    }

    public function destroy(Company $company)
    {

        $company->tickets()->delete();

        $company->delete();

        return redirect()->route('companies.index')->with("info", "La compañia {$company->name} ha sido eliminada");
    }

    public function validate_company_data($request, $company = null)
    {

        $company_id = $company ? $company->id : null;

        $company_name = $company ? $company->name : null;

        $contact = $company ? $company->contact->id : null;

        $validate = Validator::make($request->all(), [

            'password'           => 'sometimes|string|nullable',
            'name'               => ['required', 'string', Rule::unique('companies')->ignore($company_id ? $company_id : '')],
            'rfc'                => 'sometimes|string',
            'address'            => 'required|string',
            'city'               => 'required|string',
            'state'              => 'required|string',
            'zip'                => 'required|string|numeric',
            'phone'              => 'string|nullable',
            'contract_id'        => 'sometimes|nullable|exists:contracts,id',
            'contract_start_at'  => 'sometimes|date_format:m/d/Y',
            'sla_id'             => 'nullable|exists:sla,id',
            'coverage_id'        => 'nullable|exists:coverages,id',
            'email'              => ['sometimes', 'required', 'email', Rule::unique('users')->ignore($contact ? $contact : '')],
            'username'           => ['sometimes', 'required', 'string', 'min:6', 'max:15', 'alpha_num', Rule::unique('users')->ignore($contact ? $contact : '')],
            'jobtitle'           => 'sometimes|required|string',
            'contact_first_name' => 'sometimes|required|string',
            'contact_last_name'  => 'sometimes|required|string',
            'contact_phone'      => 'sometimes|string|nullable',
            'contact_phone_ext'  => 'sometimes|string|nullable',
            'contact_mobile'     => 'sometimes|string|nullable',
            'password'           => 'sometimes|string|nullable',
            'contact_id'         => 'sometimes|exists:users,id',

        ], [

            'password.required'             => 'La contraseña es requerida',
            'name.required'                 => 'El nombre de la compañia es requerido',
            'name.unique'                   => 'El nombre de la compañia no esta disponible',
            'rfc.required'                  => 'El RFC de la compañia es requerido',
            'address.required'              => 'El domicilio es requerido',
            'city.required'                 => 'La ciudad es requerida',
            'state.required'                => 'El estado es requerido',
            'zip.required'                  => 'El código postal es requerido',
            'zip.numeric'                   => 'El código postal debe ser numérico',
            'phone.required'                => 'El teléfono de la compañia es requerido',
            'contract_id.exists'            => 'El contrato seleccionado no es válido',
            'contract_start_at.date_format' => 'El formato en la fecha de inicio del contrato no es válido',
            'sla.exists'                    => 'El acuerdo de servicio seleccionado no es válido',
            'email.required'                => 'El e-mail del contacto técnico es requerido',
            'email.email'                   => 'El e-mail del contacto técnico no es válido',
            'email.unique'                  => 'El e-mail del contacto técnico no esta disponible',
            'jobtitle.required'             => 'El puesto laboral es requerido',
            'username.required'             => 'El nombre de usuario es requerido',
            'username.unique'               => 'El nombre de usuario no esta disponible',
            'username.min'                  => 'El nombre de usuario debe contener al menos 6 caracteres',
            'username.max'                  => 'El nombre de usuario debe contener como máximo 15 caracteres',
            'username.alpha_num'            => 'El nombre de usuario solo puede contener números y letras',
            'contact_first_name.required'   => 'El nombre del contacto principal es requerido',
            'contact_last_name'             => 'El apellido del contacto principal es requerido',
            'contact_password.sometimes'    => 'La contraseña es requerida',
            'contact_id.exists'             => 'El contacto seleccionado no existe',

        ])->validate();
    }
}
