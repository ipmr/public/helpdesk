<form action="{{ route('tickets.update', $ticket->ticket_id) }}" method="post" id="updateDepartmentForm" style="width: 100%;">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="form-group m-0">
        <select name="department" class="form-control" @change="active_success_btn" style="border-radius: 3px 0 0 3px" required>
            <option disabled selected value>Elige una opción</option>
            @foreach(App\Department::with('agents')->has('agents', '>', 0)->get() as $department)
            <option {{ $ticket->department_id == $department->id ? 'selected' : '' }} value="{{ $department->id }}">
                {{ $department->name }}
            </option>
            @endforeach
        </select>
    </div>
</form>


<button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Departamento" class="btn btn-success" form="updateDepartmentForm"><i class="fa fa-check" style="margin: 0 !important;"></i></button>