<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        Tipo de pregunta:
    </label>
    <div class="col-sm-6">
        <div class="d-flex align-items-center">
            <select name="department_id" class="form-control" required>
                <option disabled selected value>Elige un tipo de pregunta</option>
                @foreach( $question_types as $question_type )
                <option value="{{ $question_type->id }}" @isset ($question)
                    {{ ($question->department_id == $question_type->id)?'selected':'' }}
                @endisset>{{ $question_type->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-group row form-row mb-0">
    <label class="col-sm-4 col-form-label text-right">Pregunta:</label>
    <div class="col-sm-6">
        <textarea name="question" rows="2" class="form-control" required>{{ $question->question or old('question') }}</textarea>
    </div>
</div>