<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyUser;
use App\ContactCatalog;
use App\Notifications\NewCompanyUserAccount;
use App\Notifications\NewCompanyUserAssignation;
use App\Notifications\UpdateContactData;
use App\Notifications\TicketsSuspendedUser;
use App\User;
use App\ZoneUser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Notification;
use Validator;

class CompanyUserController extends Controller
{

    public function __construct()
    {

        $this->middleware('company_owner')->only(['index', 'show']);

    }

    public function index(Company $company)
    {

        $tab = 'team';

        return view('helpdesk.contacts.index', compact('company', 'tab'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request, Company $company)
    {

        #se valida la información del contacto
        $validate = $this->validate_contact_info($request);

        if ($request->has('contact_id')) {

            $user    = CompanyUser::find($request->contact_id)->user;
            $contact = CompanyUser::find($request->contact_id);

            $verify_if_contact_already_exists = $company->contacts()->where('user_id', $user->id)->first();

            if ($verify_if_contact_already_exists) {
                return redirect()->back()->with('warning', 'El contacto ya se encuentra asignado');
            }
        } else {
            #se crea la cuenta del contacto
            $user = $this->create_contact_account($request, $company);

            #se crea el perfil del contacto
            $contact = $this->create_contact_profile($request, $user, $company);

        }

        #se asigan al usuario a la zona
        $assign_contact_to_zone = $this->assign_contact_to_zone($request, $user, $company);

        #se retorna con mensaje de confirmación
        return redirect()->back()->with('msg', 'Se ha creado el contacto exitosamente');

    }

    public function validate_contact_info($request, $user = null)
    {

        $validations = [
            'type'               => 'sometimes|required|exists:company_user_types,id',
            'contact_first_name' => 'sometimes|required|string',
            'contact_last_name'  => 'sometimes|required|string',
            'email'              => [
                'sometimes',
                'required',
                'email',
                Rule::unique('users')->ignore($user ? $user->id : ''),
            ],
            'jobtitle'           => 'sometimes|required|string',
            'contact_phone'      => 'sometimes|string|nullable',
            'contact_phone_ext'  => 'sometimes|string|nullable',
            'contact_mobile'     => 'sometimes|string|nullable',
            'contact_id'         => 'sometimes|exists:company_users,id',
        ];

        if ($request->password) {
            $validations['password'] = 'required|string';
        }

        $validate_msgs = [
            'type.required'               => 'El tipo de contacto es requerido',
            'type.exists'                 => 'El tipo de contacto no existe en la base de datos',
            'username.required'           => 'El nombre de usuario es requerido',
            'username.unique'             => 'El nombre de usuario no esta disponible',
            'username.min'                => 'El nombre de usuario debe tener al menos 6 caracteres',
            'username.max'                => 'El nombre de usuario debe tener como maximo 15 caracteres',
            'username.alpha_num'          => 'El nombre de usuario solo puede contener números y letras',
            'contact_first_name.required' => 'El nombre del contacto es requerido',
            'contact_last_name.required'  => 'El apellido del contacto es requerido',
            'email.required'              => 'El e-mail del contacto es requerido',
            'email.email'                 => 'El e-mail del contacto no es válido',
            'email.unique'                => 'El e-mail del contacto no esta disponible',
            'jobtitle'                    => 'El puesto laboral es requerido',
            'password'                    => 'La contraseña es requerida',
            'contact_id'                  => 'El contacto seleccionado no es válido',
        ];

        $validate = Validator::make($request->all(), $validations, $validate_msgs)->validate();

    }

    public function create_contact_account($request, $company)
    {

        $user             = new User();
        $user->username   = $request->username;
        $user->first_name = $request->contact_first_name;
        $user->last_name  = $request->contact_last_name;
        $user->email      = $request->email;
        $user->username   = uniqid();
        $user->password   = \Hash::make($request->password);
        $user->role       = 'company';

        $user->save();

        $notification_data = [

            'password' => $request->password,
            'name'     => $user->full_name,
            'email'    => $user->email,
            'username' => $user->username,

        ];

        $user->notify(new NewCompanyUserAccount($notification_data));

        return $user;

    }

    public function create_contact_profile($request, $user, $company)
    {

        $profile           = new CompanyUser();
        $profile->user_id  = $user->id;
        $profile->team_id  = $company->all_parents()->reverse()->first()->id;
        $profile->phone    = $request->contact_phone;
        $profile->jobtitle = $request->jobtitle;
        $profile->ext      = $request->contact_phone_ext;
        $profile->mobile   = $request->contact_mobile;

        $profile->save();

    }

    public function assign_contact_to_zone($request, $user, $company)
    {

        $company_catalog = $company->master_parent->contacts_catalog()->pluck('user_id')->toArray();

        $contact_to_zone          = new ZoneUser();
        $contact_to_zone->user_id = $user->id;
        $contact_to_zone->zone_id = $company->id;
        $contact_to_zone->role    = $request->type;

        $contact_to_zone->save();

        if (!in_array($user->id, $company_catalog)) {

            $contact_catalog                 = new ContactCatalog();
            $contact_catalog->user_id        = $user->id;
            $contact_catalog->master_zone_id = $company->master_parent->id;
            $contact_catalog->save();

        }

        $notification_data = [
            'company' => $company,
            'user'    => $user,
        ];

        Notification::send($user, new NewCompanyUserAssignation($notification_data));

        return $contact_to_zone;

    }

    public function show(Company $company, CompanyUser $contact)
    {

        $tab = 'team';

        $user = $contact->user;

        return view('helpdesk.contacts.show', compact('company', 'user', 'tab'));

    }

    public function edit(CompanyUser $contact)
    {
        //
    }

    public function update(Request $request, Company $company, $contact)
    {

        $contact = CompanyUser::find($contact);

        $user = $contact->user;

        #se valida la informacion del contacto
        $validate = $this->validate_contact_info($request, $user);

        $user->update([
            'first_name' => $request->contact_first_name,
            'last_name'  => $request->contact_last_name,
            'email'      => $request->email,
        ]);

        if ($request->password) {

            $user->update([
                'password' => \Hash::make($request->password),
            ]);

        }

        $contact->update([
            'phone'    => $request->contact_phone,
            'jobtitle' => $request->jobtitle,
            'ext'      => $request->contact_phone_ext,
            'mobile'   => $request->contact_mobile,
        ]);

        $notification_data = [
            'company' => $company,
            'contact' => $contact,
        ];

        Notification::send($user, new UpdateContactData($notification_data));

        #si se intenta modificar el tipo de contacto en una zona
        if ($request->has('type')) {

            $contact_zone       = $company->contacts()->where('user_id', $user->id)->first();
            $contact_zone->role = $request->type;
            $contact_zone->save();

        }

        return redirect()->back()->with('msg', 'La información del contacto ha sido actualizada.');

    }

    public function destroy(Request $request, Company $company, $contact)
    {
        $contact = User::where('id', $contact)->firstOrFail();

        if ($company->contact_id == $contact->id) {

            if ($contact->id == $request->contact_id) {
                return redirect()
                    ->back()
                    ->withErrors(['El usuario que seleccionaste es el que sera suspendido, intenta nuevamente']);
            }

            $company->contact_id = $request->contact_id;

            $company->save();
        }

        $status = \App\Status::where('name', 'Cerrado')->first();

        $tickets = optional($contact)->tickets_contact()->whereNotIn('status_id', [$status->id])->get();

        if ($tickets->count() > 0) {
            foreach ($tickets as $ticket) {
                $ticket->contact_id = optional($company->contact)->id;

                $ticket->save();

                $history = new \App\History();

                $history->ticket_id   = $ticket->ticket_id;
                $history->description = 'Contacto suspendido, '.optional($contact->user)->full_name.' se asigna a contacto '.optional($company->contact)->full_name;

                $history->save();
            }

            $notification_data = [
                'title'   => 'Tickets de Contacto Suspendido',
                'tickets' => $tickets,
            ];

            // enviar correos de asignaci'on de tickets
            // registrar movimiento en log de cada ticket
            Notification::send($company->contact, new TicketsSuspendedUser($notification_data));
        }


        $msg = 'El contacto '.$contact->full_name.' fue suspendido correctamente';

        $contact->delete();

        return redirect()
            ->route('companies.contact-catalogs.index', $company)
            ->with( 'msg', $msg );
    }

    public function get_zone_contacts(Company $zone)
    {

        $response = [
            'contacts'       => $zone
                ->contacts()
                ->with('contact', 'company_user_type', 'contact.user')
                ->get(),
            'models'         => $zone->gears_models,
            'urgent_tickets' => $zone->urgent_tickets,
        ];

        if (request()->ajax()) {

            return response()->json($response);

        }

    }
}
