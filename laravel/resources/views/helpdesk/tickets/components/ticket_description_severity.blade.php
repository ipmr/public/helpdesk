<form action="{{ route('tickets.update', $ticket->ticket_id) }}" id="updateSeverityForm" method="post" style="width: 100%;">
    
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    
    <div class="form-group m-0">
        <select name="severity_id" class="form-control" @change="active_success_btn" style="border-radius: 3px 0 0 3px">
            @if($ticket->company->urgent_tickets)
                @foreach(App\Severity::get() as $severity)
                <option {{ $ticket->severity_id == $severity->id ? 'selected' : '' }} value="{{ $severity->id }}">
                    {{ $severity->name }}
                </option>
                @endforeach
            @else
                @foreach(App\Severity::where('name', '<>', 'Urgente')->get() as $severity)
                <option {{ $ticket->severity_id == $severity->id ? 'selected' : '' }} value="{{ $severity->id }}">
                    {{ $severity->name }}
                </option>
                @endforeach
            @endif
        </select>
    </div>  
</form>

<button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Severidad" form="updateSeverityForm" class="btn btn-success"><i class="fa fa-check" style="margin: 0 !important;"></i></button>