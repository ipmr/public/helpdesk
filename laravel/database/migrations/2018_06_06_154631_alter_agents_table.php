<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->string('cellphone')->nullable()->after('active');
            $table->string('short_number')->nullable()->after('cellphone');
            $table->string('extension')->nullable()->after('short_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn('cellphone');
            $table->dropColumn('short_number');
            $table->dropColumn('extension');
        });
    }
}
