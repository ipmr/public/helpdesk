{{-- Codigo Anterior --}}

    {{-- @component('helpdesk.components.modal', [
        'id'    => 'chose_contact',
        'title' => 'Asignar Contacto Principal'
    ])
        @slot('body')
            
            @isset ($vue)
                <form :action="url_new_contact_principal" method="post" id="assign_new_contact_to_company">
            @else
                <form action="{{ route('zoneusers.update', ['zoneuser' => $zone_user->id]) }}" method="post" id="assign_new_contact_to_company">
            @endisset
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group mb-0">                   
                    <label for="">Contacto:</label>
                    <select name="new_contact_id" class="form-control" required>
                        <option value="">Seleccionar un Contacto</option>
                        @foreach ($company->contacts as $contact)
                            @if ($company->contact_id != $contact->user_id)
                                <option value="{{ $contact->user_id }}">{{ $contact->user->full_name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </form>
        @endslot
        @slot('footer')
            <button type="submit" form="assign_new_contact_to_company" class="btn btn-success">
                <i class="fa fa-check"></i>
                Asignar Contacto
            </button>
        @endslot
    @endcomponent --}}

{{-- Codigo Anterior --}}



@component('helpdesk.components.modal', [

    'id' => 'choose_contact',
    'title' => 'Asignar un nuevo contacto principal'

])


    @slot('body')
    
        <form :action="relief_contact_form_route" id="relief_contact_form" method="POST">
            
            {{ method_field('DELETE') }}

            {{ csrf_field() }}

            <div class="form-group">
                
            
                <p>
                    
                    Estas intentando desasignar a un contacto principal,
                    por lo que es necesario seleccionar a un nuevo contacto
                    que tome su lugar.

                </p>


                <select name="contact_id" class="form-control" @change="choose_relief_contact" required>
                    
                
                    <option disabled selected value="">Elige una opción</option>


                    @foreach($company->master_parent->contacts_catalog as $contact)

                        <option value="{{ optional($contact->contact)->id }}">{{ $contact->full_name }}</option>

                    @endforeach

                </select>

            </div>


        </form>

    @endslot

    @slot('footer')

        
        <a href="#" class="btn btn-light" data-dismiss="modal" aria-label="Close" @click.prevent="relief_contact = null">
    
            <i class="fa fa-times"></i>

            Cancelar

        </a>

    
        <button type="submit" form="relief_contact_form" class="btn btn-success" id="assign_relief_contact">
            
            <i class="fa fa-check"></i>

            Asignar Contacto

        </button>

    @endslot


@endcomponent