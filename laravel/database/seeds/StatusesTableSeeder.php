<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Status::class, 1)->create([
            'name'        => 'Abierto',
            'description' => 'Tu ticket esta siendo procesado',
            'color'       => '#2a70d8',
            'static'      => 1,
        ]);

        factory(Status::class, 1)->create([
            'name'        => 'Resuelto',
            'description' => 'Tu ticket ha sido resuelto',
            'color'       => '#23b919',
            'static'      => 1,
        ]);

        factory(Status::class, 1)->create([
            'name'        => 'En proceso',
            'description' => 'Tu ticket esta siendo procesado',
            'color'       => '#233b6f',
            'static'      => 1,
        ]);

        factory(Status::class, 1)->create([
            'name'        => 'En espera del cliente',
            'description' => 'Estamos esperando tu respuesta',
            'color'       => '#ef8012',
            'static'      => 1,
        ]);

        factory(Status::class, 1)->create([
            'name'        => 'Monitoreo',
            'description' => 'Se esta validando la efectividad del servicio',
            'color'       => '#ff593f',
            'static'      => 1,
        ]);

        factory(Status::class, 1)->create([
            'name'        => 'Cerrado',
            'description' => 'Tu ticket se ha cerrado',
            'color'       => '#d82a2a',
            'static'      => 1,
        ]);
    }
}
