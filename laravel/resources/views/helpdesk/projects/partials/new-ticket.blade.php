@component('helpdesk.components.modal', [

	'id' => 'newTicketModal',
	'title' => 'Nuevo Ticket',
	'size' => 'modal-lg'

])

		
	@slot('body')

	
		<form action="{{ route('new-project-ticket', [$company, $project->fup]) }}/" method="post" id="newTicketForm" @submit.prevent="new_ticket">
			
			@include('helpdesk.projects.partials.ticket-form')

		</form>


	@endslot


	@slot('footer')
	
		<button type="submit" form="newTicketForm" class="btn btn-success">
			
			<i class="fa fa-check"></i> Enviar Ticket

		</button>

	@endslot


@endcomponent