new_ticket: function(e){

	var t 		= this;
	var form 	= $(e.target);
	var form_id = form.attr('id');
	var btn 	= $('button[form="'+form_id+'"]');
	var data 	= form.serialize();
	var url 	= form.attr('action');

	$.post(url, data, function(response){
		
		btn.removeAttr('disabled');

	}).done(function(response){

		form[0].reset();

		t.tickets 					= response.tickets;
		t.newticket.categories.b 	= [];
		t.newticket.categories.c 	= [];

		$('.modal').modal('hide');

		t.show_notification(response.msg);

	});
},

get_department_agents: function(e){
	
	var t 		= this;
	var select 	= $(e.target);
	var value 	= select.val();
	var url 	= "{{ route('get_department_agents', '#department_id') }}";
	url 		= url.replace('#department_id', value);

	t.newticket.department.id 		= value;
	t.newticket.department.agents 	= null;

	$.getJSON(url, function(response){

		t.newticket.department.agents = response;

	});

},

get_sub_categories: function(level){

	var t 			= this;
	var select 		= $(event.target);
	var category_id = select.val();
	var url 		= "{{ route('category.subcategory.index', '#category_id') }}";
	url 			= url.replace("#category_id", category_id);

	$.getJSON(url, function(response){

		var subcategories_b = $('[name="subcategory_b"]');
		var subcategories_c = $('[name="subcategory_c"]');

		if(level == 2){

			t.newticket.categories.b = response;
			t.newticket.categories.c = [];

			subcategories_b.prop('selectedIndex',0);
			subcategories_c.prop('selectedIndex',0);

		}

		if(level == 3){

			t.newticket.categories.c = response;
			
			subcategories_c.prop('selectedIndex',0);

		}

	});

}
