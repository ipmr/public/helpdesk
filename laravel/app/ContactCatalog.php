<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactCatalog extends Model
{
    public function contact()
    {

        return $this->hasOne('App\User', 'id', 'user_id');

    }

    public function company()
    {

        return $this->hasOne('App\Company', 'id', 'master_zone_id');

    }


    public function getEmailAttribute(){

    	return optional($this->contact)->email;

    }

    public function getFullNameAttribute(){

    	return optional($this->contact)->full_name;

    }

    public function getZonesAttribute(){


    	return optional($this->contact)->zones;

    }

}
