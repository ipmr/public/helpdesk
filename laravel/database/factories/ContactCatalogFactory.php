<?php

use Faker\Generator as Faker;

$factory->define(App\ContactCatalog::class, function (Faker $faker) {
    return [
        'user_id'        => $faker->name,
        'master_zone_id' => $faker->name,
    ];
});
