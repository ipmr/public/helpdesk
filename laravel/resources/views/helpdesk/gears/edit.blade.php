@extends('helpdesk.companies.layouts.tabs')

@section('tab_content')

	<div class="card-body" id="gears_app">
		<div class="form-row">
			<div class="col-sm-8 ml-auto">
				<div class="mb-3">
					<a class="btn btn-link p-0" href="{{ route('companies.gears.show', [$company, $gear]) }}/">
						<i class="fa fa-angle-left"></i>
						Regresar
					</a>
				</div>
			</div>
		</div>
		<form v-cloak action="{{ route('companies.gears.update', [$company, $gear]) }}" method="post">
			{{ method_field('PATCH') }}
			@include('helpdesk.gears.partials.form', [
				'btn' => 'Actualizar equipo'
			])
		</form>
	</div>
@stop

@section('scripts')
	
	@include('helpdesk.gears.partials.scripts')

@stop