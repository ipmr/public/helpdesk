<?php

use Faker\Generator as Faker;

$factory->define(App\ZoneUser::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'zone_id' => 1,
        'role'    => 1,
    ];
});
