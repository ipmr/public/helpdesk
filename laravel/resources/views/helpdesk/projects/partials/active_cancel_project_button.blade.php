@role('admin', 'manager')    
    @if ($project->condition)
        <form action="{{ route('cancel_project', $project->fup) }}" method="post" enctype="multipart/form-data">
    @else
        <form action="{{ route('active_project', $project->fup) }}" method="post" enctype="multipart/form-data">
    @endif
        {{ csrf_field() }}
        @if ($project->condition)
            <button type="submit" class="btn btn-outline-danger" onclick="return confirm('¿Estás seguro que deseas cancelar el proyecto {{ $project->title }}?')">
                Cancelar proyecto
            </button>
        @else
            <button type="submit" class="btn btn-outline-success">
                Activar proyecto
            </button>
        @endif
    </form>
@endrole