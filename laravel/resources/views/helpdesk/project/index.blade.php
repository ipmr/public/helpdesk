@extends('layouts.master')
@section('content')
<div class="container-fluid" id="app_vue">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">Lista de proyectos</p>
            <a href="{{ route('projects.create') }}/" class="btn btn-primary ml-auto">
                <i class="fa fa-plus"></i> Nuevo Proyecto
            </a>
        </div>
        <div class="accordion" id="projectsCollapse">
            @foreach($companies as $key => $company)
                <div class="card no_shadow b-radius-0">
                    <div class="card-header {{ $loop->first ? 'bg-secondary' : '' }} d-flex align-items-center" id="heading_{{ $key }}">
                        
                        <a href="#" class="text-dark p-0 project_header {{ $loop->first ? '' : 'collapsed' }}"  @click.prevent="scrollToHeader" data-toggle="collapse" data-target="#collapse_{{ $key }}" aria-expanded="{{ $loop->first ? 'true' : 'false' }}" aria-controls="collapse_{{ $key }}">
                            {{ $company->name }}
                        </a>

                        <div class="ml-auto">
                            <span class="badge badge-pill badge-success">({{ $company->active_projects->count() }}) Activos</span>
                            @if($company->cancelled_projects->count() > 0)
                            <span class="badge badge-pill badge-danger">({{ $company->cancelled_projects->count() }}) Cancelados</span>
                            @endif
                        </div>

                    </div>
                    <div id="collapse_{{ $key }}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading_{{ $key }}" data-parent="#projectsCollapse">
                        
                        @include('helpdesk.project.partials.project_table', ['company_projects' => $company->projects])

                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @include('helpdesk.project.html.modal_status_data')
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var app_vue = new Vue({
        el: '#app_vue',
        data: {
            histories : [],
            status    : [
                '_',
                'Inicio',
                'Cotización',
                'Producción',
                'Documentación',
                'Facturación',
            ],
            condition : [
                'Cancelado',
                'Activo',
            ],
        },
        mounted(){

        },
        methods: {
            open_modal: function(status, fup){

                $('#modal_status_data').modal('show');
                this.get_project_details(status,fup);

            },

            get_project_details: function(status, fup)
            {
                var t = this;
                var url = '{{ route('project_details') }}';
                var data = {
                    status: status,
                    fup : fup,
                };

                t.histories = []

                $.get(url,data).done(function(response){
                    t.histories = response.histories
                });
            },

            scrollToHeader: function(e){

                var t = this;

                var link = $(e.target);

                var header = link.closest('.card-header');

                header.addClass('bg-secondary')
                    .parent()
                    .siblings()
                    .find('.card-header')
                    .removeClass('bg-secondary')
                    .addClass('bg-white');

                setTimeout(function(){

                    $('html, body').stop().animate({scrollTop: header.offset().top - 60});

                }, 350);

            }
        }
    })
</script>
@endsection