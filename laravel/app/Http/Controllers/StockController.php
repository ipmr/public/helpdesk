<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class StockController extends Controller
{
    public function __construct(){

        // $this->middleware('under-construction');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Inventory $inventory)
    {

        if(request()->ajax()){

            $stocks = $inventory->stocks()
                    ->where('assigned',0)
                    ->with(['gear' => function($q){
                        $q->where('active', 1);
                    },'gear.brand', 'gear.model'])
                ->get();

            return response()->json($stocks);

        }else{
            
            $stocks = $inventory->stocks()->paginate(12);

            return view('helpdesk.stock.index', compact('stocks', 'inventory'));
            
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Inventory $inventory)
    {
        return view('helpdesk.stock.create', compact('inventory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Inventory $inventory)
    {
        $this->validate_gear_data($request);

        $request->request->add(['company_id'=> 0]);

        $gear = app('App\Http\Controllers\GearController')->create_company_gear($request, null);

        $stock = new Stock();

        $stock->gear_id      = $gear->id;
        $stock->inventory_id = $inventory->id;
        $stock->assigned     = 0;

        $stock->save();

        return redirect()
                ->route('inventories.stock.index', $inventory)
            ->with('msg', 'El equipo fue agregado correctamente al stock del almacen "'.$inventory->name.'"');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory,Stock $stock)
    {
        return view('helpdesk.stock.show', compact('inventory', 'stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory,Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Inventory $inventory, Stock $stock)
    {
        $gear = $stock->gear;
        $this->validate_gear_data($request, $gear);

        if (!empty($request->zone_id)) {
            $request->request->add(['company_id'=> $request->zone_id]);
        }

        if (empty($request->company_id)) {
            $request->request->add(['company_id'=> 0]);
            $stock->assigned = 0;
        } else {
            $stock->assigned = 1;
        }
        
        $stock->save();

        app('App\Http\Controllers\GearController')->update_gear_data($request, $gear);

        return redirect()
                ->route('inventories.stock.index', $inventory)
            ->with('msg', 'El equipo fue actualizado correctamente');
    }

    public function assign_gear_to_company(Request $request,Inventory $inventory, Stock $stock)
    {
        $msg  = '';
        $gear = $stock->gear;

        if (!empty($request->zone_id)) {
            $request->request->add(['company_id'=> $request->zone_id]);
        }

        $validate = Validator::make($request->all(), [
            'company_id'        => 'nullable|exists:companies,id',
        ], [
            'company_id.exists'    => 'La compañia no existe en la base de datos',
        ])->validate();

        if ( !empty( $request->company_id ) ) {
            $stock->assigned = 1;
            $gear->company_id = $request->company_id;
            $msg = 'El equipo "'.$gear->serial_number.'" fue  asignado a una compañia';
        } else {
            $stock->assigned = 0;
            $gear->company_id = 0;

            $msg = 'El equipo "'.$gear->serial_number.'" fue  desasignado de la compañia';
        }

        $stock->save();
        $gear->save();

        return redirect()
                ->route('inventories.stock.index', $inventory)
            ->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }

    public function validate_gear_data($request, $gear = null)
    {

        $gear_id = $gear ? $gear->id : null;

        $validate = Validator::make($request->all(), [
            'type_id'           => 'required|exists:gear_types,id',
            'brand_id'          => 'required|exists:gear_brands,id',
            'model_id'          => 'required|exists:gear_models,id',
            'serial_number'     => ['sometimes', 'string', Rule::unique('gears')->ignore($gear_id ? $gear_id : '')],
            'status'            => 'required|in:Funciona,No Funciona,Tiene Fallas',
            'description'       => 'nullable|string',
            'comments'          => 'nullable|string',
            'company_id'        => 'nullable|exists:companies,id',
        ], [
            'type_id.required'     => 'El tipo del equipo es requerido',
            'type_id.exists'       => 'El tipo del equipo no es válido',
            'brand_id.required'    => 'La marca del equipo es requerida',
            'brand_id.exists'      => 'La marca del equipo no es válida',
            'model_id.required'    => 'El modelo del equipo es requerido',
            'model_id.exists'      => 'El modelo del equipo no es válido',
            'serial_number.unique' => 'El número de serie ya se encuentra registrado',
            'serial_number.string' => 'El número de serie debe ser una cadena de texto',
            'status.required'      => 'El estado del equipo es requerido',
            'status.in'            => 'El estado del equipo no es válido',
            'description.string'   => 'La descripción debe ser una cadena de texto',
            'comments.string'      => 'Los comentarios deben ser una cadena de texto',
            'company_id.exists'    => 'La compañia no existe en la base de datos',
        ])->validate();
    }
}
