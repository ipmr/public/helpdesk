<div class="card mt-3" v-if="quotation && quotation.accepted == 1 && project.condition">
	<div class="card-header d-flex align-items-center">
		<p class="lead m-0">Tickets de proyecto</p>
		<a href="#" class="btn btn-primary ml-auto" data-toggle="modal" data-target="#newTicketModal">
			<i class="fa fa-plus"></i> Nuevo Ticket
		</a>
	</div>
	<table v-if="tickets" class="table table-striped m-0">
		<thead>
			<tr>
				<th>#Ticket</th>
				<th>Asunto</th>
				<th>Estado</th>
			</tr>
		</thead>
		<tbody>
			<tr v-for="item in tickets">
				<td>
					
					<a :href="ticket_route(item.ticket.ticket_id)" target="_blank">
						#@{{ item.ticket.ticket_id }}
					</a>

				</td>
				<td>@{{ item.ticket.subject }}</td>
				<td>@{{ item.status.name }}</td>
			</tr>
		</tbody>
	</table>

	<div v-else class="card-body">
		No se han agregado tickets al proyecto.
	</div>

</div>