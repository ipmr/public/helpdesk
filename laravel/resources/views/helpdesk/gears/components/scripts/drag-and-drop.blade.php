drag_drop_gear: function(){

	var t = this;

	function reset_position(draggable){

		draggable.css({

			left: 0,
			top: 0

		});

	}

	$( ".draggable_tr" ).draggable({
		revert: 'invalid'
	});


	$( ".droppable_tr" ).droppable({
		accept: ".draggable_tr",
		classes: {
			"ui-droppable-active": "bg-drop-active",
			"ui-droppable-hover": "bg-drop-hover"
		},
		drop: function( event, ui ) {

			var droppable = $(this);
			var draggable = ui.draggable;

			var draggable_gear = draggable.data('gear');
			var droppable_gear = droppable.data('gear');

			if(draggable_gear != droppable_gear){

				
				var question = 'Estas intentando insertar el módulo "'+draggable_gear+'" en el equipo "'+droppable_gear+'", por favor define el slot que deseas utilizar.'

				var ask = prompt(question);

				var slot = ask;

				if(slot){


					var url = "{{ route('insert_module', ['#module_serial_number', '#gear_serial_number']) }}";


					url = url.replace('#module_serial_number', draggable_gear)
							 .replace('#gear_serial_number', droppable_gear);


					var data = {


						_token: "{{ csrf_token() }}",
						slot: slot


					}


					t.$http.post(url, data).then(function(response){


						if(response.body.error){

							
							alert(response.body.error);


							reset_position(draggable);



						}else{

							location.reload();
							
						}



					});


				}else{

					reset_position(draggable);

				}


			}else{


				reset_position(draggable);


			}

			
		}
	});

},

remove_module_form: function(e){


	var form = $(e.target);

	var btn = $("button", form);

	var question = confirm('¿Estas seguro que deseas retirar este equipo?');

	if(question){


		form.submit();


	}else{


		setTimeout(function(){

			btn.removeAttr('disabled');

		}, 150);


	}


}