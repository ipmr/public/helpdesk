<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function agent()
    {

        return $this->hasOne('App\Agent', 'user_id', 'id');

    }

    public function zones()
    {

        return $this->hasMany('App\ZoneUser', 'user_id', 'id');

    }

    public function contact()
    {

        return $this->hasOne('App\CompanyUser', 'user_id', 'id');

    }

    public function getFullNameAttribute()
    {

        return "{$this->first_name} {$this->last_name}";

    }

    public function companies()
    {

        #coleccion de compañias
        $companies = collect([]);
        
        #se verifica que una compañia se agregue solo una vez
        $zones = collect([]);

        $this->zones->each(function($zone) use ($companies, $zones) {
            collect($zone->company->all_childs())->each(function($zone) use ($companies, $zones) {

                if(!in_array($zone->id, $zones->toArray())){
                    
                    $zones->push($zone->id);
                    $companies->push($zone);

                }

            });
        });

        return $companies;
    }

    public function getAgentIsSupervisorAttribute()
    {
        $result = false;

        if ( isset( $this->agent ) ) {
            
            if ( optional( $this->agent->department )->supervisor_id == $this->agent->id ) {
                
                $result = $this->agent->department;

            }

        }

        return $result;
    }

    public function getAgentAsManagerAttribute()
    {
        $result = false;

        if ( isset( $this->agent ) ) {
            if ( $this->agent->as_manager ) {
                $result = true;
            }
        }

        return $result;
    }

    public function getAgentsInGroupAttribute()
    {   
        $agents_in_groups = false;

        if ( isset( $this->agent ) ) {
            $groups = $this->agent->supervisor_in_groups;

            if($groups->count() > 0){

                $agents_in_groups = [];

                foreach($groups as $group){
                    
                    if ($agents = $group->agents()->count() > 0) {

                        $agents = $group->agents()->with('agent')->get()->pluck('agent.id');

                        foreach($agents as $agent){

                            if( ! in_array($agent, $agents_in_groups)){

                                array_push($agents_in_groups, $agent); 

                            }
                        }
                    }
                }
            }
        }

        return $agents_in_groups;
    }

    public function getZonesFromContactTypeAttribute()
    {
        $ids = false;
        if ( isset( $this->contact ) ) {

            foreach ($this->zones as $zone) {

                $ids[$zone->company->id] = $zone->company->id;
                #se obtendran las companias de las zonas
                if (optional($zone->company)->id) {
                    #se obtendran los hijos de la compania si son de contacto principal
                    if ($zone->role == 'Contacto Principal') {

                        $company_childs = $zone->company->all_childs();

                    } else {

                        $company_childs = $zone->company->childs;

                    }

                    if (count($company_childs) > 0) {

                        foreach ($company_childs as $company_child) {

                            $ids[$company_child->id] = $company_child->id;
                        }

                    }
                }
            }
        }

        return $ids;
    }

    public function tickets_contact()
    {
        return $this->hasMany('App\Ticket', 'contact_id', 'id');
    }
}
