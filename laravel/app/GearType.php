<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GearType extends Model
{

		protected $table   = 'gear_types';
	    public $timestamps = true;
	    protected $guarded = [];

    public function brands(){

    	return $this->hasMany('App\GearBrand', 'type_id', 'id');

    }
}
