<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandType extends Model
{
    
	public function type(){

		return $this->belongsTo('App\GearType','type_id','id');

	}

	public function brand(){

		return $this->belongsTo('App\GearBrand','brand_id','id');

	}

	public function models(){

		return $this->hasMany('App\GearModel','brand_type_id','id');

	}

	public function getTypeNameAttribute(){

		return $this->type->name;

	}

}
