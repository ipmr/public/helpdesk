<tr v-if="step >= 2">
	<th>Cotización</th>
	<td>

		<a v-if="! quotation" href="#" class="btn btn-link p-0" data-toggle="modal" data-target="#generateQuotationModal">

			Agregar Cotización

		</a>

		<div v-else>
			
			<div v-if="quotation.accepted == 1">
				
				( <i class="fa fa-check fa-sm text-success"></i> )
				La cotización ha sido validada

				[ <a href="#">Ver Cotización</a> ]

			</div>
			
			<div v-else>
				
				Se realizó la cotización 
				pero no ha sido validada 
				[ <a href="#" data-toggle="modal" data-target="#validateQuotationModal">
					Validar Ahora
				</a> ]

			</div>

		</div>
	</td>
</tr>