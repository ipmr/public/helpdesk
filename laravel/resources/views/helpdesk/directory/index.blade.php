@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Directorio
            </p>
            <form action="{{ route('directory.index') }}" method="get" enctype="multipart/form-data" class="ml-auto form-inline">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
        </div>
        @if ($users->count() > 0)
            <table class="table table-striped mb-0">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Usuario</th>
                        <th>Telefono</th>
                        <th>Num Corto</th>
                        <th>Extensión</th>
                        <th>Puesto</th>
                        <th>Departamento</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td nowrap>
                            <a href="{{ route('agents.show', ['agent' => $user->agent_id]) }}">
                                {{ $user->full_name }}
                            </a>
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            {{ $user->username }}
                        </td>
                        <td>
                            {{ $user->cellphone or 'Sin Celular' }}
                        </td>
                        <td>
                            {{ $user->short_number or 'Sin Número Corto' }}
                        </td>
                        <td>
                            {{ $user->extension or 'Sin Extensión' }}
                        </td>
                        <td>
                            {{ $user->jobtitle or 'Sin Puesto' }}
                        </td>
                        <td>
                            @if ($user->department_id)
                                <a href="{{ route('departments.show', ['department' => $user->department_id ]) }}">
                                    {{ $user->name }}
                                </a>
                            @else
                            Sin Departamento
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-footer">
                {{ $users->links() }}
            </div>
        @else
            <div class="card-body">
                No existen registros
            </div>
        @endif
    </div>
</div>
@stop
