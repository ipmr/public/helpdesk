<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentGroup extends Model
{
    

	public function department(){


		return $this->belongsTo('App\Department', 'department_id', 'id');


	}


	public function agents(){

		return $this->hasMany('App\DepartmentGroupAgent', 'group_id', 'id');

	}


	public function supervisor(){


		return $this->hasOne('App\Agent', 'id', 'supervisor_id');


	}


}
