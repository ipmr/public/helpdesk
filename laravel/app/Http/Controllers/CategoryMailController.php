<?php

namespace App\Http\Controllers;

use App\CategoryMail;
use Illuminate\Http\Request;

class CategoryMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryMail  $categoryMail
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryMail $categoryMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryMail  $categoryMail
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryMail $categoryMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryMail  $categoryMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryMail $categoryMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryMail  $categoryMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryMail $categoryMail)
    {
        //
    }
}
