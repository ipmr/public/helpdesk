<?php

namespace App\Http\Controllers;

use App\GearBrand;
use App\GearType;
use Illuminate\Http\Request;
use Validator;

class GearBrandController extends Controller
{

    public function index(Request $request)
    {

        $brands = GearBrand::where('id', '>', 0);

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $brands = $brands->where(function ($q) use ($query_string, $request) {
                $q = $q->where('name', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('name', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $brands->toSql() );
            }

            if ($request->debug == 2) {
                dd( $brands->get() );
            }
        }
        
        $brands = $brands->orderBy('id', 'DESC')->get();

        return view('helpdesk.gears.brands.index', compact('brands'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $brands = collect(explode(',', $request->brands_list));

        $brands->each(function($item) {

            $brand_exists = GearBrand::where('name', $item)->first();

            if(!$brand_exists){
                
                $brand = new GearBrand();
                $brand->name = $item;
                $brand->save();

            }

        });

        return redirect()->back()->with('msg', 'Las marcas agregadas se guardaron exitosamente.');

    }

    public function show(Request $request, GearBrand $brand)
    {    

        $types = \App\BrandType::select('*')
                ->join('gear_brands', 'brand_types.brand_id', 'gear_brands.id')
                ->join('gear_types', 'brand_types.type_id', 'gear_types.id')
                ->where('brand_types.brand_id', $brand->id );

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $types = $types->where(function ($q) use ($query_string, $request) {
                $q = $q->where('gear_types.name', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->where('gear_types.name', 'like', '%'.$request->q.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $types->toSql() );
            }

            if ($request->debug == 2) {
                dd( $types->get() );
            }
        }

        $types = $types->get();
        
        return view('helpdesk.gears.brands.show', compact('brand', 'types'));

    }

    public function edit(GearBrand $brand)
    {
        //
    }

    public function update(Request $request, GearBrand $brand)
    {

        $this->validate_brand_information($request);

        $old_name = $brand->name;

        $brand->name = strtoupper($request->name);
        $brand->save();


        return redirect()->back()->with("msg", "Se modificó el nombre de la marca '{$old_name}' a '{$brand->name}' exitosamente");

    }

    public function destroy(GearBrand $brand)
    {
        //
    }

    public function validate_brand_information($request){

        $validate = Validator::make($request->all(), [

            'name' => 'required|unique:gear_brands'

        ], [

            'name.required' => 'El nombre de la marca es requerido.',
            'name.unique' => 'El nombre de la marca ya se encuentra en uso.'

        ])->validate();

    }
}
