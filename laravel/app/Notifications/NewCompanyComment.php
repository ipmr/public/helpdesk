<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewCompanyComment extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->ticket = $request['ticket'];
        $this->comment = $request['comment'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /*return (new MailMessage)
                    ->subject('Nuevo Comentario')
                    ->greeting( optional($this->ticket->agent)->user->FullName )
                    ->line('El usuario '. optional($this->comment->user)->FullName .' ha realizado un nuevo comentario en el ticket #' . $this->ticket->ticket_id)
                    ->action('Atender ticket', route('tickets.show', $this->ticket->ticket_id));*/
        return (new MailMessage)
                    ->subject('#'.$this->ticket->ticket_id.' - '.$this->ticket->subject)
                    ->line("#{$this->ticket->ticket_id} - {$this->ticket->subject}")
                    ->line('"'.$this->comment->comment.'"')
                    ->action('Ver evento', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg' => 'El cliente '. $this->ticket->author->full_name .' ha realizado un nuevo comentario en el ticket #' . $this->ticket->ticket_id . ' ['. str_limit($this->comment->comment, 50) .']',
            'ticket' => $this->ticket
        ];
    }
}
