<?php

use Faker\Generator as Faker;

$factory->define(App\QuestionOptions::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'description' => $faker->text,
    ];
});
