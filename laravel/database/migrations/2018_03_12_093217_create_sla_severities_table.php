<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlaSeveritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sla_severities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sla_id');
            $table->integer('severity_id');
            $table->string('respond_number');
            $table->string('respond_format');
            $table->string('resolve_number');
            $table->string('resolve_format');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sla_severities');
    }
}
