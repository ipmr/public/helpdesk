<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role1 = null, $role2 = null, $role3 = null, $role4 = null)
    {


        $user = $request->user();


        if( $user->role == $role1 || 
            $user->role == $role2 || 
            $user->role == $role3 ||
            $user->role == $role4)
        {

            return $next($request);
            
        }elseif($user->role == 'agent'){

            if(

                $role1 == 'manager' ||
                $role2 == 'manager' ||
                $role3 == 'manager' ||
                $role4 == 'manager'

            ){

                if($user->agent->as_manager){

                    return $next($request);

                }

            }

        }

        return redirect()->back()->withErrors(['Acceso Denegado.']);

    }
}
