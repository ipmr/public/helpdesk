@extends('layouts.master')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Listado de tipos de contacto
            </p>
            <a class="btn btn-primary ml-auto" href="{{ route('contact-types.create') }}/">
                <i class="fa fa-plus"></i>
                Nuevo Tipo De Contacto
            </a>
        </div>
        @if(count($contact_types) > 0)
            <table class="table table-striped m-0">
                <thead>
                    <tr>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contact_types as $contact_type)
                    <tr>
                        <td><a href="{{ route('contact-types.show', $contact_type) }}/">{{ $contact_type->name }}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-footer">
                {{ $contact_types->links() }}
            </div>
        @else
            <div class="card-body">No se han agregado tipos de contacto</div>
        @endif
    </div>
</div>
@stop
