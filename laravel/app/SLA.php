<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SLA extends Model
{
    protected $table   = 'sla';
    public $timestamps = true;
    protected $guarded = [];

    public function severities(){

    	return $this->hasMany('App\SlaSeverity', 'sla_id', 'id');

    }

}
