<?php

namespace App\Http\Controllers;

use App\BrandType;
use App\GearBrand;
use App\GearType;
use App\GearModel;
use Illuminate\Http\Request;

class BrandTypeController extends Controller
{
    
    public function index(GearBrand $brand)
    {
        
        $types = $brand->gear_types;

        if(request()->ajax()){

            return response()->json($types);
        }

    }

    public function create()
    {
        //
    }

    public function store(Request $request, GearBrand $brand)
    {
        
        $this->add_types_to_brand($request, $brand);

        return redirect()
            ->back()
            ->with('msg', "Se han agregado los tipos de equipo a la marca {$brand->name}");

    }

    public function add_types_to_brand($request, $brand){

        $types = collect(explode(',', $request->types_list));

        $types->each(function($item) use ($brand) {

            $type = GearType::where('name', $item)->first();

            if($type){

                $already_added = $brand->types()->where('type_id', $type)->first();

                if(!$already_added){

                    $brand_type = new BrandType();
                    $brand_type->brand_id = $brand->id;
                    $brand_type->type_id = $type->id;
                    $brand_type->save();

                }


            }else{

                $new_type = new GearType();
                $new_type->name = $item;
                $new_type->save();

                $brand_type = new BrandType();
                $brand_type->brand_id = $brand->id;
                $brand_type->type_id = $new_type->id;
                $brand_type->save();

            }

        });

    }

    public function show(Request $request, GearBrand $brand, GearType $type)
    {

        $brand_type = $brand->types()->where('type_id', $type->id)->first();

        $models  = GearModel::where('brand_type_id', $brand_type->id );

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $models = $models->where(function ($q) use ($query_string, $request) {
                $q = $q->where('name', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('name', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        $models = $models->get();

        return view('helpdesk.gears.types.show', compact('brand', 'type', 'brand_type', 'models'));

    }

    public function edit(BrandType $brandType)
    {
        //
    }

    public function update(Request $request, BrandType $brandType)
    {
        //
    }

    public function destroy(BrandType $brandType)
    {
        //
    }
}
