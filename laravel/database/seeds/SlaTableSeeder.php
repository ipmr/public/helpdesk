<?php

use App\Severity;
use App\SLA;
use App\SlaSeverity;
use Illuminate\Database\Seeder;

class SlaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SLA::class, 2)->create()->each(function ($sla) {

            foreach (Severity::get() as $severity) {

                factory(SlaSeverity::class, 1)->create([
                    'sla_id'      => $sla->id,
                    'severity_id' => $severity->id,
                ]);

            }

        });
    }
}
