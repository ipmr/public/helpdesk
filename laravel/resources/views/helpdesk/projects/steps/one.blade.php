<tr>
	<th style="width: 20%">FUP:</th>
	<td>{{ $project->fup }}</td>
</tr>


<tr>
	<th>Título:</th>
	<td>{{ $project->title }}</td>
</tr>


<tr>
	<th>Descripción:</th>
	<td>{{ $project->description }}</td>
</tr>

<tr v-if="!quotation">
	<th class="no-wrap">Levantamiento:</th>
	<td>

		<div v-if=" ! revision_ticket " class="d-flex align-items-center">
			
			<button type="button" class="btn btn-link p-0 mr-3" data-toggle="modal" data-target="#requireRevisionModal" id="requireRevisionBtn"> 
				Solicitar Levantamiento
			</button>

			
			<div class="custom-control custom-checkbox">
				
				<input type="checkbox" class="custom-control-input" id="withOutRevision" @change="no_require_revision">
				<label class="custom-control-label" for="withOutRevision">No requiere levantamiento</label>

			</div>

		</div>
		

		<p v-else class="m-0">
			

			<span v-if="revision_ticket_status !== 'Resuelto'">
				( <i class="fa fa-clock fa-sm text-warning"></i> )
				Esperando la resolución del ticket 
				<a :href="ticket_route(revision_ticket.ticket_id)" target="_blank" class="text-danger">#@{{ revision_ticket.ticket_id }}</a> 
				para continuar.
			</span>

			<span v-else>
				( <i class="fa fa-check fa-sm text-success"></i> )
				El ticket <a :href="ticket_route(revision_ticket.ticket_id)" target="_blank" class="text-danger">#@{{ revision_ticket.ticket_id }}</a> de levantamiento ha sido resuelto.
			</span>


		</p>


	</td>
</tr>