<script>
	var gears = new Vue({
		el: '#gears_app',
		data: {
			brands: @json(App\GearBrand::get()),
			@isset($gear)
				types: @json(App\GearType::select(['id','name'])->get()),
				models: @json(App\GearModel::select(['id','name'])->where('brand_type_id', $gear->type_id)->get()),
				contract: '{{ $gear->contract_id ? true : false }}',
				coverage: '{{ $gear->coverage_id ? true : false }}',
				modules: @json($gear->modules()->with('module')->get()->pluck('module.serial_number')),
			@else
				types: null,
				models: null,
				contract: false,
				coverage: false,
				serial_numbers: [],
				stock_gear: null,
				inventories: @json(App\Inventory::get()),
				inventory_stock: null,
			@endisset
			@isset ($inventory)
			    sites: null,
			@endisset
			loading_types: false,
			loading_models: false
		},
		mounted: function(){
			@isset($gear)

				$('[name="type_id"] option[value="{{ $gear->type->id }}"]').attr('selected', true);
				$('[name="brand_id"] option[value="{{ $gear->brand->id }}"]').attr('selected', true);
				$('[name="model_id"] option[value="{{ $gear->model->id }}"]').attr('selected', true);
				
			@endisset

			this.drag_drop_gear();
		},
		methods: {
			@isset ($inventory)
				get_sites: function(e){
					var t = this;
					var select = $(e.target);
					var company = select.val();
					var url = '{{ route('companies.gears.index', '#company_id') }}';
					
					t.sites = null;
					if(select){
						url = url.replace('#company_id', company);

						$.get(url, function(response){
							t.sites = response.zones;
						});
					}
				},
			@endisset
			get_types: function(e){

				var t = this;
				var select = $(e.target);
				var value = select.val();
				var types_select = $('#types_select');
				var url = '{{ route("brands.types.index", "#brand_id") }}';
				url = url.replace('#brand_id', value);

				t.models = null;
				t.loading_types = true;
				types_select.val($('option:first', types_select).val());

				t.$http.get(url).then(function(response){
					t.types = response.body;
					t.loading_types = false;
				});
			},
			get_models: function(e){
				
				var t = this;
				var brand = $('#brands_select').val();
				var select = $(e.target);
				var value = select.val();
				var models_select = $('#models_select');
				var url = '{{ route('brands.types.models.index', ['#brand_id', '#type_id']) }}';

				url = url.replace('#brand_id', brand).replace('#type_id', value);

				t.loading_models = true;

				models_select.val($('option:first', models_select).val());

				t.$http.get(url).then(function(response){

					t.models = response.body;
					t.loading_models = false;

				});
			},
			set_contract: function(e){
				var t = this;
				var select = $(e.target);
				var value = select.val();
				if(value){
					t.contract = true;

					setTimeout(function(){

						$( ".datepicker" ).datepicker({
							monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
							monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
							dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
							dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
							dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
						});

					}, 150);
				}else{
					t.contract = false;
				}
			},
			add_master_contract: function(e){

				var t = this;
				var checkbox = $(e.target);
				var select = $('[name="contract_id"]');

				if(checkbox.is(':checked')){
					select.attr('disabled', 'disabled').val($('option:first', select).val());
					t.contract = false;
				}else{
					select.removeAttr('disabled');
					select.val($('option:eq(1)', select).val());
					t.contract = false;
				}
			},
			add_master_coverage: function(e){

				var t = this;
				var checkbox = $(e.target);
				var select = $('[name="coverage_id"]');

				if(checkbox.is(':checked')){
					select.attr('disabled', 'disabled').val($('option:first', select).val());
					t.coverage = false;
				}else{
					select.removeAttr('disabled');
					select.val($('option:eq(1)', select).val());
					t.coverage = false;
				}
			},
			choose_stock_gear: function(e){

				var t = this;

				var checkbox = $(e.target);

				var checked = checkbox.prop('checked');
				
				if(checked){

					t.stock_gear = true;

				}else{

					t.stock_gear = null;

				}
			},
			get_inventory_gear: function(e){

				var t = this;

				var select = $(e.target);

				var value = select.val();

				var route = '{{ route('inventories.stock.index', '#inventory_id') }}';

				route = route.replace('#inventory_id', value);

				t.$http.get(route).then(function(response){

					t.inventory_stock = response.body;

				});
			},			
			@include('helpdesk.gears.components.scripts.drag-and-drop')
		}
	});
</script>