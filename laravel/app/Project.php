<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    

	protected $table   = 'projects';
	public $timestamps = true;
	protected $guarded = [];


    public function project_tickets()
    {

        
        return $this->hasMany('App\ProjectTicket', 'project_id', 'id');


    }

    public function company_project()
    {
        return $this->hasOne('App\CompanyProject', 'project_id', 'id');
    }

    public function get_tickets()
    {


    	return $this->hasMany('App\ProjectTicket', 'project_id', 'id');


    }

    public function members()
    {

        return $this->hasMany('App\ProjectMember', 'project_id', 'id');

    }

    public function history()
    {

        return $this->hasMany('App\ProjectHistory', 'project_id', 'id');

    }

    public function getOwnerMemberAttribute()
    {
        $member = null;

        if ($this->members()->count() > 0){
            $member = $this->members()->where('type', 'owner')->first();

            $member = $member->user;
        }

        return $member;

    }


    public function getTicketsAttribute(){


        $tickets = collect([]);

        $this->get_tickets->each(function($item) use ($tickets) {

            $ticket = [

                'ticket' => $item->ticket,
                'status' => $item->ticket->get_status

            ];


            $tickets->push($ticket);

        });

        return $tickets;

    }


    public function quotation(){


        return $this->hasOne('App\Quotation', 'project_id', 'id');

    }

    public function getStepAttribute(){


        $step = null;

        switch ($this->status) {
            case 2:
                $step = 'Cotización';
                break;

            case 3:
                $step = 'Producción';
                break;

            case 4:
                $step = 'Documentación';
                break;
            
            case 3:
                $step = 'Facturación';
                break;

            default:
                $step = 'Inicio';
                break;
        }

        return $step;


    }

    public function getRevisionTicketsAttribute()
    {

        $tickets = $this->tickets()->where('phase', 1)->get();

    }

    public function getQuotationTicketsAttribute()
    {

        $tickets = $this->tickets()->where('phase', 2)->get();

    }

    public function getProductionTicketsAttribute()
    {

        $tickets = $this->tickets()->where('phase', 3)->get();

    }

    public function getDocumentationTicketsAttribute()
    {

        $tickets = $this->tickets()->where('phase', 4)->get();

    }

    public function getBillingTicketsAttribute()
    {

        $tickets = $this->tickets()->where('phase', 5)->get();

    }
}