<?php

namespace App\Console\Commands;

use App\History;
use App\Agent;
use App\Department;
use App\Notifications\NoActivityTicket;
use App\Status;
use App\Ticket;
use App\User;
use Illuminate\Console\Command;
use Notification;

class NoActivityTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:no_activity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Script para avisar dependiendo del status del ticket el tiempo de inactividad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date_search       = date('Y-m-d H:i:s', strtotime('-24 hour ' . date('Y-m-d H:i:s')));
        $data_status       = Status::whereNotIn('name', ['Resuelto', 'Cerrado'])->get();

        $statuses          = [];
        foreach ($data_status as $status) {
            $statuses[] = $status->id;
        }

        $tickets           = Ticket::where( 'updated_at', '<=', $date_search )->whereIn( 'status_id', $statuses );

        $send_notification = [];
        if ($tickets->count() > 0) {
            foreach ($tickets->get() as $ticket) {
                // echo $date_search."\n";
                #validar comentarios de ticket
                $comments = $ticket->comments()->whereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();

                #si los comentarios del ticket son recientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($comments->count() > 0) {
                    echo 'tiene comentarios vigentes #' . $ticket->ticket_id . "\n";
                    continue;
                }

                #validar historial de ticket
                $histories = $ticket->histories()->whereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();
                #si el historial del ticket son recientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($histories->count() > 0) {
                    echo 'tiene historial vigente #' . $ticket->ticket_id . "\n";
                    continue;
                }

                $tasks = $ticket->tasks()->where('status', 1)->orWhereDate('created_at', '>=', date('Y-m-d', strtotime($date_search)))->get();
                #si el ticket tiene tareas pendientes, el ticket no se cierra, hasta que los comentarios sean mayores a 72 hrs
                if ($tasks->count() > 0) {
                    echo 'tiene tareas pendientes #' . $ticket->ticket_id . "\n";
                    continue;
                }

                #se guarda el historico de notificacion de sistema
                $history = new History();
                $history->ticket_id   = $ticket->ticket_id;
                $history->description = 'Sin registro de actividad mayor a 24 hrs, validado por sistema';
                $history->save();

                if ($ticket->agent_id) {

                    $send_notification['agents'][$ticket->agent_id][] = $ticket;
                } else if ($ticket->department_id){

                    $send_notification['departments'][$ticket->department_id][] = $ticket;
                } else {

                    $send_notification['not_asigned'][] = $ticket;
                }


            }
            
            #notificando al agente
            if (!empty($send_notification['agents'])) {
                foreach ($send_notification['agents'] as $agent => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $agent = Agent::where('id', $agent)->first();

                    #notificacion
                    Notification::send($agent->user, new NoActivityTicket($notification_data));
                    echo "Enviando a agente " . $agent->user->FullName . " las notificaciones\n";
                }
            }

            #notificando al supervisor
            if (!empty($send_notification['departmens'])) {
                foreach ($send_notification['departmens'] as $department => $tickets) {
                    $notification_data = [
                        'tickets' => $tickets,
                    ];

                    $department = Department::where('id', $department)->first();

                    #notificacion
                    if (!empty($department->supervisor)) {
                        
                        Notification::send($department->supervisor->user, new NoActivityTicket($notification_data));
                        echo "Enviando a supervisores " . $agent->user->FullName . " las notificaciones\n";
                    }
                }
            }

            #notificando al manager
            if (!empty($send_notification['not_asigned'])) {
                $notification_data = [
                    'tickets' => $send_notification['not_asigned'],
                ];

                $managers = User::where('role', 'manager')->get();

                Notification::send($managers, new NoActivityTicket($notification_data));
                echo "Enviando a managers las notificaciones\n";
            }
            
        } else {

            echo "Tickets con actividad, no hay tickets abandonados\n";
        }
    }
}
