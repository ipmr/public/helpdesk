<?php

namespace App\Http\Middleware;

use Closure;

class TicketClosed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $ticket = \App\Ticket::where('ticket_id', request('ticket'))->first();
        $closed_status = \App\Status::where('name', 'Cerrado')->first();


        if($ticket->get_status->id != $closed_status->id){

            return $next($request);

        } else{



            #si es un usuario administrador o agente
            if (in_array( auth()->user()->role, ['admin', 'manager', 'agent'])) {


                    #validamos si tiene solicitudes
                    if ($ticket->reopen_tickets()->count() > 0) {

                        #validamos si quieren cambiar el status
                        if ( isset( $request->status_id ) ) {

                            return $next($request);

                        }

                        #validamos si quieren enviar comentarios
                        if ( isset( $request->comment ) ) {

                            return $next($request);

                        }
                    }else{

                        return redirect()->back()->withErrors(['El ticket se encuentra cerrado, por lo tanto no puede ser modificado.']);

                    }

            }else{

                
                return redirect()->back()->withErrors(['El ticket se encuentra cerrado, por lo tanto no puede ser modificado.']);

                
            }



        }

    }
}
