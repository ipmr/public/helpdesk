<div class="tab-pane fade {{ session()->get('tab') == 'tasks' ? 'show active' : '' }}" id="tasks" role="tabpanel" aria-labelledby="tasks-tab">
	<div class="mb-3 d-flex align-items-center">
		

		@if($ticket->get_status->name != 'Cerrado' && $ticket->get_status->name != 'Resuelto')
		
			@role('admin', 'manager')

				@if ($ticket->agent_id)
					<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_task_modal">
						<i class="fa fa-plus"></i>
						Nueva Tarea
					</a>
				@endif

			@endrole


			@role('agent')

		
				@if( ! auth()->user()->agent->as_manager && $ticket->agent)
		
					@if($ticket->agent->user->id == auth()->user()->id)

						@if ($ticket->agent_id)
							<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_task_modal">
								<i class="fa fa-plus"></i>
								Nueva Tarea
							</a>
						@endif

					@endif

				@endif


			@endrole

		@endif


		@php
			$percent = 0;
			if ($ticket->tasks->count()) {
				$percent = ($ticket->tasks()->where('status', 1)->count() * 100)/$ticket->tasks->count();
			}
		@endphp
		


		<div class="col-sm-4 pr-0 ml-auto">

			<div class="d-flex align-items-center">
				Tareas completadas
				<b class="ml-auto">{{ number_format($percent) }}%</b>
			</div>

			<div class="progress" style="height: 3px;">
			 	<div 
			 		@if ($percent < 30)
				 	class="progress-bar bg-danger" 
				 	@elseif($percent >=30 && $percent < 60)
				 	class="progress-bar bg-warning" 
				 	@elseif($percent >=60 && $percent < 90)
				 	class="progress-bar" 
				 	@elseif($percent >= 90)
				 	class="progress-bar bg-success" 
			 		@endif
				 	role="progressbar" 
				 	style="width: {{ $percent }}%" 
				 	aria-valuenow="{{ $percent }}" 
				 	aria-valuemin="0" 
				 	aria-valuemax="100">
				 	
				</div>
			</div>
		</div>
	</div>
	
	@include('helpdesk.tasks.partials.list', [
		'tasks' => $ticket->tasks()->latest()->get(),
		'type' => 'general'
	])
	
</div>