no_require_revision: function(e){
	

	var t = this;

	var checkbox = $(e.target);

	var is_checked = checkbox.is(":checked");
	

	if(is_checked){

		$('#requireRevisionBtn').attr('disabled', 'disabled');

		t.go_to_step(2);

		setTimeout(function(){
			t.need_revision = false;
		}, 500);

	}else{

	

		$('#requireRevisionBtn').removeAttr('disabled');

		t.go_to_step(1);

		setTimeout(function(){
			t.need_revision = true;
		}, 500);

	}



},