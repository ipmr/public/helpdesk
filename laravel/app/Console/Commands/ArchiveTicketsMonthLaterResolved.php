<?php

namespace App\Console\Commands;

use App\History;
use App\Status;
use App\Ticket;
use Illuminate\Console\Command;

class ArchiveTicketsMonthLaterResolved extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'archive:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archivar tickets despues de un mes de estar cerrados automaticamente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today     = date('Y-m-d');
        $month_ago = date('Y-m-d', strtotime('-1 month '.$today));
        $status    = Status::where('name', 'Cerrado')->first();

        // echo $today.' '.$month_ago."\n";
        echo "archive tickets\n";

        $closed_tickets = Ticket::where('status_id', $status->id)->whereDate('updated_at', '<=', $month_ago)->get();

        foreach ($closed_tickets as $ticket) {
            
            #se guarda el historico del cambio de status
            $history = new History();
            $history->ticket_id   = $ticket->ticket_id;
            $history->description = 'Ticket archivado por sistema';
            $history->save();


            echo $ticket->ticket_id."\n";
            $ticket->delete();

        }
    }
}
