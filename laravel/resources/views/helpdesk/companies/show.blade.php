@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')

<table class="table table-striped m-0">
	<thead>
		<tr>
			<td colspan="2">
				<div class="d-flex align-items-center py-2">
					<label class="m-0"><b>Información General</b></label>
					@role('admin','manager')
					<a href="{{ route('companies.edit', $company) }}/" class="btn btn-primary ml-auto">
						<i class="fa fa-pencil-alt"></i> 
						Editar Información
					</a>
					@endrole
				</div>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>Compañia:</th>
			<td>{{ $company->name }}</td>
		</tr>
		<tr>
			<th>Domicilio:</th>
			<td>{{ $company->full_address }}</td>
		</tr>
		<tr>
			<th>Teléfono:</th>
			<td>{{ $company->phone }} {{ $company->ext ? "Ext. {$company->ext}" : "" }}</td>
		</tr>
		<tr>
			<th>RFC:</th>
			<td>{{ $company->rfc or $company->master_parent->rfc }} {{ $company->master_rfc ? '(RFC Global)' : '' }}</td>
		</tr>
		<tr>
			<th>Contacto Principal:</th>
			<td>
				{{ $company->contact->full_name }} / {{ $company->contact->contact->jobtitle }}<br>
				{{ $company->contact->email }}
			</td>
		</tr>
		<tr>
			<th>Contrato:</th>
			<td>
				@if($company->contract)
					{{ $company->contract->name }}
					{{ $company->master_contract ? '(Contrato Global)' : '' }}
				@else
					Sin Contrato
				@endif
			</td>
		</tr>
		@if($company->contract)
		<tr>
			<th>Vigencia De Contrato:</th>
			<td>
				{{ $company->contract_life }} ( {!! $company->validate_contract !!} )
			</td>
		</tr>
		<tr>
			<th>SLA:</th>
			<td>
				{{ optional($company->sla)->name }}
			</td>
		</tr>
		<tr>
			<th>Cobertura:</th>
			<td>
				{{ $company->coverage->name }}
			</td>
		</tr>
		@endif
		@role('admin','manager','agent')
		<tr>
			<th>Tickets Urgentes:</th>
			<td>
				@if($company->urgent_tickets)
					<i class="fa fa-check text-success mr-1"></i> Habilitados
				@else
					<i class="fa fa-times text-danger mr-1"></i> Deshabilitados
				@endif
			</td>
		</tr>
		@endrole
	</tbody>
</table>

@stop