<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<label><b>Información del departamento</b></label>
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 col-form-label text-right">
		<span class="text-danger">*</span>
		Nombre:
	</label>
	<div class="col-sm-4">
		<input type="text" name="name" value="{{ $department->name or old('name') }}" class="form-control" autofocus required>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 col-form-label text-right">
		Descripción:
	</label>
	<div class="col-sm-6">
		<textarea name="description" rows="2" class="form-control">{{ $department->description or old('description') }}</textarea>
	</div>
</div>

<div class="form-group row form-row mb-0">
	<label class="col-sm-4 col-form-label text-right">
		<span class="text-danger">*</span>
		Supervisor:
	</label>
	<div class="col-sm-5">
		<div class="d-flex align-items-center">
			<select name="supervisor_id" class="form-control" required>
				<option disabled selected value>Elige un ingeniero</option>
				@foreach($agents as $agent)
				<option 
					value="{{ $agent->id }}" 
					@isset($department) 
						{{ $department->supervisor_id == $agent->id ? 'selected' : '' }} 
					@endisset
					@if(request('supervisor'))
						{{ request('supervisor') == $agent->id ? 'selected' : '' }}
					@endif
				>
					{{ $agent->user->first_name .' '. $agent->user->last_name }}
				</option>
				@endforeach
			</select>

			<a href="{{ route('agents.create') }}/" class="btn btn-link px-0 ml-3">
				<i class="fa fa-plus"></i>
				Nuevo Agente
			</a>

		</div>
	</div>
</div>

<div class="form-group row form-row mt-3 mb-0">
	<label class="col-sm-4 text-right">Agentes:</label>
	<div class="col-sm-6">
		@if(count($agents) > 0)
			@foreach($agents as $key => $agent)
			<div class="custom-control custom-checkbox">
				<input 
					type="checkbox"
					class="custom-control-input"
					name="agents[]" 
					value="{{ $agent->id }}" id="customCheck{{ $key }}"
					@isset($department)
						@if($department->id == $agent->department_id || $department->supervisor_id == $agent->id)
						checked
						@endif
					@endisset
				>
				<label class="custom-control-label" for="customCheck{{ $key }}">
					{{ $agent->full_name }}
				</label>
			</div>
			@endforeach
		@else
			No se han agregado agentes
		@endif
	</div>
</div>

