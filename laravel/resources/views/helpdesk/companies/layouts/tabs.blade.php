@extends('layouts.master')
@section('content')
<div class="container container-lg" id="companyApp">


	@if( ! $company->master)

		<div class="card mb-3">
			<div class="card-body">
				<ul class="list-inline m-0">
					@foreach($company->all_parents()->reverse() as $parent)
						@role('company')
							@if(in_array($parent->id, auth()->user()->companies()->pluck('id')->toArray()))
								@include('helpdesk.companies.components.breadcrumbs')
							@endif
						@else
							@include('helpdesk.companies.components.breadcrumbs')
						@endrole
					@endforeach
				</ul>
			</div>
		</div>

	@endif


	<div class="row">
		
		<div class="col-sm-3">
			
			<div class="card" style="overflow: hidden;">
				
				<div class="card-body bg-gradient" style="position: relative;">
					
					
					@if($company->master)
		
						<i class="fa fa-star master_company_star"></i>

					@endif


					<p class="lead">
						<a href="{{  route('companies.show', $company) }}/" style="text-decoration: none; color: #fff;">
							
							<b>{{ $company->name }}</b>

						</a>
					</p>

					<p class="m-0"><small>{{ $company->full_address }}</small></p>

				</div>



				<ul class="list-group list-group-flush no-striped">

					<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'general' ? 'bg-light' : 'bg-white' }} @endisset">
						
						<i class="fa fa-building text-muted" style="width: 24px;"></i>

						<a href="{{ route('companies.show', $company) }}/">
							Información General
						</a>
					</li>

					@role('admin', 'manager')

				
					<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'projects' ? 'bg-light' : 'bg-white' }} @endisset">
						
						<i class="fas fa-clipboard text-muted" style="width: 24px;"></i>

						<a href="{{ route('companies.projects.index', $company) }}/">

							Proyectos

						</a>

						<b class="ml-auto text-muted">
							({{ $company->projects()->count() }})
						</b>
					</li>	


					@endrole
					
					<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'team' ? 'bg-light' : 'bg-white' }} @endisset">
						
						<i class="fa fa-user text-muted" style="width: 24px;"></i>

						<a href="{{ route('companies.contacts.index', $company) }}/">


							Contactos

						</a>

						<b class="ml-auto text-muted">
							({{ $company->contacts->count() }})
						</b>
					</li>
					
					<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'gear' ? 'bg-light' : 'bg-white' }} @endisset">
						
						<i class="fa fa-server text-muted" style="width: 24px;"></i>

						<a href="{{ route('companies.gears.index', $company) }}/">
						

							Equipos

						</a>

						<b class="ml-auto text-muted">
							({{ $company->all_gears->count() }})
						</b>
					</li>
					
					<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'zones' ? 'bg-light' : 'bg-white' }} @endisset">
						
						<i class="fa fa-sitemap text-muted" style="width: 24px;"></i>

						<a href="{{ route('companies.zones.index', $company) }}/">

							Sitios

						</a>

						<b class="ml-auto text-muted">
							({{ $company->childs()->count() }})
						</b>
					</li>
	

					@role('admin', 'manager')


					@if($company->master)
				
						<li class="list-group-item d-flex align-items-center @isset($tab) {{ $tab == 'zones' ? 'bg-light' : 'bg-white' }} @endisset">
							
							<i class="fa fa-users text-muted" style="width: 24px;"></i>

							<a href="{{ route('companies.contact-catalogs.index', $company) }}/">

								Miembros del corporativo

							</a>

							<b class="ml-auto text-muted">
								({{ $company->contacts_catalog->count() }})
							</b>
						</li>

					@endif

					@endrole

				</ul>

			</div>

		</div>

		<div class="col-sm-9">


			<div class="card" style="overflow: hidden;">

				@yield('tab_content')

			</div>

			@yield('extra_panels')

		</div>

	</div>



</div>
@stop