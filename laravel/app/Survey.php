<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function departament()
    {

        return $this->belongsTo('App\Department', 'department_id', 'id');

    }

    public function questions()
    {

        return $this->hasMany('App\Question', 'survey_id', 'id');

    }
}
