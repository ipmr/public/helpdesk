@component('mail::message')

La compañia <b>{{ $ticket->company->name }}</b> ha creado el ticket #{{ $ticket->ticket_id }} con severidad {!! $ticket->severity() !!}. <br>
Asunto del ticket: "{{ $ticket->subject }}"

<small>Para darle seguimiento al ticket haz click en "Ir al ticket"</small> <br>

@component('mail::button', ['url' => route('tickets.show', $ticket->ticket_id)])
Ir al ticket
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
