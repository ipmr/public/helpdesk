@if(count($notifications) > 0)
    <table class="table table-bordered table-striped m-0">
        <thead>
            <tr>
                @isset ($is_archived)
                @else
                <th>
                    <input type="checkbox" class="control-check" @click="control_check">
                </th>
                @endisset
                <th>Fecha</th>
                <th>Notificación</th>
            </tr>
        </thead>

        <tbody>
            @foreach($notifications as $notification)
                
                <tr class="{{ $notification->read_at ? '' : 'bg-secondary' }}">
                    @isset ($is_archived)
                    @else
                    <td>
                        <input type="checkbox" class="check-notification" @click="change_checked" value="{{ $notification->id }}">
                    </td>
                    @endisset
                    <td class="no-wrap">{{ $notification->created_at->format('d M, Y h:i a') }}</td>
                    <td>
                        @if( in_array( $notification->type, [
                                'App\Notifications\NewTicket',
                                'App\Notifications\NewTicketCompany',
                                'App\Notifications\NewCompanyComment',
                                'App\Notifications\NewCommentToCompany',
                                'App\Notifications\NewInternalComment',
                                'App\Notifications\DepartmentAssigned',
                                'App\Notifications\AgentAssigned',
                                'App\Notifications\AgentAssignedCompany',
                                'App\Notifications\ChangeTicketStatus',
                                'App\Notifications\TicketClose',
                                'App\Notifications\TicketResolve',
                                'App\Notifications\EndTask',
                            ]) 
                        )
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route('tickets.show', $notification['data']['ticket']['ticket_id']) }}/#tabs">
                                Ir al ticket
                            </a>]
                        @elseif($notification->type == 'App\Notifications\ReopenTicketNotification' )
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route('read_reopen_ticket', $notification['data']['reopen_ticket']['id']) }}/">
                                Ir al ticket
                            </a>]
                        @elseif($notification->type == 'App\Notifications\RequestTaskReAssign')
                            
                            {{ $notification['data']['agent']['first_name'] }} 
                            ha solicitado reasignar 
                            una de sus tareas en el ticket 
                            <span class="text-danger">#{{ $notification['data']['task']['ticket_id'] }}</span> <br>
                            <small class="text-muted">...{{ $notification['data']['msg'] }}</small> <br>
                            
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route('tickets.tasks.show', ['ticket' => $notification['data']['task']['ticket_id'], 'task' => $notification['data']['task']['id']]) }}">
                                Ver tarea
                            </a>]
                        @elseif($notification->type == 'App\Notifications\NoActivityTicket')
                            {{ $notification['data']['msg'] }}[
                            @php
                                $ids = explode(',',$notification['data']['ids']);
                                foreach ($ids as $id) {
                            @endphp
                                <a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route('tickets.show', $id) }}/">
                                    #{{ $id }}
                                </a>
                            @php
                                }
                            @endphp
                            ]
                        @elseif($notification->type == 'App\Notifications\NewCompanyUserAssignation')
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route( 'companies.show', [ 'company' => $notification['data']['company']['id'] ] ) }}/">
                                Ver detalles
                            </a>]
                        @elseif($notification->type == 'App\Notifications\UpdateContactData')
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route( 'companies.contacts.show', [ 'company' => $notification['data']['company']['id'], 'contact' => $notification['data']['contact']['id'] ] ) }}/">
                                Ver datos
                            </a>]
                        @elseif(in_array($notification->type, ['App\Notifications\AsignTask','App\Notifications\NewEventTask']))
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route( 'agent-tasks' ) }}/">
                                Ver tareas
                            </a>]
                        @elseif($notification->type == 'App\Notifications\UpdateAgentData')
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route( 'profile.index' ) }}/">
                                Ver cuenta
                            </a>]
                        @elseif( in_array( $notification->type, [
                                'App\Notifications\AssignSupervisorDepartment',
                                'App\Notifications\AssignAgentDepartment',
                            ])
                        )
                            {{ $notification['data']['msg'] }}
                            [<a class="mark_notification_as_read" data-notification="{{ $notification->id }}" href="{{ route( 'tickets.index' ) }}/">
                                Ver tickets
                            </a>]
                        @endif
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
    @isset ($is_archived)
        <div class="card-footer">
            {{ $notifications->links() }}                   
        </div>
    @endisset
@else
    <div class="card-body">
        No hay notificaciones
    </div>
@endif