<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectHistory extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Project', 'id', 'project_id');
    }
}
