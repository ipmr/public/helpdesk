<?php

namespace App\Http\Controllers;

use App\Company;
use App\Gear;
use App\GearBrand;
use App\GearModel;
use App\GearType;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class GearController extends Controller
{
    public function __construct()
    {

        $this->middleware('role:admin,manager')->except('index', 'get_gears_by_model');

        $this->middleware('company_owner')->only(['index', 'show']);
    }

    public function index(Company $company)
    {

        $tab = 'gear';


        if (request()->ajax()) {


            $zones = $company->all_childs()->where('id', '<>', $company->id);


            $response = [
                'zones'          => $zones->count() > 0 ? $zones : null,
                'contacts'       => $company
                    ->contacts()
                    ->with('contact', 'company_user_type', 'contact.user')
                    ->get(),
                'models'         => $company->gears_models,
                'urgent_tickets' => $company->urgent_tickets,
            ];

            return response()->json($response);

        }

        $gears = $company->all_gears->sortBy('brand.name')->values()->where('module_from', null);

        if (request()->ajax) {

            return response()->json($gears);

        }

        return view('helpdesk.gears.index', compact('company', 'tab', 'gears'));
    }

    public function get_brands(Request $request, GearType $type)
    {

        $brands = $type->brands;

        return response()->json($brands);
    }

    public function get_models(Request $request, GearBrand $brand)
    {

        $models = $brand->models;

        return response()->json($models);
    }

    public function get_gears_by_model(Company $company, GearModel $model)
    {

        $gears = $company->gears()->where('model_id', $model->id)->with('company')->get();

        return response()->json($gears);
    }

    public function create()
    {
        //
    }

    public function store(Request $request, Company $company)
    {
        if (isset($request->choose_stock_gear)) {
            
            //get stock
            $stock = Stock::where('id', $request->stock_id)->first();
            if (empty($stock)) {
                return redirect()
                        ->back()
                    ->withErrors(['No ha seleccionado un equipo']);
            }
            $gear  = $stock->gear;

            $gear->company_id = $company->id;
            $gear->comments   = $request->comments;
            
            $gear->save();

            $stock->delete();
        } else {

            $this->validate_gear_data($request);

            $gear = $this->create_company_gear($request, $company);
        }        

        $add_contract = $this->add_contract_to_gear($request, $gear);

        if ($request->module_from) {

            $gear = $company->gears()->find($request->module_from);

            return redirect(route('companies.gears.show', [$company, $gear]) . '?module=' . $gear->serial_number)
                ->with([
                    "msg" => "Se ha agregado nuevo equipo a {$company->name}",
                ]);

        } else {

            return redirect()
                ->route('companies.gears.index', $company)
                ->with("msg", "Se ha agregado nuevo equipo a {$company->name}");
        }
    }

    public function create_company_gear($request, $company)
    {
        $gear                = new Gear();
        $gear->company_id    = $request->company_id;
        $gear->type_id       = $request->type_id;
        $gear->brand_id      = $request->brand_id;
        $gear->model_id      = $request->model_id;
        $gear->serial_number = strtoupper($request->serial_number);
        $gear->status        = $request->status;
        $gear->description   = $request->description;
        $gear->comments      = $request->comments;
        $gear->status        = $request->status;
        $gear->active        = $request->active ? 0 : 1;

        $gear->save();

        return $gear;
    }

    public function add_contract_to_gear($request, $gear)
    {

        if ($request->has('contract_id')) {

            if ($request->contract_id) {

                $contract          = \App\Contract::find($request->contract_id);
                $contract_start_at = Carbon::parse($request->contract_start_at);

                $gear->contract_id       = $contract->id;
                $gear->contract_start_at = $contract_start_at->format('Y-m-d');
                $gear->master_contract   = 0;

                if ($request->has('coverage_id')) {

                    $gear->coverage_id     = $request->coverage_id;
                    $gear->master_coverage = 0;

                } else {

                    $gear->coverage_id     = null;
                    $gear->master_coverage = 1;

                }

            } else {

                $gear->master_contract   = 0;
                $gear->master_coverage   = 0;
                $gear->contract_id       = null;
                $gear->contract_start_at = null;
                $gear->coverage_id       = null;

            }

        } else {

            $gear->master_contract   = 1;
            $gear->master_coverage   = 1;
            $gear->contract_id       = null;
            $gear->contract_start_at = null;
            $gear->coverage_id       = null;

        }

        $gear->save();
    }

    public function show(Company $company, Gear $gear)
    {

        $tab = 'gear';

        return view('helpdesk.gears.show', compact('company', 'gear', 'tab'));
    }

    public function edit(Company $company, Gear $gear)
    {

        $tab = 'gear';

        return view('helpdesk.gears.edit', compact('company', 'gear', 'tab'));
    }

    public function update(Request $request, Company $company, Gear $gear)
    {

        $this->validate_gear_data($request, $gear);

        $this->update_gear_data($request, $gear);

        $this->add_contract_to_gear($request, $gear);

        return redirect()
            ->route('companies.gears.show', [$company, $gear])
            ->with("msg", "El equipo ha sido actualizado.");
    }

    public function update_gear_data($request, $gear)
    {

        $gear->update([

            'company_id'    => $request->company_id,
            'type_id'       => $request->type_id,
            'brand_id'      => $request->brand_id,
            'model_id'      => $request->model_id,
            'serial_number' => strtoupper($request->serial_number),
            'description'   => $request->description,
            'comments'      => $request->comments,
            'status'        => $request->status,
            'active'        => $request->active ? 0 : 1,

        ]);
    }

    public function reassign_gear_to_zone(Request $request, Gear $gear)
    {

        $company = Company::findOrFail($request->company_id);

        $gear->update([

            'company_id' => $company->id,

        ]);

        $gear->all_modules->each(function ($item) use ($company) {

            $item->module->update([

                'company_id' => $company->id,

            ]);

        });

        return redirect()->back()->with("msg", "El equipo ha sido reasignado a {$company->name}");
    }

    public function destroy(Company $company, Gear $gear)
    {

    }

    public function validate_gear_data($request, $gear = null)
    {

        $gear_id = $gear ? $gear->id : null;

        $validate = Validator::make($request->all(), [

            'company_id'        => 'required|exists:companies,id',
            'type_id'           => 'required|exists:gear_types,id',
            'brand_id'          => 'required|exists:gear_brands,id',
            'model_id'          => 'required|exists:gear_models,id',
            'serial_number'     => ['sometimes', 'string', Rule::unique('gears')->ignore($gear_id ? $gear_id : '')],
            'status'            => 'required|in:Funciona,No Funciona,Tiene Fallas',
            'description'       => 'nullable|string',
            'comments'          => 'nullable|string',
            'contract_id'       => 'sometimes|exists:contracts,id|nullable',
            'contract_start_at' => 'sometimes|date_format:m/d/Y',
            'coverage_id'       => 'sometimes|exists:coverages,id',

        ], [

            'company_id.required'           => 'El sitio es requerido',
            'company_id.exists'             => 'El sitio seleccionado no es válido',
            'type_id.required'              => 'El tipo del equipo es requerido',
            'type_id.exists'                => 'El tipo del equipo no es válido',
            'brand_id.required'             => 'La marca del equipo es requerida',
            'brand_id.exists'               => 'La marca del equipo no es válida',
            'model_id.required'             => 'El modelo del equipo es requerido',
            'model_id.exists'               => 'El modelo del equipo no es válido',
            'serial_number.unique'          => 'El número de serie ya se encuentra registrado',
            'serial_number.string'          => 'El número de serie debe ser una cadena de texto',
            'status.required'               => 'El estado del equipo es requerido',
            'status.in'                     => 'El estado del equipo no es válido',
            'description.string'            => 'La descripción debe ser una cadena de texto',
            'comments.string'               => 'Los comentarios deben ser una cadena de texto',
            'contract_id.exists'            => 'El contrato especificado no es válido',
            'contract_start_at.date_format' => 'El formato de fecha de inicio de contrato no es válido',
            'coverage_id.exists'            => 'La cobertura especificada no es válida',

        ])->validate();
    }

    public function search_gear(Request $request, Company $company)
    {

        $gears = collect([]);

        $tab = 'gear';

        $search = true;

        $q = $request->q;

        foreach ($company->all_childs() as $child) {
            foreach ($child->gears()->where('serial_number', 'like', "%{$request->q}%")->get() as $child_gear) {
                $gears->push($child_gear);
            }
        }

        return view('helpdesk.gears.index', compact('company', 'tab', 'gears', 'search', 'q'));
    }

    public function download_company_gear(Request $request, Company $company)
    {

        $gears = $company->all_gears;

        $data = [
            'company' => $company,
            'gears'   => $gears,
        ];

        $pdf = \PDF::loadView('helpdesk.gears.pdf.all', $data);

        return $pdf->stream();
    }

}
