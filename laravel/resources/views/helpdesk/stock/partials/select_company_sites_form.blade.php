<div class="form-row form-group">
    <label class="col-form-label col-sm-4 text-right">Compañia</label>
    <div class="col-sm-6">
        <select name="company_id" class="form-control" @change="get_sites">
            <option value="">Selecciona una Compañia</option>
            @foreach(App\Company::whereNull('parent_id')->get() as $company)
                <option value="{{ $company->id }}"
                    @if(isset($gear) && $gear->company_id == $company->id)
                    selected
                    @endif
                >
                    {{ $company->name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-row form-group" v-if="sites">
    <label class="col-form-label col-sm-4 text-right">Sitio</label>
    <div class="col-sm-6">
        <select name="zone_id" class="form-control">
            <option selected value>Elige un sitio (opcional)</option>
            <option v-for="site in sites" :value="site.id" @isset ($gear) :selected="site.id == '{{$gear->company_id}}'" @endisset :selected="site.id == company_id">@{{ site.name }}</option>
        </select>
    </div>
</div>