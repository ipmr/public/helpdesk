<?php

namespace App\Http\Controllers;

use App\Coverage;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CoverageController extends Controller
{


    public function __construct(){


        $this->middleware('role:admin,manager');


    }


    public function index(Request $request)
    {

        $coverages = Coverage::where('id', '>', 0);

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $coverages = $coverages->where(function ($q) use ($query_string, $request) {
                $q = $q->where('name', 'like', '%'.$request->q.'%')
                    ->orWhere('from_day', 'like', '%'.$request->q.'%')
                    ->orWhere('to_day', 'like', '%'.$request->q.'%')
                    ->orWhere('from_hour', 'like', '%'.$request->q.'%')
                    ->orWhere('to_hour', 'like', '%'.$request->q.'%')
                    ->orWhere('all_week', 'like', '%'.$request->q.'%')
                    ->orWhere('all_day', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('name', 'like', '%'.$search.'%')
                    ->orWhere('from_day', 'like', '%'.$search.'%')
                    ->orWhere('to_day', 'like', '%'.$search.'%')
                    ->orWhere('from_hour', 'like', '%'.$search.'%')
                    ->orWhere('to_hour', 'like', '%'.$search.'%')
                    ->orWhere('all_week', 'like', '%'.$search.'%')
                    ->orWhere('all_day', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $coverages->toSql() );
            }

            if ($request->debug == 2) {
                dd( $coverages->get() );
            }
        }

        $coverages = $coverages->orderBy('id', 'DESC')->get();

        return view('helpdesk.coverages.index', compact('coverages'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $this->validate_coverage_data($request);

        $coverage = new Coverage();

        $coverage->name = $request->name;

        if ($request->all_week) {
            $coverage->from_day = 1;
            $coverage->to_day   = 7;
            $coverage->all_week = 1;
        } else {

            $coverage->from_day = $request->from_day;
            $coverage->to_day   = $request->to_day;

        }

        if ($request->all_day) {
            $coverage->from_hour = 1;
            $coverage->to_hour   = 24;
            $coverage->all_day   = 1;
        } else {

            $coverage->from_hour = $request->from_hour;
            $coverage->to_hour   = $request->to_hour;

        }

        $coverage->save();

        return redirect()
            ->back()
            ->with('msg', 'Se ha creado la cobertura ' . $coverage->name);

    }

    public function show(Coverage $coverage)
    {

        return view('helpdesk.coverages.show', compact('coverage'));

    }

    public function edit(Coverage $coverage)
    {
        //
    }

    public function update(Request $request, Coverage $coverage)
    {

        $this->validate_coverage_data($request, $coverage);

        if ($request->all_week) {
            $coverage->update([
                'from_day' => 1,
                'to_day'   => 7,
                'all_week' => 1,
            ]);
        } else {
            $coverage->update([
                'from_day' => $request->from_day,
                'to_day'   => $request->to_day,
                'all_week' => null,
            ]);
        }

        if ($request->all_day) {
            $coverage->update([
                'from_hour' => 1,
                'to_hour'   => 24,
                'all_day'   => 1,
            ]);
        } else {
            $coverage->update([
                'from_hour' => $request->from_hour,
                'to_hour'   => $request->to_hour,
                'all_day'   => null,
            ]);
        }

        $coverage->update([
            'name' => $request->name,
        ]);

        return redirect()->route('coverages.index')->with('msg', 'La cobertura ha sido modificada');

    }

    public function destroy(Coverage $coverage)
    {
        //
    }

    public function validate_coverage_data($request, $coverage = null)
    {

        $coverage_id = $coverage ? $coverage->id : null;

        $validate = Validator::make($request->all(), [
            'name'      => ['required', 'string', Rule::unique('coverages')->ignore($coverage_id)],
            'all_week'  => 'sometimes|in:on,off',
            'all_day'   => 'sometimes|in:on,off',
            'from_day'  => 'sometimes|numeric|min:1|max:7',
            'to_day'    => 'sometimes|numeric|min:1|max:7',
            'from_hour' => 'sometimes|numeric|min:1|max:24',
            'to_hour'   => 'sometimes|numeric|min:1|max:24',

        ], [

            'name.required'     => 'El nombre de la cobertura es requerido',
            'name.unique'       => 'El nombre de la cobertura ya se encuentra registrado',
            'all_week.in'       => 'Verifica que la información enviada este correcta',
            'all_day.in'        => 'Verifica que la información enviada este correcta',
            'from_day.numeric'  => 'El dia de inicio no es válido',
            'from_day.min'      => 'El dia de inicio no es válido',
            'from_day.max'      => 'El dia de inicio no es válido',
            'to_day.numeric'    => 'El dia final no es válido',
            'to_day.min'        => 'El dia final no es válido',
            'to_day.max'        => 'El dia final no es válido',
            'from_hour.numeric' => 'La hora de inicio no es válida',
            'from_hour.min'     => 'La hora de inicio no es válida',
            'from_hour.max'     => 'La hora de inicio no es válida',
            'to_hour.numeric'   => 'La hora final no es válida',
            'to_hour.min'       => 'La hora final no es válida',
            'to_hour.max'       => 'La hora final no es válida',

        ])->validate();

    }
}
