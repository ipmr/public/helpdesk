@extends('helpdesk.companies.layouts.tabs')

@section('tab_content')
	
	<div class="card-body">
		<a href="{{ route('companies.gears.index', $company) }}/" class="btn btn-link p-0">
			<i class="fa fa-angle-left"></i>
			Regresar
		</a>
	</div>
	
	<hr class="m-0">

	<div class="card-body d-flex align-items-center">
		<label class="m-0"><b>Datos del equipo</b></label>
		<a href="{{ route('companies.gears.edit', [$company, $gear]) }}/" class="btn btn-primary ml-auto">
			<i class="fa fa-pencil-alt"></i> Editar
		</a>
	</div>

	<table class="table table-striped m-0">
		<tbody>
			
			<tr style="border-top: 1px solid #f2f2f2">
				<th width="25%">Sitio</th>
				<td><a href="{{ route('companies.show', $gear->company->id) }}/">{{ $gear->company->name }}</a></td>
			</tr>
			<tr>
				<th>Marca</th>
				<td>{{ $gear->brand->name }}</td>
			</tr>
			<tr>
				<th>Tipo</th>
				<td>{{ $gear->type->name }}</td>
			</tr>
			<tr>
				<th>Modelo</th>
				<td>{{ $gear->model->name }}</td>
			</tr>
			<tr>
				<th>Número de serie</th>
				<td>{{ $gear->serial_number }}</td>
			</tr>
			<tr>
				<th>Descripción</th>
				<td>{{ $gear->description }}</td>
			</tr>
			<tr>
				<th>Comentarios</th>
				<td>{{ $gear->comments }}</td>
			</tr>
			<tr>
				<th>Contrato</th>
				<td>{{ $gear->contract ? $gear->contract->name : 'Sin contrato' }}</td>
			</tr>
			@if($gear->module_from)
			<tr>
				<th>Parte del equipo</th>
				<td>
					<a href="{{ route('companies.gears.show', [$company, $gear->module_from->gear]) }}/">
						{{ $gear->module_from->gear->model->name }} / 
						{{ $gear->module_from->gear->serial_number }}
					</a>
					<br>
					En Slot: {{ $gear->module_from->slot }}
				</td>
			</tr>
			@endif

			@if( ! $gear->active)
		
				<tr>
					<th></th>
					<td class="text-danger">Equipo dado de baja</td>
				</tr>

			@endif

		</tbody>
	</table>
	
	<hr class="m-0">

	<div class="card-body d-flex align-items-center">
		<label class="m-0"><b>Módulos</b></label>
		<a href="#add_new_module" data-toggle="collapse" @click="scroll_to_modules" class="btn btn-primary ml-auto">
			<i class="fa fa-plus"></i> Nuevo Módulo
		</a>
	</div>

	<div class="card-body py-0" id="modules">
		<div class="collapse" id="add_new_module">
			
			<form action="{{ route('companies.gears.modules.store', [$company, $gear]) }}" method="post" class="pb-3">
				
				<div class="form-row">
					<div class="col-sm-4 mx-auto">
						<label><b>Datos del módulo</b></label>
						<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
					</div>
				</div>

				@include('helpdesk.gears.partials.module-form')

				<div class="form-row">
					<div class="col-sm-4 mx-auto">
						<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Agregar Módulo</button>
					</div>
				</div>
				
			</form>

		</div>
	</div>

	@if($gear->modules->count() > 0)
		
		@component('helpdesk.gears.components.modules-box', [
			'g' => $gear,
			'company' => $company,
			'show' => true
		])

		@endcomponent

	@else
	
	<div class="card-body">
		No se han agregado módulos a este equipo.
	</div>

	@endif

@stop

@section('scripts')


	@include('helpdesk.gears.partials.scripts-show')


@stop