<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
    protected $table   = 'company_users';
    public $timestamps = true;
    protected $guarded = [];

    public function user(){

    	return $this->belongsTo('App\User', 'user_id', 'id');

    } 
}
