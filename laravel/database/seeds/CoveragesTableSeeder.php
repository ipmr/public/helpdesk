<?php

use Illuminate\Database\Seeder;
use App\Coverage;

class CoveragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Coverage::class, 3)->create();
    }
}
