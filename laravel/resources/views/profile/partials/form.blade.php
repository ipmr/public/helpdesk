<div class="form-group row form-row">
    <label class="col-form-label col-sm-4 text-right">
        <span class="text-danger">*</span>
        Usuario:
    </label>
    <div class="col-sm-2">
        <input type="text" placeholder="Mínimo 6 caracteres" name="username" value="{{ $user->username or old('username') }}" class="form-control" required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-form-label col-sm-4 text-right">
        <span class="text-danger">*</span>
        Nombre Completo:
    </label>
    <div class="col-sm-6">
        <div class="row form-row">
            <div class="col-sm-6">
                <input type="text" name="first_name" placeholder="Nombre(s)" value="{{ $user->first_name or old('first_name') }}" class="form-control" required>
            </div>
            <div class="col-sm-6">
                <input type="text" name="last_name" placeholder="Apellido(s)" value="{{ $user->last_name or old('last_name') }}" class="form-control" required>
            </div>
        </div>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-form-label col-sm-4 text-right">
        <span class="text-danger">*</span>
        E-mail:
    </label>
    <div class="col-sm-3">
        <input type="email" name="email" value="{{ $user->email or old('email') }}" class="form-control" required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-form-label col-sm-4 text-right">
        Contraseña:
    </label>
    <div class="col-sm-3">
        <input type="password" name="password" class="form-control">
    </div>
</div>