<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskEvent;
use Illuminate\Http\Request;
use App\Notifications\NewEventTask;
use Notification;

class TaskEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ticket, Task $task)
    {
        $user = auth()->user();

        $event = new TaskEvent();

        $event->task_id = $task->id;

        $event->user_id = $user->id;

        $event->description = $request->description;

        $event->save();

        $notification_data = [
            'ticket' => $task->ticket,
            'task'   => $task,
            'event'  => $event,
        ];

        if (isset($task->ticket->agent_id)) {
            # code...
            Notification::send( $task->ticket->agent->user, new NewEventTask( $notification_data ) );
        } else if (isset($task->ticket->department_id)){

            Notification::send( $task->ticket->department->supervisor->user, new NewEventTask( $notification_data ) );
        }

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha agregado el evento exitosamente.',
            'tab' => 'tasks',
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaskEvent  $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function show(TaskEvent $taskEvent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskEvent  $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskEvent $taskEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskEvent  $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskEvent $taskEvent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskEvent  $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskEvent $taskEvent)
    {
        //
    }
}
