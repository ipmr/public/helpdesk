@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Listado De Agentes</p>
			<form action="{{ route('agents.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
			<a href="{{ route('agents.create') }}/" class="btn btn-primary ml-2">
				<i class="fa fa-plus"></i>
				Nuevo Agente
			</a>
		</div>
		@if(count($agents) > 0)
			
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>E-mail</th>
						<th>Departamento</th>
						<th class="text-center no-wrap">Supervisor</th>
						<th class="text-center">Generar Tickets</th>
						<th class="text-center">Permisos de manager</th>
					</tr>
				</thead>
				<tbody>

					@foreach($agents as $agent)

					<tr>
						<td>
							<a href="{{ route('agents.show', ['agent' => $agent->agent_id]) }}/">{{ $agent->full_name }}</a>
						</td>
						<td>{{ $agent->email }}</td>
						<td>{{ $agent->name ? $agent->name : 'Sin asignar' }}</td>
						<td class="text-center">

							@if($agent->agent->id == $agent->agent->department->supervisor->id)
								<i class="fa fa-check text-success"></i>
							@else
								<i class="fa fa-times text-danger"></i>
							@endif

						</td>
						<td class="text-center">

							<i class="fa fa-{{ $agent->agent->generate_tickets ? 'check' : 'times' }} text-{{ $agent->agent->generate_tickets ? 'success' : 'danger' }}"></i>

						</td>
						<td class="text-center">

							<i class="fa fa-{{ $agent->agent->as_manager ? 'check' : 'times' }} text-{{ $agent->agent->as_manager ? 'success' : 'danger' }}"></i>

						</td>
					</tr>

					@endforeach
				</tbody>
			</table>

			<div class="card-footer">
				{{ $agents->links() }}
			</div>
			
		@else
			<div class="card-body">
				No se han agregado agentes
			</div>
		@endif
	</div>
</div>
@stop