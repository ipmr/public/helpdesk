@component('helpdesk.components.modal', [

    'id'    => 'modal_suspend_contact',
    'title' => 'Suspender Contacto Y Asignar Contacto Principal'
])


    @slot('body')
    
        <form :action="contact_suspend_url" id="suspend_contact" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <p>
                Elige a un contacto
            </p>
            <div class="form-group">
                
                <select name="contact_id" class="form-control" required>
                    
        
                    <option disabled selected value="">Elige una opción</option>


                    @foreach($contacts as $contact)
                        @isset ($contact->contact)
                            <option value="{{ $contact->user_id }}">{{ $contact->full_name }}</option>
                        @endisset
                    @endforeach

                </select>

            </div>
        </form>

    @endslot

    @slot('footer')

        
        <a href="#" class="btn btn-light" data-dismiss="modal" aria-label="Close">
    
            <i class="fa fa-times"></i>

            Cancelar

        </a>

    
        <button type="submit" form="suspend_contact" class="btn btn-success">
            
            <i class="fa fa-check"></i>

            Suspender Contacto

        </button>

    @endslot


@endcomponent