<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationResource extends Model
{
    public function quotation(){


		return $this->belongsTo('App\Quotation', 'quotation_id', 'id');


    }
}
