<style>
	body{
		font-family: 'Helvetica', sans-serif;
		font-size: 14px;
	}
	.text-danger {
	    color: #ed1c24!important;
	}
	h1, h2, h3, h4{
		font-weight: lighter;
	}
	.m-0{
		margin: 0 !important;
	}
	table{
		width: 100%;
		font-size: 12px;
	}
	table tr, table th, table td{
		vertical-align: top;
	}
	.bg-warning{
	background: #eef3f9 !important;
	}
</style>