@if (session('msg'))
<div class="alert mb-3 fixed_alert alert-success alert-dismissible fade show" role="alert">
	{{ session('msg') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
@if (session('info'))
<div class="alert mb-3 fixed_alert alert-info alert-dismissible fade show" role="alert">
	{{ session('info') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
@if (session('warning'))
<div class="alert mb-3 fixed_alert alert-warning alert-dismissible fade show" role="alert">
	{{ session('warning') }}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
@if (isset($errors) && $errors->any())
<div class="alert mb-3 fixed_alert alert-danger alert-dismissible fade show" role="alert">
	@foreach ($errors->all() as $error)
		{{ $error }} @if( ! $loop->last) <br> @endif
	@endforeach
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
