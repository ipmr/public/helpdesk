{{ csrf_field() }}


<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<label><b>Datos del equipo</b></label>
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>
@if (isset($inventory))
	@isset ($update)
	    @include('helpdesk.stock.partials.select_company_sites_form')
	@endisset
@else

	<div class="form-row mb-4">
		<div class="col-sm-4 mx-auto">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input" name="choose_stock_gear" id="choose_stock_gear" @change="choose_stock_gear">
				<label class="custom-control-label" for="choose_stock_gear">Seleccionar equipo en almancén</label>
			</div>
		</div>
	</div>
	<div class="form-row form-group" v-if="!stock_gear">
		<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Sitio</label>
		<div class="col-sm-4">
			<select name="company_id" class="form-control" required>
				@foreach($company->all_childs() as $child)
					<option value="{{ $child->id }}"
						@if(isset($gear) && $gear->company_id == $child->id)
						selected
						@endif
					>
						{{ $child->name }}
					</option>
				@endforeach
			</select>
		</div>
	</div>
@endif

<div v-if="!stock_gear">
	<div class="form-row form-group">
		<label for="" class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>  Marca
		</label>
		<div class="col-sm-4">
			<select name="brand_id" id="brands_select" class="form-control" @change="get_types" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="brand in brands" :value="brand.id" @isset ($gear) :selected="brand.id == '{{$gear->brand_id}}'" @endisset>@{{ brand.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row form-group" v-if="loading_types">
		<div class="col-sm-4 mx-auto d-flex align-items-center">
			<p class="m-0"><i class="fa fa-spin fa-spinner fa-lg mr-1"></i> Cargando</p>
		</div>
	</div>

	<div class="form-row form-group" v-if="types">
		<label for="" class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>  Tipo
		</label>
		<div class="col-sm-4">
			<select name="type_id" class="form-control" @change="get_models" id="types_select" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="type in types" :value="type.id">@{{ type.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row form-group" v-if="loading_models">
		<div class="col-sm-4 mx-auto d-flex align-items-center">
			<p class="m-0"><i class="fa fa-spin fa-spinner fa-lg mr-1"></i> Cargando</p>
		</div>
	</div>

	<div class="form-row form-group" v-if="models">
		<label for="" class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>  Modelo
		</label>
		<div class="col-sm-4">
			<select name="model_id" id="models_select" class="form-control" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="model in models" :value="model.id" @isset ($gear) :selected="model.id == '{{ $gear->model_id }}'" @endisset>@{{ model.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row form-group">
		<label class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>
			# Serie
		</label>
		<div class="col-sm-4">
			<input type="text" name="serial_number" value="{{ $gear->serial_number or old('serial_number') }}" class="form-control">
		</div>
	</div>

	<div class="form-row form-group">
		<label class="col-form-label col-sm-4 text-right">
			Descripción
		</label>
		<div class="col-sm-5">
			<textarea name="description" class="form-control">{{ $gear->description or old('description') }}</textarea>
		</div>
	</div>

	<div class="form-row form-group">
		<label class="col-form-label col-sm-4 text-right">
			Estado del equipo:
		</label>
		<div class="col-sm-2">
			<select name="status" class="form-control">
				<option @if(isset($gear) && $gear->status == 'Funciona') selected @endif value="Funciona">Funciona</option>
				<option @if(isset($gear) && $gear->status == 'No Funciona') selected @endif value="No Funciona">No Funciona</option>
				<option @if(isset($gear) && $gear->status == 'Tiene Fallas') selected @endif value="Tiene Fallas">Tiene Fallas</option>
			</select>
		</div>
	</div>
</div>
<div v-else>
	
	<div class="form-row form-group">
		<label class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>
			Almacén
		</label>
		<div class="col-sm-4">
			<select name="inventory_id" class="form-control" @change="get_inventory_gear">
				<option disabled selected value="">Elige una opción</option>
				<option v-for="inventory in inventories" :value="inventory.id">@{{ inventory.name }}</option>
			</select>
		</div>
	</div>

	<div v-if="inventory_stock" class="form-row form-group">
		<label class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>
			Equipos en almacen
		</label>
		<div class="col-sm-4">
			<select name="stock_id" class="form-control" required>
				<option disabled selected value="">Elige una opción</option>
				<option v-for="stock in inventory_stock" :value="stock.id" v-if="stock.gear">@{{ stock.gear.brand.name + ' | ' + stock.gear.model.name + ' | ' + stock.gear.description }}</option>
			</select>
		</div>
	</div>
</div>
<div class="form-row form-group">
	<label class="col-form-label col-sm-4 text-right">
		Comentarios
	</label>
	<div class="col-sm-5">
		<textarea name="comments" class="form-control">{{ $gear->comments or old('comments') }}</textarea>
	</div>
</div>

@if (!isset($inventory))
	<div class="form-row form-group d-flex align-items-center">
		<label class="col-form-label col-sm-4 text-right">
			Contrato
		</label>
		<div class="col-sm-3">
			<select name="contract_id" class="form-control" required @change="set_contract"
				@if(isset($gear) && $gear->master_contract)
					disabled
				@elseif(empty($gear))
					disabled 
				@endif
			>
				<option disabled selected value="null"
					@if(isset($gear) && $gear->master_contract)
					selected
					@endif
					>
					Utilizar contrato global
				</option>

				<option value
					@if(isset($gear) && $gear->contract_id == null && !$gear->master_contract)
					selected
					@endif
					>
					Sin Contrato
				</option>
				@foreach(\App\Contract::get() as $contract)
					<option 
						value="{{ $contract->id }}"
						@isset($gear)
							@if($gear->contract_id && $gear->contract_id == $contract->id)
								selected 
							@endif
						@endisset
					>
						{{ $contract->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-5">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input" id="master_contract" @change="add_master_contract"
				@if(isset($gear) && $gear->master_contract)
					checked
				@elseif(empty($gear))
					checked
				@endif
				>
				<label class="custom-control-label" for="master_contract">Utilizar contrato global</label>
			</div>
		</div>
	</div>

	<div class="form-row form-group" v-if="contract">
		<label class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>  Inicio de contrato
		</label>
		<div class="col-sm-3">
			<input type="text" name="contract_start_at" class="form-control datepicker" required
				@isset($gear)
					@if($gear->contract_id)
					value="{{ \Carbon\Carbon::parse($gear->contract_init)->format('m/d/Y') }}" 
					@else
					value="{{ \Carbon\Carbon::now()->format('m/d/Y') }}" 
					@endif
				@else
				value="{{ \Carbon\Carbon::now()->format('m/d/Y') }}" 
				@endisset
			 >
		</div>
	</div>

	<div class="form-row form-group d-flex align-items-center" v-if="contract">
		<label class="col-form-label col-sm-4 text-right">
			<span class="text-danger">*</span>  Cobertura
		</label>
		<div class="col-sm-3">
			<select name="coverage_id" class="form-control" required
				@if(@isset($gear) && $gear->master_coverage)
					disabled
				@elseif(empty($gear))
					disabled 
				@endif
			>
				<option disabled selected value>Utilizar cobertura global</option>
				@foreach(App\Coverage::get() as $coverage)
					<option
						value="{{ $coverage->id }}"
						@isset($gear)
							@if($gear->coverage_id && $gear->coverage_id == $coverage->id)
								selected
							@endif
						@endisset
					>
						{{ $coverage->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-5">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input" id="master_coverage" @change="add_master_coverage"
				@if(@isset($gear) && $gear->master_coverage)
					checked
				@elseif(empty($gear))
					checked 
				@endif
				>
				<label class="custom-control-label" for="master_coverage">Utilizar cobertura global</label>
			</div>
		</div>
	</div>
@endif

@isset($gear)
	<div class="form-row form-group d-flex align-items-center py-3">
		<div class="col-sm-4 mx-auto">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" name="active" class="custom-control-input" id="gear_unactive"
					@if(isset($gear) && !$gear->active)
						checked
					@endif
				>
				<label class="custom-control-label text-danger" for="gear_unactive">Dar de baja este equipo</label>
			</div>
		</div>
	</div>
@endisset

@if( !isset($inventory) )
	<div class="form-row form-group">
		<div class="col-sm-8 ml-auto">
			<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> {{$btn}}</button>
		</div>
	</div>
@endif