<?php

use Faker\Generator as Faker;

$factory->define(App\Coverage::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'from_day' => rand(1,3),
        'to_day' => rand(4,6),
        'from_hour' => rand(1,12),
        'to_hour' => rand(13,24),
        'all_week' => rand(0,1),
        'all_day' => rand(0,1),
    ];
});

