<?php

use Faker\Generator as Faker;

$factory->define(App\Contract::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text($maxNbChars = 70),
        'period_number' => rand(1,6),
        'period_format' => $faker->randomElement($array = array ('d','w','m','y')),
    ];
});
