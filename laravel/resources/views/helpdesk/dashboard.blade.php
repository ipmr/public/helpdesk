@extends('layouts.master')
@section('content')
	<div class="container" id="dashboard_app">
		<div class="card mb-3">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-8 mx-auto">
						
						<div class="text-center">
							<h5 class="lead m-0 mb-2">
							<b>
							Actividad del
							@{{ from_date }}
							al
							@{{ to_date }}
							</b>
							</h5>
							<a class="btn btn-link pb-0" href="#reportRange" data-toggle="collapse">
								<i class="fa fa-pencil-alt fa-xs"></i>
								Modificar Rango de Reporte
							</a>
						</div>
						<div id="reportRange" class="collapse">
							<form action="{{ route('home') }}" id="reportRangeForm" class="mt-4" @submit.prevent="reset_report">
								{{ csrf_field() }}
								<div class="form-group d-flex align-items-start mb-0">
									<div style="width: 100%">
										<input type="text" name="from_date" class="form-control datepicker" value="{{ date('Y-m-d',strtotime('-7 day')) }}" placeholder="Fecha de inicio" required>
									</div>
									<div style="width: 100%" class="ml-3">
										<input type="text" name="to_date" class="form-control datepicker" value="{{ date('Y-m-d') }}" placeholder="Fecha final" required>
									</div>
									<button class="btn btn-success ml-3" style="height: 33px;"><i class="fa fa-check fa-sm mr-1"></i> Generar Reporte</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div v-cloak>
			<div class="row form-row">
				<div class="col-sm-3">
					<div class="card mb-3 mb-sm-0">
						<div class="card-body">
							<h1 class="m-0" :style="{color: report.status['Abierto'].color}">@{{ report.total['Abierto'] ? report.total['Abierto'] : 0 }}</h1>
							<p class="lead m-0"><b>Tickets Abiertos</b></p>
							[<a href="{{ route('tickets.index') }}?status_id={{ App\Status::where('name','Abierto')->first()->id }}">
								Ver tickets abiertos
							</a>]
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="card mb-3 mb-sm-0">
						<div class="card-body">
							<h1 class="m-0" :style="{color: report.status['En proceso'].color}">@{{ report.total['En proceso'] ? report.total['En proceso'] : 0 }}</h1>
							<p class="lead m-0"><b>Tickets En Proceso</b></p>
							[<a href="{{ route('tickets.index') }}?status_id={{ App\Status::where('name','En Proceso')->first()->id }}">
								Ver tickets en proceso
							</a>]
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="card mb-3 mb-sm-0">
						<div class="card-body">
							<h1 class="m-0" :style="{color: report.status['Resuelto'].color}">@{{ report.total['Resuelto'] ? report.total['Resuelto'] : 0 }}</h1>
							<p class="lead m-0"><b>Tickets Resueltos</b></p>
							[<a href="{{ route('tickets.index') }}?status_id={{ App\Status::where('name','Resuelto')->first()->id }}">
								Ver tickets resueltos
							</a>]
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="card mb-3">
						<div class="card-body">
							<h1 class="m-0" :style="{color: report.status['Cerrado'].color}">@{{ report.total['Cerrado'] ? report.total['Cerrado'] : 0 }}</h1>
							<p class="lead m-0"><b>Tickets Cerrados</b></p>
							[<a href="{{ route('tickets.index') }}?status_id={{ App\Status::where('name','Cerrado')->first()->id }}">
								Ver tickets cerrados
							</a>]
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					<p class="lead">Actividad de tickets</p>
				</div>
				<div class="card-body" id="ticketsChartByStatus">
					{{-- se agrega el canvas para la grafica --}}
				</div>
			</div>
			@role('admin', 'manager')
				{{-- <div class="card mt-3">
					<div class="card-header">
						<p class="lead">Actividad Por Departamento</p>
					</div>
					<table class="table table-sm table-bordered table-striped m-0" v-if="report.departments.length > 0">
						<thead>
							<tr>
								<td>Departamento</td>
								<td width="20%" class="text-center">Total de Tickets</td>
								<td width="30%">Tickets por ingeniero</td>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(department, index) in report.departments">
								<td><b>@{{ department.name }}</b></td>
								<td class="text-center">@{{ department.total }}</td>
								<td>								
									[<a :href="'#ticketsByAgent'+index" data-toggle="collapse">Tickets por ingeniero</a>]
									<div class="collapse" :id="'ticketsByAgent'+index">
										<div class="card box-shadow-none mt-2">
											<ul class="list-group list-group-flush">
												<li class="list-group-item d-flex" v-for="agent in department.agents">
													<span>@{{ agent.name }}</span>
													<span class="ml-auto">
														[<span class="text-primary">@{{ agent.tickets }}</span>] Tickets
													</span>
												</li>
											</ul>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<p class="m-0" v-else>No hay actividad por departamentos</p>
				</div> --}}
				<div class="card mt-3">
			        <div class="card-header d-flex align-items-center">
			            <p class="lead">Lista de departamentos</p>
			        </div>
			        <div class="accordion" id="departmentsCollapse">
						<div class="card no_shadow b-radius-0" v-for="(department, index) in report.agents_by_departments">
							<div class="card-header">
								<a href="#" 
									class="p-0" 
									data-toggle="collapse" 
									:data-target="'#collapse_deps_'+index" 
									aria-expanded="false" 
									:aria-controls="'collapse_deps_'+index">
									@{{ department.name }}
								</a>
							</div>
							<div :id="'collapse_deps_'+index" 
								class="collapse" 
								:aria-labelledby="'heading_'+index" 
								data-parent="#departmentsCollapse">
								<table class="table table-sm table-striped m-0">
									<thead>
										<tr>
											<td>Agente</td>
											<td>Tickets Asignados</td>
											<td>Tickets Abiertos</td>
											<td>Tickets En Proceso</td>
											<td>Tickets Resueltos</td>
											<td>Tickets Cerrados</td>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(agent, index) in department.agents">
											<td width="25%">
												<b>@{{ agent.agent.first_name + ' ' + agent.agent.last_name }}</b>
											</td>
											<td width="15%" class="text-center">
												@{{ agent.total_tickets }}
											</td>
											<td width="15%">
												<div class="text-center">
							                        <b>@{{ agent.abierto + '%' }} </b>
							                    </div>
												<div class="progress mb-1" style="height: 3px;">
												  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="agent.abierto" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: agent.abierto + '%', 'background-color': 
												  	agent.colors.abierto + ' !important' }">
												  	</div>	
												</div>
											</td>
											<td width="15%">
												<div class="text-center">
							                        <b>@{{ agent.en_proceso + '%' }} </b>
							                    </div>
												<div class="progress mb-1" style="height: 3px;">
												  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="agent.en_proceso" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: agent.en_proceso + '%', 'background-color': 
												  	agent.colors.en_proceso + ' !important' }">
												  	</div>
												</div>
											</td>
											<td width="15%">
												<div class="text-center">
							                        <b>@{{ agent.resuelto + '%' }} </b>
							                    </div>
												<div class="progress mb-1" style="height: 3px;">
												  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="agent.resuelto" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: agent.resuelto + '%','background-color': agent.colors.resuelto + ' !important' }">
												  	</div>
												</div>
											</td>
											<td width="15%">
												<div class="text-center">
							                        <b>@{{ agent.cerrado + '%' }} </b>
							                    </div>
												<div class="progress mb-1" style="height: 3px;">
												  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="agent.cerrado" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: agent.cerrado + '%','background-color': agent.colors.cerrado + ' !important' }">
												  	</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="card mt-3" v-if="report.brands_details_tickets.length > 0">
					<div class="card-header d-flex align-items-center">
			            <p class="lead">Lista de marcas</p>
			        </div>
					<table class="table table-sm table-striped m-0">
						<thead>
							<tr>
								<td>Marca</td>
								<td>Equipos</td>
								<td>Tickets</td>
								<td>Tickets Abiertos</td>
								<td>Tickets En Proceso</td>
								<td>Tickets Resueltos</td>
								<td>Tickets Cerrados</td>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(brand, index) in report.brands_details_tickets">
								<td width="20%">
									<b>@{{ brand.name }}</b>
								</td>
								<td width="5%" class="text-center">
									@{{ brand.total_gears }}
								</td>
								<td width="5%" class="text-center">
									@{{ brand.total_tickets }}
								</td>
								<td width="17%">
									<div class="text-center">
				                        <b>@{{ brand.tickets.abierto + '%' }} </b>
				                    </div>
									<div class="progress mb-1" style="height: 3px;">
									  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="brand.tickets.abierto" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: brand.tickets.abierto + '%', 'background-color': 
									  	brand.colors.abierto + ' !important' }">
									  	</div>	
									</div>
								</td>
								<td width="17%">
									<div class="text-center">
				                        <b>@{{ brand.tickets.en_proceso + '%' }} </b>
				                    </div>
									<div class="progress mb-1" style="height: 3px;">
									  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="brand.tickets.en_proceso" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: brand.tickets.en_proceso + '%', 'background-color': 
									  	brand.colors.en_proceso + ' !important' }">
									  	</div>
									</div>
								</td>
								<td width="17%">
									<div class="text-center">
				                        <b>@{{ brand.tickets.resuelto + '%' }} </b>
				                    </div>
									<div class="progress mb-1" style="height: 3px;">
									  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="brand.tickets.resuelto" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: brand.tickets.resuelto + '%','background-color': brand.colors.resuelto + ' !important' }">
									  	</div>
									</div>
								</td>
								<td width="17%">
									<div class="text-center">
				                        <b>@{{ brand.tickets.cerrado + '%' }} </b>
				                    </div>
									<div class="progress mb-1" style="height: 3px;">
									  	<div class="progress-bar" role="progressbar" v-bind:aria-valuenow="brand.tickets.cerrado" aria-valuemin="0" aria-valuemax="100" v-bind:style="{ width: brand.tickets.cerrado + '%','background-color': brand.colors.cerrado + ' !important' }">
									  	</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			@endrole
		</div>

	</div>
@endsection
@section('scripts')
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
	
	<script src="{{ asset('js/vue.js') }}"></script>
	
	<script>
		var dashboardApp = new Vue({
		    el: '#dashboard_app',
		    data: {
		    	from_date: '{{ date('Y-m-d',strtotime('-7 day')) }}',
		    	to_date: '{{ date('Y-m-d') }}',
		        report: @json($report)
		    },
		    mounted: function() {
		        
		        var t = this;
		        t.set_chart_tickets_by_day();

		    },
		    methods: {
		        reset_report: function(e) {
		            var t = this;
		            var form = $(e.target);
		            var url = form.attr('action');
		            var data = form.serialize();
		            var button = $('button', form);
		            form.find('.error_in_dates').remove();
		            $.get(url, data, function(response) {
		                if (response.error) {
		                    var alert = `<p class="m-0 mt-3 error_in_dates text-center text-danger">
												<b>La fecha inicial debe ser igual o anterior a la fecha final</b>
										</p>`;
		                    form.append(alert);
		                } else {
		                    t.report = response;
		                    t.from_date = response.from_date;
		                    t.to_date = response.to_date;
		                    t.set_chart_tickets_by_day();
		                }
		            }).done(function() {
		                button.removeAttr('disabled').find('.preload').remove();
		            });
		        },
		        set_chart_tickets_by_day: function() {
		            var t = this;
		            var datasets = [];
		            var labels = t.report.labels;
		            var canvas_container = $('#ticketsChartByStatus');
		            var canvas = '<canvas id="chartTicketsByDay" height="80"></canvas>';
		            canvas_container.html('');
		            canvas_container.append(canvas);
		            $.each(t.report.status, function(i, o) {
		                var dataset = {
		                    label: i,
		                    data: o.tickets,
		                    backgroundColor: 'transparent',
		                    borderColor: o.color,
		                    borderWidth: 1
		                }
		                datasets.push(dataset);
		            });
		            var ctx = $('#chartTicketsByDay');
		            var myChart = new Chart(ctx, {
		                type: 'line',
		                data: {
		                    labels: labels,
		                    datasets: datasets
		                },
		                options: {
		                    scales: {
		                        yAxes: [{
		                            ticks: {
		                                beginAtZero: true
		                            }
		                        }]
		                    }
		                }
		            });
		        }
		    }
		});
	</script>
@endsection