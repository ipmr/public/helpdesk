<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GearBrand extends Model
{

	protected $table   = 'gear_brands';
    public $timestamps = true;
    protected $guarded = [];

    
    public function types(){


    	return $this->hasMany('App\BrandType', 'brand_id', 'id');
    }

    public function gears(){

        return $this->hasMany('App\Gear', 'brand_id', 'id');
    }

    public function getGearTypesAttribute(){

    	$get_types = $this->types()->with('type')->get();
    	
    	$types = collect([]);

    	$get_types->each(function($item) use ($types) {

    	    $types->push($item->type);

    	});

    	return $types;

    }

}
