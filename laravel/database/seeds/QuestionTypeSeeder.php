<?php

use App\QuestionType;
use App\QuestionOptions;
use Illuminate\Database\Seeder;

class QuestionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta satisfacción',
            'description' => 'Evalúa la calidad del servicio y/o actividad solicitada y resuelta',
        ])->each(function ($questiontype) {

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Excelente',
                'description' => 'Un servicio completamente satisfactorio, hasta, recomendable',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Muy bueno',
                'description' => 'Un servicio satisfactorio',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Bueno',
                'description' => 'Un servicio satisfactorio, podría mejorar',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Regular',
                'description' => 'El servicio no es recomendable, debe mejorar',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Malo',
                'description' => 'El servicio no cubre en lo absoluto satisfacción, debe mejorar',
            ]);
        });

        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta cerrada Si/No',
            'description' => 'Obtiene resultados concretos',
        ])->each(function ($questiontype) {

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Si',
                'description' => 'Valor de aprobación',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'No',
                'description' => 'Valor de desaprobación',
            ]);
        });

        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta cerrada Verdadero/Falso',
            'description' => 'Obtiene resultados concretos',
        ])->each(function ($questiontype) {

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Verdadero',
                'description' => 'Valor de confirmación',
            ]);

            factory(QuestionOptions::class, 1)->create([
                'question_type_id' => $questiontype->id,
                'name'        => 'Falso',
                'description' => 'Valor de negación',
            ]);
        });

        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta abierta',
            'description' => 'Obtiene observaciones, mejoras y/o quejas del servicio',
        ]);

        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta personalizada elige un dato',
            'description' => 'Define los tipos de opciones y selecciona con un dato como respuesta',
        ]);


        factory(QuestionType::class, 1)->create([
            'name'        => 'Pregunta personalizada elige multiples datos',
            'description' => 'Define los tipos de opciones y selecciona varios datos como respuesta',
        ]);
    }
}
