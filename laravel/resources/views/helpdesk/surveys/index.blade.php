@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Listado de encuestas
            </p>
            <a class="btn btn-primary ml-auto" href="{{ route('surveys.create') }}/">
                <i class="fa fa-plus"></i>
                Nueva Encuesta
            </a>
        </div>
        <div class="card-body">
            @if(count($surveys) > 0)
                <table class="table table-sm table-bordered table-striped m-0">
                    <thead>
                        <tr>
                            <td>Nombre</td>
                            <td>Descripción</td>
                            <td>Departamento</td>
                            <td>Total Preguntas</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($surveys as $survey)
                        <tr>
                            <td><a href="{{ route('surveys.show', $survey) }}/">{{ $survey->name }}</a></td>
                            <td>{{ $survey->description }}</td>
                            <td>
                                @if($survey->department_id)
                                    <a href="{{ route('departments.show', $survey->department_id) }}/">
                                        {{ $survey->departament->name }}
                                    </a>
                                @else
                                    Sin departamento
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('surveys.questions.index', [ 'survey' => $survey->id ] ) }}">
                                    {{ $survey->questions->count() }}
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="m-0">No se han agregado encuestas</p>
            @endif
        </div>
    </div>
</div>
@stop
