{{ csrf_field() }}

<div class="form-row">
	<label class="col-form-label col-sm-4 text-right">
		<span class="text-danger">*</span>
		Módulo / Slot
	</label>
	<div class="form-group col-sm-3">
		<input type="text" name="slot" class="form-control" required>
	</div>
</div>

<div class="form-row">
	<label class="col-form-label col-sm-4 text-right">Descripción:</label>
	<div class="form-group col-sm-4">
		<textarea name="description" rows="4" class="form-control"></textarea>
	</div>
</div>

<div class="form-row form-group d-flex align-items-center">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> # Serie:</label>
	<div class="col-sm-4">
		<input type="text" name="serial_number" class="form-control serial_numbers_input" placeholder="Ingresa el número de serie..." required autocomplete="off">
	</div>
	<div class="col-sm-4">
		<a href="#" v-if="!create_new_gear" @click.prevent="go_to_create_new_gear" class="btn btn-link p-0">
			<i class="fa fa-plus"></i> Nuevo Equipo
		</a>
		<a href="#" v-else @click.prevent="cancel_new_gear_creation" class="text-danger"><i class="fa fa-times"></i> Cancelar</a>
	</div>
</div>

<div v-if="create_new_gear" id="createNewGear">
	
	<input type="hidden" name="new_gear">

	<div class="form-row">
		<div class="col-sm-4 offset-sm-4">
			<p class="text-info">Llena los siguientes campos para dar de alta un nuevo equipo.</p>
		</div>
	</div>
	
	<div class="form-row" @change="get_types">
		<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Marca:</label>
		<div class="form-group col-sm-4">
			<select name="brand_id" id="brands_select" class="form-control" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="(brand, index) in brands" :value="brand.id">@{{ brand.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row" v-if="types">
		<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Tipo:</label>
		<div class="form-group col-sm-4">
			<select name="type_id" class="form-control" @change="get_models" id="types_select" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="type in types" :value="type.id">@{{ type.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row" v-if="models">
		<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Modelo:</label>
		<div class="form-group col-sm-4">
			<select name="model_id" id="models_select" class="form-control" required>
				<option disabled selected value>Elige una opción</option>
				<option v-for="model in models" :value="model.id">@{{ model.name }}</option>
			</select>
		</div>
	</div>

	<div class="form-row">
		<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> # Serie:</label>
		<div class="form-group col-sm-4">
			<input type="text" name="serial_number" class="form-control">
		</div>
	</div>

</div>