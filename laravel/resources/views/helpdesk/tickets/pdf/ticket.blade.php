<style>
	body{
		font-family: 'Helvetica', sans-serif;
		font-size: 14px;
	}
	.text-danger {
	    color: #ed1c24!important;
	}
	h1, h2, h3, h4{
		font-weight: lighter;
	}
	.m-0{
		margin: 0 !important;
	}
	table{
		width: 100%;
		font-size: 12px;
	}
	table tr, table th, table td{
		vertical-align: top;
	}
	.bg-warning{
	background: #eef3f9 !important;
	}
</style>

<table cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
	<tr>
		<td style="vertical-align: middle;">
			<h2 style="margin: 0">
				Ticket <span class="text-danger">#{{ $ticket->ticket_id }}</span>
			</h2>
		</td>
		<td style="vertical-align: middle;" align="right">
			Fecha de exportación: {{ \Carbon\Carbon::now()->format('d M, Y - h:i a') }}
		</td>
	</tr>
</table>

<table cellpadding="5" cellspacing="0" border="1">
	<tr>
		<th>Fecha:</th>
		<td>{{ $ticket->created_at->format('d M, Y - h:i a') }}</td>
	</tr>
	<tr>
		<th>Asunto:</th>
		<td>{{ $ticket->subject }}</td>
	</tr>
	<tr>
		<th>Descripción:</th>
		<td class="bg-warning">{{ $ticket->description }}</td>
	</tr>
	<tr>
		<th>Autor:</th>
		<td>{{ $ticket->author->first_name .' '. $ticket->author->last_name }}</td>
	</tr>
	<tr>
		<th>Compañia:</th>
		<td>
			<a href="{{ route('companies.show', $ticket->company) }}">{{ $ticket->company->name }}</a> <br>
			Direccion: {{ $ticket->company->address .', '. $ticket->company->city .' '. $ticket->company->state .' '. $ticket->company->zip }} <br>
			Teléfono: {{ $ticket->company->phone }} {{ $ticket->company->ext ? '('.$ticket->company->ext.')' : '' }} <br>
			RFC: {{ $ticket->company->rfc }}
		</td>
	</tr>
	@if(optional($ticket->gear)->contract)
	<tr>
		<th>Equipo contratado</th>

		<td>	
			<a href="{{ route('companies.gears.show', [$ticket->company, $ticket->gear->id]) }}" class="text-uppercase">
				{{ $ticket->gear->type->name }} |
				{{ $ticket->gear->brand->name }} |
				{{ $ticket->gear->model->name }}
			</a>
			<br>
			Cobertura: {{ $ticket->gear->contract->name }}
		</td>

	</tr>
	@endif
	<tr>
		<th>Estado actual:</th>
		<td>{{ $ticket->get_status->name }}</td>
	</tr>
	<tr>
		<th>Severidad actual:</th>
		<td>{{ $ticket->get_severity->name }}</td>
	</tr>
	<tr>
		<th>Categoría:</th>
		<td>{{ $ticket->category->name }}</td>
	</tr>
	<tr>
		<th>Departamento:</th>
		<td>{{ $ticket->department ? $ticket->department->name : 'Sin asignar' }}</td>
	</tr>
	<tr>
		<th>Agente:</th>
		<td>{{ $ticket->agent ? $ticket->agent->user->first_name .' '. $ticket->agent->user->last_name : 'Sin asignar' }}</td>
	</tr>
</table>


<table cellpadding="0" cellspacing="0" style="margin-bottom: 20px; margin-top: 20px;">
	<tr>
		<td style="vertical-align: middle;">
			<h2 style="margin: 0">
				Comentarios al cliente
			</h2>
		</td>
	</tr>
</table>

@if(count($ticket->comments()->where('type', 'public')->get()) > 0)
<table cellpadding="5" cellspacing="0" border="1">
	<thead>
		<tr>
			<th>Fecha y hora</th>
			<th>Autor</th>
			<th>Comentario</th>
		</tr>
	</thead>
	<tbody>
		@foreach($ticket->comments()->where('type', 'public')->latest()->get() as $comment)
		<tr>
			<td class="no-wrap">{{ $comment->created_at->format('d M, Y - h:i a') }}</td>
			<td class="no-wrap">
				@if($comment->user->role == 'manager' || $comment->user->role == 'admin')
					Asesor
				@else
					{{ $comment->user->first_name .' '. $comment->user->last_name }}
				@endif
			</td>
			<td>{{ $comment->comment }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
@else
<p class="m-0">No se han agregado comentarios</p>
@endif

<p><small>©{{ \Carbon\Carbon::now()->format('Y') .' '. config('app.name') }}</small></p>