<?php

namespace App\Http\Controllers;

use App\Severity;
use App\SLA;
use App\SlaSeverity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class SLAController extends Controller
{

    public function __construct(){


        $this->middleware('role:admin,manager');


    }

    public function index(Request $request)
    {

        $slas = SLA::where('id', '>', 0);

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $slas = $slas->where(function ($q) use ($query_string, $request) {
                $q = $q->where('name', 'like', '%'.$request->q.'%')
                    ->orWhere('description', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('name', 'like', '%'.$search.'%')
                    ->orWhere('description', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $slas->toSql() );
            }

            if ($request->debug == 2) {
                dd( $slas->get() );
            }
        }

        $slas = $slas->orderBy('id', 'DESC')->get();

        return view('helpdesk.sla.index', compact('slas'));

    }

    public function create()
    {

        $severities = Severity::orderBy('id')->get();

        return view('helpdesk.sla.create', compact('severities'));

    }

    public function store(Request $request)
    {

        #se valida la informacion del SLA
        $validate = $this->validate_sla_data($request);

        #se guarda el SLA
        $sla = $this->create_sla($request);

        #se agregan los tiempos de cada severidad al nuevo SLA
        $this->add_severities_to_sla($request, $sla);

        #se retorna con mensaje de confirmación
        return redirect()->route('sla.show', $sla)->with('msg', 'Se ha creado un nuevo acuerdo de servicio.');

    }

    public function create_sla($request)
    {

        $sla              = new SLA();
        $sla->name        = $request->name;
        $sla->description = $request->description;
        $sla->save();

        return $sla;

    }

    public function add_severities_to_sla($request, $sla)
    {

        foreach ($request->severity_id as $key => $severity) {

            $severity = Severity::find($severity);

            $sla_severity                 = new SlaSeverity();
            $sla_severity->sla_id         = $sla->id;
            $sla_severity->severity_id    = $severity->id;
            $sla_severity->respond_number = $request->respond_number[$key];
            $sla_severity->respond_format = $request->respond_format[$key];
            $sla_severity->resolve_number = 1;
            $sla_severity->resolve_format = "i";

            $sla_severity->save();

        }

    }

    public function show(SLA $sla)
    {
        return view('helpdesk.sla.show', compact('sla'));
    }

    public function edit(SLA $sla)
    {

        $severities = Severity::orderBy('id')->get();

        return view('helpdesk.sla.edit', compact('sla', 'severities'));

    }

    public function update(Request $request, SLA $sla)
    {

        #se valida la informacion del SLA
        $validate = $this->validate_sla_data($request, $sla);

        #se actualiza la informacion del SLA
        $sla->update($request->only(['name', 'description']));

        #se actualizan las severidades del SLA
        foreach($sla->severities as $key => $severity){
            $severity->update([
                'respond_number' => $request->respond_number[$key],
                'respond_format' => $request->respond_format[$key],
                'resolve_number' => 1,
                'resolve_format' => 'i'
            ]);
        }

        #se retorna con mensaje de confirmación
        return redirect()->route('sla.show', $sla)->with('msg', 'El acuerdo de servicio (SLA) ha sido actualizado.');

    }

    public function destroy(SLA $sla)
    {
        //
    }

    public function validate_sla_data($request, $sla = null)
    {

        $validate = Validator::make($request->all(), [
            'name'             => ['required','string',Rule::unique('sla')->ignore($sla ? $sla->id : '')],
            'description'      => 'string|nullable',
            'severity.*'       => 'numeric|exists:severities,id',
            'respond_number.*' => 'required|numeric|nullable',
            'respond_format.*' => 'required|in:i,h,d,m|nullable'
        ], [
            'name.required'           => 'El nombre del acuerdo de servicio es requerido',
            'name.unique'             => 'El nombre del acuerdo de servicio no esta disponible',
            'respond_number.required' => 'El tiempo de respuesta es requerido',
            'respond_number.numeric'  => 'El tiempo de respuesta debe ser numérico',
            'respond_format.required' => 'El formato de tiempo de respuesta es requerido',
            'respond_format.in'       => 'El formato de tiempo de respuesta no es válido'

        ])->validate();

    }
}
