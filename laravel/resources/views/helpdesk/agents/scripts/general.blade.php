<script>
	
	var agent_app = new Vue({


		el: '#agentApp',


		data: {

			groups: @isset($agent) 
						@json(optional($agent->department)->groups)
					@else
						null
					@endisset,
			loading_groups: false,
			@isset($agent)

			@if ( $agent->id == optional( optional( $agent->department )->supervisor )->id )
			
			agents: @json(optional($agent->department)->agents()->whereNotIn('id',[$agent->id])->with('user')->get()),

			@endif

			@endisset

		},

		mounted: function(){



		},


		methods: {


			@isset($agent)

			current_group: function(){

				var current_group_id = "{{$agent->group ? $agent->group->id : null}}";

				return current_group_id;

			},

			@endisset

			get_department_groups: function(e){


				var t = this;

				var select = $(e.target);

				var value = select.val();

				var route = "{{ route('departments.groups.index', "#department_id") }}";

				route = route.replace('#department_id', value);

				t.loading_groups = true;

				$.getJSON(route, function(response){

					t.groups = response;

					t.loading_groups = false;

				});


			},
			remove_agent: function(e)
			{
				@isset($agent)

				@if ( $agent->id == optional( optional( $agent->department )->supervisor )->id )

				e.preventDefault()

				$('#assign_new_supervisor').modal('show')

				@endif
				
				@endisset
			}

		}


	});

</script>