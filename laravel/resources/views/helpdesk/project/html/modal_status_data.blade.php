@component('helpdesk.components.modal', [
    'id'    => 'modal_status_data',
    'title' => 'Actividad del proyecto',
    'size'  => 'modal-lg',
    'extra_body_class'=> 'p-0',
])

    @slot('body')
        <div class="list-group list-group-flush" v-for="history in histories">
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <small class="ml-auto">@{{ history.created_at }}</small>
                </div>
                <h5 class="mb-1">@{{ history.description }}</h5>
                <small>Proyecto @{{ condition[ history.condition ] }}</small>
            </div>
        </div>
    @endslot

@endcomponent