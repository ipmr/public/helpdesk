@component('helpdesk.components.modal', [
    'id'    => 'last_comment_to_company_ticket',
    'title' => 'Evento de Resolución'
])

    @slot('body')
            
        <form action="{{ route('public-comment', $ticket->ticket_id) }}" id="lastCommentForm" @submit.prevent="send_last_event_ticket">
            
            {{ csrf_field() }}

            <div class="form-group">
                <label>Comentario:</label>
                <textarea id="last_comment" name="comment" class="form-control" placeholder="Escribe aquí..." rows="5" required autofocus></textarea>
            </div>

            <div class="form-group">
                <label>Tipo de atención:</label>
                <select id="last_attention_type" name="atention" class="form-control" required>
                    <option disabled selected value="">Elige una opción</option>
                    <option value="Remota">Remota</option>
                    <option value="En sitio">En sitio</option>
                </select>
            </div>
        </form>

    @endslot
    
    @slot('footer')
        <button class="btn btn-link" id="skip_comment_button" @click.prevent="skip_comment">
            Omitir evento
        </button>
        <button type="submit" class="btn btn-success" id="last_comment_button"  form="lastCommentForm">
            <i class="fa fa-paper-plane"></i>
            Resolver
        </button>

    @endslot

@endcomponent