@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		
		<div class="card-header d-flex align-items-center">
			<a class="btn btn-link p-0" href="{{ route('departments.index') }}/">
				<i class="fa fa-angle-left fa-sm mr-1"></i>
				Regresar
			</a>
			<a class="btn btn-primary ml-auto" href="{{ route('departments.edit', $department) }}/">
				<i class="fa fa-pencil-alt fa-xs"></i>
				Editar
			</a>
		</div>

		<table class="table table-striped m-0">
			<tr>
				<th width="15%">Nombre</th>
				<td>{{ $department->name }}</td>
			</tr>

			<tr>
				<th>Descripción</th>
				<td>{{ $department->description }}</td>
			</tr>

			<tr>
				<th>Supervisor</th>
				<td>
					@if(optional($department->supervisor))
						<a href="{{ route('agents.show', $department->supervisor) }}/">
							{{ optional( optional( $department->supervisor )->user )->FullName }}
						</a>
					@else
						Sin supervisor
					@endif
				</td>
			</tr>

			<tr>
				<th>Agentes</th>
				<td>

					@if ($department->agents()->count() > 0)
						@foreach($department->agents()->where('id', '<>', $department->supervisor->id)->get() as $agent)
							<a href="{{ route('agents.show', $agent) }}/">
								{{ $agent->full_name }}
							</a> 
							@if(!$loop->last) <br> @endif
						@endforeach
					@endif

				</td>
			</tr>
		</table>

	</div>

	
	<div class="card mt-3" id="departmentsApp">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Grupos</p>
			<a href="#createNewGroup" class="btn btn-primary ml-auto" data-toggle="collapse"><i class="fa fa-plus"></i> Nuevo grupo</a>
		</div>


		<div class="collapse" id="createNewGroup">
			
			<div class="row">
				<div class="col-sm-5 mx-auto">
					<form action="{{ route('departments.groups.store', $department) }}" class="mt-3 mb-3" @submit.prevent="createNewGroup">
						{{ csrf_field() }}
						<div class="form-group">
							<label>Nombre del grupo:</label>
							<input type="text"name="name" autofocus class="form-control" required>
						</div>

						<div class="form-group">
							<label>Supervisor del grupo:</label>
							<select name="supervisor_id" class="form-control" required>
								<option disabled selected value>Elige una opción</option>
								@foreach($department->agents as $agent)
								<option value="{{ $agent->id }}">{{ $agent->full_name }}</option>
								@endforeach
							</select>
						</div>

						<button class="btn btn-success"><i class="fa fa-check"></i> Crear Grupo</button>
					</form>
				</div>
			</div>

		</div>

		<table v-if="groups.length > 0" class="table table-striped m-0">
			<thead>
				<tr>
					<th>Grupo</th>
					<th>Supervisor</th>
					<th class="text-center">Agentes</th>
					<th class="text-center">Editar</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="(group, index) in groups">
					<td width="30%">@{{ group.name }}</td>
					<td width="30%">
						<span v-if="group.supervisor">
							@{{ 
								group.supervisor.user.first_name + 
								' ' + 
								group.supervisor.user.last_name 
							}}
						</span>

						<span v-else>
							Sin supervisor
						</span>
					</td>
					<td width="30%" class="text-center">
						@{{ group.agents.length }}
						Agente@{{ group.agents.length == 1 ? '' : 's' }}
					</td>
					<td class="text-center">
						<a 	href="#editDepartmentGroupModal" 
							data-toggle="modal"
							@click.prevent="edit_group(index)">
							<i class="fa fa-pencil-alt"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>

	
		<div v-else class="card-body">
			<p class="m-0">No se han agregado grupos aun</p>
		</div>


		@component('helpdesk.components.modal', [

			'id' 	=> 'editDepartmentGroupModal',
			'title' => 'Editar información de grupo',
			'size' 	=> 'modal-lg'

		])
			
			@slot('body')

				<form :action="updateDepartmentRoute()" id="updateGroupForm" v-if="group" method="post" @submit.prevent="update_group_post">
					
					{{ method_field('PATCH') }}

					{{  csrf_field() }}

					<div class="form-group">
						<div class="form-row">
							<label class="col-sm-4 text-right col-form-label">Grupo:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="name" :value="group.name">
							</div>
						</div>
					</div>
	

					<div class="form-group">
						<div class="form-row">
							<label class="col-sm-4 text-right col-form-label">Supervisor:</label>
							<div class="col-sm-4">
								<select name="agents[]" 
									class="form-control"
									:value="group.supervisor ? group.supervisor.id : ''">
									
							
								<option disabled selected value>
									Elige una opción
								</option>

								@foreach($department->agents as $agent)
									
									<option value="{{ $agent->id }}">
										{{ $agent->full_name }}
									</option>

								@endforeach


								</select>
							</div>
						</div>
					</div>

			
					<div class="form-row">
						<div class="col-sm-4 mx-auto">
							<label>
								<b>Agentes en el grupo</b>
							</label>


							@foreach($department->agents as $agent)

								<div class="custom-control custom-checkbox">
								  
								  <input type="checkbox" 
								  		 class="custom-control-input"
								  		 name="agents[]"
								  		 value="{{ $agent->id }}" 
								  		 id="agentCheckBox{{ $agent->id }}"
								  		 :checked="check_if_agent_is_on_group({{$agent->id}})">


								  <label class="custom-control-label" 
								  		 for="agentCheckBox{{ $agent->id }}">
								  		
								  		{{ $agent->full_name }}

								  </label>

								</div>


							@endforeach


						</div>
					</div>

				</form>

			@endslot

			@slot('footer')

				<button type="submit" form="updateGroupForm" class="btn btn-success">
					<i class="fa fa-check"></i>
					Actualizar Grupo
				</button>

			@endslot
	

		@endcomponent
	

	</div>


</div>
@stop


@section('scripts')
	
	<script>
		var departments_app = new Vue({

			el: "#departmentsApp",

			data: {

				groups: @json($department->groups()->with(['supervisor.user', 'agents'])->latest()->get()),
				group: null

			},

			mounted: function(){


				


			},

			methods: {

				agent_route: function(agent_id){

					var route = "{{ route("agents.show", '#agent_id') }}";

					route = route.replace("#agent_id", agent_id);

					return route;

				},


				updateDepartmentRoute: function(){


					var t = this;

					var route = "{{ route('departments.groups.update', [$department, "#group_id"]) }}";

					route = route.replace("#group_id", t.group.id);

					return route;


				},


				update_group_post: function(e){


					var t = this;

					var form = $(e.target);

					var form_id = form.attr('id');

					var data = form.serialize();

					var route = form.attr('action');

					var btn = $("button[form='"+form_id+"']");

					btn.attr('disabled', 'disabled');

					$.post(route, data, function(response){


						btn.removeAttr('disabled');


					}).done(function(response){


						if(response.type == 'error'){


							alert(response.errors.join('\n'));


						}else{


							$('.modal').modal('hide');

							t.groups = response.groups;

							
						}


					});


				},


				createNewGroup: function(e){

					var t = this;
					var form = $(e.target);
					var data = form.serialize();
					var route = form.attr('action');
					var btn = $('button', form);

					$.post(route, data, function(response){

						btn.removeAttr('disabled');

					}).done(function(response){

						if(response.type == 'error'){

							alert(response.errors.join('\n'));

						}else{

							form[0].reset();

							t.groups = response;

							$('#createNewGroup').collapse('hide');

						}

					});

				},

				edit_group: function(index){


					var t = this;


					t.group = t.groups[index];


				},

				check_if_agent_is_on_group: function(agent_id){

					var t = this;

					var agents = t.group.agents;

					var response = false;

					$.each(agents, function(i, o){

						if(o.agent_id == agent_id){

							response = true;

						}

					});

					return response;

				}

			}

		});
	</script>

@stop
