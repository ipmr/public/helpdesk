<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

	protected $table   = 'tasks';
    public $timestamps = true;
    protected $guarded = [];
    
	public function ticket(){

		return $this->belongsTo('App\Ticket', 'ticket_id', 'ticket_id');

	}

	public function agent(){

		return $this->belongsTo('App\Agent', 'agent_id', 'id');

	}

    public static function diff_time($str_start_time, $str_end_time, $number_format = false)
    {

        $time_elapsed = null;

        if ($str_start_time) {

            $start_time = \Carbon\Carbon::parse($str_start_time);

            if ($number_format) {
                $time_elapsed = 0;

                if ($str_end_time) {

                    $end_time     = \Carbon\Carbon::parse($str_end_time);
                    $time_elapsed = ($start_time->diffInMinutes($end_time) / 60);
                    $time_elapsed = round($time_elapsed, 2);

                }

            } else {

                if ($str_end_time) {

                    $end_time     = \Carbon\Carbon::parse($str_end_time);
                    $time_elapsed = date('H \h\r\s i \m\i\n', mktime(0, $start_time->diffInMinutes($end_time)));
                    $time_elapsed = str_replace('00 hrs ', '', $time_elapsed);

                }

            }

        }

        return $time_elapsed;
    }


    public function events(){


        return $this->hasMany('App\TaskEvent', 'task_id', 'id');
        

    }

}
