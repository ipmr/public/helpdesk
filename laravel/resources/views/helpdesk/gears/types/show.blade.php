@extends('layouts.master')
@section('content')

<div class="container" id="typeApp">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('brands.show', $brand) }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<table class="table table-striped m-0">
			<thead>
				<tr style="border-bottom: 1px solid #f2f2f2" v-cloak class="bg-secondary">
					<td style="vertical-align: middle;">
						<div v-show="edit_type == true">
							<form action="{{ route('types.update', $type) }}" method="post" class="d-flex align-items-center">
								{{ csrf_field() }}
								{{ method_field('PATCH') }}
								<input type="text" class="form-control mr-3 text-uppercase" id="update_type_name_input" name="name" value="{{ $type->name }}">
								<button type="submit" class="btn btn-success ml-auto">
									<i class="fa fa-check"></i> Actualizar
								</button>
								<a href="#" class="btn btn-link text-danger px-0 ml-3" @click.prevent="cancel_type_name_edition">
									<i class="fa fa-times"></i> Cancelar
								</a>
							</form>
						</div>
						<div v-if="!edit_type" class="d-flex align-items-center">
							<b><a href="{{ route('brands.show', $brand) }}">{{ $brand->name }}</a> / {{ $type->name }}</b>
							<span class="ml-auto">
								<a href="#" class="btn btn-info m-0" @click.prevent="edit_type_name">
									<i class="fa fa-pencil-alt"></i> Editar
								</a>
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<th colspan="2">
						<div class="d-flex align-items-center">
							Modelos
							<form action="{{ route('brands.types.show', [$brand, $type]) }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
				                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
				                    value="{{ $_GET['q'] }}"
				                @endisset>
				                <button class="btn btn-success" type="submit">
				                    <i class="fa fa-search"></i> Buscar
				                </button>
				            </form>
							<a href="#add_models_collapse" data-toggle="collapse" class="btn btn-primary ml-2">
								<i class="fa fa-plus"></i> Agregar Modelos
							</a>
						</div>
					</th>
				</tr>

				<tr>
					<td class="py-0" colspan="2">
						<div id="add_models_collapse" class="collapse">

							<div class="py-3">
								
								<div class="form-row d-flex align-items-center">
									<label class="col-form-label col-sm-3 text-right">Tipo:</label>
									<form class="col-sm-6 d-flex align-items-center" @submit.prevent="save_and_add_more">
										<input type="text" name="name" id="new_model_input" class="form-control text-uppercase" autocomplete="off" autofocus required>
										<button class="btn btn-info ml-2">
											<i class="fa fa-plus" style="margin: 0 !important"></i>
										</button>
									</form>
								</div>

								<div class="form-row" v-if="models.length > 0">
									<div class="col-sm-6 mx-auto">
										<ul class="list-group mb-3 mt-3">
											<li class="list-group-item d-flex align-items-center" v-for="(model, index) in models">
												<span class="text-uppercase">@{{ model }}</span>
												<a href="#" class="btn btn-link text-danger pr-0 ml-auto" @click.prevent="remove_model_from_list(index)">
													<i class="fa fa-times"></i> 
												</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="form-row" v-show="models.length > 0">
									<div class="form-group col-sm-9 ml-auto">
										<form action="{{ route('brands.types.models.store', [$brand, $type]) }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" :value="models" name="models_list">
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check"></i>
												Guardar Modelos de Equipo
											</button>
										</form>
									</div>
								</div>

							</div>

						</div>
					</td>
				</tr>
			</thead>

			<tbody>
				@if(count($models) > 0)
					@foreach($models as $model)
					
					<tr>
						<td colspan="2" class="d-flex">
							<a href="{{ route('brands.types.models.show', [$brand, $type, $model]) }}/">{{ $model->name }}</a>
						</td>
					</tr>

					@endforeach
				@else
					<tr>
						<td colspan="2">
							No se han agregado modelos de equipo a {{ $brand_type->brand->name .' / '. $brand_type->type->name }}
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('scripts')
	
	<script>

		var type_app = new Vue({

			el: '#typeApp',
			data: {
				edit_type: false,
				models: [],
				existing_models: @json($brand_type->models()->get()->pluck('name')),
				all_types: @json($brand_type->models()->get()->pluck('name'))
			},
			methods: {
				edit_type_name: function(){

					var t = this;
					t.edit_type = true;
					setTimeout(function(){

						$('#update_type_name_input').focus();

					}, 150);

				},

				cancel_type_name_edition: function(){

					var t = this;
					t.edit_type = false;

				},

				save_and_add_more: function(e){

					var t = this;
					var form = $(e.target);
					var input = $('#new_model_input');
					var value = input.val().toUpperCase();
					var btn = $('button', form);

					var exists = false;

					$.each(t.models, function(i,o){

						if(o == value){

							exists = true;

						}

					});

					$.each(t.existing_models, function(i,o){

						if(o == value){

							exists = true;

						}

					});

					if(!exists){
						t.models.push(value.toUpperCase());
						input.val('').focus();
					}else{
						alert('El modelo de equipo ya ha sido agregado.');
					}

					setTimeout(function(){
						btn.removeAttr('disabled');
					}, 300);

				},

				remove_model_from_list: function(index){

					this.models.splice(index, 1);

				}
			}

		})

	</script>

@stop
