<?php

namespace App\Http\Middleware;

use Closure;

class UnderConstructionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        return redirect()->route('under-construction');


        // return $next($request);


    }
}
