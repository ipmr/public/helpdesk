<div class="form-row">
    <div class="col-sm-8 ml-auto">
        <label><b>Información del estado</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

{{ csrf_field() }}
<div class="row form-row">
    <label class="col-sm-4 col-form-label text-right">
    	<span class="text-danger">*</span>
    	Nombre:
    </label>
    <div class="col-sm-6">
        <div class="form-group m-0">
            <input type="text" name="name" value="{{ $severity->name or old('name') }}" class="form-control" autofocus autocomplete="off" required @isset($severity) {{ $severity->static ? 'disabled' : '' }} @endisset>
			@if(isset($severity) && $severity->static)
            <span class="help-block d-block mt-2 text-danger">El nombre de la severidad no puede ser modificado</span>
            @endif
        </div>
    </div>        
</div>