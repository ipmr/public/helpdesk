go_to_step: function(number){

	var t = this;

	var box = $('[data-step="'+number+'"]');

	var project_progress_bar = $('#projectProgressBar');

	var pos = box.position().left;

	project_progress_bar.css({width: pos});

	t.save_project_status(number);

	setTimeout(function(){

		t.step = number;

	}, 500);

},