<?php

namespace App\Http\Controllers;

use App\Department;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::paginate(10);

        return view('helpdesk.surveys.index', compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::select('id', 'name')->get();

        return view('helpdesk.surveys.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_survey_data($request);

        $survey = new Survey();

        $survey->name          = $request->name;
        $survey->description   = $request->description;
        $survey->department_id = $request->department_id;

        $survey->save();

        return redirect()
            ->route('surveys.show', $survey )
            ->with('msg', 'Se ha creado la encuesta ' . $survey->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        return view('helpdesk.surveys.show', compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        $departments = Department::select('id', 'name')->get();


        return view('helpdesk.surveys.edit', compact('survey', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        $this->validate_survey_data($request);

        $survey->name          = $request->name;
        $survey->description   = $request->description;
        $survey->department_id = $request->department_id;

        $survey->save();

        return redirect()
            ->route('surveys.show', $survey )
            ->with('msg', 'Se ha actualizado la encuesta ' . $survey->name . ' exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        //
    }

    public function validate_survey_data($request, $survey = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required'          => 'El nombre de la encuesta es requerido',
            'name.unique'            => 'El nombre de la encuesta no esta disponible',
            'description.required'   => 'La descripcion de la encuesta es requerido',
            'department_id.required' => 'El departamento es requerido',
            'department_id.exists'   => 'El departamento seleccionado no es válido',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), [

            'name'          => ['required', 'string', Rule::unique('surveys')->ignore($survey ? $survey->id : '')],
            'description'   => 'required|string',
            'department_id' => 'required|exists:departments,id',

        ], $msgs)->validate();

    }
}
