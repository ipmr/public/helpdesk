@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">
                Listado de preguntas
            </p>
            <a class="btn btn-primary ml-auto" href="{{ route('surveys.questions.create', ['survey' => $survey->id ]) }}/">
                <i class="fa fa-plus"></i>
                Nueva pregunta
            </a>
        </div>
        <div class="card-body">
            @if(count($questions) > 0)
                <table class="table table-sm table-bordered table-striped m-0">
                    <thead>
                        <tr>
                            <td>Pregunta</td>
                            <td>Tipo de pregunta</td>
                            <td>Es pregunta abierta</td>
                            <td>Opciones</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questions as $question)
                        <tr>
                            <td><a href="{{ route('surveys.questions.show', ['survey' => $survey->id, 'question' => $question->id ]) }}/">{{ $question->question }}</a></td>
                            <td>{{ $question->question_type->name }}</td>
                            <td>{{ ($question->is_open)?'Si':'No' }}</td>
                            <td>{{ $question->custom_options }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="m-0">No se han agregado encuestas</p>
            @endif
        </div>
    </div>
</div>
@stop
