<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneUser extends Model
{
    protected $table   = 'zone_users';
    public $timestamps = true;
    protected $guarded = [];

    public function user(){

    	return $this->belongsTo('App\User', 'user_id', 'id');

    }

    public function contact(){

    	return $this->belongsTo('App\CompanyUser', 'user_id', 'user_id');

    }

    public function company_user_type(){

        
        return $this->belongsTo('App\CompanyUserType', 'role', 'id');
        
    }

    public function getFullNameAttribute(){

        return $this->contact->user->full_name;

    }

    public function getEmailAttribute(){

        return $this->contact->user->email;

    }

    public function getJobtitleAttribute(){

        return $this->contact->jobtitle;

    }

    public function getPhoneAttribute(){

        return $this->contact->phone;

    }

    public function getMobileAttribute(){

        return $this->contact->mobile;

    }


    public function company(){

    	return $this->belongsTo('App\Company', 'zone_id', 'id');

    }

}
