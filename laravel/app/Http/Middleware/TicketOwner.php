<?php

namespace App\Http\Middleware;

use Closure;

class TicketOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        $ticket = \App\Ticket::where('ticket_id', $request['ticket'])->firstOrFail();

        if ($user->role == 'company') {

            $contacts_ids = collect([]);

            $parents = $ticket->company->all_parents();

            foreach ($parents as $parent) {
                foreach($parent->contacts as $contact){

                    $contacts_ids->push($contact->user->id);

                }
            }

            if (in_array($user->id, $contacts_ids->toArray())) {

                return $next($request);

            }else {

                return redirect()->back()->withErrors(['Acceso Denegado']);

            }

        } elseif ($user->role == 'agent') {


            if($user->agent->as_manager){

                return $next($request);
                
            } elseif ($user->agent->supervisor_in_groups) {

                $groups = $user->agent->supervisor_in_groups;

                foreach ($groups as $key => $group) {
                    
                    foreach($group->agents as $agent){

                        if($agent->agent_id == $ticket->agent_id){

                            return $next($request);

                        }

                    }

                }

            } elseif ($user->agent->id == $ticket->agent_id) {

                return $next($request);

            } elseif ($user->agent->id == optional($ticket->department)->supervisor_id) {

                return $next($request);

            } elseif ($user->agent->tasks()->where('ticket_id', $ticket->ticket_id)->count() > 0) {

                return $next($request);

            } else {

                return redirect()->back()->withErrors(['Acceso Denegado']);

            }

        }

        return $next($request);
    }
}
