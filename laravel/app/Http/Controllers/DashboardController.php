<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Department;
use App\Status;
use App\GearBrand;
use App\Gear;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        $user = auth()->user();

        if (!in_array($user->role, ['admin', 'manager'])) {

            return redirect()->route('tickets.index');
        }

        $from_date = $request->from_date ? Carbon::parse((string)$request->from_date)->format('Y-m-d') : Carbon::today()->subDays(7)->format('Y-m-d');
        $to_date   = $request->to_date ? Carbon::parse((string)$request->to_date)->format('Y-m-d') : Carbon::today()->format('Y-m-d');

        if($request->ajax()){

            if($from_date > $to_date){
                
                $response = [
                    'error' => 'La fecha de inicio debe ser igual o anterior a la fecha final.'
                ];

                return response()->json($response);
            }            
        }

        #se genera el reporte
        $report = $this->generate_report($user, $from_date, $to_date);

        if ($request->ajax()) {

            #si la peticion es en ajax
            return response()->json($report);
        }


        return view('helpdesk.dashboard', compact('report'));
    }

    public function generate_report($user, $from_date, $to_date)
    {
        $days                    = Carbon::parse($from_date)->diffInDays(Carbon::parse($to_date));
        $statuses                = Status::get();
        $agents_details_tickets  = [];
        $brands_details_tickets  = [];

        $tickets_by_status      = $this->order_tickets_by_status($statuses, $from_date, $to_date);
        // $tickets_by_department = $this->order_tickets_by_department($from_date, $to_date);
        $agents_details_tickets = $this->get_agent_details_tickets($statuses, $from_date, $to_date);
        $brands_details_tickets = $this->get_brands_details_tickets($statuses, $from_date, $to_date);


        $from_date = Carbon::parse($from_date)->format('d/m/Y');
        $to_date = Carbon::parse($to_date)->format('d/m/Y');


        $response = [
            'labels'                 => $tickets_by_status['labels'],
            'total'                  => $tickets_by_status['total_tickets_by_status'],
            'status'                 => $tickets_by_status['tickets_by_status'],
            'from_date'              => $from_date,
            'to_date'                => $to_date,
            // 'departments'            => $tickets_by_department,
            'agents_by_departments'  => $agents_details_tickets,
            'brands_details_tickets' => $brands_details_tickets,
        ];

        return $response;
    }

    public function order_tickets_by_status($statuses, $from_date, $to_date)
    {
        $from_date = date('Y-m-d',strtotime($from_date));
        $to_date = date('Y-m-d',strtotime($to_date));
        $tickets_by_status       = [];
        $labels                  = [];
        $total_tickets_by_status = [];


        foreach ($statuses as $status) {
            $help_day = $from_date;

            $tickets_by_status[ $status->name ]['color']   = $status->color;
            $tickets_by_status[ $status->name ]['tickets'] = [];
            $total_tickets_by_status[$status->name] = 0;

            while ($help_day <= $to_date) {

                $total_tickets = Ticket::withTrashed()
                        ->where('status_id', $status->id)
                        ->whereDate('created_at', $help_day)
                    ->count();

                $tickets_by_status[ $status->name ]['tickets'][] = $total_tickets;
                $total_tickets_by_status[$status->name] += $total_tickets;

                $labels[ $help_day ] = $help_day;

                $help_day = date('Y-m-d',strtotime('+1 day '.$help_day));
            }
        }

        return [
            'total_tickets_by_status' => $total_tickets_by_status,
            'tickets_by_status'       => $tickets_by_status,
            'labels'                  => array_values($labels),
        ];
    }

    public function order_tickets_by_department($from_date, $to_date)
    {
        $get_departments = Department::get();
        $departments     = collect();
        foreach ($get_departments as $department) {
            $dep = [
                'id' => $department->id,
                'name' => $department->name,
                'total'  => $department
                    ->tickets()
                    ->whereDate('created_at', '>=', $from_date)
                    ->whereDate('created_at', '<=', $to_date)
                    ->count(),
                'agents' => [],
            ];
            foreach ($department->agents as $agent) {
                $a = [
                    'id'      => $agent->id,
                    'tickets' => $agent
                        ->tickets()
                        ->whereDate('created_at', '>=', $from_date)
                        ->whereDate('created_at', '<=', $to_date)
                        ->count(),
                    'name'    => $agent->user->first_name .' '. $agent->user->last_name,
                ];
                array_push($dep['agents'], $a);
            }
            $departments->push($dep);
        }

        return $departments;
    }

    public function get_agent_details_tickets($statuses, $from_date, $to_date)
    {
        $agents_details_tickets = [];

        $departments    = Department::get();
        foreach ($departments as $department) {
            $department_details = [];
            $department_details['name'] = $department->name;

            foreach ($department->agents as $agent) {
                $agent_data = [];
                $agent_data['agent'] = $agent->user;
                $agent_data['total_tickets'] = $agent->tickets()
                        ->whereDate('created_at', '>=', $from_date)
                        ->whereDate('created_at', '<=', $to_date)
                    ->count();
                foreach ($statuses as $status) {

                    $total_status = $agent->tickets()
                            ->whereDate('created_at', '>=', $from_date)
                            ->whereDate('created_at', '<=', $to_date)
                            ->where('status_id', $status->id)
                        ->count();

                    $total_percentage = 0;
                    if ($total_status > 0 && $agent_data['total_tickets'] > 0) {
                        $total_percentage = (($total_status * 100)/$agent_data['total_tickets']);
                    }

                    $agent_data[ str_slug($status->name, '_') ] =  number_format( $total_percentage, 2 );

                    $agent_data['colors'][ str_slug($status->name, '_') ] = $status->color;
                }

                $department_details['agents'][] = $agent_data;
            }

            $agents_details_tickets[] = $department_details;
        }

        return $agents_details_tickets;
    }

    public function get_brands_details_tickets($statuses, $from_date, $to_date)
    {
        $brands_details_tickets = [];
        
        $gear_brands = GearBrand::get();
        foreach ($gear_brands as $gear_brand) {

            if ( $gear_brand->gears()->count() ) {
                $gears = [];
                foreach ($gear_brand->gears as $gear) {
                    $gears[] = $gear->id;
                }
                $brand_detail = [];

                $brand_detail['name']          = $gear_brand->name;
                $brand_detail['total_gears']   = $gear_brand->gears()->count();
                $brand_detail['total_tickets'] = Ticket::whereIn( 'gear_id', $gears )
                        ->whereDate( 'created_at', '>=', $from_date )
                        ->whereDate( 'created_at', '<=', $to_date )
                    ->count();

                foreach ($statuses as $status) {
                    $total_tickets_status = Ticket::whereIn( 'gear_id', $gears )
                            ->whereDate( 'created_at', '>=', $from_date )
                            ->whereDate( 'created_at', '<=', $to_date )
                            ->where( 'status_id', $status->id )
                        ->count();

                    $percentage = 0;
                    if ( $brand_detail['total_tickets'] > 0 && $total_tickets_status > 0 ) {
                        $percentage = (($total_tickets_status * 100)/$brand_detail['total_tickets']);
                    }

                    $brand_detail[ 'tickets'][ str_slug( $status->name, '_' ) ] = number_format( $percentage, 2 );
                    $brand_detail[ 'colors'][ str_slug( $status->name, '_' ) ]  = $status->color;
                }

                $brands_details_tickets[] = $brand_detail;
            }
        }

        return $brands_details_tickets;
    }
}
