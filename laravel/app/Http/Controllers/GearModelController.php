<?php

namespace App\Http\Controllers;

use App\GearBrand;
use App\GearModel;
use App\GearType;
use Validator;
use Illuminate\Http\Request;

class GearModelController extends Controller
{

    public function index(GearBrand $brand, GearType $type)
    {

        $brand_type = $brand->types()->where('type_id', $type->id)->first();

        $models = $brand_type->models;

        if(request()->ajax()){

            return response()->json($models);

        }

    }

    public function create()
    {
        //
    }

    public function store(Request $request, GearBrand $brand, GearType $type)
    {
        

        $brand_type = $brand->types()->where('type_id', $type->id)->first();

        $this->add_models_to_brand_type($request, $brand_type);

        return redirect()->back()->with("msg", "Se han agregado nuevos modelos a {$brand->name}/{$type->name} exitosamente.");

    }

    public function add_models_to_brand_type($request, $brand_type){

        $models = collect(explode(',', $request->models_list));

        $models->each(function($item) use ($brand_type) {

            $exists = $brand_type->models()->where('name', $item)->first();

            if(!$exists){

                $model = new GearModel();
                $model->brand_type_id = $brand_type->id;
                $model->name = strtoupper($item);
                $model->save();
                
            }


        });

    }

    public function show(GearBrand $brand, GearType $type, GearModel $model)
    {

        return view('helpdesk.gears.models.show', compact('brand', 'type', 'model'));
        
    }

    public function edit(GearModel $model)
    {
        //
    }

    public function update(Request $request, GearBrand $brand, GearType $type, GearModel $model)
    {

        $this->validate_model_data($request);

        $model->name = strtoupper($request->name);
        $model->save();

        return redirect()->back()->with('msg', "El modelo {$model->name} ha sido modificado exitosamente");

    }

    public function destroy(GearModel $model)
    {
        //
    }

    public function validate_model_data($request){

        $validate = Validator::make($request->all(), [
            'name' => 'required|unique:gear_models'
        ], [

            'name.required' => 'El nombre del modelo es requerido',
            'name.unique' => 'El nombre del modelo no esta disponible'
        ])->validate();

    }
}
