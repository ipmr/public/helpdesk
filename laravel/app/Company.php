<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

    use SoftDeletes;

    protected $table   = 'companies';
    public $timestamps = true;
    protected $guarded = [];

    #Contacto principal de la compañia
    public function contact()
    {

        return $this->hasOne('App\User', 'id', 'contact_id');

    }

    #Contactos de una zona
    public function contacts()
    {

        return $this->hasMany('App\ZoneUser', 'zone_id', 'id');

    }

    public function all_contacts(){

        $last_parent = $this->all_parents()->reverse()->first();

        $contacts = \App\CompanyUser::where('team_id', $last_parent->id)->get();

        return $contacts;

    }


    public function find_contact($contact){


        return $this->contacts()->where('user_id', $contact->id)->first();


    }


    public function tickets()
    {

        return $this->hasMany('App\Ticket', 'company_id', 'id');

    }

    public function getFullAddressAttribute(){

        

        return "{$this->address}, {$this->city} {$this->state}, {$this->zip}";


    }


    public function gears()
    {

        
        return $this->hasMany('App\Gear', 'company_id', 'id');


    }

    public function contacts_catalog()
    {
        

        return $this->hasMany('App\ContactCatalog', 'master_zone_id', 'id');


    }



    public function getGearsModelsAttribute(){

        $gear_groups = $this->gears->groupBy('model_id');

        $models = collect([]);

        foreach($gear_groups as $key => $group){

            $model = \App\GearModel::find($key);

            $models->push($model);

        }

        return $models;

    }

    public function getAllGearsAttribute(){

        $gears = collect([]);

        foreach($this->all_childs() as $child){

            $child_gears = $child->gears;

            foreach($child_gears as $child_gear){
                $gears->push($child_gear);
            }
        }

        return $gears;

    }

    public function get_contract(){

        return $this->belongsTo('App\Contract', 'contract_id', 'id');

    }

    public function getContractAttribute(){

        $contract = null;

        if($this->get_contract){

            $contract = $this->get_contract;

        }elseif($this->master_contract && $this->master_parent->contract){

            $contract = $this->master_parent->contract;

        }

        return $contract;

    }

    public function getContractEndAtAttribute(){

        $contract_end_at = null;
        $contract_start_at = null;
        $period_number = null;
        $period_format = null;

        if($this->get_contract && is_null($this->master_contract)){
            $contract_start_at = $this->contract_start_at;
            $period_number = $this->contract->period_number;
            $period_format = $this->contract->period_format;
        }elseif(!is_null($this->master_contract) && $this->master_parent->contract){
            $contract_start_at = $this->master_parent->contract_start_at; 
            $period_number = $this->master_parent->contract->period_number;
            $period_format = $this->master_parent->contract->period_format;
        }

        if ($period_format == 'd') {
            $contract_end_at = \Carbon\Carbon::parse($contract_start_at)->addDays($period_number);
        } elseif ($period_format == 'w') {
            $contract_end_at = \Carbon\Carbon::parse($contract_start_at)->addWeeks($period_number);
        } elseif ($period_format == 'm') {
            $contract_end_at = \Carbon\Carbon::parse($contract_start_at)->addMonths($period_number);
        } elseif ($period_format == 'y') {
            $contract_end_at = \Carbon\Carbon::parse($contract_start_at)->addYears($period_number);
        }

        return $contract_end_at;

    }

    public function getContractLifeAttribute(){

        if($this->get_contract){

            $start = \Carbon\Carbon::parse($this->contract_start_at)->format('d/m/Y');
            $end = \Carbon\Carbon::parse($this->contract_end_at)->format('d/m/Y');

        }elseif($this->master_contract && $this->master_parent->contract){
            $start = \Carbon\Carbon::parse($this->master_parent->contract_start_at)->format('d/m/Y');
            $end = \Carbon\Carbon::parse($this->master_parent->contract_end_at)->format('d/m/Y');

        }
        
        return "{$start} - {$end}";

    }

    public function getValidateContractAttribute(){

        $today = \Carbon\Carbon::now();
        $contract_end = null;

        if($this->get_contract){

            $contract_end = \Carbon\Carbon::parse($this->contract_end_at);

        }elseif($this->master_contract && $this->master_parent->contract){

            $contract_end = \Carbon\Carbon::parse($this->master_parent->contract_end_at);

        }

        if($today > $contract_end){
            return "<span class='text-danger'>El contrato ha expirado</span>";
        }else{
            return "<span class='text-success'>Contrato Vigente</span>";
        }

    }

    public function get_sla(){

        return $this->belongsTo('App\SLA', 'sla_id', 'id');

    }

    public function getSlaAttribute(){

        $sla = null;

        if($this->get_sla){
            $sla = $this->get_sla;
        }elseif($this->master_contract && $this->master_parent->sla){
            $sla = $this->master_parent->sla;
        }

        return $sla;

    }

    public function get_coverage(){

        return $this->belongsTo('App\Coverage', 'coverage_id', 'id');

    }

    public function getCoverageAttribute(){

        $coverage = null;

        if($this->get_coverage){
            $coverage = $this->get_coverage;
        }elseif($this->master_contract && $this->master_parent->coverage){
            $coverage = $this->master_parent->coverage;
        }

        return $coverage;

    }

    public function parent()
    {

        return $this->belongsTo('App\Company', 'parent_id', 'id');

    }

    public function childs()
    {

        return $this->hasMany('App\Company', 'parent_id', 'id');

    }

    public function all_parents()
    {

        $parents = collect([$this]);

        $parent = $this->parent;

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents;

    }
    
    public function getMasterParentAttribute(){

        return $this->all_parents()->last();

    }

    public function all_childs()
    {

        $all_childs = collect([$this]);
        $next_childs = collect($this->childs);
        
        while (!is_null($next_childs)) {

            $add_to_next_childs = collect([]);

            $next_childs->each(function ($child) use ($all_childs, $add_to_next_childs) {
                $all_childs->push($child);
                $child->childs()->each(function ($c) use ($add_to_next_childs) {
                    $add_to_next_childs->push($c);
                });
            });

            if ($add_to_next_childs->count() > 0) {
                $next_childs = $add_to_next_childs;
            } else {
                $next_childs = null;
            }
        }
        return $all_childs;

    }

    public function team()
    {

        return $this->all_parents()->reverse()->first();

    }

    public function childs_tickets()
    {

        $all_tickets = collect([]);

        $tickets = $this->all_childs()->each(function ($child) use ($all_tickets) {

            $child->tickets()->each(function ($ticket) use ($all_tickets) {

                $all_tickets->push($ticket);

            });

        });

        $all_tickets = $all_tickets->sortBy(function ($ticket) {
            return $ticket->created_at;
        })->reverse();

        return $all_tickets;

    }

    public static function paginate_companies($request, $companies){

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 12;
        $currentPageItems = $companies->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($companies), $perPage);
        $paginatedItems->setPath($request->url());

        return $paginatedItems;

    }

    #proyectos por companias
    public function projects()
    {

        return $this->hasMany('App\CompanyProject', 'company_id', 'id');

    }


    public function getActiveProjectsAttribute(){


        return $this->projects()->with('project')->get()->where('project.condition', '==', '1');


    }


    public function getCancelledProjectsAttribute(){


        return $this->projects()->with('project')->get()->where('project.condition', '==', '0');


    }

}
