@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('contracts.index') }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<div class="card-body">
			<form action="{{ route('contracts.update', $contract) }}" id="updateContractForm" method="post">
				{{ method_field('PATCH') }}
				@include('helpdesk.contracts.partials.form', [
					'btn' => 'Actualizar Contrato'
				])
			</form>
		</div>
		<div class="card-footer">
			<div class="row form-row d-flex">
				<div class="col-sm-8 ml-auto">
					<button class="btn btn-success" form="updateContractForm">
						<i class="fa fa-check"></i> Actualizar Contrato
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
