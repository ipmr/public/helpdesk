@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')
<div class="card-body" id="zone_app">

	@role('admin','manager')
	<form action="{{ route('companies.update', $company) }}" method="post" id="updateCompanyForm">
		{{ method_field('PATCH') }}
	@endrole
		
		<div class="form-row d-flex mb-3">
			<div class="col-sm-8 ml-auto">
				<a class="btn btn-link p-0" href="{{ route('companies.show', $company) }}/">
					<i class="fa fa-angle-left"></i> 
					Regresar
				</a>
			</div>
		</div>

		@include('helpdesk.companies.partials.form', [

			'intention' => 'edit_company'

		])

	</form>
</div>

@role('admin','manager')
<div class="card-footer">
	<div class="row form-row d-flex">
		<div class="col-sm-8 ml-auto">
			<button type="submit" id="company_form_btn" form="updateCompanyForm" class="btn btn-success">
				<i class="fa fa-check"></i>
				Actualizar Información
			</button>
		</div>
	</div>
</div>
@endrole

@stop

@section('scripts')

	<script src="{{ asset('js/vue.js') }}"></script>
	<script>
		@include('helpdesk.contacts.scripts.create_new_contact', [
			'intention' => 'edit_company'
		])
	</script>

@stop