<?php
use App\CompanyProject;
use Faker\Generator as Faker;

$factory->define(CompanyProject::class, function (Faker $faker) {
    return [
        'project_id' => 1,
        'company_id' => 1,
    ];
});
