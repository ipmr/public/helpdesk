@if ($company->projects()->count())
    <table class="table table-striped table-bordered border-0 mb-0 shadow-none projects_table">
        <thead>
            <tr>
                <th style="width: 10%">
                    FUP
                </th>
                <th style="width: 20%">
                    Proyecto
                </th>
                <th style="width: 10%" class="text-center">
                    Propietario
                </th>
                <th style="width: 10%" class="text-center">
                    Inicio
                </th>
                <th style="width: 10%" class="text-center">
                    Cotización
                </th>
                <th style="width: 10%" class="text-center">
                    Producción
                </th>
                <th style="width: 10%" class="text-center">
                    Documentación
                </th>
                <th style="width: 10%" class="text-center">
                    Facturación
                </th>
                <th style="width: 10%" class="text-center">
                    Fecha
                </th>
                {{-- <th>
                    Duración Aprox
                </th> --}}

            </tr>
        </thead>
        <tbody>
            @foreach ($company_projects as $company_project)
                <tr @if($company_project->project->condition == 0) style="background-color: #FFF2F2; color: #c3535d;" @endif>
                    <td nowrap>
                        <div class="ml-0" style="width: 5px; height: 100%; background-color:{{ ($company_project->project->condition == 0)?'#3aad48':'#c3535d' }};">
                            
                        </div>
                        <a href="{{ route('companies.projects.show', [$company_project->company, optional($company_project->project)->fup]) }}" target="_blank">
                            {{ optional($company_project->project)->fup }}
                        </a>
                    </td>
                    <td style="cursor: default;" data-toggle="tooltip" data-placement="right" title="{{ optional($company_project->project)->description }}">
                        {{ str_limit(optional($company_project->project)->title, 30) }}
                    </td>
                    <td class="no-wrap text-center">
                        {{ (optional($company_project->project)->OwnerMember)?optional($company_project->project)->owner_member->full_name:'Sin Propietario' }}
                    </td>
                    <td @if($company_project->project->status>=1)
                        class="text-center" 
                        style="cursor: pointer; background-color: {{ ($company_project->project->condition)?'#3aad48':'#c3535d' }};color:white;"
                        @click.prevent="open_modal(1, {{ $company_project->project->fup }})"
                        >
                        <a href="#" class="text-white"><i class="fa fa-check fa-xs"></i></a>
                    @else
                        >
                    @endif
                    </td>
                    <td @if($company_project->project->status>=2)
                        class="text-center" 
                        style="cursor: pointer; background-color: {{ ($company_project->project->condition)?'#3aad48':'#c3535d' }};color:white;"
                        @click.prevent="open_modal(2, {{ $company_project->project->fup }})"
                        >
                        <a href="#" class="text-white">
                            <i class="fa fa-check fa-xs"></i>
                        </a>
                    @else
                        >
                    @endif
                    </td>
                    <td @if($company_project->project->status>=3)
                        class="text-center" 
                        style="cursor: pointer; background-color: {{ ($company_project->project->condition)?'#3aad48':'#c3535d' }};color:white;"
                        @click.prevent="open_modal(3, {{ $company_project->project->fup }})"
                        >
                        <a href="#" class="text-white">
                            <i class="fa fa-check fa-xs"></i>
                        </a>
                    @else
                        >
                    @endif
                    </td>
                    <td @if($company_project->project->status>=4)
                        class="text-center" 
                        style="cursor: pointer; background-color: {{ ($company_project->project->condition)?'#3aad48':'#c3535d' }};color:white;"
                        @click.prevent="open_modal(4, {{ $company_project->project->fup }})"
                        >
                        <a href="#" class="text-white">
                            <i class="fa fa-check fa-xs"></i>
                        </a>
                    @else
                        >
                    @endif
                    </td>
                    <td @if($company_project->project->status>=5)
                        class="text-center" 
                        style="cursor: pointer; background-color: {{ ($company_project->project->condition)?'#3aad48':'#c3535d' }};color:white;"
                        @click.prevent="open_modal(5, {{ $company_project->project->fup }})"
                        >
                        <a href="#" class="text-white">
                            <i class="fa fa-check fa-xs"></i>
                        </a>
                    @else
                        >
                    @endif
                    </td>
                    <td nowrap class="text-center">
                        {{ date('d/m/Y', strtotime(optional($company_project->project)->created_at)) }}
                    </td>
                    {{-- <td class="no-wrap">
                        <label class="badge-pill badge-dark">
                            {{ date('d M,Y', strtotime($company_project->project->start_date)). ' - '.date('d M,Y', strtotime($company_project->project->end_date)) }}
                        </label>
                    </td> --}}
                </tr>            
            @endforeach
        </tbody>
    </table>
@else
    <div class="card-body">
        <p class="lead">
            No hay proyectos en la compañia
        </p>
    </div>
@endif