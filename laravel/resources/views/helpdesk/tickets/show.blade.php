@extends('layouts.master')

@section('content')


	<div class="container" id="task">
		<div class="card">
			<div class="card-header d-flex align-items-center">
				<p class="lead text-danger">#{{ $ticket->ticket_id }} @if ($ticket->deleted_at) - Archivado @endif</p>
				@role('admin', 'manager', 'agent')
				<form action="{{ route('export_ticket_to_pdf', $ticket->ticket_id) }}/" method="post" class="ml-auto">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-light" data-toggle="tooltip" data-placement="left" title="Descarga en PDF la actividad del ticket #{{ $ticket->ticket_id }}">
						<i class="fa fa-download"></i> Descargar
					</button>
				</form>
				@endrole
			</div>
			<div class="card-body">
				@include('helpdesk.tickets.html.company_information')
				@include('helpdesk.tickets.html.ticket_description')
			</div>
		</div>
		@include('helpdesk.tickets.html.tabs')
		@include('helpdesk.tickets.partials.modal-public-comment')
		@include('helpdesk.tickets.partials.modal-public-resolve-comment')

		@role('admin', 'manager', 'agent')
			@include('helpdesk.tickets.partials.modal-new-task')
			@include('helpdesk.tickets.partials.modal-update-task')
			@include('helpdesk.tickets.partials.modal-add-event-to-task')
		@endrole
		@role('agent')
			@include('helpdesk.tickets.partials.modal-reopen-ticket')
		@endrole
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		var task_app = new Vue({
			el: '#task',
			data: {
				task: [],
				update_url: null,
				task_id: null,
				change_ticket_status_to_resolve: false,
			},
			mounted(){
			},
			methods: {
				minute_value: function(){
					var value = $('[name="minutes"]').val()
					if (value) {
						if ( value < 1 ) {
							$('[name="minutes"]').val(1)
						}
					}
				},

				active_success_btn: function(e){

					var select = $(e.target);

					var btn = select.closest('form').siblings('button');

					btn.removeAttr('disabled');

				},


				submit_comment: function(e){


					var form = $(e.target);


					var btn = $("button[form='new_comment_to_company_form']");


					var public_comment = $('#commentPublicCheckbox').val();


					if(public_comment == 1){


						var ask = confirm('¿Estas seguro que deseas hacer público este comentario?');


						if(ask){


							form.submit();

							
						}else{


							setTimeout(function(){


								btn.removeAttr('disabled');

							}, 150);
							


						}


					}else{


						form.submit();


					}


				},


				get_task( task_id ){
					var t = this;

	                var show_url = '{{ route('tickets.tasks.show', ['ticket'=>'#ticket_id', 'task' => '#task_id']) }}';
	                var update_url = '{{ route('tickets.tasks.update', ['ticket'=>'#ticket_id', 'task' => '#task_id']) }}';

	                show_url = show_url.replace( '#ticket_id', '{{ $ticket->ticket_id }}' );
	                show_url = show_url.replace( '#task_id', task_id );

	                update_url = update_url.replace( '#ticket_id', '{{ $ticket->ticket_id }}' );
	                update_url = update_url.replace( '#task_id', task_id );

	                t.$http.get(show_url).then(function(response){
	                    t.task = response.body;

	                    t.update_url = update_url;

	                    $('#update_task_modal').modal('show')
	                });
				},


				add_event_to_task: function(task, ticket){

					var t = this;

					
					var modal = $('#add_event_to_task_modal');

					
					modal.modal('show');


					t.task_id = task;


					t.ticket_id = ticket;



				},


				comment_to_task_route: function(){


					var t = this;


					var route = "{{ route('tickets.tasks.events.store', ["#ticket_id", "#task_id"]) }}";

					
					route = route.replace('#ticket_id', t.ticket_id).replace('#task_id', t.task_id);


					return route;


				},
				change_status_ticket: function(){
					var t 					= this;
					var get_selected_status = $('[name="status_id"]').val();
					var get_status_id       = '{{ App\Status::where('name', 'Resuelto')->first()->id }}';

					if (get_selected_status == get_status_id) {

						t.change_ticket_status_to_resolve = true;

						$('#last_comment_to_company_ticket').modal('show')

					} else {

						$('#updateStatusForm').submit()
					}
				},
				send_last_event_ticket: function(e){
					var t 		  = 	this;
					var form 	  = 	$(e.target);
					var form_id   = 	form.attr('id');
					var data  	  = 	form.serializeArray();
					var url 	  = 	form.attr('action');

					data.push({ name: 'days', value: '0'});
					data.push({ name: 'hours', value: '0'});
					data.push({ name: 'minutes', value: '0'});
					data.push({ name: 'type', value: 1});
					data.push({ name: 'last_comment', value: 1});

					$.post(url, data, function(response){

						if (response.result) {
	                    	$('#updateStatusForm').submit();
						}

					});


				},
				skip_comment: function(){
					var t = this;

					$('#skip_comment_button').attr('disabled', true)

					if (t.change_ticket_status_to_resolve) {
						
		                $('#updateStatusForm').submit()

					}
				}

			}
		})
	</script>

@stop