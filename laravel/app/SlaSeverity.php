<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlaSeverity extends Model
{
    protected $table   = 'sla_severities';
    public $timestamps = true;
    protected $guarded = [];


    public function sla(){

    	return $this->belongsTo('App\SLA', 'sla_id', 'id');

    }

    public function severity(){

    	return $this->belongsTo('App\Severity', 'severity_id', 'id');

    }


    public function name(){

    	return $this->severity->name;

    }

    public function respond(){

    	$format = null;

    	switch ($this->respond_format) {
    		case 'i':
    			$format = 'Minuto(s)';
    			break;
    		case 'h':
    			$format = 'Hora(s)';
    			break;
    		case 'd':
    			$format = 'Día(s)';
    			break;
    		case 'm':
    			$format = 'Mes(es)';
    			break;
    	}

    	return $this->respond_number .' '. $format;

    }

    public function resolve(){

    	$format = null;

    	switch ($this->resolve_format) {
    		case 'i':
    			$format = 'Minuto(s)';
    			break;
    		case 'h':
    			$format = 'Hora(s)';
    			break;
    		case 'd':
    			$format = 'Día(s)';
    			break;
    		case 'm':
    			$format = 'Mes(es)';
    			break;
    	}

    	return $this->resolve_number .' '. $format;

    }

}
