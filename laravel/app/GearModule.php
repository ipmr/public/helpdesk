<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GearModule extends Model
{
    	protected $table   = 'gear_modules';
        public $timestamps = true;
        protected $guarded = [];

        public function gear(){

        	return $this->belongsTo('App\Gear', 'gear_id', 'id');

        }

        public function module(){

        	return $this->belongsTo('App\Gear', 'module_id', 'id');

        }

}
