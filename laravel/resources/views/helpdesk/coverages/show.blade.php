@extends('layouts.master')

@section('content')

	<div class="container" id="coverages_app">
		<div class="card">
			<div class="card-header">
				<a class="btn btn-link p-0" href="{{ route('coverages.index') }}/">
					<i class="fa fa-angle-left fa-xs mr-1"></i>
					Regresar
				</a>
			</div>
			<div class="card-body">
				<form action="{{ route('coverages.update', $coverage) }}" method="post" id="updateCoverageForm">
					{{ method_field('PATCH') }}
					@include('helpdesk.coverages.partials.form', ['btn' => 'Actualizar'])
				</form>
			</div>
			<div class="card-footer">
				<div class="row form-row d-flex">
					<div class="col-sm-8 ml-auto">
						<button class="btn btn-success" form="updateCoverageForm"><i class="fa fa-check"></i> Actualizar Cobertura</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('scripts')
	@include('helpdesk.coverages.partials.scripts')
@stop