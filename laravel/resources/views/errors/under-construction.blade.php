@extends('layouts.master')

@section('content')

	<div class="container text-center">
		<h1 class="display-4">Módulo en construcción</h1>
		<p class="lead">La página que intentas buscar se encuentra en construcción</p>
		<p>Para más información contacta al equipo de IP Media River</p>
	</div>

@stop