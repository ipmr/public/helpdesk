<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMail extends Model
{
    public function category()
    {

        return $this->hasOne('App\Category', 'id', 'category_id');

    }

    public function severity()
    {

        return $this->hasOne('App\Severity', 'id', 'severity_id');

    }

    public function department()
    {

        return $this->hasOne('App\Department', 'id', 'department_id');

    }

}
