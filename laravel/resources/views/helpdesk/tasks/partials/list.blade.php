@if(count($tasks) > 0)
	
	<table class="table table-striped table-bordered m-0">
		<thead>
			<tr>
				<th>Fecha</th>
				<th width="45%">Tarea</th>
				@if($type == 'personal')
				<th>Ticket</th>
				@endif
				<th class="no-wrap text-center" width="200">Ingeniero Asignado</th>
				<th class="text-center"></th>
			</tr>
		</thead>
		<tbody>
			
			@foreach($tasks as $key => $task)
				<tr>
					<td class="no-wrap">
						{{ $task->created_at->format('d M, Y') }}
					</td>
					
					<td class="bg-secondary">
						<p class="mb-3">{{ $task->description }}</p>
			

						@if($task->status == 0)


						[

							<a  href="#" 
								data-toggle="tooltip" 
								data-placement="top" 
								title="Redactar Evento"
								@click.prevent="add_event_to_task({{ $task->id }}, {{ $task->ticket->ticket_id }})">
						
								<i class="fa fa-plus"></i>

							    Nuevo Evento


							</a>

						]

						@endif

						@if($task->events->count() > 0)
							

							[

								<a href="#showTaskEvents_{{ $task->id }}" data-toggle="collapse">
									({{ $task->events->count() }}) Evento{{ $task->events->count() == 1 ? '' : 's' }}
								</a>

							]


							<div class="collapse" id="showTaskEvents_{{ $task->id }}">
								
								<table class="simple_table mt-3">	

									@foreach($task->events()->latest()->get() as $event)

										<tr>
											
											<td>


												<p class="m-0">
													
													<b>{{ $event->author->first_name }} | {{ $event->created_at->format('d M, Y - h:i a') }}</b>

												</p>

												{{  $event->description }}


											</td>

										</tr>

									@endforeach

								</table>

							</div>


						@endif

					</td>


					@if($type == 'personal')
						<td>
							<a href="{{ route('tickets.show', $task->ticket->ticket_id) }}/">
								#{{ $task->ticket->ticket_id }}
							</a>
						</td>
					@endif


					<td class="text-center">
						@if ( $task->status == 0 )
							@ticket_owner($task->ticket)
								

								<form action="{{ route('tickets.tasks.update', ['ticket' => $task->ticket->ticket_id, 'task' => $task->id]) }}" method="post" class="d-flex m-0">
									
									{{ csrf_field() }}
									{{ method_field('PATCH') }}
									

									<div class="form-group m-0">
										<select name="agent_id" class="form-control m-0" style="min-width: 180px" required>
											<option disabled selected value>Elige un agente</option>
											@foreach(App\Agent::get() as $agent)
												<option value="{{ $agent->id }}" {{ $task->agent_id == $agent->id ? 'selected' : '' }}>{{ $agent->user->first_name .' '. $agent->user->last_name }} | {{ $agent->department->name }}</option>
											@endforeach
										</select>
									</div>
									

									<button type="submit" class="btn btn-success ml-1" style="line-height: 0;">
										<i class="fa fa-check" style="margin: 0 !important;"></i>
									</button>


								</form>


							@else
								
						
								@task_owner($task)


									<p>

										Si no puedes atender esta tarea por ahora, 
										solicita una reasignación 
										<a href="#reassign_task" data-toggle="collapse">aquí</a>

									</p>


									<div class="collapse" id="reassign_task">
										<form action="{{ route('reassing-task', [$task->ticket->ticket_id, $task]) }}" method="post">
											{{ csrf_field() }}
											<div class="form-group mb-2">
												<label for=""><small>Motivo de reasignación:</small></label>
												<textarea name="reasson" rows="2" class="form-control form-control-sm"></textarea>
											</div>
											<button type="submit" class="btn btn-success btn-sm">Enviar</button>
										</form>
									</div>
				

								@else


									{{ $task->agent->full_name }}


								@endtask_owner

							@endticket_owner
						@else
							

							{{ optional(optional($task->agent)->user)->FullName }}


						@endif
					</td>

					
					@if ($task->status == 1)
						
						
						<td class="no-wrap text-center" colspan="2">
							<span class="text-success"><i class="fa fa-check"></i></span>
						</td>


					@else
						<td class="no-wrap task_actions text-center">
								
				
								@task_owner($task)
	

									@isset ($ticket)
										

										<form action="{{ route( 'start-end-task', ['ticket' => $ticket->ticket_id, 'task' => $task ] ) }}" method="POST" class="m-0 d-inline-block">


									@else
										

										<form action="{{ route( 'start-end-task', ['ticket' => $task->ticket->ticket_id, 'task' => $task ] ) }}" method="POST" class="m-0 d-inline-block">


									@endisset

							            {{ csrf_field() }}
							            <button 
							                type="submit" 
							                class="finish_task" 
							                data-toggle="tooltip" data-placement="top" title="Terminar Tarea"
							                onclick="return confirm('Al confirmar se finalizará la tarea seleccionada, ¿deseas continuar?')">
							                
							                <i class="fa fa-check" style="margin: 0 !important"></i>
							            
							            </button>


							        </form>

		
								@endtask_owner


						        @if ($task->status != 1)
						        
									
									@ticket_owner($task->ticket)

								        <a  href="#"
								        	data-toggle="tooltip" data-placement="top" title="Editar Tarea" 
								        	@click.prevent="get_task( '{{ $task->id }}' )" 
								        	ticketid="{{ $task->ticket_id }}" 
								        	class="edit-task-{{ $task->id }} edit_task">


								            <i class="fa fa-pencil-alt"></i>

								        </a>


								        @isset ($ticket)
								        	<form action="{{ route( 'tickets.tasks.destroy', ['ticket' => $ticket->ticket_id, 'task' => $task->id ] ) }}" method="POST" class="d-inline-block">
								        @else
								        	<form action="{{ route( 'tickets.tasks.destroy', ['ticket' => $task->ticket->ticket_id, 'task' => $task->id ] ) }}" method="POST" class="d-inline-block">
								        @endisset
									            {{ method_field('DELETE') }}
									            {{ csrf_field() }}
									            <button type="submit" 
									            		class="delete_task"
									            		data-toggle="tooltip" data-placement="top" title="Eliminar Tarea" 
									            		onclick="return confirm('¿Estas seguro que deseas eliminar esta tarea?')">

									                
									                <i class="fa fa-trash-alt"></i>


									            </button>
									        </form>

			
									@endticket_owner
							

						        @endif
						</td>
					@endif
					
				</tr>
			@endforeach

		</tbody>
	</table>

@else
	<p class="m-0">No se han agregado tareas.</p>
@endif