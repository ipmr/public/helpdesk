<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketResolve extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->user   = $request['user'];
        $this->ticket = $request['ticket'];
        $this->status = $request['status'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $extra = '. Si tienes dudas, tienes 72 horas para compartirlas con los involucrados, antes de que el ticket cierre';

        $text  = $this->status->description . $extra;

        $last_comment = '';

        if (in_array($this->user->role, ['agent','manager'])) {
            $text = $this->user->FullName . ' marcó el ticket como ' . $this->ticket->get_status->name.$extra;
        }

        if ($this->ticket->comments()->count() > 0) {
            $comment = $this->ticket->comments()->where('last_comment', 1)->orderBy('id', 'DESC')->first();

            if (isset($comment->id)) {
                $last_comment = $comment->comment;
            }
        }

        return (new MailMessage)
            ->subject('Ticket resuelto')
            ->greeting('Ticket #' . $this->ticket->ticket_id)
            ->line($text)
            ->line($last_comment)
            ->action('Ver ticket', route('tickets.show', $this->ticket->ticket_id));;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $extra = '. Si tienes dudas, tienes 72 horas para compartirlas con los involucrados, antes de que el ticket cierre';

        $text  = $this->status->description . $extra;

        if (in_array($this->user->role, ['agent','manager'])) {
            $text = $this->user->FullName . ' marcó el ticket como ' . $this->ticket->get_status->name.$extra;
        }

        return [
            'msg'    => $text,
            'ticket' => $this->ticket,
        ];
    }
}
