<div class="form-row">
    <div class="col-sm-9 ml-auto">
        <label><b>Información de la categoría</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

{{ csrf_field() }}
<div class="row form-row">
    <label class="col-sm-3 col-form-label text-right">
        <span class="text-danger">*</span>
        Nombre:
    </label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="text" name="name" value="{{ $category->name or old('name') }}" class="form-control" autofocus required>
        </div>
    </div>
</div>

<div class="row form-row mb-3">
    <div class="col-sm-3"></div>
    <div class="col-sm-7">
        <hr class="my-0">
        <p class="my-3 text-info">Define los departamentos o e-mails a quienes debe notificarse en base a la severidad del ticket.</p>

        <div id="accordion">
            @foreach ($severities as $key => $severity)

            @php
                $category_email = null;
                if (isset($category)) {
                    $category_email = $category->mails()->where('severity_id', $severity->id )->first();
                }
            @endphp
            <input type="text" name="severity_id[]" value="{{ $severity->id }}" hidden>
            <div class="card mb-1">
                <div class="card-header border-bottom-0" id="heading_{{$key}}">
                    <a class="btn btn- p-0 {{ $loop->first ? '' : 'collapsed' }}" data-toggle="collapse" data-target="#collapse_{{$key}}" aria-expanded="{{ $loop->first ? 'true' : 'false'}}" aria-controls="collapse_{{$key}}" style="color: {{ $severity->color  }}">
                        Severidad {{ $severity->name }}
                    </a>
                </div>
                <div id="collapse_{{$key}}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading_{{$key}}" data-parent="#accordion">
                    <div class="card-body">
                        <label class="text-right">Departmento:</label>
                        <div class="form-group">
                            <select name="department_id[]" class="form-control">
                                <option selected value>Elige un departamento</option>
                                @foreach ($departments as $department)
                                <option value="{{ $department->id }}" @if ($category_email) {{ ($category_email->department_id == $department->id )?'selected':'' }} @endif> {{ $department->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <label class="text-right">Enviar correos a:</label>
                        <div class="form-group mb-0">
                            <textarea name="mails[]" class="form-control">@if ($category_email){{ $category_email->mails }}@endif</textarea>
                            <small>Los correos electronicos, deberan estar separados por coma (,)</small>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>


<div class="row form-row d-flex">
    <div class="col-sm-9 ml-auto">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> {{ $btn }} Categoría</button>
    </div>
</div>