@extends('layouts.master')
@section('content')

<div class="container" id="brandApp">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('brands.index') }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<table class="table table-striped m-0">
			
			<thead>
				<tr style="border-bottom: 1px solid #f2f2f2" v-cloak class="bg-secondary">
					<td style="vertical-align: middle;">
						<div v-show="edit_brand == true">
							<form action="{{ route('brands.update', $brand) }}" method="post" class="d-flex align-items-center">
								{{ csrf_field() }}
								{{ method_field('PATCH') }}
								<input type="text" class="form-control mr-3 text-uppercase" id="update_brand_name_input" name="name" value="{{ $brand->name }}">
								<button type="submit" class="btn btn-success ml-auto">
									<i class="fa fa-check"></i> Actualizar
								</button>
								<a href="#" class="btn btn-link text-danger px-0 ml-3" @click.prevent="cancel_brand_name_edition">
									<i class="fa fa-times"></i> Cancelar
								</a>
							</form>
						</div>
						<div v-if="!edit_brand" class="d-flex align-items-center">
							<b>{{ $brand->name }}</b>
							<span class="ml-auto">
								<a href="#" class="btn btn-info m-0" @click.prevent="edit_brand_name">
									<i class="fa fa-pencil-alt"></i> Editar
								</a>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<th colspan="2">
						<div class="d-flex align-items-center">
							Tipos
							<form action="{{ route('brands.show', ['brand' => $brand->id]) }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
				                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
				                    value="{{ $_GET['q'] }}"
				                @endisset>
				                <button class="btn btn-success" type="submit">
				                    <i class="fa fa-search"></i> Buscar
				                </button>
				            </form>
							<a href="#add_types_collapse" data-toggle="collapse" class="btn btn-primary ml-2">
								<i class="fa fa-plus"></i> Agregar Tipos
							</a>
						</div>
					</th>
				</tr>

				<tr>
					<td class="py-0" colspan="2">
						<div id="add_types_collapse" class="collapse">

							<div class="py-3">
								
								<div class="form-row d-flex align-items-center">
									<label class="col-form-label col-sm-3 text-right">Tipo:</label>
									<form class="col-sm-6 d-flex align-items-center" @submit.prevent="save_and_add_more">
										<input type="text" name="name" id="new_type_input" class="form-control text-uppercase" autocomplete="off" autofocus required>
										<button class="btn btn-info ml-2">
											<i class="fa fa-plus" style="margin: 0 !important"></i>
										</button>
									</form>
								</div>

								<div class="form-row" v-if="types.length > 0">
									<div class="col-sm-6 mx-auto">
										<ul class="list-group mb-3 mt-3">
											<li class="list-group-item d-flex align-items-center" v-for="(type, index) in types">
												<span class="text-uppercase">@{{ type }}</span>
												<a href="#" class="btn btn-link text-danger pr-0 ml-auto" @click.prevent="remove_type_from_list(index)">
													<i class="fa fa-times"></i> 
												</a>
											</li>
										</ul>
									</div>
								</div>

								<div class="form-row" v-show="types.length > 0">
									<div class="form-group col-sm-9 ml-auto">
										<form action="{{ route('brands.types.store', $brand) }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" :value="types" name="types_list">
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check"></i>
												Guardar Tipos de Equipo
											</button>
										</form>
									</div>
								</div>

							</div>

						</div>
					</td>
				</tr>
			</thead>

			<tbody>
				@if(count($types) > 0)
					@foreach($types as $type)
					
					<tr>
						<td colspan="2" class="d-flex">
							<a href="{{ route('brands.types.show', [$brand, $type->type->id]) }}/">{{ $type->type_name }}</a>
						</td>
					</tr>

					@endforeach
				@else
					<tr>
						<td colspan="2">
							No se han agregado tipos de equipo a {{ $brand->name }}
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('scripts')
	
	<script>

		var brand_app = new Vue({

			el: '#brandApp',
			data: {
				edit_brand: false,
				types: [],
				existing_types: @json($brand->types()->with('type')->get()->pluck('type.name')),
				all_types: @json(App\GearType::get()->pluck('name'))
			},
			mounted:function(){

				this.get_types_list();

			},
			methods:{

				get_types_list: function(){

					var t = this;

				    $( "#new_type_input" ).autocomplete({
				      	source: t.all_types
				    });

				},

				edit_brand_name: function(){

					var t = this;
					t.edit_brand = true;
					setTimeout(function(){

						$('#update_brand_name_input').focus();

					}, 150);

				},

				cancel_brand_name_edition: function(){

					var t = this;
					t.edit_brand = false;

				},

				save_and_add_more: function(e){

					var t = this;
					var form = $(e.target);
					var input = $('#new_type_input');
					var value = input.val().toUpperCase();
					var btn = $('button', form);

					var exists = false;

					$.each(t.types, function(i,o){

						if(o == value){

							exists = true;

						}

					});

					$.each(t.existing_types, function(i,o){

						if(o == value){

							exists = true;

						}

					});

					if(!exists){
						t.types.push(value.toUpperCase());
						input.val('').focus();
					}else{
						alert('El tipo de equipo ya ha sido agregado.');
					}

					setTimeout(function(){
						btn.removeAttr('disabled');
					}, 300);

				},

				remove_type_from_list: function(index){

					this.types.splice(index, 1);

				}

			}

		})

	</script>

@stop
