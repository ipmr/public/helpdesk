<div class="form-row">
    <div class="col-sm-4 mx-auto">
        <label><b>Información del tipo de contacto</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Nombre:
    </label>
    <div class="col-sm-4">
        <input type="text" name="name" value="{{ $contact_type->name or old('name') }}" class="form-control" autofocus required>
    </div>
</div>