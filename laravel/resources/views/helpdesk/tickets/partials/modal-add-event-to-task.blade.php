@component('helpdesk.components.modal', [
    'id'    => 'add_event_to_task_modal',
    'title' => 'Agregar Evento a Tarea'
])

    @slot('body')
        

        <form :action="comment_to_task_route()" method="post" id="add_event_to_task_form">
            

            {{ csrf_field() }}


            <div class="form-group mb-0">
                
                <label for="">Descripción:</label>

                <textarea name="description" rows="3" autofocus placeholder="Escribe aquí..." class="form-control"></textarea>

            </div>



        </form>


    @endslot


    @slot('footer')
        <button type="submit" form="add_event_to_task_form" class="btn btn-success">
            <i class="fa fa-check"></i>
            Agregar Evento
        </button>
    @endslot


@endcomponent