<div class="form-row">
    <div class="col-sm-9 ml-auto">
        <label><b>Información de acuerdo de servicio (SLA)</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

{{ csrf_field() }}
<div class="row form-row">
	<label class="col-form-label col-sm-3 text-right"><span class="text-danger">*</span> Nombre</label>
	<div class="form-group col-sm-7">
		<input type="text" name="name" value="{{ $sla->name or old('name') }}" class="form-control" autofocus required>
	</div>
</div>
<div class="row form-row">
	<label class="col-form-label col-sm-3 text-right">Descripción</label>
	<div class="form-group col-sm-7">
		<textarea name="description" class="form-control" rows="4" placeholder="Escribe aquí...">{{ $sla->description or old('description') }}</textarea>
	</div>
</div>
<div class="row form-row">
    <div class="col-sm-3"></div>
    <div class="col-sm-7">
        <p>Define los tiempos de respuesta y resolución en base a la severidad del ticket</p>
        <div id="accordion">
            @foreach ($severities as $key => $severity)
            <input type="text" name="severity_id[]" value="{{ $severity->id }}" hidden>
            <div class="card mb-1">
                <div class="card-header border-bottom-0" id="heading_{{$key}}">
                    <a class="btn btn- p-0 {{ $loop->first ? '' : 'collapsed' }}" data-toggle="collapse" data-target="#collapse_{{$key}}" aria-expanded="{{ $loop->first ? 'true' : 'false'}}" aria-controls="collapse_{{$key}}" style="color: {{ $severity->color  }}">
                        Severidad {{ $severity->name }}
                    </a>
                </div>
                <div id="collapse_{{$key}}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading_{{$key}}" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row form-row">
                            <label class="col-form-label col-sm-5 text-right">
                                <span class="text-danger">*</span> 
                                Tiempo de respuesta:
                            </label>
                            <div class="form-group col-sm-7 d-flex">
                                <input type="text" class="form-control" name="respond_number[]" value="{{ $sla->severities[$key]->respond_number or 1 }}" style="width: 40%" required>
                                <select name="respond_format[]" class="form-control ml-2" required>
                                    <option value="i" @isset($sla) {{ $sla->severities[$key]->respond_format == 'i' ? 'selected' : ''}} @endisset >Minuto(s)</option>
                                    <option value="h" @isset($sla) {{ $sla->severities[$key]->respond_format == 'h' ? 'selected' : ''}} @endisset >Hora(s)</option>
                                    <option value="d" @isset($sla) {{ $sla->severities[$key]->respond_format == 'd' ? 'selected' : ''}} @endisset >Día(s)</option>
                                    <option value="m" @isset($sla) {{ $sla->severities[$key]->respond_format == 'm' ? 'selected' : ''}} @endisset >Mes(es)</option>
                                </select>
                            </div>
                        </div>

                        {{-- <div class="row form-row">
                            <label class="col-form-label col-sm-5 text-right">
                                <span class="text-danger">*</span>
                                Resolver dentro de:
                            </label>
                            <div class="form-group col-sm-7 d-flex m-0">
                                <input type="text" class="form-control" name="resolve_number[]" value="{{ $sla->severities[$key]->resolve_number or 1 }}" style="width: 40%" required>
                                <select name="resolve_format[]" class="form-control ml-2" required>
                                    <option value="i" @isset($sla) {{ $sla->severities[$key]->resolve_format == 'i' ? 'selected' : ''}} @endisset>Minuto(s)</option>
                                    <option value="h" @isset($sla) {{ $sla->severities[$key]->resolve_format == 'h' ? 'selected' : ''}} @endisset>Hora(s)</option>
                                    <option value="d" @isset($sla) {{ $sla->severities[$key]->resolve_format == 'd' ? 'selected' : ''}} @endisset>Día(s)</option>
                                    <option value="m" @isset($sla) {{ $sla->severities[$key]->resolve_format == 'm' ? 'selected' : ''}} @endisset>Mes(es)</option>
                                </select>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>