@extends('layouts.master')

@section('content')
<div class="container-fluid" id="app_ticket">
    <div class="card">
        <div class="card-header text-center text-sm-left">
            <div class="d-block d-sm-flex align-items-center">
                <p class="lead m-0 mt-2 mb-2">Tickets Archivados</p>
                <a class="btn btn-light ml-auto" id="unarchive-tickets">
                </a>
            </div>
        </div>
        @if ($tickets->count() > 0)
            <div class="table-responsive">
                <table class="table m-0 table-striped data_table">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="control-archive-tickets">
                            </th>
                            <th>#Ticket</th>
                            <th>Compañia</th>
                            <th>Asunto</th>
                            <th>Severidad</th>
                            <th>Estado</th>
                            <th>Categoría</th>
                            <th>Agente</th>
                            <th>Fecha Creado</th>
                            <th>Fecha Eliminado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tickets as $ticket)
                            <tr>
                                <td>
                                    <input type="checkbox" value="{{ $ticket->ticket_id }}" class="archive-ticket">
                                </td>
                                <td>#{{ $ticket->ticket_id }}</td>
                                <td>{{ optional($ticket->company)->name }}</td>
                                <td>{{ $ticket->subject }}</td>
                                <td>{!! $ticket->severity() !!}</td>
                                <td>{!! $ticket->status() !!}</td>
                                <td>{{ optional($ticket->category)->name }}</td>
                                <td>{{ optional(optional($ticket->agent)->user)->full_name }}</td>
                                <td>{{ $ticket->created_at }}</td>
                                <td>{{ $ticket->deleted_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{ $tickets->links() }}
            </div>
        @else
            <div class="card-body">
                No existen registros
            </div>
        @endif
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('.control-archive-tickets').click(function(){
                var is_checked = $(this).prop('checked')
                $.each($('.archive-ticket'), function(){
                    $(this).prop('checked', is_checked)
                })

                if ( is_checked ) {
                    $('#unarchive-tickets').show()
                } else {
                    $('#unarchive-tickets').hide()
                }
            })

        }).on('click', '[type="checkbox"]', function(){
            var principal_is_check = false;
            var count_elements = 0;
            $.each($('.archive-ticket'),function(){
                if ( $(this).prop('checked') ){
                    principal_is_check = true;
                    count_elements += 1;
                } else {
                    $('.control-archive-tickets').prop('checked', false)
                }
            });

            class_button_create = (principal_is_check)?'ml-3':'ml-auto';
            $('.list-archive').removeClass('ml-auto').removeClass('ml-3').addClass(class_button_create);
            $('#unarchive-tickets').empty().append(
                $('<i/>',{
                    class: 'fa fa-archive',
                }),
                'Desarchivar ('+count_elements+')'
            );

            if ( principal_is_check ) {
                $('#unarchive-tickets').show()
            } else {
                $('#unarchive-tickets').hide()
            }
        }).on('click', '#unarchive-tickets', function(){
            $(this).empty().append( 
                $('<i/>',{
                    class: 'fa fa-spin fa-spinner',
                }),
                'Desarchivando Tickets'
            )

            var tickets = [];

            $.each($('.archive-ticket'),function(){
                
                var value_id = $(this).val()

                if ( $(this).prop('checked') ){

                    tickets.push({
                        name: 'ticket-id',
                        value: value_id
                    })
                }
            });

            $.ajax({
                url: '{{ route('unarchive-tickets') }}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    tickets: tickets,
                },
            }).done(function( response ) {
                window.location.href = response.result
            });            
        })

    </script>
@endsection