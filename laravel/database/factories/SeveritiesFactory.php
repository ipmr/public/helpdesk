<?php

use Faker\Generator as Faker;

$factory->define(App\Severity::class, function (Faker $faker) {
    return [
        'name'  => 'normal',
        'color' => '#dddddd',
        'static'=> 1,
    ];
});
