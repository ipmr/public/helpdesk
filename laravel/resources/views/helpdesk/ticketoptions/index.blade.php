@extends('layouts.master')

@section('content')
    <div class="container" id="contracts_app">
        
        <div class="card mb-3">
            <div class="card-header d-flex align-items-center">
                <p class="lead">Listado de estado</p>
                <a class="btn btn-primary ml-auto" href="#new_status" data-toggle="collapse">
                    <i class="fa fa-plus"></i>
                    Nuevo
                </a>
            </div>
            <div class="card-body py-0">
                <div id="new_status" class="collapse">
                    <form action="{{ route('status.store') }}" method="post" id="newStatusForm" class="py-3">
                        @include('helpdesk.status.partials.form', ['btn' => 'Crear'])
                        <div class="row form-row">
                            <div class="col-sm-8 ml-auto">
                                <button type="submit" form="newStatusForm" class="btn btn-success"><i class="fa fa-check"></i> Crear Estado</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(count($statuses) > 0)
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>Nombre para agentes</th>
                            <th>Nombre para clientes</th>
                            <th width="10%" class="text-center">Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($statuses as $status)
                        <tr>
                            <td>
                                <a href="{{ route('status.edit', $status) }}">
                                    {{ $status->name }}
                                </a>
                            </td>
                            <td>{{ $status->description }}</td>
                            <td class="text-center" style="font-size: 0;">
                                <div class="field_color_block" style="background-color: {{ $status->color }}"></div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="card-body pt-0">
                    No se han agregado contratos
                </div>
            @endif
        </div>

        <div class="card mb-3">
            <div class="card-header d-flex align-items-center">
                <p class="lead">Listado de severidad</p>
                <a class="btn btn-primary ml-auto" href="#new_severity" data-toggle="collapse"><i class="fa fa-plus"></i> Nueva</a>
            </div>
            <div class="card-body py-0">
                <div id="new_severity" class="collapse">
                    <form action="{{ route('severity.store') }}" method="post" id="newContractForm" class="py-3">
                        @include('helpdesk.severity.partials.form', ['btn' => 'Crear'])
                        <div class="row form-row">
                            <div class="col-sm-8 ml-auto">
                                <button type="submit" class="btn btn-success mt-3 mb-2"><i class="fa fa-check"></i> Crear Severidad</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if(count($severities) > 0)
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th width="10%" class="text-center">Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($severities as $severity)
                        <tr>
                            <td>
                                <a href="{{ route('severity.edit', $severity) }}">
                                    {{ $severity->name }}
                                </a>
                            </td>
                            <td class="text-center" style="font-size: 0">
                                <div class="field_color_block" style="background-color: {{ $severity->color }};"></div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="card-body">
                    No se han agregado contratos
                </div>
            @endif
        </div>

        <div class="card mb-3">
            <div class="card-header d-flex align-items-center">
                <p class="lead">Listado de categorias</p>
                <a class="btn btn-primary ml-auto" href="{{ route('category.create') }}"><i class="fa fa-plus"></i> Nueva</a>
            </div>
            @if(count($categories) > 0)
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Departamentos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr>
                            <td>
                                @component('helpdesk.categories.components.folder_link', ['category' => $category])@endcomponent
                                <a href="{{ route('category.show', $category) }}">
                                    {{ $category->name }}
                                </a>
                                @if($category->childs->count() > 0)
                                    <div class="collapse company_tree" id="category_{{ $category->id }}">
                                        <div class="pl-4 tree_link">
                                            @foreach($category->childs as $category)
                                                @component('helpdesk.categories.components.tree', ['category' => $category]) 
                                                @endcomponent
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if (count($category->department_names()))
                                    @foreach ($category->department_names() as $id => $department)
                                        <a href="{{ route('departments.show', ['department'=>$id]) }}" title="">
                                        {{ $department }}
                                        </a><br>
                                    @endforeach
                                @else
                                No asignados
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="card-body">
                    No se han agregado contratos
                </div>
            @endif
        </div>

    </div>
@stop