@component('helpdesk.components.modal', [
	'id' 	=> 'generateQuotationModal',
	'title' => 'Generar Cotización',
	'size' 	=> 'modal-lg'
])

	@slot('body')

		<form @submit.prevent="addResourceToQuotation">
			
			{{ csrf_field() }}
			
			<div class="form-group" style="background: #f8f9fa; margin: -14px -14px 15px; padding: 15px; border-bottom: 1px solid #f2f2f2;">
				
				<div class="form-row d-flex align-items-end">
					
					<div class="col-sm-2">
						<label>Cantidad</label>
						<input name="qty" type="number" class="form-control" required>
					</div>
					<div class="col-sm-6">
						<label>Tipo de recurso</label>
						
						<input name="name" type="text" class="form-control" required>
					</div>
					<div class="col-sm-3">
						<label>Sub-total</label>
						
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">$</span>
							</div>
							<input name="subtotal" type="number" step=".01" class="form-control" aria-label="Amount (to the nearest dollar)" required>
						</div>
					</div>
					<div class="col-sm-1">
						<button type="submit" class="btn btn-info btn-block">
							<i class="fa fa-plus" style="margin: 0 !important"></i>
						</button>
					</div>
					
				</div>
			</div>

		</form>

		<table v-if="quotation_resources.length > 0" class="table table-bordered table-striped mt-4 mb-0">
			
			<thead>
				<tr>
					<th style="width: 15%">Cantidad</th>
					<th>Tipo de recurso</th>
					<th style="width: 20%" class="text-right">Sub-total</th>
				</tr>
			</thead>
			
			<tbody>
				
				<tr v-for="(resource, index) in quotation_resources">
					<td>@{{ resource.qty }}</td>
					<td class="d-flex">
						
						@{{ resource.name }}
						
						<a href="#" class="ml-auto" @click.prevent="remove_resource(index)">
							<i class="fa fa-times fa-xs text-danger"></i>
						</a>

					</td>
					<td class="text-right">@{{ resource.subtotal | currency }}</td>
				</tr>
				
				<tr class="bg-secondary">
					<th colspan="2" class="text-right">TOTAL:</th>
					<th class="text-right">@{{ quotation_total() | currency }}</th>
				</tr>

			</tbody>
			
		</table>

		<p v-else class="m-0">No se han agregado recursos a la cotización</p>

	@endslot
	
	@slot('footer')
		
		<form action="{{ route('companies.projects.quotations.store', [$company, $project]) }}" method="POST" @submit.prevent="save_quotation">
			
			{{ csrf_field() }}


			<input type="hidden" name="resources" :value="resources_list()">


			<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Guardar Cotización</button>

		</form>

	@endslot

@endcomponent