<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryMail;
use App\Department;
use App\Severity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $severities = Severity::get();

        $departments = Department::get();

        return view('helpdesk.categories.create', compact('severities', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate_data_category($request);

        $category = new Category();

        $category->name = $request->name;

        $category->save();

        foreach ($request->severity_id as $index => $severity_id) {

            $this->insert_mails(
                $category,
                $severity_id,
                $request->department_id[$index],
                $request->mails[$index]
            );
        }

        return redirect()
            ->route('ticket-options.index')
            ->with('msg', 'Se ha creado la categoría ' . $category->name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $severities = Severity::get();

        // dd($category->childs());
        
        return view('helpdesk.categories.show', compact('category', 'severities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $severities = Severity::get();

        $departments = Department::get();

        return view('helpdesk.categories.edit', compact('category', 'severities', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        $this->validate_data_category($request, $category);

        $category->name = $request->name;

        $category->save();

        foreach ($request->severity_id as $index => $severity_id) {

            $this->insert_mails(
                $category,
                $severity_id,
                $request->department_id[$index],
                $request->mails[$index]
            );

        }

        return redirect()
            ->route('category.show', ['category' => $category->id])
            ->with('msg', 'Se ha actualizado la categoría ' . $category->name . ' exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    public function insert_mails($category, $severity_id, $department_id, $mails)
    {
        $category_mail = $category->mails()->where('severity_id', $severity_id)->first();

        if (empty($category_mail)) {

            $category_mail = new CategoryMail();

        }

        $category_mail->category_id   = $category->id;
        $category_mail->severity_id   = $severity_id;
        $category_mail->department_id = $department_id;
        $category_mail->mails         = $mails;

        $category_mail->save();
    }

    public function validate_data_category($request, $category = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required'            => 'El nombre de la categoría es requerido',
            'name.unique'              => 'El nombre de la categoría no esta disponible',
            'severity_id.required'     => 'La severidad de la categoría es requerida',
            'severity_id.exists'       => 'La severidad de la categoría no existe',
            'department_id.*.exists'   => 'El departamento seleccionado no es válido',
            'mails.*.string'           => 'Los correos deben ser caracteres alfanuméricos',
        ];

        $validations = [
            'name'            => ['required', 'string', Rule::unique('categories')->ignore($category ? $category->id : '')],
            'severity_id.*'   => 'required|exists:severities,id',
            'department_id.*' => 'nullable|sometimes|exists:departments,id',
            'mails.*'         => 'nullable|string',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}
