<?php

use App\Severity;
use Illuminate\Database\Seeder;

class SeveritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Severity::class, 1)->create([
            'id'     => 1,
            'name'   => 'Urgente',
            'color'  => '#d82a2a',
            'static' => 1,
        ]);

        factory(Severity::class, 1)->create([
            'id'     => 2,
            'name'   => 'Alta',
            'color'  => '#d8922a',
            'static' => 1,
        ]);

        factory(Severity::class, 1)->create([
            'id'     => 3,
            'name'   => 'Normal',
            'color'  => '#2a70d8',
            'static' => 1,
        ]);

        factory(Severity::class, 1)->create([
            'id'     => 4,
            'name'   => 'Baja',
            'color'  => '#8e8e92',
            'static' => 1,
        ]);

    }
}
