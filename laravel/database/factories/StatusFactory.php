<?php

use Faker\Generator as Faker;

$factory->define(App\Status::class, function (Faker $faker) {
    return [
        'name'  => 'Open',
        'color' => '#dddddd',
        'static'=> 1,
    ];
});
