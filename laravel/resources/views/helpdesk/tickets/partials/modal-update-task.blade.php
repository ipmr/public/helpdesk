@component('helpdesk.components.modal', [
    'id'    => 'update_task_modal',
    'title' => 'Actualiza tarea'
])

    @slot('body')
        <form :action="update_url" method="post" id="update_task_form">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <textarea name="description" rows="3" class="form-control" placeholder="Descripción de la tarea...">@{{task.description}}</textarea>
            </div>
            <div class="form-group m-0">

                <label><b>Asignar tarea a agente</b> <span class="text-muted">(Opcional)</span></label>

                <select name="agent_id" class="form-control">
                    <option value="null">Elige un agente</option>
                    @foreach(App\Agent::get() as $agent)
                        <option value="{{ $agent->id }}" :selected="task.agent_id == '{{$agent->id}}'">{{ $agent->user->first_name .' '. $agent->user->last_name }}</option>
                    @endforeach
                </select>
            </div>

        </form>
    @endslot
    @slot('footer')
        <button type="submit" form="update_task_form" class="btn btn-success">
            <i class="fa fa-check"></i>
            Actualizar Tarea
        </button>
    @endslot
@endcomponent