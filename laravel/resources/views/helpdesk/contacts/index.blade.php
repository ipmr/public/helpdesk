@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')

<table class="table table-striped mb-0">
	<thead>
		<tr>
			<td colspan="4">

				<div class="d-flex align-items-center py-2">
					<label class="m-0"><b>Lista De Contactos</b></label>
					@role('admin','manager')
					<a class="btn btn-primary ml-auto" href="#add_new_contact_form" data-toggle="collapse">
						<i class="fa fa-plus fa-sm mr-1"></i>
						Nuevo Contacto
					</a>
					@endrole
				</div>
				
				@role('admin', 'manager')

				<div id="add_new_contact_form" class="collapse @if($errors->any() || request('new')) show @endif">
					<form action="{{ route('companies.contacts.store', $company) }}" method="post" id="zone_app">
						
						{{ csrf_field() }}
						
						<div class="form-row">
							<div class="col-sm-4 mx-auto">
								<label><b>Datos del contacto</b></label>
								<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
							</div>
						</div>

						<div class="form-group row form-row">
							<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span>  Tipo de contacto:</label>
							<div class="col-sm-3">
								<select name="type" class="form-control" required>
									<option disabled selected value>Elige una opción</option>
									@foreach (App\CompanyUserType::where('id','>',1)->get() as $type)
										<option value="{{ $type->id }}">{{ $type->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						@include('helpdesk.contacts.partials.select_contact', [
							'intention' => 'add_new_contact_in_zone'
						])

						<div id="createNewContactForm" v-cloak v-if="new_contact">

							@include('helpdesk.contacts.partials.form')

						</div>
						
						<div class="row form-row mb-3">
							<div class="col-sm-8 ml-auto">
								<button class="btn btn-success" type="submit">
									<i class="fa fa-check"></i>
									Guardar Contacto
								</button>
							</div>
						</div>

					</form>
				</div>

				@endrole

			</td>
		</tr>
	</thead>
	@if($company->contacts->count() > 0)
	<thead>
		<tr style="border-top: 1px solid #f2f2f2">
			<th>Nombre</th>
			<th>Puesto</th>
			<th>E-mail</th>
			<th>Tipo</th>
		</tr>
	</thead>
	@endif
	<tbody>
		@if($company->contacts->count() > 0)


			@foreach($company->contacts as $member)
				<tr>
					@role('admin','manager')
						<td><a href="{{ route('companies.contacts.show', [$company, $member->contact]) }}/">
							{{ $member->full_name }}</a>
						</td>
					@else
						<td>
							{{ $member->full_name }}
						</td>
					@endif
					<td>{{ $member->jobtitle }}</td>
					<td>{{ $member->email }}</td>
					<td>{{ $member->company_user_type->name }}</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="6">
					<p class="m-0">No se han agregado usuarios a {{ $company->name }}</p>
				</td>
			</tr>
		@endif
	</tbody>
</table>

@stop


@section('scripts')

	<script src="{{ asset('js/vue.js') }}"></script>
	<script>
		@include('helpdesk.contacts.scripts.create_new_contact')
	</script>

@stop