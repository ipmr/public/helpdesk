<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gear extends Model
{
    
	protected $table   = 'gears';
    public $timestamps = true;
    protected $guarded = [];

	public function type(){

		return $this->belongsTo('App\GearType', 'type_id', 'id');

	}

	public function brand(){

		return $this->belongsTo('App\GearBrand', 'brand_id', 'id');

	}

	public function model(){

		return $this->belongsTo('App\GearModel', 'model_id', 'id');

	}

	public function modules(){

		return $this->hasMany('App\GearModule', 'gear_id', 'id');

	}

	public function tickets(){

		return $this->hasMany('App\Ticket', 'gear_id', 'id');
	}


	public function getAllModulesAttribute()
	{
		$all_modules = collect([]);
		$next_modules = collect($this->modules);
		
		while (!is_null($next_modules)) {

		    $add_to_next_modules = collect([]);

		    $next_modules->each(function ($module) use ($all_modules, $add_to_next_modules) {

		        $all_modules->push($module);
		        
		        $module->module->modules->each(function ($m) use ($add_to_next_modules) {
		            $add_to_next_modules->push($m);
		        });


		    });

		    if ($add_to_next_modules->count() > 0) {
		        $next_modules = $add_to_next_modules;
		    } else {
		        $next_modules = null;
		    }
		}

		return $all_modules;
	}


	public function module_from()
	{

		return $this->hasOne('App\GearModule', 'module_id', 'id');
	}

	public function company()
	{

		return $this->belongsTo('App\Company', 'company_id', 'id');
	}

	public function getNameAttribute()
	{

		$brand = $this->brand->name;
		$model = $this->model->name;

		return "{$brand} - {$model}";
	}

	public function getBrandTypeAttribute()
	{

		$brand_type = \App\BrandType::where('brand_id', $this->brand->id)->where('type_id', $this->type->id)->first();

		return $brand_type;
	}

	public function get_contract()
	{

		return $this->belongsTo('App\Contract', 'contract_id', 'id');
	}

	public function get_coverage()
	{

		return $this->belongsTo('App\Coverage', 'coverage_id', 'id');
	}

	public function getCoverageAttribute()
	{

		if($this->get_coverage){
			return $this->get_coverage;
		}elseif($this->master_coverage){
			return $this->company->master_parent->coverage;
		}else{
			return null;
		}
	}

	public function getContractAttribute()
	{

		if($this->get_contract){
			return $this->get_contract;
		}elseif($this->master_contract){
			return $this->company->master_parent->contract;
		}else{
			return null;
		}
	}

	public function getContractEndAtAttribute()
	{

		$start_at = $this->contract_id ? $this->contract_start_at : $this->company->contract_start_at;

	    $contract_end_at = null;

	    if ($this->contract->period_format == 'd') {
	        $contract_end_at = \Carbon\Carbon::parse($start_at)->addDays($this->contract->period_number);
	    } elseif ($this->contract->period_format == 's') {
	        $contract_end_at = \Carbon\Carbon::parse($start_at)->addWeeks($this->contract->period_number);
	    } elseif ($this->contract->period_format == 'm') {
	        $contract_end_at = \Carbon\Carbon::parse($start_at)->addMonths($this->contract->period_number);
	    } elseif ($this->contract->period_format == 'y') {
	        $contract_end_at = \Carbon\Carbon::parse($start_at)->addYears($this->contract->period_number);
	    }

	    return $contract_end_at;
	}

	public function getContractInitAttribute()
	{

		return $this->contract_id ? $this->contract_start_at : $this->company->contract_start_at;
	}

	public function getContractLifeAttribute()
	{


		if($this->contract_id){

			$start = \Carbon\Carbon::parse($this->contract_start_at)->format('d/m/Y');
			$end = \Carbon\Carbon::parse($this->contract_end_at)->format('d/m/Y');
			
		}else{

			$start = \Carbon\Carbon::parse($this->company->contract_start_at)->format('d/m/Y');
			$end = \Carbon\Carbon::parse($this->company->contract_end_at)->format('d/m/Y');

		}

		return "{$start} - {$end}";
	}

	public function coverage()
	{

		return $this->belongsTo('App\Coverage', 'coverage_id', 'id');
	}

	public function stock()
	{
		$this->hasOne('App\Stock', 'gear_id', 'id');
	}

	public function getInventoryAttribute()
	{
		return optional($this->stock)->inventory;
	}
}
