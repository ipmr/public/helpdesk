<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Category;
use App\CategoryMail;
use App\Comment;
use App\CommentFile;
use App\Company;
use App\History;
use App\Mail\NewTicketMail;
use App\Notifications\AgentAssigned;
use App\Notifications\AgentAssignedCompany;
use App\Notifications\ChangeTicketStatus;
use App\Notifications\DepartmentAssigned;
use App\Notifications\NewCommentToCompany;
use App\Notifications\NewCompanyComment;
use App\Notifications\NewInternalComment;
use App\Notifications\NewTicket;
use App\Notifications\NewTicketCompany;
use App\Notifications\TicketClose;
use App\Notifications\TicketResolve;
use App\Status;
use App\Ticket;
use App\TicketTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Notification;
use Validator;

class TicketController extends Controller
{

    public function __construct()
    {
        #se aplica el middleware ticket_owner para validar
        #que el usuario puede agregar o modificar la información
        $this->middleware('ticket_owner')->only(['show', 'update']);

        #se aplica el middleware ticket_closed para evitar que se modifique
        #una vez que se encuentra en estado cerrado.
        $this->middleware('ticket_closed')->only([
            'update',
            'set_public_comment',
            'set_internal_comment',
        ]);
    }

    public function index(Request $request)
    {

        #se instancia el usuario en sesión
        if (\Request::ajax()) {

            $user = auth()->user();

            $tickets = $this->get_tickets_by_role($request, $user);

            return response()->json($tickets);

        }

        return view('helpdesk.tickets.index');
    }

    public function get_tickets_by_role($request, $user)
    {
        $columns = [
            'ticket_id',
            'companies.name as company',
            'company_id',
            'subject',
            'severities.name as severity',
            'severities.color as severity_color',
            'status_id',
            'statuses.name as status',
            'statuses.color as status_color',
            'categories.name as category',
            'agent_id',
            'users.first_name as agent',
            'users.first_name',
            'users.last_name',
            'tickets.created_at as created',
            'tickets.deleted_at as deleted',
        ];

        $tickets = Ticket::select($columns)
            ->leftJoin('companies', 'tickets.company_id', '=', 'companies.id')
            ->leftJoin('severities', 'tickets.severity_id', '=', 'severities.id')
            ->leftJoin('statuses', 'tickets.status_id', '=', 'statuses.id')
            ->leftJoin('categories', 'tickets.category_id', '=', 'categories.id')
            ->leftJoin('agents', 'tickets.agent_id', '=', 'agents.id')
            ->leftJoin('users', 'agents.user_id', '=', 'users.id');

        $total_tickets = $tickets->count();

        $tickets = $tickets->when('tickets.company_id', function ($q) use ($request,$user) {
            if ( $user->zones_from_contact_type ) {
                $q->whereIn('company_id', $user->zones_from_contact_type );
            }

            if ( $request->company_id ) {
                return $q->where('tickets.company_id', $request->company_id);
            }
        })->when('tickets.agent_id', function ($q) use ($request,$user) {
            if ( $request->agent_id ) {
                return $q->where('tickets.agent_id', $request->agent_id);
            }
            if ($user->role == 'agent') {
                if (!$user->agent_as_manager) {
                    if ( $user->agents_in_group ){
                        
                        $q->whereIn('tickets.agent_id', $user->agents_in_group );
                    } elseif( !$user->agent_is_supervisor ) {
                        
                        $q->where('tickets.agent_id', $user->agent->id );
                    }
                }
            }
        })->when('tickets.status_id', function ($q) use ($request,$user) {
            if ( $request->status_id ) {
                return $q->where('tickets.status_id', $request->status_id);
            }
        })->when('tickets.severity_id', function ($q) use ($request,$user) {
            if ( $request->severity_id ) {
                return $q->where('tickets.severity_id', $request->severity_id);
            }
        })->when('tickets.category_id', function ($q) use ($request,$user) {
            if ( $request->category_id ) {
                return $q->where('tickets.category_id', $request->category_id);
            }
        })->when('tickets.department_id', function ($q) use ($request,$user) {
            if ( $request->department_id ) {
                return $q->where('tickets.department_id', $request->department_id);
            }

            if ($user->role == 'agent') {
                if (!$user->agent_as_manager) {

                    if ( $user->agent_is_supervisor ) {
                        $q->where('tickets.department_id', $user->agent_is_supervisor->id );
                    }
                }
            }
        })->when($request->my_tickets, function ($q) use ($request,$user) {
            if ( $request->my_tickets ) {
                return $q->where('tickets.contact_id',  auth()->user()->id);
            }
        })->when('tickets.ticket_id', function ($q) use ($request,$user) {
            if( $request->q ){
                if ( ctype_digit( $request->q ) ) {
                    $q->where('tickets.ticket_id', 'like', '%'.$request->q.'%');
                }
            }
        })->when('tickets.subject', function ($q) use ($request,$user) {
            if( $request->q ){
                if ( !ctype_digit( $request->q ) ) {
                    $q->where('tickets.subject', 'like', '%'.$request->q.'%');
                }
            }
        })/*->when('companies.name', function ($q) use ($request,$user) {
            if( $request->q ){
                if ( !ctype_digit( $request->q ) ) {
                    $q->where('companies.name', 'like', '%'.$request->q.'%');
                }
            }
        })->when('statuses.name', function ($q) use ($request,$user) {
        })->when('severities.name', function ($q) use ($request,$user) {
        })->when('categories.name', function ($q) use ($request,$user) {
        })*/;

        if ($user->role == 'company') {
            $tickets = $tickets->withTrashed();
        }

        foreach ($request->columns as $col) {
            $order_columns[] = $col['data'];
        }

        $tickets_filtered = $tickets->count();

        $tickets = $tickets->orderBy( $order_columns[$request->order[0]['column']] , $request->order[0]['dir'] )
            ->limit( $request->length )
            ->offset( $request->start )
            ->get();

        #se recorre porque hay un metodo que no se puede solucionar con la query y joins...
        $data = collect([]);
        foreach ($tickets as $object_ticket) {
            $ticket = Ticket::where('ticket_id', $object_ticket->ticket_id)->first();

            $object_ticket->overdue = false;
            if (isset($ticket->id)) {
                $object_ticket->overdue = $ticket->overdue();
            }

            $data->push($object_ticket);
        }

        return [
            "draw"            => $request->draw,
            "recordsTotal"    => $total_tickets,
            "recordsFiltered" => $tickets_filtered,
            "data"            => $data,
        ];
    }

    public function create()
    {

        $user = auth()->user();

        if ($user->role == 'agent') {

            if ($user->agent->generate_tickets == 0 && $user->agent->as_manager == 0) {

                return redirect()->back()->withErrors(['Acceso Denegado']);

            }

        }

        if ($user->role != 'company') {

            $companies = Company::latest()->where('master', 1)->get();

        } else {

            $companies = $user->companies();

        }

        return view('helpdesk.tickets.create', compact('tickets', 'companies'));
    }

    public function store(Request $request)
    {
        #se valida la información obtenida
        $this->validate_data($request);

        #se especifica al contacto del ticket
        $contact = User::find($request->contact_id);

        #se crea el ticket
        $ticket = $this->create_ticket($request, $contact);

        #se notifica a los managers del nuevo ticket
        $this->notify_to_managers_about_new_ticket($ticket);

        #se notifica a quienes esten relacionados con la categoria y la severidad del ticket
        $this->notify_to_related_users($ticket);

        #se notifica al contacto principal de la compania
        $this->notify_to_company_contact($ticket);


        if($request->ajax()){

            return $ticket;

        }else{

            #se retorna el mensaje de confirmacion dependiendo el tipo de
            #usuario que haya levantado el ticket
            if ($request->company_id) {
                $msg = 'Se ha creado un nuevo ticket para la compañia ' . $ticket->company->name;
            } else {
                $msg = 'Se ha creado un nuevo ticket de servicio, en la brevedad posible te atenderemos, Gracias.';
            }

            return redirect()->route('tickets.show', $ticket->ticket_id)->with('msg', $msg);
            
        }
    }

    public function create_ticket($request, $contact)
    {
        #buscamos la compania
        $company = Company::where('id', $request->zone_id ? $request->zone_id : $request->company_id)->first();

        $user = auth()->user();

        if ($user->role == 'company') {

            $contact = $user;

        }

        #se especifica el status abierto
        $status = Status::where('name', 'Abierto')->first();

        #se crea una nueva instancia de ticket
        $ticket              = new Ticket();
        $ticket->company_id  = $company->id;
        $ticket->subject     = $request->subject;
        $ticket->description = $request->description;
        $ticket->severity_id = $request->severity_id;
        $ticket->contact_id  = $contact->id;
        $ticket->status_id   = $status->id;
        $ticket->ticket_id   = '00000000';

        if ($user->role == 'agent') {

            if (!$user->agent->as_manager) {

                $ticket->department_id = $user->agent->department->id;
                $ticket->agent_id      = $user->agent->id;

            }

        }

        #obtener departamento por severidad y asignar, enviar correo
        $ticket->category_id = $request->category_id;

        $category_mail = $this->notify_department_by_category_to_supervisor($request->category_id, $request->severity_id, $ticket);

        if ($category_mail) {

            $ticket->department_id = $category_mail->department_id;

        }

        #se verifica que el equipo seleccionado pertenezca a la compañia seleccionada
        $gear_is_from_company = $this->validate_if_company_has_gear($request);

        if (!is_null($gear_is_from_company)) {

            $ticket->gear_id = $request->gear_id;

        }

        #se guarda el nuevo ticket
        $ticket->save();

        #se genera el ID del ticket
        $ticket->update([
            'ticket_id' => str_pad($ticket->company_id, 3, '0', STR_PAD_LEFT) . str_pad($ticket->id, 5, '0', STR_PAD_LEFT),
        ]);

        return $ticket;
    }

    public function validate_if_company_has_gear($request)
    {
        $gear = null;

        if ($request->gear_id) {

            $company = Company::find($request->zone_id ? $request->zone_id : $request->company_id);

            $gear = $company->gears()->where('id', $request->gear_id)->first();

        }

        return $gear;
    }

    public function notify_to_company_contact($ticket)
    {
        $contact = $ticket->author;

        $notification_data = [
            'ticket' => $ticket,
        ];

        Notification::send($contact, new NewTicketCompany($notification_data));
    }

    public function notify_to_managers_about_new_ticket($ticket)
    {
        $managers           = User::where('role', 'manager')->orWhere('role', 'admin')->get();
        $agents_as_managers = Agent::where('as_manager', 1)->get();

        $notification_data = [
            'ticket' => $ticket,
        ];

        if (!$ticket->department_id) {

            Notification::send($managers, new NewTicket($notification_data));

            foreach ($agents_as_managers as $agent) {

                $agent->user->notify(new NewTicket($notification_data));

            }

        }
    }

    public function notify_to_related_users($ticket)
    {

        $mails = $ticket->mailTo();

        if (count($mails) > 0) {

            foreach ($mails as $mail) {

                Mail::to($mail)->send(new NewTicketMail($ticket));

            }

        }
    }

    public function notify_department_by_category_to_supervisor($category_id, $severity_id, $ticket)
    {
        $category_mail = CategoryMail::where('category_id', $category_id)
            ->where('severity_id', $severity_id)
            ->first();
        if (!empty($category_mail->department_id)) {

            $supervisor = optional(optional($category_mail->department)->supervisor)->user;

            if ($supervisor) {
                $notification_data = [
                    'ticket' => $ticket,
                ];

                Notification::send($supervisor, new NewTicket($notification_data));

                return $category_mail;
            }
        }

        return false;
    }

    public function show($ticket)
    {
        #se busca el ticket corresponiente al id recibido
        $ticket = Ticket::where('ticket_id', $ticket)
            ->firstOrFail();

        return view('helpdesk.tickets.show', compact('ticket'));
    }

    public function show_deleted($ticket){
        $ticket = Ticket::where('ticket_id', $ticket)->onlyTrashed()->first();
        
        return view('helpdesk.tickets.show', compact('ticket'));
    }

    public function edit($ticket)
    {
        //
    }

    public function update(Request $request, $ticket)
    {
        $ticket = Ticket::where('ticket_id', $ticket)->firstOrFail();

        $user = auth()->user();

        #si se intenta actualizar la categoria del ticket
        if ($request->has('category')) {
            $response = $this->update_category($request, $ticket, $user);
        }
        #si se intenta actualizar la fecha de compromiso
        if ($request->has('day')) {
            $response = $this->update_deadline($request, $ticket, $user);
        }
        #si se intenta actualizar la severidad del ticket
        if ($request->has('severity_id')) {
            $response = $this->update_severity($request, $ticket, $user);
        }
        #si se intenta actualizar el status del ticket
        if ($request->has('status_id')) {

            $status = Status::find($request->status_id);

            #se revisa que el ticket no tenga tareas pendientes
            if ($ticket->tasks()->where('status', 0)->count() > 0 && in_array($status->name, ['Resuelto', 'Cerrado'])) {

                return redirect()->back()->with('warning', 'El ticket no puede concluirse porque hay tareas sin resolver.');

            }

            $response = $this->update_status($request, $ticket, $user);
        }
        #si se intenta actualizar el departamento
        if ($request->has('department')) {
            $response = $this->update_department($request, $ticket, $user);
        }
        #si se intenta actualizar el agente
        if ($request->has('agent')) {
            $response = $this->update_agent($request, $ticket, $user);
        }

        #se crea el eveto en el historial del ticket
        $this->history($ticket, $response['history_msg']);
        if (isset($response['extra'])) {
            $this->history($ticket, $response['extra']);
        }

        #se retorna con mensaje de confirmacion
        return redirect()->back()->with('msg', $response['msg']);
    }

    public function list_archive(Request $request)
    {
        $tickets = Ticket::onlyTrashed()->orderBy('deleted_at', 'DESC')->paginate(25);

        return view('helpdesk.tickets.archived', compact('tickets'));
    }

    public function archive_tickets(Request $request)
    {
        if (isset($request->tickets)) {
            foreach ($request->tickets as $data_ticket) {
                $ticket = Ticket::where('ticket_id', $data_ticket['value'])->first();

                $ticket->delete();
            }
        }

        if (\Request::ajax()) {
            $result = [
                'result' => true,
            ];

            return response()
                ->json($result);
        }

        return redirect()
            ->back()
            ->with('msg', count( $request->tickets ).' Tickets archivadas');
    }

    public function unarchive_tickets(Request $request )
    {
        if (isset($request->tickets)) {
            foreach ($request->tickets as $data_ticket) {
                $ticket = Ticket::withTrashed()->where('ticket_id', $data_ticket['value'])->restore();
            }
        }

        if (\Request::ajax()) {
            $result = [
                'result' => redirect()->back()->getTargetUrl(),
            ];

            return response()
                ->json($result);
        }

        return redirect()
            ->back()
            ->with('msg', count( $request->tickets ).' Tickets desarchivadas');
    }

    public function destroy($ticket)
    {
        //
    }

    public function update_deadline(Request $request, $ticket, $user)
    {
        $date = str_pad($request->year, 4, "0", STR_PAD_LEFT)
        . '-'
        . str_pad($request->month, 2, "0", STR_PAD_LEFT)
        . '-'
        . str_pad($request->day, 2, "0", STR_PAD_LEFT)
        . ' '
        . str_pad($request->hour, 2, "0", STR_PAD_LEFT)
        . ':'
        . str_pad($request->minutes, 2, "0", STR_PAD_LEFT)
        . ' '
        . $request->meridian;

        $ticket->update([
            'deadline' => $date,
        ]);

        $response = [
            'history_msg' => $user->first_name . ' ' . $user->last_name . ' modificó la fecha de compromiso para el día ' . Carbon::parse($date)->format('d/m/Y h:i a'),
            'msg'         => 'Se ha establecido una nueva fecha de compromiso.',
        ];

        return $response;
    }

    public function set_public_comment(Request $request, $ticket)
    {

        $user = auth()->user();

        $ticket = Ticket::where('ticket_id', $ticket)->firstOrFail();

        $type = null;

        if ($user->role == 'company') {

            $type = 'public';

        } else {

            $type = 'internal';

            if ($request->has('type')) {
                $type = $request->type ? 'public' : 'internal';
            }

        }

        $comment = $this->new_comment($request, $ticket, $type);


        $notification_data = [
            'ticket'  => $ticket,
            'comment' => $comment,
        ];

        if ($type == 'public') {

            if ($user->role == 'company') {

                if ($ticket->agent) {

                    $agent = $ticket->agent->user;

                    $agent->notify(new NewCompanyComment($notification_data));

                }

            } else {

                if ( optional($ticket->get_status)->name != 'Resuelto' ) {
                    $author = $ticket->author;

                    // if ($author->role == 'company') {

                        $author->notify(new NewCommentToCompany($notification_data));

                    /*} else {

                        $contacts = $ticket->company->contacts;

                        foreach ($contacts as $contact) {

                                
                            $contact->user->notify(new NewCommentToCompany($notification_data));
                        }
                    }*/
                }

            }


        } else if ($type == 'internal') {
            #si el ticket tiene agente
            $agent = $ticket->agent;
            if ($agent) {
                #notificacion agente asignado si el usuario que comento no es el autor
                if ($comment->user->id != $ticket->agent->user->id) {

                    Notification::send($ticket->agent->user, new NewInternalComment($notification_data));

                }
            } else {
                #si el ticket no tiene agente, se notifica a los managers para dar seguimiento
                $managers = User::where('role', 'manager')->get();

                Notification::send($managers, new NewInternalComment($notification_data));
            }

            #notificacion supervisor si el usuario que comento no es el supervisor
            if ($ticket->department_id) {
                $supervisor = optional(optional($ticket->department)->supervisor)->user;

                if (isset($supervisor)) {
                    #se valida que el agente del ticket, no sea el supervisor,para evitar repetir las notificaciones
                    if ($supervisor->id != $comment->user->id && $supervisor->id != $agent->user->id) {

                        Notification::send($supervisor, new NewInternalComment($notification_data));

                    }

                }
            }
        }

        $this->create_update_ticket_time($ticket);

        if (\Request::ajax()) {

            return response()->json([ 'result'=> true ]);
        }

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha agregado un nuevo evento al cliente',
            'tab' => 'public',
        ]);
    }

    public function set_internal_comment(Request $request, $ticket)
    {
        $ticket = Ticket::where('ticket_id', $ticket)->firstOrFail();

        $comment = $this->new_comment($request, $ticket, 'internal');

        $notification_data = [
            'ticket'  => $ticket,
            'comment' => $comment,
        ];

        #si el ticket tiene agente
        $agent = $ticket->agent;
        if ($agent) {
            #notificacion agente asignado si el usuario que comento no es el autor
            if ($comment->user->id != $ticket->agent->user->id) {

                Notification::send($ticket->agent->user, new NewInternalComment($notification_data));

            }
        } else {
            #si el ticket no tiene agente, se notifica a los managers para dar seguimiento
            $managers = User::where('role', 'manager')->get();

            Notification::send($managers, new NewInternalComment($notification_data));
        }

        #notificacion supervisor si el usuario que comento no es el supervisor
        if ($ticket->department_id) {
            $supervisor = optional(optional($ticket->department)->supervisor)->user;

            if (isset($supervisor)) {
                #se valida que el agente del ticket, no sea el supervisor,para evitar repetir las notificaciones
                if ($supervisor->id != $comment->user->id && $supervisor->id != $agent->user->id) {

                    Notification::send($supervisor, new NewInternalComment($notification_data));

                }

            }
        }

        return redirect(url()->previous() . '#tabs')->with([
            'msg' => 'Se ha agregado un nuevo evento interno',
            'tab' => 'internal',
        ]);
    }

    public function new_comment($request, $ticket, $type)
    {

        $user = auth()->user();


        \Log::info($request);


        if($user->role == 'company'){

            $validate = Validator::make($request->all(), [
                'comment'  => 'required|string',
                'files.*'  => 'image',
            ], [
                'comment.required' => 'El evento es requerido',
                'files.*image'     => 'Solo pueden adjuntarse imagenes de tipo jpg, jpeg ó png',
            ])->validate();


        }else{

            if( ! $request->has('last_comment')){
                
                $validate = Validator::make($request->all(), [
                    'comment'  => 'required|string',
                    'days'     => 'required|numeric|min:0',
                    'hours'    => 'required|numeric|min:0',
                    'minutes'  => 'required|numeric|min:1',
                    'atention' => 'required|in:Remota,En sitio',
                    'files.*'  => 'image',
                ], [
                    'comment.required' => 'El evento es requerido',
                    'days.required'    => 'El total de dias es requerido',
                    'days.numeric'     => 'El total de días debe ser númerico',
                    'days.min'         => 'El total de días debe ser mayor o igual a 0',
                    'hours.required'   => 'El total de horas es requerido',
                    'hours.numeric'    => 'El total de horas debe ser númerico',
                    'hours.min'        => 'El total de horas debe ser mayor o igual a 0',
                    'minutes.required' => 'El total de minutos es requerido',
                    'minutes.numeric'  => 'El total de minutos debe ser númerico',
                    'minutes.min'      => 'El total de minutos debe ser mayor o igual a 1',
                    'atention.in'      => 'El tipo de atención no es válido',
                    'files.*image'     => 'Solo pueden adjuntarse imagenes de tipo jpg, jpeg ó png',
                ])->validate();
                
            }

        }


        $days    = $request->days;
        $hours   = $request->hours;
        $minutes = $request->minutes;

        $total_minutes = ((($days * 24) + $hours) * 60) + $minutes;

        $comment = new Comment();

        $comment->comment = $request->comment;

        $comment->ticket_id  = $ticket->ticket_id;
        $comment->type       = $type;
        $comment->event_time = $total_minutes;
        $comment->atention   = $request->atention;
        $comment->user_id    = $user->id;

        if (isset($request->last_comment)) {

            $comment->last_comment = $request->last_comment;

        }

        $comment->save();


        if ($request->file('files')) {
            foreach ($request->file('files') as $file) {

                $filename  = $file->getClientOriginalName();
                $filename  = str_replace('.' . $file->getClientOriginalExtension(), '', $filename);
                $extension = '.' . $file->getClientOriginalExtension();

                $filename = $filename . '-' . uniqid() . $extension;

                $file->move('uploads', $filename);

                $comment_file = new CommentFile();

                $comment_file->ticket_id  = $ticket->ticket_id;
                $comment_file->comment_id = $comment->id;
                $comment_file->filename   = $filename;

                $comment_file->save();

            }
        }

        return $comment;
    }

    public function create_update_ticket_time(Ticket $ticket)
    {
        $ticket_time = $ticket->ticket_time;

        if (isset($ticket_time->id)) {

            $ticket_time->remote_time += $ticket->remote_attention_minutes;
            $ticket_time->site_time   += $ticket->site_attention_minutes;
            $ticket_time->total_time  += $ticket->remote_attention_minutes + $ticket->site_attention_minutes;

        } else {

            $ticket_time = new TicketTime();
            $ticket_time->ticket_id   = $ticket->ticket_id;
            $ticket_time->remote_time = $ticket->remote_attention_minutes;
            $ticket_time->site_time   = $ticket->site_attention_minutes;
            $ticket_time->total_time  = $ticket->remote_attention_minutes + $ticket->site_attention_minutes;

        }

        $ticket_time->save();
    }

    public function update_severity(Request $request, $ticket, $user)
    {
        $data = [
            'severity_id' => $request->severity_id,
        ];

        $extra         = null;
        $category_mail = $this->notify_department_by_category_to_supervisor($ticket->category_id, $request->severity_id, $ticket);
        if ($category_mail) {

            $data['department_id'] = $category_mail->department_id;
            if ($ticket->department_id) {

                $extra = 'El ticket cambio de departamento de "' . $ticket->department->name . '" a "' . $category_mail->department->name . '"';

            } else {

                $extra = 'Se asigna al ticket el departamento "' . $category_mail->department->name . '"';

            }

        }

        $ticket->update($data);

        $response = [
            'history_msg' => $user->first_name . ' ' . $user->last_name . ' modificó la severidad del ticket a ' . $ticket->get_severity->name,
            'msg'         => 'La severidad del ticket cambió a ' . $ticket->get_severity->name,
            'extra'       => $extra,
        ];

        return $response;
    }

    public function update_category(Request $request, $ticket, $user)
    {
        $data = [
            'category_id' => $request->category,
        ];

        $extra         = null;
        $category_mail = $this->notify_department_by_category_to_supervisor($request->category, $ticket->severity_id, $ticket);
        if ($category_mail) {

            $data['department_id'] = $category_mail->department_id;
            if ($ticket->department_id) {

                $extra = 'El ticket cambio de departamento de "' . $ticket->department->name . '" a "' . $category_mail->department->name . '"';

            } else {

                $extra = 'Se asigna al ticket el departamento "' . $category_mail->department->name . '"';

            }

        }

        $ticket->update($data);

        $response = [
            'history_msg' => $user->first_name . ' ' . $user->last_name . ' modificó la categoría del ticket a ' . $ticket->category->name,
            'msg'         => 'La categoría del ticket ha sido modificada',
            'extra'       => $extra,
        ];

        return $response;
    }

    public function update_status(Request $request, $ticket, $user)
    {
        $status = Status::find($request->status_id);

        $ticket->update([
            'status_id' => $status->id,
        ]);

        $msg = "El ticket se encuentra " . strtolower($ticket->get_status->name);

        $response = [
            'history_msg' => $user->FullName . ' modificó el status del ticket a ' . $ticket->get_status->name,
            'msg'         => $msg,
        ];

        #notificacion cambio status compania
        $notification_data = [
            'ticket' => $ticket,
            'status' => $status,
            'user'   => $ticket->author,
        ];

        $managers = User::where('role', 'manager')->get();

        if ($status->name == 'Resuelto') {

            Notification::send($ticket->author, new TicketResolve($notification_data));


            #Resolucion de ticket de levantamiento en un proyecto
            app('App\Http\Controllers\ProjectController')->complete_revision_ticket($ticket);




            // if ($managers->count() > 0) {

            //     Notification::send($managers, new TicketResolve($notification_data));
            // }
        } elseif ($status->name == 'Cerrado') {

            Notification::send($ticket->author, new TicketClose($notification_data));

            #notificacion cambio status al supervisor
            // if ($ticket->department_id) {
            //     $supervisor = optional(optional($ticket->department)->supervisor)->user;

            //     if ( isset( $supervisor ) ) {

            //         $notification_data['user'] = $supervisor;

            //         Notification::send($supervisor, new TicketClose($notification_data));

            //     }
            // }

            // if ($managers->count() > 0) {

            //     Notification::send($managers, new TicketClose($notification_data));
            // }
            
        } else {

            Notification::send($ticket->author, new ChangeTicketStatus($notification_data));

            #notificacion cambio status al supervisor
            // if ($ticket->department_id) {
            //     $supervisor = optional(optional($ticket->department)->supervisor)->user;

            //     if (isset($supervisor)) {

            //         $notification_data['user'] = $supervisor;

            //         Notification::send($supervisor, new ChangeTicketStatus($notification_data));

            //     }
            // }

            // if ($managers->count() > 0) {

            //     Notification::send($managers, new ChangeTicketStatus($notification_data));
            // }
        }

        return $response;
    }

    public function update_department(Request $request, $ticket, $user)
    {
        $ticket->update([
            'department_id' => $request->department,
            'agent_id'      => null,
        ]);

        if ($ticket->department->supervisor) {

            $supervisor = $ticket->department->supervisor->user;

            $notification_data = [

                'ticket' => $ticket,

            ];

            $supervisor->notify(new DepartmentAssigned($notification_data));

        }

        $response = [
            'history_msg' => $user->first_name . ' ' . $user->last_name . ' asignó el ticket al departamento ' . $ticket->department->name,
            'msg'         => "El ticket #{$ticket->ticket_id} fue asignado al departamento {$ticket->department->name}",
        ];

        return $response;
    }

    public function update_agent(Request $request, $ticket, $user)
    {
        $ticket->update([
            'agent_id' => $request->agent,
        ]);

        $agent = $ticket->agent->user;

        $notification_data = [
            'ticket' => $ticket,
        ];

        if ($ticket->get_status->name == 'Abierto') {
            $in_process_status = Status::where('name', 'En proceso')->first();

            $ticket->update([
                'status_id' => $in_process_status->id,
            ]);
        }

        $agent->notify(new AgentAssigned($notification_data));

        $contact = $ticket->company->contact;

        $contact->notify(new AgentAssignedCompany($notification_data));

        $response = [
            'history_msg' => $user->first_name . ' ' . $user->last_name . ' asignó el ticket al agente ' . $ticket->agent->user->first_name . ' ' . $ticket->agent->user->last_name,
            'msg'         => "El ingeniero {$agent->full_name} fue asignado al ticket #{$ticket->ticket_id}",
        ];

        return $response;
    }

    public function history($ticket, $description)
    {
        $history = new History();

        $history->ticket_id   = $ticket->ticket_id;
        $history->description = $description;

        $history->save();
    }

    public function advance_search(Request $request)
    {
        $user = auth()->user();

        $tickets = null;

        if ($user->role == 'admin' || $user->role == 'manager') {
            $tickets = Ticket::latest();
        } elseif ($user->role == 'agent') {
            if ($user->agent->department->supervisor_id == $user->agent->id) {
                $tickets = $user->agent->department->tickets()->latest();
            } else {
                $tickets = $user->agent->tickets()->latest();
            }
        } elseif ($user->role == 'company') {
            $companies = [];

            foreach ($user->zones as $zone) {
                #se obtendran las companias de las zonas
                if (optional($zone->company)->id) {
                    #se obtendran los hijos de la compania
                    if ($zone->company->childs) {
                        foreach ($zone->company->childs as $company_child) {

                            $companies[$company_child->id] = $company_child->id;
                        }
                    }

                    $companies[$zone->company->id] = $zone->company->id;
                }
            }

            $tickets = Ticket::whereIn('company_id', $companies);
        }

        $tickets = $tickets->when($request->company_id, function ($q) use ($request) {
            return $q->where('company_id', $request->company_id);
        })
            ->when($request->agent_id, function ($q) use ($request) {
                return $q->where('agent_id', $request->agent_id);
            })
            ->when($request->status_id, function ($q) use ($request) {
                return $q->where('status_id', $request->status_id);
            })
            ->when($request->severity_id, function ($q) use ($request) {
                return $q->where('severity_id', $request->severity_id);
            })
            ->when($request->category_id, function ($q) use ($request) {
                return $q->where('category_id', $request->category_id);
            })
            ->paginate(25);

        return view('helpdesk.tickets.index', compact('tickets'));
    }

    public function validate_data($request)
    {
        #se crean los mensajes para las validaciones
        $msgs = [

            'subject.required'     => 'El asunto del ticket es requerido',
            'description.required' => 'La descripción del ticket es requerida',
            'severity_id.required' => 'La severidad es requerida',
            'severity_id.exists'   => 'La severidad seleccionada no es válida',
            'category_id.required' => 'La categoría es requerida',
            'category_id.exists'   => 'La categoría seleccionada no es válida',
            'company_id.exists'    => 'La compañia seleccionada no es válida',

        ];

        #se valida la informacion obtenida
        $validate = Validator::make($request->all(), [

            'subject'     => 'required|string',
            'description' => 'required|string',
            'severity_id' => 'required|exists:severities,id',
            'category_id' => 'required|exists:categories,id',
            'company_id'  => 'sometimes|exists:companies,id',

        ], $msgs)->validate();
    }

    public function to_pdf(Request $request, $ticket)
    {

        $ticket = Ticket::where('ticket_id', $ticket)->first();

        $data = [
            'ticket' => $ticket,
        ];

        $pdf = \PDF::loadView('helpdesk.tickets.pdf.ticket', $data);

        return $pdf->stream();
    }
}
