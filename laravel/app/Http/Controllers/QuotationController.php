<?php

namespace App\Http\Controllers;

use App\Quotation;
use App\QuotationResource;
use App\Company;
use App\Project;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company, Project $project)
    {
        

        $resources = json_decode($request->resources);

        $quotation = $this->create_new_quotation($project);

        $this->add_resources_to_quotation($quotation, $resources);


        $response = [

            'quotation' => $quotation,
            'quotation_resources' => $quotation->resources,
            'msg' => "La cotización ha sigo guardada correctamente"

        ];

        app('App\Http\Controllers\ProjectController')->load_history($project, $response['msg']);

        return response()->json($response);




    }

    public function create_new_quotation($project){


        $quotation = new Quotation();

        $quotation->project_id = $project->id;
        $quotation->description = "Cotización del proyecto '{$project->fup}'";
        $quotation->accepted = 0;

        $quotation->save();


        return $quotation;


    }

    public function add_resources_to_quotation($quotation, $resources){


        foreach ($resources as $resource) {
            

            $add_resource = new QuotationResource();


            $add_resource->quotation_id = $quotation->id;
            $add_resource->qty = $resource->qty;
            $add_resource->name = $resource->name;
            $add_resource->subtotal = $resource->subtotal;


            $add_resource->save();


        }


    }




    public function validate_quotation(Company $company, Project $project){



        $quotation = Quotation::find(request()->quotation);

        $quotation->accepted = 1;

        $quotation->save();

        $response = [

            'quotation' => $quotation,
            'msg' => 'La cotización ha sido validada correctamente'

        ];

        app('App\Http\Controllers\ProjectController')->load_history($project, $response['msg']);

        return response()->json($response);


    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function show(Quotation $quotation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function edit(Quotation $quotation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quotation $quotation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quotation $quotation)
    {
        //
    }
}
