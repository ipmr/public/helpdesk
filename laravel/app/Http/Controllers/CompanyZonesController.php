<?php

namespace App\Http\Controllers;

use App\Company;
use App\ZoneUser;
use App\ContactCatalog;
use Illuminate\Http\Request;

class CompanyZonesController extends Controller
{

    public function __construct()
    {

        #se establece el middleware company_owner para que
        #un cliente pueda visualizar su información
        $this->middleware('company_owner')->only(['index', 'show']);

    }

    public function index(Company $company)
    {

        $zones = $company->childs;

        $tab = 'zones';

        return view('helpdesk.zones.index', compact('company', 'zones', 'tab'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request, Company $company)
    {


        #se crea la zona
        $zone = app('App\Http\Controllers\CompanyController')->store($request, true, $company);

        #se añade la zona como hija de la compañia que la esta creado
        $this->convert_zone_as_child_from_company($zone, $company);

        #se retorna a la previsualizacion de la nueva zona
        return redirect()
        ->route('companies.zones.index', $company)
        ->with('msg', 'Se ha creado la zona ' . $zone->name);

    }

    public function convert_zone_as_child_from_company($zone, $company)
    {


        $zone->update([
            'master'    => 0,
            'parent_id' => $company->id,
        ]);


        $company_catalog = $zone->master_parent->contacts_catalog()->pluck('user_id')->toArray();
        

        if( ! in_array($zone->contact->id, $company_catalog) ){

            $contact_catalog                 = new ContactCatalog();
            $contact_catalog->user_id        = $zone->contact->id;
            $contact_catalog->master_zone_id = $zone->master_parent->id;
            $contact_catalog->save();

        }

    }

    public function show(Company $company, Company $zone)
    {

        $z = true;

        $parent = $company;

        $company = $zone;

        $tab = 'general';

        return view('helpdesk.companies.show', compact('company', 'parent', 'tab'));

    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
