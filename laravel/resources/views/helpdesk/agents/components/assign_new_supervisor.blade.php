@component('helpdesk.components.modal', [
    'id'    => 'assign_new_supervisor',
    'title' => 'Suspender Agente'
])

    @slot('body')
        

        <form action="{{ route('agents.destroy', $agent) }}" method="post" id="delete_agent_new_supervisor">
            
            {{ method_field('DELETE') }}
            {{ csrf_field() }}


            <div class="form-group mb-0">
                
                <label for="">Supervisor Nuevo:</label>

                <select name="supervisor_id" class="form-control" required>
                    <option value="">Selecciona</option>
                    <option :value="agent.id" v-for="agent in agents">@{{ agent.user.first_name+' '+agent.user.last_name }}</option>
                </select>

            </div>



        </form>


    @endslot


    @slot('footer')
        <button type="submit" form="delete_agent_new_supervisor" class="btn btn-success">
            <i class="fa fa-check"></i>
            Suspender Agente y Asignar Supervisor
        </button>
    @endslot


@endcomponent