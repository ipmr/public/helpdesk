@extends('layouts.master')

@section('content')

	<div class="container" id="zone_app">
		<div class="card">
			<div class="card-header">
				<p class="lead">Crear Nueva Compañia</p>
			</div>
			<div class="card-body">

				<form action="{{ route('companies.store') }}" method="post" id="newCompanyForm">
					
					@include('helpdesk.companies.partials.form', [

						'intention' => 'new_company'

					])

				</form>
			</div>
			<div class="card-footer">
				<div class="row form-row">
					<div class="col-sm-8 ml-auto">
						<button type="submit" form="newCompanyForm" id="company_form_btn" class="btn btn-success">
							<i class="fa fa-check"></i>
							Guardar Compañia
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('scripts')

	<script src="{{ asset('js/vue.js') }}"></script>
	<script>
		@include('helpdesk.contacts.scripts.create_new_contact', [

			'intention' => 'new_company'

		])
	</script>

@stop