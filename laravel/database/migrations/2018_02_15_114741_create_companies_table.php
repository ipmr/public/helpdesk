<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('rfc')->nullable();
            $table->string('master_rfc')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('phone')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('sla_id')->nullable();
            $table->integer('coverage_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->string('master_contract')->nullable();
            $table->date('contract_start_at')->nullable();
            $table->boolean('master')->default(1);
            $table->integer('parent_id')->nullable();
            $table->boolean('urgent_tickets')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
