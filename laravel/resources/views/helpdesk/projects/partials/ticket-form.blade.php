{{ csrf_field() }}


<div class="form-row">
	<div class="col-sm-9 ml-auto">
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>


{{-- Asunto --}}
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
	
			<span class="text-danger">*</span>

			Asunto:

		</label>
		<div class="col-sm-6">
			<input type="text" class="form-control" name="subject" required>
		</div>
	</div>

</div>

{{-- Descripcion --}}
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Descripción:
		</label>
		<div class="col-sm-6">
			<textarea name="description" rows="4" class="form-control" required></textarea>
		</div>
	</div>

</div>

{{-- Departamento y agente --}}
{{-- Departamento --}}	
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Departamento:
		</label>

		<div class="col-sm-4">
			<select name="department" class="form-control" required @change.prevent="get_department_agents" required>
				<option disabled selected value="">Elige una opción</option>

				@foreach(App\Department::get() as $department)

				<option value="{{ $department->id }}">{{ $department->name }}</option>

				@endforeach
			</select>
		</div>
	</div>

</div>	

{{-- Agente --}}
<div class="form-group" v-if="newticket.department.agents">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Agente:
		</label>
		
		<div class="col-sm-4">
			<select name="agent" class="form-control" required>
				<option disabled selected value="">Elige una opción</option>
				<option v-for="agent in newticket.department.agents" :value="agent.id">@{{ agent.user.first_name +' '+ agent.user.last_name }}</option>
			</select>
		</div>
	</div>

</div>



{{-- Severidad --}}
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Severidad:
		</label>

		<div class="col-sm-2">
			<select name="severity_id" class="form-control" required>
				<option disabled selected value="">Elige una opción</option>

				@if($company->urgent_tickets)
			
					@foreach(App\Severity::get() as $severity)
						<option value="{{ $severity->id }}" {{ $severity->name == 'Normal' ? 'selected' : '' }}>{{ $severity->name }}</option>
					@endforeach

				@else
				
					@foreach(App\Severity::where('name', '<>', 'Urgente')->get() as $severity)
						<option value="{{ $severity->id }}" {{ $severity->name == 'Normal' ? 'selected' : '' }}>{{ $severity->name }}</option>
					@endforeach

				@endif

			</select>
		</div>
	</div>

</div>



{{-- Contacto --}}
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Contacto:
		</label>

		<div class="col-sm-4">
			<select name="contact_id" class="form-control" required>
				<option disabled selected value="">Elige una opción</option>

				@foreach($company->contacts as $contact)

				<option value="{{ $contact->user->id }}">{{ $contact->full_name }}</option>

				@endforeach
			</select>
		</div>
	</div>

</div>



{{-- Categorias --}}
<div class="form-group">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			<span class="text-danger">*</span>
			Categoría 1:
		</label>
		
		<div class="col-sm-4">
			<select name="category_id" class="form-control" @change.prevent="get_sub_categories(2)" required>
				<option disabled selected value>Elige una opción</option>
				@foreach(App\Category::where('parent_id', null)->get() as $category)
		
				<option value="{{ $category->id }}">{{ $category->name }}</option>

				@endforeach
			</select>
		</div>
	</div>

</div>

<div class="form-group" v-if="newticket.categories.b.length > 0">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			Categoría 2:
		</label>
		
		<div class="col-sm-4">
			<select name="subcategory_b" class="form-control" id="subCategoriesB" @change.prevent="get_sub_categories(3)">
				<option disabled selected value>Elige una opción</option>
				<option v-for="subcategory_b in newticket.categories.b" :value="subcategory_b.id">@{{ subcategory_b.name }}</option>
			</select>
		</div>
	</div>

</div>

<div class="form-group" v-if="newticket.categories.c.length > 0">
	
	<div class="form-row">
		<label class="col-form-label col-sm-3 text-right">
			Categoría 3:
		</label>
		
		<div class="col-sm-4">
			<select name="subcategory_c" class="form-control" id="subCategoriesC">
				<option disabled selected value>Elige una opción</option>
				<option v-for="subcategory_c in newticket.categories.c" :value="subcategory_c.id">@{{ subcategory_c.name }}</option>
			</select>
		</div>
	</div>

</div>