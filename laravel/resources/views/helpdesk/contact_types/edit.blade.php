@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header card_header_fixed">
            <a class="btn btn-link p-0" href="{{ route('contact-types.show', $contact_type) }}/">
                <i class="fa fa-angle-left"></i>
                Regresar
            </a>
        </div>
        <div class="card-body">
            <form action="{{ route('contact-types.update', $contact_type) }}" method="post" id="newDepartmentForm">
                
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                @include('helpdesk.contact_types.partials.form')
                
            </form>
        </div>
        <div class="card-footer">
            <div class="row d-flex">
                <div class="col-sm-8 ml-auto">
                    <button type="submit" form="newDepartmentForm" id="department_form_btn" class="btn btn-success">
                        <i class="fa fa-check"></i>
                        Actualizar Tipo Contacto
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop