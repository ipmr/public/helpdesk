@extends('layouts.master')

@section('content')
<div class="container-fluid" id="app_ticket">
	<div class="card">
		<div class="card-header text-center text-sm-left">
			<div class="d-block d-sm-flex align-items-center">
				@if(request()->has('q'))
				    <p class="lead m-0 mb-sm-0">Resultados de búsqueda</p>
				@else
				    <p class="lead m-0 mb-sm-0">Listado de tickets</p>
				@endif
                <div class="form-inline ml-3">
                    <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar...">
                    <button class="btn btn-success" type="submit" id="search_btn">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </div>
                <a href="#advanceSearch" class="btn ml-3 btn-link" data-toggle="collapse"> 
                    <i class="fa fa-chevron-down fa-xs"></i> Búsqueda Avanzada
                </a>
                @role('company')
                    <div class="custom-control custom-checkbox ml-auto">
                        <input type="checkbox" class="custom-control-input" name="my_tickets" id="myTicketsCheckbox"/>
                        <label class="custom-control-label" for="myTicketsCheckbox">Mostrar Solo Mis Tickets</label>
                    </div>
                <div class="ml-3 text-right">
                @else
                <div class="ml-auto text-right">
                @endrole
				@role('admin', 'manager', 'company', 'agent')



    				@if(auth()->user()->role == 'agent')
            
                        @if(auth()->user()->agent->generate_tickets || auth()->user()->agent->as_manager)
                            <a class="btn btn-light ml-auto" id="archive-tickets" hidden>
                            </a>
                            <a href="{{ route('list-archive-tickets') }}" class="btn btn-sm btn-link ml-auto list-archive">
                                <i class="fa fa-list"></i>
                                Lista Archivados
                            </a>
                            <a href="{{ route('tickets.create') }}/" class="btn btn-primary ml-3 mb-3 mb-sm-0 create-ticket"> 
                                <i class="fa fa-plus"></i> Nuevo Ticket
                            </a>

                        @endif

                    @else
                        
                        @if ( in_array( auth()->user()->role, [ 'admin', 'manager' ] ) )
                            <a class="btn btn-light ml-auto" id="archive-tickets" hidden>
                            </a>
                            <a href="{{ route('list-archive-tickets') }}" class="btn btn-sm btn-link ml-auto list-archive">
                                <i class="fa fa-list"></i>
                                Lista Archivados
                            </a>
                        @endif
                        <a href="{{ route('tickets.create') }}/" class="btn btn-primary ml-3 mb-3 mb-sm-0 create-ticket"> 
                            <i class="fa fa-plus"></i> Nuevo Ticket
                        </a>

                    @endif
				@endrole
                </div>
			</div>
			<div id="advanceSearch" class="collapse @isset ($_GET['status_id']) show @endisset">
                <div class="d-flex justify-content-between pt-3">
                    <div class="form-group m-0 mr-1 w-100">
                        <select name="status_id" id="status_id" class="form-control">
                            <option value="">Buscar por status</option>
                            @foreach(App\Status::select('id', 'name')->get() as $status)
                            <option value="{{ $status->id }}" @isset ($_GET['status_id'])
                                {{ ($_GET['status_id'] == $status->id)?'selected':'' }}
                            @endisset>{{ $status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-0 mx-1 w-100">
                        <select name="severity_id" id="severity_id" class="form-control">
                            <option value="">Buscar por severidad</option>
                            @foreach(App\Severity::select('id', 'name')->get() as $severity)
                            <option value="{{ $severity->id }}">{{ $severity->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-0 mx-1 w-100">
                        <select name="category_id" id="category_id" class="form-control">
                            <option value="">Buscar por categoría</option>
                            @foreach(App\Category::select('id', 'name')->get() as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @role('admin', 'manager', 'company')
                        <div class="form-group m-0 mx-1 w-100">
                            @php
                                $user = auth()->user();

                                $companies = App\Company::select(['id','name']);

                                if ($user->role == 'company' ) {

                                    if ( $user->zones_from_contact_type ) {
                                        
                                        $companies = $companies->whereIn('id', $user->zones_from_contact_type );

                                    }
                                }

                                $companies = $companies->get();
                            @endphp
                            <select name="company_id" id="company_id" class="form-control">
                                <option value="">Buscar por compañia</option>
                                @foreach($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endrole
                    @role('admin', 'manager')
                        <div class="form-group m-0 mx-1 w-100">
                            @php
                            $agents = App\Agent::select('id', 'user_id')->with('user:id,first_name,last_name')->get();
                            @endphp
                            <select name="agent_id" id="agent_id" class="form-control">
                                <option value="">Buscar por agente</option>
                                @foreach($agents as $agent)
                                <option value="{{ $agent->id }}">{{ $agent->user->last_name .' '. $agent->user->first_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-0 mx-1 w-100">
                            <select name="department_id" id="department_id" class="form-control">
                                <option value="">Buscar por departamento</option>
                                @foreach(App\Department::get() as $department)
                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endrole
                    <div class="ml-1">
                        <button class="btn btn-success btn-block" id="reload_data">

                            <i class="fa fa-search fa-sm mr-1"></i> Buscar

                        </button>
                    </div>
                </div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table m-0 table-striped data_table">
				<thead>
					<tr>
                        {{-- @role('admin', 'manager')
                        <th>
                            <input type="checkbox" class="control-archive-tickets">
                        </th>
                        @endrole --}}
						<th>#Ticket</th>
						<th>Compañia</th>
						<th>Asunto</th>
						<th>Severidad</th>
						<th>Estado</th>
						<th>Categoría</th>
						<th>Agente</th>
						<th>Fecha</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
        
        var table = $('.data_table').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Spanish.json"
            },
            /*"preDrawCallback": function( settings ) {
            	$('tbody').html('Cargando tickets')
            },*/
            "paging": true,
            "pageLength" : 25,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "order": [ [ 7, 'desc'] ],
            {{-- @role('admin', 'manager')
            "order": [ [ 8, 'desc'] ],
            @else
                @if(auth()->user()->role == 'agent')
                    @if(auth()->user()->agent->generate_tickets || auth()->user()->agent->as_manager)
                        "order": [ [ 8, 'desc'] ],
                    @else 
                    "order": [ [ 7, 'desc'] ],
                    @endif
                @else
                    "order": [ [ 7, 'desc'] ],
                @endif
            @endrole --}}
            "serverSide": true,
            "responsive": true,
            "bAutoWidth": false,
            "search": "Important",
            "ajax": {
                "url": "{{ route('tickets.index') }}",
                "data": function ( d ) {
                	var serialize = {
                        q           : $('[name="q"]').val(),
                		status_id	: $('#status_id').val(),
                		severity_id : $('#severity_id').val(),
                		category_id : $('#category_id').val(),
                        @if (auth()->user()->role != 'agent')
                        company_id  : $('#company_id').val(),
                        department_id  : $('#department_id').val(),
                        @endif
                        @role('company')
                        my_tickets  : ($('[name="my_tickets"]').prop('checked'))?1:0,
                        @endrole
						@role('admin', 'manager')
                		agent_id	: $('#agent_id').val(),
						@endrole
                	}
                    return $.extend( {}, d,  serialize );
                }
            },
            "columns": [
                {{-- @role('admin', 'manager')
                {
                    "data": "ticket_id",
                    "className" : "td-archive-ticket",
                    "orderable" : false
                },
                @endrole --}}
				{
					"data": "ticket_id",
					"className" : "number_ticket",
				},
				{
					"data": "company",
					"className" : "company_name",
				},
				{
					"data": "subject",
				},
				{
					"data": "severity",
					"className" : "severity",
				},
				{
					"data": "status",
					"className" : "status no-wrap",
				},
				{
					"data": "category",
				},
				{
					"data": "agent", 
					"className" : "agent_name no-wrap" 
				},
				{
					"data": "created",
					"className" : "created no-wrap" 
				},
            ]
        });

		$('.data_table').on( 'draw.dt', function (){ 

            {{-- @role('admin', 'manager') 
            $('tbody .td-archive-ticket').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                $(this).empty().append(
                    $('<input/>',{
                        type: 'checkbox',
                        class: 'archive-ticket',
                        value: data.ticket_id
                    })
                )
            })
            @endrole --}}

			$('tbody .number_ticket').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                var url = '{{ route('tickets.show', '#ticket_id') }}'
                url = url.replace('#ticket_id', data.ticket_id )
                var text = '#' + data.ticket_id
                var content = $('<a/>',{
                        href: url,
                        text: text
                    });

                if( data.overdue ){
                    tr.addClass('bg-danger-light')
                }

                if( data.deleted ){
                    url = '{{ route('show_deleted', '#ticket_id') }}'
                    url = url.replace('#ticket_id', data.ticket_id )
                    tr.addClass('text-muted')
                    content = $('<a/>',{
                        href: url,
                        text: text,
                        class: 'text-muted'
                    })
                }

                $(this).empty().append(
                    content
                )
            });

            $('tbody .company_name').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                var url = '{{ route('companies.show', '#company_id') }}'
                url = url.replace('#company_id', data.company_id )

                $(this).empty().append(
                	$('<a/>',{
                		href: url,
                		text: data.company
                	})
                )
            });

            $('tbody .severity').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                $(this).empty().append(
                	$('<span/>',{
                		style: "color:"+data.severity_color+";",
                		text: data.severity
                	})
                )
            });

            $('tbody .status').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                $(this).empty().append(
                	$('<span/>',{
                		style: "color:"+data.status_color+";",
                		text: data.status
                	})
                )
            });

            $('tbody .agent_name').each(function(){
                var tr = $( this ).closest('tr');
                var row = table.row( tr );
                var data = row.data();

                var name_agent = 'Sin asignar'
                
                @role('admin', 'manager')
                    if(data.agent_id){
                        name_agent = data.first_name + " " + data.last_name

                        url = '{{ route('agents.show', '#agent_id') }}'
                        url = url.replace('#agent_id', data.agent_id)
                        $(this).empty().append(
                            $('<a/>',{
                                href: url,
                                text: name_agent
                            })
                        )
                    } else {

                        $(this).text(name_agent)

                    }
                @else 
                    
                    if(data.first_name){
                        name_agent = data.first_name + " " + data.last_name
                    }

                    $(this).text(name_agent)
                @endrole
            });
		});

        $(document).ready(function(){
            $('#reload_data').click(function(e){
            	e.preventDefault()

                table.draw()
            })

            @role('company')
            $('[name="my_tickets"]').click(function(){
                table.draw()
            })
            @endrole

            {{--@role('admin', 'manager')
            $('.control-archive-tickets').click(function(){
                var is_checked = $(this).prop('checked')
                $.each($('.archive-ticket'), function(){
                    $(this).prop('checked', is_checked)
                })

                if ( is_checked ) {
                    $('#archive-tickets').removeAttr('hidden')
                } else {
                    $('#archive-tickets').attr('hidden',true)
                }
            })
        }).on('click', '[type="checkbox"]', function(){
            var principal_is_check = false;
            var count_elements = 0;
            $.each($('.archive-ticket'),function(){
                if ( $(this).prop('checked') ){
                    principal_is_check = true;
                    count_elements += 1;
                } else {
                    $('.control-archive-tickets').prop('checked', false)
                }
            });

            class_button_create = (principal_is_check)?'ml-3':'ml-auto';
            $('.list-archive').removeClass('ml-auto').removeClass('ml-3').addClass(class_button_create);
            $('#archive-tickets').empty().append(
                $('<i/>',{
                    class: 'fa fa-archive',
                }),
                'Archivar ('+count_elements+')'
            );

            if ( principal_is_check ) {
                $('#archive-tickets').removeAttr('hidden')
            } else {
                $('#archive-tickets').attr('hidden',true)
            }
        }).on('click', '#archive-tickets', function(){
            $(this).empty().append( 
                $('<i/>',{
                    class: 'fa fa-spin fa-spinner',
                }),
                'Archivando Tickets'
            )

            var tickets = [];

            $.each($('.archive-ticket'),function(){
                
                var value_id = $(this).val()

                if ( $(this).prop('checked') ){

                    tickets.push({
                        name: 'ticket-id',
                        value: value_id
                    })
                }
            });

            $.ajax({
                url: '{{ route('archive-tickets') }}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    tickets: tickets,
                },
            }).done(function( response ) {
                if (response.result) {
                    $('.control-archive-tickets').prop('checked', false)
                    $('#archive-tickets').empty().attr('hidden',true)
                    $('.list-archive').removeClass('ml-auto').removeClass('ml-3').addClass('ml-auto');
                    table.draw()
                }
            });            
            @endrole --}}
        }).on('click', '#search_btn', function(){
            table.draw();
        }).on('keypress', '[name="q"]', function(e){
            if (e.keyCode == 13) {

                table.draw()
            }
        })

	</script>
@endsection
