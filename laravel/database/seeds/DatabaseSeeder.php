<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(CompanyUserTypeSeeder::class);
        $this->call(SeveritiesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        // $this->call(CoveragesTableSeeder::class);
        // $this->call(ContractsTableSeeder::class);
        // $this->call(DepartmentsTableSeeder::class);
        // $this->call(SlaTableSeeder::class);
        // $this->call(GearsTableSeeder::class);
        // $this->call(CompaniesTableSeeder::class);
        $this->call(QuestionTypeSeeder::class);
    }
}
