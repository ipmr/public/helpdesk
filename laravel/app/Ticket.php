<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\SoftDeletes;


class Ticket extends Model
{

    use SoftDeletes;

    protected $table   = 'tickets';
    public $timestamps = true;
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function author()
    {

        return $this->belongsTo('App\User', 'contact_id', 'id');
    }

    public function company()
    {

        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function category()
    {

        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function agent()
    {

        return $this->belongsTo('App\Agent', 'agent_id', 'id');
    }

    public function gear()
    {

        return $this->hasOne('App\Gear', 'id', 'gear_id');
    }

    public function department()
    {

        return $this->belongsTo('App\Department', 'department_id', 'id');
    }

    public function comments()
    {

        return $this->hasMany('App\Comment', 'ticket_id', 'ticket_id');
    }

    public function histories()
    {

        return $this->hasMany('App\History', 'ticket_id', 'ticket_id');
    }

    public function tasks()
    {

        return $this->hasMany('App\Task', 'ticket_id', 'ticket_id');
    }


    public function project_ticket()
    {
        return $this->hasOne('App\ProjectTicket', 'ticket_id', 'ticket_id');
    }


    public function ticket_time()
    {
        return $this->hasOne('App\TicketTime', 'ticket_id', 'ticket_id');
    }


    public function getRemoteAttentionTimeAttribute(){

        $minutes = $this->comments->where('atention', 'Remota')->sum('event_time');

        $format_time = $this->diff_minutes_for_humans($minutes);

        return $format_time;

    }


    public function getSiteAttentionTimeAttribute(){


        $minutes = $this->comments->where('atention', 'En sitio')->sum('event_time');

        $format_time = $this->diff_minutes_for_humans($minutes);

        return $format_time;
        
        
    }


    public function getRemoteAttentionPercentAttribute(){


        $total_minutes = $this->comments->sum('event_time');

        $minutes = $this->comments->where('atention', 'Remota')->sum('event_time');

        if($minutes > 0){

            $percent = number_format(($minutes / $total_minutes) * 100);
            
        }else{

            $percent = 0;

        }

        return $percent;

    }


    public function getSiteAttentionPercentAttribute(){


        $total_minutes = $this->comments->sum('event_time');

        $minutes = $this->comments->where('atention', 'En sitio')->sum('event_time');

        if($minutes > 0){

            $percent = number_format(($minutes / $total_minutes) * 100);
            
        }else{

            $percent = 0;

        }


        return $percent;
        
    }


    public function getRemoteAttentionMinutesAttribute()
    {
        $minutes = $this->comments->where('atention', 'Remota')->sum('event_time');

        return $minutes;
    }

    public function getSiteAttentionMinutesAttribute()
    {
        $minutes = $this->comments->where('atention', 'En sitio')->sum('event_time');

        return $minutes;
    }


    public function diff_minutes_for_humans($minutes){


        $total_days = number_format(floor ($minutes / 1440));
        
        $total_hours = number_format(floor(($minutes - ($total_days * 1440)) / 60));

        $total_minutes = number_format(floor(($minutes - ($total_days * 1440)) - ($total_hours * 60)));


        $format_days = $total_days > 0 ? $total_days . 'd' : '';

        $format_hours = $total_hours > 0 ? $total_hours . 'hrs' : '';

        $format_minutes = $total_minutes > 0 ? $total_minutes . 'min' : '';

        if($format_days || $format_hours || $format_minutes){

            return "{$format_days} {$format_hours} {$format_minutes}";

        }else{
            
            return null;

        }


    }

    public static function format_minutes( $minutes = 0 )
    {


        $total_days = number_format(floor ($minutes / 1440));
        
        $total_hours = number_format(floor(($minutes - ($total_days * 1440)) / 60));

        $total_minutes = number_format(floor(($minutes - ($total_days * 1440)) - ($total_hours * 60)));

        $format_days = $total_days > 0 ? $total_days . ' d' : '';

        $format_hours = $total_hours > 0 ? $total_hours . ' hrs' : '';

        $format_minutes = $total_minutes > 0 ? $total_minutes . ' min' : '';

        if($format_days || $format_hours || $format_minutes)
        {

            return "{$format_days} {$format_hours} {$format_minutes}";

        } else {
            
            return null;

        }
    }

    public function reopen_tickets()
    {

        return $this->hasMany('App\ReopenTicket', 'ticket_id', 'ticket_id');
    }

    public function get_status()
    {

        return $this->belongsTo('App\Status', 'status_id', 'id');
    }

    public function status()
    {

        $status = $this->get_status;

        return '<span style="color: ' . $status->color . '">' . $status->name . '</span>';
    }

    public function get_severity()
    {

        return $this->belongsTo('App\Severity', 'severity_id', 'id');
    }

    public function severity()
    {

        $severity = $this->get_severity;

        return '<span style="color: ' . $severity->color . '">' . $severity->name . '</span>';
    }

    public static function by_status($status)
    {

        $tickets = \App\Ticket::get()->filter(function ($ticket) use ($status) {

            if ($ticket->get_status->name == $status) {
                return $ticket;
            }

        });

        return $tickets;
    }

    public function mailTo()
    {
        $mails = [];
        $severity = $this->severity_id;
        $category = $this->category->mails()->where('severity_id', $severity)->first();

        if ( !empty( $category->mails ) ) {
            $category_mails = explode(',', str_replace(' ', '', $category->mails));

            /*$department = $category->department;

            if ( $department ) {

                $mails[] = $department->supervisor->user->email;

            }*/
            foreach ($category_mails as $mail) {

                $mails[] = $mail;

            }
        }

        return $mails;
    }

    public static function paginate_tickets($request, $tickets)
    {

        $currentPage      = LengthAwarePaginator::resolveCurrentPage();
        $perPage          = 20;
        $currentPageItems = $tickets->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems   = new LengthAwarePaginator($currentPageItems, count($tickets), $perPage);
        $paginatedItems->setPath($request->url());

        return $paginatedItems;
    }

    public function overdue()
    {

        $closed_status = Status::where('name', 'Cerrado')->first();

        if( $this->status_id <> $closed_status->id && 
            $this->company->sla && 
            Carbon::now() > $this->sla_resolve)
        {
            return true;
        }
    }

    public function getSlaRespondAttribute()
    {

        $time = $this->format_attend_date('response');

        return $time;
    }

    public function getContractAttribute()
    {

        $contract = null;

        if($this->gear && $this->gear->contract_id){
            $contract = $this->gear->contract;
        }elseif($this->company->contract){
            $contract = $this->company->contract;
        }elseif($this->gear && $this->gear->master_contract && $this->company->master_parent->contract){
            $contract = $this->company->master_parent->contract;
        }

        return $contract;
    }

    public function getCoverageAttribute()
    {

        $coverage = null;

        if($this->gear && $this->gear->coverage_id){
            $coverage = $this->gear->coverage;
        }elseif($this->company->coverage){
            $coverage = $this->company->coverage;
        }

        return $coverage;
    }

    public function getSlaResolveAttribute()
    {

        $time = $this->format_attend_date('resolve');

        return $time;
    }

    public function format_attend_date($type)
    {

        $created_at      = $this->created_at;
        $ticket_severity = $this->get_severity->id;
        $coverage        = null;
        if($this->gear && $this->gear->coverage){
            $coverage = $this->gear->coverage;
        }else{
            $coverage = $this->company->coverage;
        }
        $sla             = $this->company->sla;
        $severity        = $sla->severities()->where('severity_id', $ticket_severity)->first();

        $number = $type == 'resolve' ? $severity->resolve_number : $severity->respond_number;
        $format = $type == 'resolve' ? $severity->resolve_format : $severity->respond_format;

        $time = $this->add_sla_to_time($format, $created_at, $number);

        $reset_time = $this->reset_time_by_coverage($time, $coverage, $format, $number);

        return $reset_time;
    }

    public function add_sla_to_time($format, $time, $number)
    {

        $new_time = null;

        if ($format == 'i') {
            $new_time = $time->addMinutes($number);
        } elseif ($format == 'h') {
            $new_time = $time->addHours($number);
        } elseif ($format == 'd') {
            $new_time = $time->addDays($number);
        } elseif ($format == 'm') {
            $new_time = $time->addMonths($number);
        }

        return $new_time;
    }

    public function reset_time_by_coverage($time, $coverage, $format, $number)
    {

        $coverage_init = $this->created_at->hour($coverage->from_hour)->minute(0)->second(0);
        $coverage_end  = $this->created_at->hour($coverage->to_hour)->minute(0)->second(0);
        $days          = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $weekDay       = $time->dayOfWeek;

        if ($time < $coverage_init) {
            
            $time = $this->add_sla_to_time($format, $coverage_init, $number);
            return $time;

        } elseif ($time >= $coverage_init && $time <= $coverage_end) {
            if ($weekDay > $coverage->to_day || $weekDay < $coverage->from_day) {
                $nextWeek = new Carbon('next ' . $days[$coverage->from_day - 1]);
                $nextWeek->hour($coverage->from_hour);
                $time = $this->add_sla_to_time($format, $nextWeek, $number);
                return $time;
            } else {
                return $time;
            }
        } elseif ($time > $coverage_end) {
            if ($weekDay >= $coverage->to_day) {
                $nextWeek = new Carbon('next ' . $days[$coverage->from_day - 1]);
                $nextWeek->hour($coverage->from_hour);
                $time = $this->add_sla_to_time($format, $nextWeek, $number);
                return $time;
            } else {
                $tomorrow = new Carbon('tomorrow');
                $tomorrow->hour($coverage->from_hour);
                $time = $this->add_sla_to_time($format, $tomorrow, $number);
                return $time;
            }
        }
    }
}
