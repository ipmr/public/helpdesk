<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public function inventory(){

        return $this->belongsTo('App\Inventory', 'id', 'inventory_id');

    }

    public function gear(){

        return $this->hasOne('App\Gear', 'id', 'gear_id');

    }
}
