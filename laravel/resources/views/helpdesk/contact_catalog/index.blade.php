@extends('helpdesk.companies.layouts.tabs')

@section('tab_content')
    
    <div id="catalog_app">
        
        <table class="table table-striped m-0">
            
        
            <thead>
                
                <tr>
                    <th>Nombre</th>
                    <th>E-mail</th>
                    <th>Zonas asignadas</th>
                    <th class="text-center">Asignar zonas</th>
                    @role('admin')
                    <th class="text-center">Acciones</th>
                    @endrole
                </tr>

            </thead>


            <tbody>
                
            
                @foreach($contacts as $contact)

                    @isset ($contact->contact)
                        <tr>
                            
                            <td>{{ $contact->full_name }}</td>
                            <td>{{ $contact->email }}</td>
                            
                            <td>
            
                    
                                @if($contact->zones)
        

                                    <ul class="list-group no-striped">

                                        @foreach($contact->zones as $zone) 

                                            <li class="list-group-item d-flex align-items-center"
                                                data-rol="{{ $zone->company->find_contact($contact->contact)->role }}">


                                                <span>
                                                    
                                                    <a href="{{ route('companies.show', $zone->company) }}/">

                                                        {{  $zone->company->name }}

                                                    </a>

                                                    | {{  $zone->company->find_contact($contact->contact)->company_user_type->name }}

                                                </span>
                                                
                                                <form   action="{{ route('zoneusers.destroy', $zone) }}/" 
                                                        method="POST"
                                                        class="ml-auto" 
                                                        @submit.prevent="unassign_contact_from_zone">
                                                    
                                                    {{ csrf_field() }}

                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn btn-link text-danger p-0">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    
                                                </form>


                                            </li>

                                        @endforeach

                                    </ul>


                                @else
        

                                    El contacto no se encuentra asignado en ninguna zona.


                                @endif

                            </td>

                            <td class="text-center">
                                
                                <a href="#" @click.prevent="assign_contact_to_zone({{ optional($contact->contact)->id }})">Asignar</a>

                            </td>
                            @role('admin')
                            <td class="text-center">
                                @if ($contact->company->contact_id === $contact->user_id)
                                    <a href="#" @click.prevent="suspend_principal('{{ $contact->company->id }}','{{ $contact->user_id }}')" class="btn btn-link text-danger">
                                        Suspender Contacto
                                    </a>
                                @else
                                    <button type="submit" class="btn btn-link text-danger" form="delete_form">
                                        Suspender Contacto
                                    </button>
                                    <form action="{{ route('companies.contacts.destroy', [
                                        'company' => $contact->company, 
                                        'contact' => $contact->user_id]) }}" id="delete_form" method="post">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                    </form>
                                @endif
                            </td>
                            @endrole
                        </tr>
                    @endisset

                @endforeach


            </tbody>


        </table>

        
        @include('helpdesk.contact_catalog.components.modal-assign-principal')
        @include('helpdesk.contact_catalog.components.modal-assign-contact-to-zone')
        @role('admin')
        @include('helpdesk.contact_catalog.components.modal-suspend-contact-assign-principal-to-zone')
        @endrole
    </div>


@endsection

@section('scripts')
    <script>
        
        
        var catalogApp = new Vue({


            el: "#catalog_app",

            data: {


                relief_contact: null,
                relief_contact_form_route: null,
                contact_to_assign: null,
                zone_to_assign_contact: null,

                @role('admin')
                contact_suspend_url: null,
                @endrole

            },

            methods: {


                unassign_contact_from_zone: function(e){


                    var t = this;

                    
                    var form = $(e.target);


                    var btn = $("button", form);


                    var role = form.closest('li').data('rol');


                    var ask = confirm('¿Estas seguro que deseas desasignar al contacto de esta zona?');


                    t.relief_contact_form_route = form.attr('action');

                    
                    if(ask){


                        if(role  ===  1){


                            var modal = $('#choose_contact');

                            
                            modal.modal('show');

                            
                            modal.on('hidden.bs.modal', function (e) {
                                    
                                btn.removeAttr('disabled');

                            });


                        }else{


                            form.submit();


                        }


                    }else{


                        setTimeout(function(){

                            btn.removeAttr('disabled');

                            t.relief_contact_form_route = null;

                        }, 150);

                    }


                },


                choose_relief_contact: function(e){


                    var t = this;

                    var select = $(e.target);

                    var value = select.val();

                    t.relief_contact = value;


                },

                assign_contact_to_zone: function(contact_id){


                    var t = this;

                    var modal = $('#assign_contact_to_zone_modal')

                    modal.modal('show');

                    t.contact_to_assign = contact_id; 


                },

                assign_contact_to_zone_route: function(){

                    var t = this;

                    var route = "{{ route('companies.contact-catalogs.store', "#company_id") }}";

                    route = route.replace('#company_id', t.zone_to_assign_contact);

                    return route;

                },

                select_zone_to_assign_contact: function(e){


                    var t = this;

                    var select = $(e.target);

                    var value = select.val();


                    t.zone_to_assign_contact = value;


                },
                suspend_principal: function(company_id, contact_id){
                    var t = this
                    t.contact_suspend_url = '{{ route('companies.contacts.destroy', ['company' => '#company_id', 'contact' => '#contact_id']) }}'

                    t.contact_suspend_url = t.contact_suspend_url.replace('#company_id', company_id)
                    t.contact_suspend_url = t.contact_suspend_url.replace('#contact_id', contact_id)

                    $('#modal_suspend_contact').modal('show')
                }
            }
        });


    </script>

@endsection