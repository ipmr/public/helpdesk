<?php

use Faker\Generator as Faker;

$factory->define(App\GearType::class, function (Faker $faker) {
    return [
        'name' => strtoupper($faker->word)
    ];
});
