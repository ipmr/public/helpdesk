<?php

use Faker\Generator as Faker;

$factory->define(App\CompanyUser::class, function (Faker $faker) {
    return [
    	'user_id' => 1,
        'team_id' => 1,
        'jobtitle' => $faker->jobTitle,
        'phone' => $faker->tollFreePhoneNumber,
        'ext' => rand(300,400),
        'mobile' => $faker->tollFreePhoneNumber
    ];
});
