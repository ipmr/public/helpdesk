<?php

use Faker\Generator as Faker;

$factory->define(App\Ticket::class, function (Faker $faker) {

    return [

        'ticket_id'     => '0001000001',
        'category_id'   => 1,
        'subject'       => $faker->text($maxNbChars = 70),
        'description'   => $faker->text($maxNbChars = 200),
        'company_id'    => 1,
        'department_id' => 1,
        'agent_id'      => 1,
        'contact_id'    => 1,
        'severity_id'   => 1,
        'status_id'     => 1,

    ];
});
