@extends('layouts.master')

@section('content')

	<div class="container">
		<div class="card">
			<div class="card-header d-flex align-items-center">
				<p class="lead">Listado de compañias</p>
				<form action="{{ route('companies.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
	                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
	                    value="{{ $_GET['q'] }}"
	                @endisset>
	                <button class="btn btn-success" type="submit">
	                    <i class="fa fa-search"></i> Buscar
	                </button>
	            </form>
				@role('admin','manager')
				<a href="{{ route('companies.create') }}/" class="btn btn-primary ml-2"><i class="fa fa-plus"></i> Nueva Compañia</a>
				@endrole
			</div>
			@if(count($companies) > 0)
				<table class="table table-striped m-0">
					<thead>
						<tr>
							<th>Nombre de la compañia</th>
							<th>RFC</th>
							<th>Teléfono</th>
							<th class="text-center">Fecha de alta</th>
						</tr>
					</thead>
					<tbody>
						@foreach($companies as $company)
						<tr>
							<td>
								@component('helpdesk.companies.components.folder_link', ['company' => $company])@endcomponent
								<a href="{{ route('companies.show', $company) }}">{{ $company->name }}</a>
								@if($company->childs->count() > 0)
									<div class="collapse company_tree" id="zone_{{ $company->id }}">
										<div class="pl-4 tree_link">
											@foreach($company->childs as $zone)
												@component('helpdesk.companies.components.tree', ['zone' => $zone]) 
												@endcomponent
											@endforeach
										</div>
									</div>
								@endif
							</td>
							<td>{{ $company->rfc }}</td>
							<td>{{ $company->phone }}</td>
							<td class="text-center">{{ $company->created_at->format('d M, Y') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="card-footer">
					{{ $companies->links() }}					
				</div>
			@else
				<div class="card-body">
					No se han agregado compañias
				</div>
			@endif
		</div>
	</div>

@stop