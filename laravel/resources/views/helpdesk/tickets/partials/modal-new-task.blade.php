@component('helpdesk.components.modal', [
	'id' 	=> 'new_task_modal',
	'title' => 'Nueva tarea'
])

	@slot('body')
		<form action="{{ route('tickets.tasks.store', $ticket->ticket_id) }}" method="post" id="new_task_form">
			{{ csrf_field() }}

			<div class="form-group">
				<textarea name="description" rows="3" class="form-control" placeholder="Descripción de la tarea..." required></textarea>
			</div>
			
			<div class="form-group m-0">

				<label><b>Asignar tarea a agente</b> <span class="text-muted">(Opcional)</span></label>

				<select name="agent_id" class="form-control">
					<option value="null">Elige un agente</option>
					@foreach(App\Agent::get() as $agent)
						<option value="{{ $agent->id }}" {{ ($ticket->agent_id == $agent->id)?'selected':'' }}>{{ $agent->user->first_name .' '. $agent->user->last_name }}</option>
					@endforeach
				</select>
			</div>

		</form>
	@endslot
	@slot('footer')
		<button type="submit" form="new_task_form" class="btn btn-success">
			<i class="fa fa-check"></i>
			Crear Tarea
		</button>
	@endslot
@endcomponent