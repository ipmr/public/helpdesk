@extends('layouts.master')

@section('content')

	<div class="container text-center">
		<h1 class="display-4">Error 404</h1>
		<p class="lead">La página que intentas buscar no existe</p>
		<p>Por favor regresa al <a href="{{ route('home') }}">inicio</a></p>
	</div>

@stop