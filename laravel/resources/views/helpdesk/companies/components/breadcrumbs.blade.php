@if(!$loop->last)
	<li class="list-inline-item">
		<a href="{{ route('companies.show', $parent) }}" title="{{ $parent->name }}">
			

			{{ str_limit($parent->name, 18) }}


		</a>
	</li>
	<span style="margin: 0 6px 0 0">/</span>
@else
	<li class="list-inline-item active" aria-current="page" title="{{ $company->name }}">
		{{ $company->name }}
	</li>
@endif