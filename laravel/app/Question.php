<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function question_type()
    {

        return $this->hasOne('App\QuestionType', 'id', 'question_type_id');

    }
}
