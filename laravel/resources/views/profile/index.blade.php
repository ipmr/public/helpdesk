@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <p class="lead">Mi Cuenta</p>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-sm-4 mx-auto">
                    <label><b>Información personal</b></label>
                    <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
                </div>
            </div>
            <form action="{{ route('profile.update', ['id' => $user->id]) }}" id="profileForm" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
            @if ($user->role == 'admin' || $user->role == 'manager')
                @include('profile.partials.form')
            @elseif($user->role == 'agent')
                @include('helpdesk.agents.partials.form', ['agent' => $user->agent, 'update'=>true])
            @elseif($user->role == 'company')
                @include('helpdesk.contacts.partials.form', ['company' => true])
            @endif
            </form>
        </div>
        <div class="card-footer">
            <div class="row form-row">
                <div class="col-sm-8 ml-auto">
                    <button type="submit" form="profileForm" id="company_form_btn" class="btn btn-success">
                        <i class="fa fa-check"></i>
                        Actualizar Perfil
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop