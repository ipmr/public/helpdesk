<?php
use App\ProjectHistory;
use Faker\Generator as Faker;

$factory->define(ProjectHistory::class, function (Faker $faker) {
    return [
        'project_id'  => 1,
        'description' => $faker->text,
        'status'      => $faker->name,
        'condition'   => $faker->name,
    ];
});
