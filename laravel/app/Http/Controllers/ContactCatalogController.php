<?php

namespace App\Http\Controllers;

use App\Company;
use App\ContactCatalog;
use App\ZoneUser;
use Illuminate\Http\Request;
use Validator;

class ContactCatalogController extends Controller
{


    public function __construct(){


        $this->middleware('role:admin,manager');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Company $company)
    {


        // $contacts = $company
        //     ->contacts_catalog()
        //     ->with('contact')
        //     ->paginate();


        // if (isset($request->q)) {
            
        //     $query_string = explode(' ', $request->q);

        //     $contacts = $contacts->where(function ($q) use ($query_string, $request) {
        //         $q = $q->where('users.first_name', 'like', '%' . $request->q . '%')
        //             ->orWhere('users.last_name', 'like', '%' . $request->q . '%')
        //             ->orWhere('users.email', 'like', '%' . $request->q . '%')
        //             ->orWhere('users.username', 'like', '%' . $request->q . '%');

        //         foreach ($query_string as $search) {
        //             $q = $q->orWhere('users.first_name', 'like', '%' . $search . '%')
        //                 ->orWhere('users.last_name', 'like', '%' . $search . '%')
        //                 ->orWhere('users.email', 'like', '%' . $search . '%')
        //                 ->orWhere('users.username', 'like', '%' . $search . '%');
        //         }

        //         return $q;
        //     });

        // }



        $contacts = $company->contacts_catalog()->with('contact')->get();


        return view('helpdesk.contact_catalog.index', compact('contacts', 'company'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {

        $this->validation($request);

        $exist_zone_user = $company->contacts()->where('user_id', $request->user_id)->first();

        if (empty($exist_zone_user)) {

            $contact_to_zone          = new ZoneUser();
            $contact_to_zone->user_id = $request->user_id;
            $contact_to_zone->zone_id = $company->id;
            $contact_to_zone->role    = $request->type;

            $contact_to_zone->save();

        } else {
            
            return redirect()
                ->back()
                ->with('warning', 'El usuario ya está asignado a la zona');
        }

        return redirect()
            ->back()
            ->with('msg', 'El usuario fue asignado a la zona');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactCatalog  $contact_catalog
     * @return \Illuminate\Http\Response
     */
    public function show(ContactCatalog $contact_catalog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactCatalog  $contact_catalog
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactCatalog $contact_catalog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactCatalog  $contact_catalog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contact_catalog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactCatalog  $contact_catalog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactCatalog $contact_catalog)
    {
        //
    }

    public function validation($request)
    {
        $rules = [

            'user_id' => [
                'required',
                'exists:users,id',
            ],
            'type'    => [
                'required',
                'exists:company_user_types,id',
            ],
        ];

        $msgs = [
            'user_id.required' => 'No ha sido enviado el id de usuario, el valor es requerido',
            'user_id.exists'   => 'El usario dado, no existe en la base de datos',
            'type.required'    => 'El campo  tipo de contacto es requerido',
            'type.exists'      => 'El tipo de contacto, no existe en la base de datos',
        ];

        $validate = Validator::make($request->all(), $rules, $msgs)->validate();
    }
}
