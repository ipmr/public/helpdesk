@extends('layouts.master')

@section('content')
    <div class="container" id="gears_app">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-link p-0" href="{{ route('inventories.index') }}/">
                    <i class="fa fa-angle-left fa-xs mr-1"></i>
                    Regresar
                </a>
            </div>
            <div class="card-body">
                <form action="{{ route('inventories.update', $inventory) }}" method="post" id="form_update">
                    {{ method_field('PATCH') }}
                    @include('helpdesk.inventory.partials.form')
                </form>
            </div>
            <div class="card-footer">
                <div class="form-row form-group m-0">
                    <div class="col-sm-8 ml-auto">
                        <button type="submit" class="btn btn-success" form="form_update">
                            <i class="fa fa-check"></i> Actualizar Almacen
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    
    @include('helpdesk.gears.partials.scripts')

@endsection