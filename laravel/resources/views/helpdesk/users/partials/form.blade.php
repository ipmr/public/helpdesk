<div class="form-row">
    <div class="col-sm-4 mx-auto">
        <label><b>Información del usuario</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>

<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Nombre(s):
    </label>
    <div class="col-sm-4">
        <input type="text" name="first_name" value="{{ $user->first_name or old('first_name') }}" class="form-control" autofocus required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Apellido(s):
    </label>
    <div class="col-sm-4">
        <input type="text" name="last_name" value="{{ $user->last_name or old('last_name') }}" class="form-control" required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Usuario:
    </label>
    <div class="col-sm-4">
        <input type="text" name="username" value="{{ $user->username or old('username') }}" class="form-control" required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        E-mail:
    </label>
    <div class="col-sm-4">
        <input type="email" name="email" value="{{ $user->email or old('email') }}" class="form-control" required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Tipo usuario:
    </label>
    <div class="col-sm-5">
        <div class="d-flex align-items-center">
            <select name="role" class="form-control" required>
                <option disabled selected value>Elige un tipo de usuario</option>
                <option value="admin" @isset ($user->role)
                    {{ ($user->role == 'admin')?'selected':'' }}
                @endisset>Administrador</option>
                <option value="manager" @isset ($user->role)
                    {{ ($user->role == 'manager')?'selected':'' }}
                @endisset>Manager</option>
            </select>
        </div>
    </div>
</div>
<div class="form-group row form-row mb-0">
    <label class="col-sm-4 col-form-label text-right">
        Contraseña:
    </label>
    <div class="col-sm-4">
        <input type="password" name="password" class="form-control">
    </div>
</div>