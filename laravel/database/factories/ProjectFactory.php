<?php
use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {

    $today      = date('Y-m-d');
    $start_date = date('Y-m-d', strtotime( '-'.rand(1,500).'day '.$today ));
    $end_date   = date('Y-m-d', strtotime( '+'.rand(1,500).'day '.$start_date ));

    return [
        'title'       => $faker->company,
        'description' => $faker->text,
        'fup'         => rand(11111,99999),
        'status'      => 1,
        'start_date'  => $start_date,
        'end_date'    => $end_date,
    ];
});
