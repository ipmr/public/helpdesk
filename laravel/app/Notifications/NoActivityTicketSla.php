<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NoActivityTicketSla extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->tickets = $request['tickets'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = new MailMessage();

        $message->subject('Tickets SLA sin actividad');
        $message->markdown('emails.notifications.no_activity_sla',['tickets' => $this->tickets]);

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ids = [];
        foreach ($this->tickets as $ticket) {
            $ids[] = $ticket->ticket_id;
        }

        return [
            'msg' => 'Los siguientes tickets no cuentan con actividad, favor de revisar de manera urgente.',
            'ids' => implode(',', $ids),
        ];
    }
}
