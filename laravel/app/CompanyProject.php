<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProject extends Model
{
    public function company()
    {

        return $this->belongsTo('App\Company', 'company_id', 'id');

    }

    public function project()
    {

        return $this->belongsTo('App\Project', 'project_id', 'id');

    }
}
