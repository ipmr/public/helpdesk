<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    
	protected $table   = 'departments';
    public $timestamps = true;
    protected $guarded = [];

	public function tickets(){

		return $this->hasMany('App\Ticket', 'department_id', 'id');

	}

	public function agents(){

		return $this->hasMany('App\Agent', 'department_id', 'id');

	}

	public function supervisor(){

		return $this->hasOne('App\Agent', 'id', 'supervisor_id');

	}

	public function groups(){


		return $this->hasMany('App\DepartmentGroup', 'department_id', 'id');

	}

}
