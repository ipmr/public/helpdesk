<?php

Route::redirect('/', 'login');

Route::view('email-test', 'email');

Auth::routes();

#Se redirecciona la ruta de registro a inicio de sesión
Route::redirect('register', 'login');

Route::view('under-construction', 'errors.under-construction')->name('under-construction');

Route::get('pdf-event', function(){

    $ticket = App\Ticket::first();

    return view('helpdesk.tickets.pdf.event', compact('ticket'));
});

#Rutas que requieren autenticación
Route::group(['middleware' => 'auth'], function () {


    Route::get('/home', 'DashboardController@index');


    Route::get('/', 'DashboardController@index')->name('home');
     

    #Notificaciones
    Route::get('notifications', 'NotificationsController@index')->name('notifications');
    Route::post('notification/{notification}/read', 'NotificationsController@read')->name('read-notification');
    Route::post('notification/archive', 'NotificationsController@destroy')->name('archive-notification');
    Route::get('notification/list-archive', 'NotificationsController@list_archive')->name('list-archive');


    #Busqueda avanzada
    Route::get('search', 'TicketController@advance_search')->name('advance-search');
    

    #Ticket
    Route::resource('tickets', 'TicketController');
    Route::post('tickets/archive', 'TicketController@archive_tickets')->name('archive-tickets');
    Route::get('ticket_list_archive', 'TicketController@list_archive')->name('list-archive-tickets');
    Route::get('tickets/{ticket}/show_deleted', 'TicketController@show_deleted')->name('show_deleted');
    Route::post('tickets/unarchive', 'TicketController@unarchive_tickets')->name('unarchive-tickets');
    Route::post('{ticket}/set-public-comment', 'TicketController@set_public_comment')->name('public-comment')->middleware('ticket_owner');
    // Route::post('{ticket}/set-internal-comment', 'TicketController@set_internal_comment')->name('internal-comment')->middleware('role:admin,agent,manager');
    Route::post('tickets/{ticket}/download', 'TicketController@to_pdf')->name('export_ticket_to_pdf');

    #Reabrir ticket
    Route::post('reopen_ticket/{ticket}', 'ReopenTicketController@reopen_ticket_action')->name('reopen_ticket')->middleware('role:company,agent');
    Route::get('read_reopen_ticket/{reopen_ticket}', 'ReopenTicketController@mark_reopen_ticket_as_read')->name('read_reopen_ticket');
    

    #Tareas
    Route::resource('tickets.tasks', 'TaskController')->only(['store', 'show', 'update', 'destroy']);
    Route::post('tickets/{ticket}/tasks/{tasks}/start_end_task', 'TaskController@start_end_task')->name('start-end-task');
    Route::get('tasks', 'TaskController@index')->name('agent-tasks')->middleware('role:agent');
    Route::post('tickets/{ticket}/tasks/{tasks}/reassing', 'TaskController@reassign')->name('reassing-task');
    Route::resource('tickets.tasks.events', 'TaskEventController');
    

    #Asignar proyecto
    Route::post('ticket/{ticket}/assign_to_project', 'ProjectTicketController@assign_ticket_to_project')->name('assign_to_project');

    #Departamentos
    Route::resource('departments', 'DepartmentController');
    Route::get('departments/{department}/agents', 'DepartmentController@get_department_agents')->name('get_department_agents');


    #Grupos de Departamentos
    Route::resource('departments.groups', 'DepartmentGroupController');
    

    #Agentes
    Route::resource('agents', 'AgentController');
    

    #Compañias
    Route::resource('companies', 'CompanyController');
    

    #Equipos
    Route::resource('companies.gears', 'GearController')->except(['create']);
    Route::resource('companies.gears.modules', 'GearModuleController');
    

    Route::post('insert/{module}/into/{gear}', 'GearModuleController@insert_module')->name('insert_module');
    Route::post('remove/{module}', 'GearModuleController@remove_module')->name('remove_module');


    Route::get('{type}/get-brands', 'GearController@get_brands')->name('get_brands');
    Route::get('{brand}/get-models', 'GearController@get_models')->name('get_models');
    Route::get('{company}/{model}/gears', 'GearController@get_gears_by_model')->name('get_gears_by_model');
    Route::post('reassing/{gear}', 'GearController@reassign_gear_to_zone')->name('reassign_gear_to_zone');
    Route::post('companies/{company}/gears/search', 'GearController@search_gear')->name('search_gear');
    Route::post('companies/{company}/gears/download', 'GearController@download_company_gear')->name('download_company_gear');


    #Zonas de compañias
    Route::resource('companies.zones', 'CompanyZonesController');
    
    
    #Tipos de contacto
    Route::resource('contact-types', 'CompanyUserTypeController');


    #Contactos de compañias
    Route::resource('companies.contacts', 'CompanyUserController');
    Route::get('{zone}/get_contacts', 'CompanyUserController@get_zone_contacts')->name('get_zone_contacts');


    #Catalogos de contacto
    Route::resource('companies.contact-catalogs', 'ContactCatalogController');


    #Zonas usuarios
    Route::resource('zoneusers', 'ZoneUserController');


    #Marcas de equipo
    Route::resource('brands', 'GearBrandController')->except(['create', 'edit']);
    

    #Agregar tipos de equipo a marcas
    Route::resource('brands.types', 'BrandTypeController');


    #Tipos de equipo
    Route::resource('types', 'GearTypeController')->except(['index', 'create', 'edit']);


    #Modelos de equipo
    Route::resource('brands.types.models', 'GearModelController')->except(['create', 'edit']);
    

    #Garantias
    Route::resource('warranties', 'WarrantyController');


    #Coberturas
    Route::resource('coverages', 'CoverageController');
    

    #Contratos
    Route::resource('contracts', 'ContractController');
    

    #Acuerdos se servicio (SLA)
    Route::resource('sla', 'SLAController');
    

    #Opciones ticket
    Route::resource('ticket-options', 'TicketOptionController');    


    #Estatus ticket
    Route::resource('status', 'StatusController');
    

    #Severidad ticket
    Route::resource('severity', 'SeverityController');
    

    #Categorias ticket
    Route::resource('category', 'CategoryController');
    Route::resource('category.subcategory', 'SubCategoryController');
    

    #Perfil 
    Route::resource('profile', 'ProfileController');
    

    #Directorio
    Route::resource('directory', 'DirectoryController');


    #Encuestas
    Route::resource('users', 'UserController');
    

    #Encuestas
    Route::resource('surveys', 'SurveyController');


    #Preguntas
    Route::resource('surveys.questions', 'QuestionController');


    #Reportes
    Route::get('reports', 'ReportController@index')->name('reports');
    Route::get('reports/{company}/contacts', 'ReportController@get_company_contacts')->name('get_company_contacts');
    Route::get('reports/{company}/contracts', 'ReportController@get_company_contracts')->name('get_company_contracts');
    Route::get('reports/{company}/company_gears', 'ReportController@get_company_gears')->name('get_company_gears');
    Route::get('reports/{company}/coverages', 'ReportController@get_company_coverages')->name('get_company_coverages');
    Route::post('reports/get_report', 'ReportController@get_report')->name('get_report');



    #Proyectos por compania
    Route::resource('companies.projects', 'CompanyProjectController');
    Route::post('companies/{company}/projects/{project}/project_ticket', 'ProjectController@project_ticket')->name('new-project-ticket');

    #Proyectos general
    Route::resource('projects', 'ProjectController');
    Route::get('project/details', 'ProjectController@project_details')->name('project_details');
    Route::post('project/{project}/cancel', 'ProjectController@cancel_project')->name('cancel_project');
    Route::post('project/{project}/active', 'ProjectController@active_project')->name('active_project');

    #Inventario
    Route::resource('inventories', 'InventoryController');
    Route::resource('inventories.stock', 'StockController');
    Route::post('inventories/{inventory}/stock/{stock}/assign_company_to_gear', 'StockController@assign_gear_to_company')->name('assign_gear_to_company');

    #Cotizaciones
    Route::resource('companies.projects.quotations', 'QuotationController');
    Route::post('companies/{company}/projects/{project}/validate_quotation', 'QuotationController@validate_quotation')->name('validate_quotation');
});
