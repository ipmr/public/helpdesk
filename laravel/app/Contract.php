<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table   = 'contracts';
    public $timestamps = true;
    protected $guarded = [];

    public function getLifeTimeAttribute(){

    	$number = $this->period_number;
    	$format = $this->period_format;
    	switch ($format) {
    		case 'd':
    			$format = 'Día(s)';
    			break;
    		case 'w':
    			$format = 'Semana(s)';
    			break;
    		case 'm':
    			$format = 'Mes(es)';
    			break;
    		case 'y':
    			$format = 'Año(s)';
    			break;
    	}

    	return "{$number} {$format}";

    }

}
