<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TicketsExport implements FromView
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function view(): View
    {
        return view('helpdesk.reports.tickets_report_excel', [
            'tickets'     => $this->data['tickets'],
            'complements' => $this->data['complements'],
            'params'      => $this->data['params'],
        ]);
    }
}
