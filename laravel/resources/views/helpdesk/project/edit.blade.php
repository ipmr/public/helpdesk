@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header card_header_fixed">
                <a class="btn btn-link p-0" href="{{ url()->previous() }}">
                    <i class="fa fa-angle-left"></i>
                    Regresar
                </a>
            </div>
            <div class="card-body">
                <form action="{{ route('projects.update', $project) }}" method="post" id="newProjectForm">
                    
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    @include('helpdesk.projects.partials.form')
                    
                </form>
            </div>
            <div class="card-footer">
                <div class="row form-row">
                    <div class="col-sm-8 ml-auto">
                        <button type="submit" form="newProjectForm" id="department_form_btn" class="btn btn-success">
                            <i class="fa fa-check"></i>
                            Actualizar Proyecto
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection