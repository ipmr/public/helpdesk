@component('helpdesk.components.modal', [

    'id' => 'assign_contact_to_zone_modal',
    'title' => 'Asignar contacto a una zona'

])


    @slot('body')
    
        <form :action="assign_contact_to_zone_route()" id="assign_contact_to_zone_form" method="POST">

            {{ csrf_field() }}


            <p>
                
                Elige la zona en la que deseas asignar al contacto.

            </p>


            <input type="hidden" name="user_id" :value="contact_to_assign">


            <div class="form-group">
                
                <select name="contact_id" class="form-control" @change="select_zone_to_assign_contact" required>
                    
        
                    <option disabled selected value="">Elige una opción</option>


                    @foreach($company->master_parent->all_childs() as $zone)

                        <option value="{{ $zone->id }}">{{ $zone->name }}</option>

                    @endforeach

                </select>

            </div>
            

            <p>Elige el rol que tendrá el contacto en la zona seleccionada</p>

            <div class="form-group">
                
                <select name="type" class="form-control" required>
                    
                    <option disabled selected value>Elige una opción</option>

                    @foreach (App\CompanyUserType::where('id','>',1)->get() as $type)
                        
                        <option value="{{ $type->id }}">{{ $type->name }}</option>

                    @endforeach

                </select>

            </div>


        </form>

    @endslot

    @slot('footer')

        
        <a href="#" class="btn btn-light" data-dismiss="modal" aria-label="Close">
    
            <i class="fa fa-times"></i>

            Cancelar

        </a>

    
        <button type="submit" form="assign_contact_to_zone_form" class="btn btn-success">
            
            <i class="fa fa-check"></i>

            Asignar Contacto

        </button>

    @endslot


@endcomponent