<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model
{
    public function project()
    {

        return $this->belongsTo('App\Project', 'project_id', 'id');
    }


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
