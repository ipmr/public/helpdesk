<form action="{{ route('tickets.update', $ticket->ticket_id) }}" method="post" style="width: 100%" id="updateAgentForm">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="form-group m-0">
        <select name="agent" class="form-control" @change="active_success_btn" style="border-radius: 3px 0 0 3px">
            <option disabled selected value>Elige una opción</option>
            @foreach($ticket->department->agents as $agent)
            <option value="{{ $agent->id }}"
                @if($ticket->agent)
                {{ $ticket->agent->id == $agent->id ? 'selected' : '' }}
                @endif
                >
                {{ $agent->full_name }}
            </option>
            @endforeach
        </select>
    </div>
</form>

<button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Agente" class="btn btn-success" form="updateAgentForm">
    <i class="fa fa-check" style="margin: 0 !important;"></i>
</button>