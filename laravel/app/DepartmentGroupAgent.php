<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentGroupAgent extends Model
{
    

	public function group(){


		return $this->belongsTo('App\DepartmentGroup', 'group_id', 'id');


	}


	public function agent(){


		return $this->belongsTo('App\Agent', 'agent_id', 'id');


	}


}
