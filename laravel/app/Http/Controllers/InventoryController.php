<?php

namespace App\Http\Controllers;

use App\Gear;
use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class InventoryController extends Controller
{


    public function __construct(){


        // $this->middleware('under-construction');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inventories = Inventory::where('id', '>', 0);

        if (isset($request->q)) {
            $inventories = $inventories->where('name', 'like', '%'.$request->q.'%');
        }

        $inventories = $inventories->paginate(12);

        return view('helpdesk.inventory.index', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('helpdesk.inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate_data_inventory($request);

        $inventory = new Inventory();

        $inventory->name = $request->name;

        $inventory->save();

        return redirect()
                ->route('inventories.index')
            ->with('msg', 'El almacen fue agregado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        return view('helpdesk.inventory.show', compact('inventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        $this->validate_data_inventory($request, $inventory);

        $inventory->name = $request->name;

        $inventory->save();

        return redirect()
                ->route('inventories.index')
            ->with('msg', 'El almacen fue actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
    }

    public function validate_data_inventory($request, $inventory = null)
    {

        #se especifican los mensajes para las validaciones
        $msgs = [
            'name.required' => 'El nombre del almacen es requerido',
            'name.unique'   => 'El nombre del almacen no esta disponible',
        ];

        $validations = [

            'name' => [   
                'required', 
                'required', 
                'string', 
                Rule::unique('inventories')->ignore($inventory ? $inventory->id : '')
            ]
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}
