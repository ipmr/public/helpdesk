<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyProject;
use App\Project;
use App\ProjectMember;
use App\QuotationResource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class CompanyProjectController extends Controller
{


    public function __construct(){


        // $this->middleware('under-construction');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {

        $tab = 'projects';

        return view('helpdesk.projects.index', compact('company', 'tab'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        $this->validate_project($request);

        $project = new Project();

        $project->title       = $request->title;
        $project->description = $request->description;
        $project->fup         = $request->fup;
        if (isset($request->start_date)) {
            $project->start_date = $request->start_date;
        }
        if (isset($request->end_date)) {
            $project->end_date = $request->end_date;
        }
        $project->status      = 1;

        $project->save();

        $company_project             = new CompanyProject();
        $company_project->project_id = $project->id;
        $company_project->company_id = $company->id;

        $company_project->save();

        $project_member = new ProjectMember();
        
        $project_member->project_id = $project->id;
        $project_member->user_id    = auth()->user()->id;
        $project_member->type       = 'owner';

        $project_member->save();

        return redirect()
            ->route('companies.projects.show', [$company, $project->fup])
            ->with('msg', 'El proyecto "' . $project->title . '" ha sido creado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyProject  $company_project
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company, $project)
    {   
        
        $project = Project::where('fup', $project)->firstOrFail();

        $revision_ticket = $project->project_tickets()->where('phase', 'levantamiento')->first();

        $revision_ticket = $revision_ticket ? $revision_ticket->ticket : null;

        $revision_ticket_status = $revision_ticket ? $revision_ticket->get_status->name : null;

        $quotation = $project->quotation ? $project->quotation : null;

        $quotation_resources = $quotation ? $quotation->resources : [];

        $tickets = $project->tickets->count() > 0 ? $project->tickets : null;

        $tab = 'projects';

        return view('helpdesk.projects.show', compact('company', 'project', 'revision_ticket', 'tickets', 'revision_ticket_status', 'quotation', 'quotation_resources', 'tab'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyProject  $company_project
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyProject $company_project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyProject  $company_project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyProject $company_project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyProject  $company_project
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyProject $company_project)
    {
        //
    }

    public function validate_project($request, $project = null)
    {

        #se especifican los mensajes para las validaciones
        $validations = [
            'fup'             => ['required', 'string', Rule::unique('projects')->ignore($project ? $project->id : 'unique:projects,fup')],
            'title'           => 'required|string|max:190',
            'description'     => 'required|string',
        ];

        $msgs = [
            'fup.required'           => 'El número de FUP es requerido',
            'fup.unique'             => 'El número de FUP no esta disponible',
            'title.required'         => 'El título es requerido',
            'title.string'           => 'El título debe ser alfanumérico',
            'title.max'              => 'El título debe contener 190 carácteres',
            'description.required'   => 'La descripción es requerido',
            'description.string'     => 'La descripción debe ser alfanumérico',
        ];

        #se realiza la validación
        $validate = Validator::make($request->all(), $validations, $msgs)->validate();

    }
}
