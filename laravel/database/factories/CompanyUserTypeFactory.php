<?php

use Faker\Generator as Faker;

$factory->define(App\CompanyUserType::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
