<div class="tab-pane fade {{ session()->get('tab') == 'public' ? 'show active' : '' }} {{ session()->get('tab') ? '' : 'show active' }}" id="public-comments" role="tabpanel" aria-labelledby="public-comments-tab">

	<div class="mb-3">
		<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_comment_to_company">
			<i class="far fa-comment"></i>
			Nuevo comentario
		</a>
	</div>
	
	@if(count($ticket->comments()->where('type', 'public')->get()) > 0)
	<table class="table table-sm m-0 table-bordered table-striped">
		<thead>
			<tr>
				<td>Fecha y hora</td>
				<td>Autor</td>
				<td>Comentario</td>
				<td class="text-right">Archivos</td>
			</tr>
		</thead>
		<tbody>
			@foreach($ticket->comments()->where('type', 'public')->latest()->get() as $comment)
			<tr>
				<td class="no-wrap">{{ $comment->created_at->format('d M, Y - h:i a') }}</td>
				<td class="no-wrap">
					@if($comment->user->role == 'manager' || $comment->user->role == 'admin')
						Asesor
					@else
						{{ $comment->user->first_name .' '. $comment->user->last_name }}
					@endif
				</td>
				<td>{{ $comment->comment }}</td>
				<td class="no-wrap text-right">
					@if(count($comment->files) > 0)
						@foreach($comment->files as $file)
						<a data-fancybox="public_gallery" href="{{asset('uploads/'.$file->filename)}}">
							<i class="fa fa-paperclip"></i> 
							{{ str_limit($file->filename, 30) }}
						</a>
						@if( ! $loop->last)
						<br>
						@endif
						@endforeach
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p class="m-0">No se han agregado comentarios</p>
	@endif
</div>