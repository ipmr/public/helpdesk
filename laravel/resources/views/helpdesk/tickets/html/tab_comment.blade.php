<div class="tab-pane fade {{ session()->get('tab') == 'public' ? 'show active' : '' }} {{ session()->get('tab') ? '' : 'show active' }}" id="public-comments" role="tabpanel" aria-labelledby="public-comments-tab">


    <div class="mb-3 d-flex align-items-center">
        
        @if($ticket->get_status->name != 'Cerrado' && $ticket->get_status->name != 'Resuelto')

            @role('admin', 'manager')

                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_comment_to_company">
                    <i class="far fa-comment"></i>
                    Nuevo evento
                </a>

            @endrole

            
            @role('agent')

                @if( ! auth()->user()->agent->as_manager && $ticket->agent)

                    @if($ticket->agent->user->id == auth()->user()->id)
                        
                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_comment_to_company">
                            <i class="far fa-comment"></i>
                            Nuevo evento
                        </a>

                    @endif

                @endif

            @endrole


            @role('company')
                @empty ($ticket->deleted_at)
                    
                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_comment_to_company">
                    <i class="far fa-comment"></i>
                    Nuevo evento
                </a>
                
                @endempty

            @endrole
            
        @endif

        @role('admin', 'manager', 'agent')

            <div class="ml-auto row" style="width: 50%">
                
                
                <div class="col-sm-6">
                    
                    <div class="d-flex align-item-center">
                        Atención Remota <b class="ml-auto">{{ $ticket->remote_attention_percent }}%</b>
                    </div>

                    <div class="progress mb-1" style="height: 3px;">
                      <div class="progress-bar" role="progressbar" style="width: {{ $ticket->remote_attention_percent }}%;" aria-valuenow="{{ $ticket->remote_attention_percent }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    
                    @if($ticket->remote_attention_time)

                    <small>[ {{ $ticket->remote_attention_time }} ]</small>

                    @endif

                </div>

                <div class="col-sm-6">
                    
                    <div class="d-flex align-item-center">
                        Atención En Sitio <b class="ml-auto">{{ $ticket->site_attention_percent }}%</b>
                    </div>

                    <div class="progress mb-1" style="height: 3px;">
                      <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $ticket->site_attention_percent }}%;" aria-valuenow="{{ $ticket->site_attention_percent }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    
                    @if($ticket->site_attention_time)

                    <small>[ {{ $ticket->site_attention_time }} ]</small>

                    @endif

                </div>


            </div>
    
        @endrole

    </div>


    

    @if( $ticket->comments()->count() > 0 )
        
        
        <ul class="list-group">

        
            @foreach($ticket->comments()->latest()->get() as $comment)

                
                @if( 
                    
                    $comment->type == 'public' || 

                    $comment->type == 'internal' && auth()->user()->role !== 'company'

                )


                    <li class="li list-group-item {{ $comment->type == 'internal' ? 'bg-danger-light' : '' }}">
                            
                    
                        <div class="d-flex align-items-center">
                            
                        
                            <p class="m-0">

                                <b>
                                    
                                    {{ $comment->user->full_name }} 

                                </b>

                            </p>


                            <small class="ml-auto"><b>#{{ $ticket->ticket_id . $comment->id }}</b></small>


                        </div>

            
                        <p class="text-muted">
                            
                            <small>
                                

                                @role('company')

                                    [ <i class="fa fa-calendar-alt"></i> {{ $comment->created_at->format('d M, Y - h:i a') }} ]

                                @else

                                    @if($comment->type == 'internal')   


                                        [ <i class="fa fa-lock fa-sm text-danger"></i> ]


                                    @endif

                                    
                                    [ <i class="far fa-calendar-alt"></i> {{ $comment->created_at->format('d M, Y - h:i a') }} ]
                    
                                    @if($comment->time)

                                    [ <i class="fa fa-stopwatch text-muted"></i> {{ $comment->time }} ] 

                                    @endif

                                    @if($comment->atention)
                                    
                                    [ <i class="far fa-clipboard"></i> {{ title_case('Atención ' . $comment->atention) }} ]

                                    @endif

                                @endrole


                            </small>

                        </p>


                        <p class="m-0">
                            
                            {{ $comment->comment }}

                        </p>


                        @if($comment->files->count() > 0)


                            <hr class="my-2">

                            @foreach($comment->files as $file)
                                
                                
                                <a  data-fancybox="public_gallery" 
                                    href="{{asset('uploads/'.$file->filename)}}"
                                    class="comment_thumb"
                                    style="background-image: url('{{ asset('uploads/' . $file->filename) }}');">
                                </a>


                            @endforeach


                        @endif 


                    </li>


                @endif


            @endforeach

        </ul>


    @else
        

        <p class="m-0">No se han agregado comentarios</p>


    @endif


</div>