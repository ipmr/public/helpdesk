@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<a href="{{ route('sla.index') }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
			<a href="{{ route('sla.edit', $sla) }}/" class="btn btn-primary ml-auto">
				<i class="fa fa-pencil-alt"></i> Editar
			</a>
		</div>
		<table class="table table-striped m-0">
			<tr>
				<th>Nombre:</th>
				<td>{{ $sla->name }}</td>
			</tr>
			<tr>
				<th>Descripción:</th>
				<td>{{ $sla->description }}</td>
			</tr>
		</table>
		<hr class="m-0">
		<table class="table table-striped m-0">
			<thead>
				<tr>
					<th>Severidad</th>
					<th class="text-center">Tiempo de respuesta</th>
				</tr>
			</thead>
			<tbody>
				@foreach($sla->severities as $severity)
				<tr>
					<td style="color: {{ $severity->severity->color }}">{{ $severity->name() }}</td>
					<td class="text-center">{{ $severity->respond() }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop