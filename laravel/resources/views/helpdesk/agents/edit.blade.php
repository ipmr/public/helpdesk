@extends('layouts.master')
@section('content')
<div class="container" id="agentApp">
	<div class="card">
		<div class="card-header">
			<a class="btn btn-link p-0" href="{{ route('agents.show', $agent) }}/">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<div class="card-body">
			<form action="{{ route('agents.update', $agent) }}" method="post" id="updateAgentForm">
				
				<div class="form-row">
					<div class="col-sm-4 mx-auto">
						<label><b>Información del agente</b></label>
						<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
					</div>
				</div>

				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				@include('helpdesk.agents.partials.form', [
					'update' => true
				])

			</form>
		</div>
		<div class="card-footer">
			<div class="row form-row">
				<div class="col-sm-8 ml-auto">
					<button class="btn btn-success" form="updateAgentForm">
						<i class="fa fa-check"></i>
						Actualizar Agente
					</button>
					@role('admin')
					<button type="submit" class="btn btn-link text-danger" form="delete_form">
						Suspender Agente
					</button>
					<form action="{{ route('agents.destroy', $agent) }}" id="delete_form" method="post" @submit="remove_agent">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
					@endrole
				</div>
			</div>
		</div>
	</div>
@if ( $agent->id == optional( optional( $agent->department )->supervisor )->id )
	@include('helpdesk.agents.components.assign_new_supervisor')
@endif
</div>
@stop


@section('scripts')
	

	@include('helpdesk.agents.scripts.general')


@stop