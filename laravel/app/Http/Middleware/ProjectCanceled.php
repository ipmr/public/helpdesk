<?php

namespace App\Http\Middleware;

use Closure;

class ProjectCanceled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (request('project') instanceof \App\Project) {

            $project = request('project');

        } else {

            $project = \App\Project::where('fup', request('project'))->first();

        }
        
        if (isset($project->id)) {

            if ($project->condition) {

                return $next($request);

            } 
            
        }

        return redirect()->back()->withErrors(['El proyecto se encuentra cancelado, por lo tanto no puede ser modificado.']);
    }
}
