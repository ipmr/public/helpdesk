<form action="{{ route('reassign_gear_to_zone', $g->id) }}" method="post" class="d-flex align-items-center">
	

	{{ csrf_field() }}


	<select name="company_id" class="form-control" style="border-radius: 3px 0 0 3px">
		<option disabled selected value>Elige una opción</option>
		
		@foreach($company->all_childs() as $child)
		

		<option value="{{ $child->id }}" {{ $g->company->id == $child->id ? 'selected' : '' }} >
			

			{{ $child->name }}


		</option>


		@endforeach

	</select>


	<button class="btn btn-success" style="border-radius: 0 3px 3px 0">
		<i class="fa fa-check" style="margin: 0 !important"></i>
	</button>

	
</form>