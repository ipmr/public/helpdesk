<html>
    <table width="100%" cellpadding="5" cellspacing="0" border="1">
        <tbody>
            <tr>
                <td colspan="3" align="center">
                    <b>Tiempos de atención al cliente</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;font-weight: bold;color: #007bff;">
                    <b>Atención Remota</b>  
                </td>
                <td style="text-align: center;font-weight: bold; color: #e83e8c;">
                    <b>Atención En Sitio</b>
                </td>
                <td style="text-align: center;font-weight: bold; color: #28a745;">
                    <b>Total de tiempo</b>
                </td>
            </tr>
            <tr>
                <td align="center" class="titles">
                    {{ $complements['total_remote_time'] }}
                </td>
                <td align="center" class="titles">
                    {{ $complements['total_site_time'] }}
                </td>
                <td align="center" class="titles">
                    {{ $complements['total_service_time'] }}
                </td>
            </tr>
        </tbody>
    </table>

    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="padding-top: 30px;">
        <tbody>
            <tr>
                <td colspan="{{ count($complements['status']) }}" align="center">
                    <b>Tickets por status</b>
                </td>
            </tr>
            <tr>
                @foreach ($complements['status'] as $status => $status_data)
                    <td style="text-align: center;font-weight: bold;color: {{ $status_data['color'] }};">
                        ({{ $status_data['total_tickets_status'] }}) Tickets
                    </td>
                @endforeach
            </tr>
            <tr>
                @foreach ($complements['status'] as $status => $status_data)
                    <td>
                        {{ $status }}
                    </td>
                @endforeach
            </tr>
        </tbody>
    </table>

    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="padding-top: 30px;">
        <tbody>
            <tr>
                <td colspan="{{ count($complements['severity']) }}" align="center">
                    <b>Tickets por severidad</b>
                </td>
            </tr>
            <tr>
                @foreach ($complements['severity'] as $severity => $severity_data)
                    <td style="text-align: center;font-weight: bold;color: {{ $severity_data['color'] }};">
                        ({{ $severity_data['total_tickets_severity'] }}) Tickets
                    </td>
                @endforeach
            </tr>
            <tr>
                @foreach ($complements['severity'] as $severity => $severity_data)
                    <td>
                        {{ $severity }}
                    </td>
                @endforeach
            </tr>
        </tbody>
    </table>

    <table width="100%" cellpadding="5" cellspacing="0" border="1">
        <thead>
            <tr>
                <th>#</th>
                <th># Ticket</th>
                <th>Asunto</th>
                <th>Compañia</th>
                <th>Severidad</th>
                <th>Estado</th>
                <th>Categoría</th>
                <th>Agente</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        <a href="{{ route('tickets.show', $ticket->ticket_id) }}" title="">
                            #{{ $ticket->ticket_id }}
                        </a>
                    </td>
                    <td>{{ $ticket->subject }}</td>
                    <td>{{ $ticket->company }}</td>
                    <td style="color: {{ $ticket->severity_color }};">{{ $ticket->severity }}</td>
                    <td style="color: {{ $ticket->status_color }};">{{ $ticket->status }}</td>
                    <td>{{ $ticket->category }}</td>
                    <td>{{ $ticket->first_name.' '.$ticket->last_name }}</td>
                    <td>{{ $ticket->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</html>