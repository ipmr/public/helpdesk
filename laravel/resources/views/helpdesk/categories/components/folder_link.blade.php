@if($category->childs->count() > 0)
    <a href="{{ route('category.show', ['category' => $category->id]) }}" data-target="#category_{{ $category->id }}" data-toggle="collapse" class="no_decoration folder_link">
        <i class="fa fa-chevron-circle-right fa-md"></i>
    </a>
@endif