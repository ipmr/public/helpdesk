<?php

namespace App\Http\Controllers;

use App\DepartmentGroupAgent;
use Illuminate\Http\Request;

class DepartmentGroupAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentGroupAgent  $departmentGroupAgent
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentGroupAgent $departmentGroupAgent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentGroupAgent  $departmentGroupAgent
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentGroupAgent $departmentGroupAgent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentGroupAgent  $departmentGroupAgent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentGroupAgent $departmentGroupAgent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentGroupAgent  $departmentGroupAgent
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentGroupAgent $departmentGroupAgent)
    {
        //
    }
}
