<?php

namespace App\Http\Controllers;

use App\QuotationResource;
use Illuminate\Http\Request;

class QuotationResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuotationResource  $quotationResource
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationResource $quotationResource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuotationResource  $quotationResource
     * @return \Illuminate\Http\Response
     */
    public function edit(QuotationResource $quotationResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuotationResource  $quotationResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationResource $quotationResource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuotationResource  $quotationResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuotationResource $quotationResource)
    {
        //
    }
}
