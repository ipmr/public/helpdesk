<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coverage extends Model
{

    protected $table   = 'coverages';
    public $timestamps = true;
    protected $guarded = [];

    public function from_day(){

    	$from_day = $this->from_day;
    	$day = null;
    	switch ($from_day) {
    		case '1':
    			$day = 'Lunes';
    			break;
    		case '2':
    			$day = 'Martes';
    			break;
    		case '3':
    			$day = 'Miercoles';
    			break;
    		case '4':
    			$day = 'Jueves';
    			break;
    		case '5':
    			$day = 'Viernes';
    			break;
    		case '6':
    			$day = 'Sábado';
    			break;
    		case '7':
    			$day = 'Domingo';
    			break;
    	}
    	return $day;

    }

    public function to_day(){

    	$to_day = $this->to_day;
    	$day = null;
    	switch ($to_day) {
    		case '1':
    			$day = 'Lunes';
    			break;
    		case '2':
    			$day = 'Martes';
    			break;
    		case '3':
    			$day = 'Miercoles';
    			break;
    		case '4':
    			$day = 'Jueves';
    			break;
    		case '5':
    			$day = 'Viernes';
    			break;
    		case '6':
    			$day = 'Sábado';
    			break;
    		case '7':
    			$day = 'Domingo';
    			break;
    	}
    	return $day;

    }



    public function getRangeAttribute(){

        $start_time = str_pad($this->from_hour, 2, '0', STR_PAD_LEFT);
        $end_time = str_pad($this->to_hour, 2, '0', STR_PAD_LEFT);
        $hours_range = null;

        if($this->all_week){
            $days_range = "Toda la semana";
        }else{
            $days_range = "{$this->from_day()} a {$this->to_day()}";
        }

        if($this->all_day){
            $hours_range = "Todo el día";
        }else{
            $hours_range = "{$start_time}:00 a {$end_time}:00";
        }

        return "{$days_range} | {$hours_range}";

    }

}
