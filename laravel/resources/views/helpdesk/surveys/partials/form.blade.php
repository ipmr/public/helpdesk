<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">Nombre de la encuesta:</label>
    <div class="col-sm-6">
        <input type="text" name="name" value="{{ $survey->name or old('name') }}" class="form-control" autofocus required>
    </div>
</div>
<div class="form-group row form-row">
    <label class="col-sm-4 col-form-label text-right">Descripción:</label>
    <div class="col-sm-6">
        <textarea name="description" rows="2" class="form-control">{{ $survey->description or old('description') }}</textarea>
    </div>
</div>


<div class="form-group row form-row mb-0">
    <label class="col-sm-4 col-form-label text-right">
        Departamento:
    </label>
    <div class="col-sm-6">
        <div class="d-flex align-items-center">
            <select name="department_id" class="form-control" @isset($survey) required @endisset>
                <option disabled selected value>Elige un departamento</option>
                @foreach( $departments as $department )
                <option value="{{ $department->id }}" @isset ($survey)
                    {{ ($survey->department_id == $department->id)?'selected':'' }}
                @endisset>{{ $department->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>