<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AgentAssignedCompany extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->ticket = $request['ticket'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Ticket asignado')
                    ->greeting('Ticket asignado')
                    ->line($this->ticket->agent->user->FullName.'. Fue asignado(a) para atender el ticket #' . $this->ticket->ticket_id . ' de la compañia ' . $this->ticket->company->name )
                    ->action('Ver Ticket', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg' => $this->ticket->agent->user->FullName.'. Fue asignado(a) para atender el ticket #' . $this->ticket->ticket_id . ' de la compañia ' . $this->ticket->company->name,
            'ticket' => $this->ticket
        ];
    }
}
