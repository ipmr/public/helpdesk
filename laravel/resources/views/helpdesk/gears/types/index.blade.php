@extends('layouts.master')
@section('content')
<div class="container" id="types_app">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Tipos de equipos</p>
			<a href="#new_gear_type_collapse" data-toggle="collapse" class="btn btn-primary ml-auto" @if(request('new')) aria-expanded="true" @endif>
				<i class="fa fa-plus"></i>
				Nuevo Tipo de Equipo
			</a>
		</div>
		<div class="card-body py-0">
			<div class="collapse @if($errors->any()) show @endif" id="new_gear_type_collapse">
				<form action="{{ route('gear-types.store') }}" class="py-3" method="post">

					<div class="form-row">
						<div class="col-sm-9 ml-auto">
							<label><b>Tipo de equipo</b></label>
							<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
						</div>
					</div>

					{{ csrf_field() }}
					
					<div class="form-group row d-flex form-row">
						<label class="col-sm-3 col-form-label text-right">
							<span class="text-danger">*</span>
							Tipo:
						</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-uppercase" value="{{ old('name') }}" name="name" autocomplete="off" autofocus required>
						</div>
					</div>

					<div class="row form-row">
						<div class="col-sm-9 ml-auto">
							<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Guardar</button>
						</div>
					</div>

				</form>
			</div>
		</div>
		@if(count($types) > 0)
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Tipo</th>
						<th width="20%" class="text-center">Marcas</th>
					</tr>
				</thead>
				<tbody>
					@foreach($types as $type)
					<tr>
						<td><a href="{{ route('gear-types.show', $type) }}/">{{ $type->name }}</a></td>
						<td class="text-center">{{ count($type->brands) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<div class="card-body">
				No se han agregado tipos de equipos
			</div>
		@endif
	</div>
</div>
@stop
