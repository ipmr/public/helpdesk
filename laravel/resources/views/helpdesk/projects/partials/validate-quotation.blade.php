@component('helpdesk.components.modal', [
	'id' 	=> 'validateQuotationModal',
	'title' => 'Validar Cotización',
	'size' 	=> 'modal-lg'
])

	@slot('body')
	
		<table v-if="quotation_resources.length > 0" class="table table-bordered table-striped mb-0">
			
			<thead>
				<tr>
					<th style="width: 15%">Cantidad</th>
					<th>Tipo de recurso</th>
					<th style="width: 20%" class="text-right">Sub-total</th>
				</tr>
			</thead>
			
			<tbody>
				
				<tr v-for="(resource, index) in quotation_resources">
					<td>@{{ resource.qty }}</td>
					<td class="d-flex">
						
						@{{ resource.name }}

					</td>
					<td class="text-right">@{{ resource.subtotal | currency }}</td>
				</tr>
				
				<tr class="bg-secondary">
					<th colspan="2" class="text-right">TOTAL:</th>
					<th class="text-right">@{{ quotation_total() | currency }}</th>
				</tr>

			</tbody>
			
		</table>

	@endslot
	
	@slot('footer')
		
	
		<a href="#" class="btn btn-link"><i class="fa fa-pencil-alt"></i> Editar Información</a>


		<form action="{{ route('validate_quotation', [$company, $project]) }}" method="POST" @submit.prevent="validate_quotation">
				

			{{ csrf_field() }}

		
			<input type="hidden" name="quotation" :value="quotation.id">


			<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Validar Cotización</button>


		</form>
		

	@endslot

@endcomponent