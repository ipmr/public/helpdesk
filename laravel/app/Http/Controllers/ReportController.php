<?php

namespace App\Http\Controllers;

use App\Company;
use App\Contract;
use App\Gear;
use App\Ticket;
use App\User;
use App\ZoneUser;
use Carbon\Carbon;
use App\Exports\TicketsExport;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        return view('helpdesk.reports.index');
    }

    public function get_company_contacts($company)
    {
        $company_users = ZoneUser::select('user_id')->where('zone_id', $company)->get();

        if ($company_users->count() > 0) {

            $contacts = User::whereIn('id', $company_users)->get();
        } else {

            $contacts = User::where('role', 'company')->get();
        }

        return response()->json($contacts);
    }

    public function get_company_contracts($company)
    {
        $company = Company::where('id', $company)->first();
        if (isset($company->id)) {
            $contracts = [];

            if ($company->contract_id) {

                $contracts[] = $company->Contract;
            }

            $gears = $company->gears;
            if ($gears->count() > 0) {

                foreach ($gears as $gear) {

                    if ($gear->contract_id) {

                        $contracts[] = $gear->Contract;

                    }
                }

            }
        } else {
            $contracts = Contract::select('id', 'name')->get();
        }

        return response()->json($contracts);
    }

    public function get_company_coverages($company)
    {
        $company = Company::where('id', $company)->first();
        if (isset($company->id)) {
            $coverages = [];

            if ($company->coverage_id) {

                $coverages[] = $company->Coverage;
            }

            $gears = $company->gears;
            if ($gears->count() > 0) {

                foreach ($gears as $gear) {

                    if ($gear->coverage_id) {

                        $coverages[] = $gear->Coverage;

                    }
                }

            }
        } else {
            $coverages = Coverage::select('id', 'name')->get();
        }

        return response()->json($coverages);
    }

    public function get_company_gears($company)
    {
        $company = Company::where('id', $company)->first();
        if (isset($company->id)) {
            $gears = Gear::select('id', 'serial_number')->where('company_id', $company->id)->get();
        } else {
            $gears = Gear::select('id', 'serial_number')->get();
        }

        return response()->json($gears);
    }

    public function get_report(Request $request)
    {
        if ($request->report_type == 'Tickets') {

            return $this->get_tickets_filtered($request);

        } else if ($request->report_type == 'Equipos') {

            return $this->get_gears_filtered($request);

        }
    }

    public function get_tickets_filtered(Request $request)
    {
        $columns = [
            'tickets.ticket_id',
            'companies.name as company',
            'tickets.company_id',
            'tickets.subject',
            'severities.name as severity',
            'severities.color as severity_color',
            'tickets.status_id',
            'statuses.name as status',
            'statuses.color as status_color',
            'categories.name as category',
            'departments.name as department',
            'tickets.agent_id',
            'gears.serial_number as gear',
            'gear_brands.name as brand',
            'gear_types.name as type',
            'gear_models.name as model',
            'users.first_name',
            'users.last_name',
            'tickets.created_at',
        ];

        $start_date = '';
        $end_date   = '';

        if ($request->has('report_range')) {

            $end_date = Carbon::now()->format('Y-m-d');

            if ($request->report_range == 'weekly') {

                $start_date = Carbon::parse($end_date)->subWeek();

            } elseif ($request->report_range == "monthly") {

                $start_date = Carbon::parse($end_date)->subMonth();

            } elseif ($request->report_range == "quarterly") {

                $start_date = Carbon::parse($end_date)->subMonths(3);

            }

        } else {

            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date   = date('Y-m-d', strtotime($request->end_date));

        }

        $tickets = Ticket::select($columns)
            ->leftJoin('companies', 'tickets.company_id', '=', 'companies.id')
            ->leftJoin('severities', 'tickets.severity_id', '=', 'severities.id')
            ->leftJoin('statuses', 'tickets.status_id', '=', 'statuses.id')
            ->leftJoin('categories', 'tickets.category_id', '=', 'categories.id')
            ->leftJoin('departments', 'tickets.department_id', '=', 'departments.id')
            ->leftJoin('gears', 'tickets.gear_id', '=', 'gears.id')
            ->leftJoin('gear_brands', 'gears.brand_id', '=', 'gear_brands.id')
            ->leftJoin('gear_models', 'gears.model_id', '=', 'gear_models.id')
            ->leftJoin('gear_types', 'gears.type_id', '=', 'gear_types.id')
            ->leftJoin('agents', 'tickets.agent_id', '=', 'agents.id')
            ->leftJoin('users', 'agents.user_id', '=', 'users.id')
            ->whereDate('tickets.created_at', '>=', $start_date)
            ->whereDate('tickets.created_at', '<=', $end_date);

        if (isset($request->status)) {
            $tickets = $tickets->whereIn('tickets.status_id', $request->status);
        }

        if (isset($request->severity)) {
            $tickets = $tickets->whereIn('tickets.severity_id', $request->severity);
        }

        if (isset($request->category)) {
            $tickets = $tickets->whereIn('tickets.category_id', $request->category);
        }

        if (isset($request->department)) {
            $tickets = $tickets->whereIn('tickets.department_id', $request->department);
        }

        if (isset($request->agent)) {
            $tickets = $tickets->whereIn('tickets.agent_id', $request->agent);
        }

        if (isset($request->company)) {
            $tickets = $tickets->whereIn('tickets.company_id', $request->company);
        }

        if (isset($request->contact)) {
            $tickets = $tickets->whereIn('tickets.contact_id', $request->contact);
        }

        if (isset($request->brand) || isset($request->model) || isset($request->type) || isset($request->gear_status) || isset($request->gear) || isset($request->coverage) || isset($request->contract)) {

            if (isset($request->gear)) {
                $gears = Gear::whereIn('id', $request->gear);
            } else {
                $gears = Gear::where('id', '>', 0);

                if (isset($request->company)) {
                    $gears = $gears->whereIn('company_id', $request->company);
                }

                if (isset($request->brand)) {
                    $gears = $gears->whereIn('brand_id', $request->brand);
                }

                if (isset($request->type)) {
                    $gears = $gears->whereIn('type_id', $request->type);
                }

                if (isset($request->model)) {
                    $gears = $gears->whereIn('model_id', $request->model);
                }

                if (isset($request->gear_status)) {
                    $gears = $gears->whereIn('status', $request->gear_status);
                }

                if (isset($request->coverage)) {
                    $gears = $gears->whereIn('coverage_id', $request->coverage);
                }

                if (isset($request->contract)) {
                    $gears = $gears->whereIn('contract_id', $request->contract);
                }
            }

            $tickets = $tickets->whereIn('tickets.gear_id', $gears->get());
        }

        $tickets = $tickets->orderBy('tickets.created_at', 'DESC');

        $tickets = $tickets->get();

        $data = [
            'tickets'     => $tickets,
            'complements' => $this->get_complement_data($request, $tickets),
            'params'      => $request->all(),
        ];

        if (\Request::ajax()) {
            return response()->json($data);
        }

        if ($request->format_file == 'pdf') {
            $pdf = \PDF::loadView('helpdesk.reports.tickets_report', $data)->setPaper('a4', 'landscape');

            return $pdf->stream();
        }

        if ($request->format_file == 'excel') {

            return \Excel::download(new TicketsExport( $data ), 'tickets_report.xls' );
        }
    }

    public function get_gears_filtered(Request $request)
    {
        $columns = [
            'gears.id',
            'gears.serial_number',
            'gears.description',
            'gears.comments',
            'gears.status',
            'gears.created_at as created',
            'companies.name as company',
            'gear_brands.name as brand',
            'gear_models.name as model',
            'gear_types.name as type',
            'contracts.name as contract',
            'coverages.name as coverage',
        ];

        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date   = date('Y-m-d', strtotime($request->end_date));

        $gears = Gear::select($columns)
            ->leftJoin('companies', 'gears.company_id', '=', 'companies.id')
            ->leftJoin('gear_brands', 'gears.brand_id', '=', 'gear_brands.id')
            ->leftJoin('gear_models', 'gears.model_id', '=', 'gear_models.id')
            ->leftJoin('gear_types', 'gears.type_id', '=', 'gear_types.id')
            ->leftJoin('contracts', 'gears.contract_id', 'contracts.id')
            ->leftJoin('coverages', 'gears.coverage_id', 'coverages.id');

        if (isset($request->gear)) {
            $gears = $gears->whereIn('gears.id', $request->gear);
        } else {
            if (isset($request->company)) {
                $gears = $gears->whereIn('gears.company_id', $request->company);
            }

            if (isset($request->brand)) {
                $gears = $gears->whereIn('gears.brand_id', $request->brand);
            }

            if (isset($request->type)) {
                $gears = $gears->whereIn('gears.type_id', $request->type);
            }

            if (isset($request->model)) {
                $gears = $gears->whereIn('gears.model_id', $request->model);
            }

            if (isset($request->gear_status)) {
                $gears = $gears->whereIn('gears.status', $request->gear_status);
            }

            if (isset($request->coverage)) {
                $gears = $gears->whereIn('gears.coverage_id', $request->coverage);
            }

            if (isset($request->contract)) {
                $gears = $gears->whereIn('gears.contract_id', $request->contract);
            }
        }

        $gears = $gears->orderBy('gears.created_at', 'DESC');

        if (\Request::ajax()) {
            $gears_data = [];
            foreach ($gears->get() as $gear) {
                $gear->modules = $gear->modules;
                $gears_data[]  = $gear;
            }
            return response()->json($gears_data);
        }

        $gears = $gears->get();
        $data  = [
            'gears' => $gears,
        ];
        $pdf = \PDF::loadView('helpdesk.reports.gears_report', $data)->setPaper('a4', 'landscape');

        return $pdf->stream();
    }

    public function get_complement_data(Request $request, $tickets_report)
    {
        $complement = [];

        $companies   = \App\Company::select(['id', 'name']);
        $severities  = \App\Severity::select(['id', 'name','color']);
        $statuses    = \App\Status::select(['id', 'name', 'color']);
        $departments = \App\Department::select(['id', 'name']);
        $gear_brands = \App\GearBrand::select(['id', 'name'])->get();
        $gear_types  = \App\GearType::select(['id', 'name'])->get();

        if (isset($request->status)) {
            #search status
            $statuses = $statuses->whereIn('id',$request->status);
        }

        $statuses = $statuses->get();

        if (isset($request->severity)) {
            #search severity
            $severities = $severities->whereIn('id',$request->severity);
        }

        $severities = $severities->get();

        if (isset($request->company)) {
            #search company
            $companies = $companies->whereIn('id',$request->company);
        }

        $companies = $companies->get();

        if (isset($request->department)) {
            #search department
            $departments = $departments->whereIn('id',$request->department);
        }

        $departments = $departments->get();

        $complement['total_tickets']      = 0;
        $complement['total_remote_time']  = 0;
        $complement['total_site_time']    = 0;
        $complement['total_service_time'] = 0;

        foreach ($companies as $company) {
            #tickets por compania
            $complement['companies'][$company->name]['total_tickets_company'] = 0;
            $complement['companies'][$company->name]['total_remote_time']     = 0;
            $complement['companies'][$company->name]['total_site_time']       = 0;
            $complement['companies'][$company->name]['total_service_time']    = 0;

            foreach ($statuses as $status) {
                #tickets por estatus por compania
                $complement['companies'][$company->name]['status'][$status->name]['total_tickets_status'] = 0;
                $complement['companies'][$company->name]['status'][$status->name]['color'] = $status->color;
            }

            foreach ($severities as $severity) {
                #tickets por eseverity por compania
                $complement['companies'][$company->name]['severity'][$severity->name]['total_tickets_severity'] = 0;
                $complement['companies'][$company->name]['severity'][$severity->name]['color'] = $severity->color;
            }

            foreach ($departments as $department) {
                #tickets por estatus por compania
                $complement['companies'][$company->name]['department'][$department->name]['total_tickets_department'] = 0;

                $complement['companies'][$company->name]['department'][$department->name]['total_remote_time']   = 0;
                $complement['companies'][$company->name]['department'][$department->name]['total_site_time']     = 0;
                $complement['companies'][$company->name]['department'][$department->name]['total_service_time']  = 0;
            }

            foreach ($gear_brands as $gear_brand) {
                #tickets por marcas por marca de equipo
                $complement['companies'][$company->name]['brands'][$gear_brand->name]['total_tickets_brand'] = 0;
                $complement['companies'][$company->name]['brands'][$gear_brand->name]['total_remote_time']   = 0;
                $complement['companies'][$company->name]['brands'][$gear_brand->name]['total_site_time']     = 0;
                $complement['companies'][$company->name]['brands'][$gear_brand->name]['total_service_time']  = 0;

                foreach ($gear_types as $gear_type) {
                    #tickets por marcas por tipos de companias
                    $complement['companies'][$company->name]['brands'][$gear_brand->name]['gear_types'][$gear_type->name]['total_tickets_gear_type'] = 0;
                    $complement['companies'][$company->name]['brands'][$gear_brand->name]['gear_types'][$gear_type->name]['total_remote_time']       = 0;
                    $complement['companies'][$company->name]['brands'][$gear_brand->name]['gear_types'][$gear_type->name]['total_site_time']         = 0;
                    $complement['companies'][$company->name]['brands'][$gear_brand->name]['gear_types'][$gear_type->name]['total_service_time']      = 0;
                }
            }

            foreach ($gear_types as $gear_type) {
                #tickets por tipos por compania
                $complement['companies'][$company->name]['gear_types'][$gear_type->name]['total_tickets_gear_type'] = 0;

                $complement['companies'][$company->name]['gear_types'][$gear_type->name]['total_remote_time']  = 0;
                $complement['companies'][$company->name]['gear_types'][$gear_type->name]['total_site_time']    = 0;
                $complement['companies'][$company->name]['gear_types'][$gear_type->name]['total_service_time'] = 0;
            }
        }

        foreach ($statuses as $status) {
            #tickets por status
            $complement['status'][$status->name]['total_tickets_status'] = 0;
            $complement['status'][$status->name]['color'] = $status->color;
        }

        foreach ($severities as $severity) {
            #tickets por severity
            $complement['severity'][$severity->name]['total_tickets_severity'] = 0;
            $complement['severity'][$severity->name]['color'] = $severity->color;
        }

        foreach ($departments as $department) {
            $complement['department'][$department->name]['total_tickets_department'] = 0;

            $complement['department'][$department->name]['total_remote_time']   = 0;
            $complement['department'][$department->name]['total_site_time']     = 0;
            $complement['department'][$department->name]['total_service_time']  = 0;
        }

        foreach ($gear_brands as $gear_brand) {
            #tickets por marca
            $complement['brands'][$gear_brand->name]['total_tickets_brand'] = 0;
            $complement['brands'][$gear_brand->name]['total_remote_time']   = 0;
            $complement['brands'][$gear_brand->name]['total_site_time']     = 0;
            $complement['brands'][$gear_brand->name]['total_service_time']  = 0;
        }

        foreach ($gear_types as $gear_type) {
            #tickets por tipo
            $complement['gear_types'][$gear_type->name]['total_tickets_gear_type'] = 0;
            $complement['gear_types'][$gear_type->name]['total_remote_time']       = 0;
            $complement['gear_types'][$gear_type->name]['total_site_time']         = 0;
            $complement['gear_types'][$gear_type->name]['total_service_time']      = 0;
        }

        if (count($tickets_report) > 0) {

            foreach ($tickets_report as $ticket_report) {

                $times = $ticket_report->ticket_time;
                $complement['total_tickets'] += 1;
                $complement['companies'][$ticket_report->company]['total_tickets_company'] += 1;
                $complement['companies'][$ticket_report->company]['status'][$ticket_report->status]['total_tickets_status'] += 1;
                $complement['companies'][$ticket_report->company]['severity'][$ticket_report->severity]['total_tickets_severity'] += 1;
                $complement['status'][$ticket_report->status]['total_tickets_status'] += 1;
                $complement['severity'][$ticket_report->severity]['total_tickets_severity'] += 1;

                if (!empty($ticket_report->brand)) {
                    $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['total_tickets_brand'] += 1;
                    $complement['brands'][$ticket_report->brand]['total_tickets_brand'] += 1;

                    if (isset($times->id)) {
                        $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['total_remote_time'] += $times->remote_time;
                        $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['total_site_time'] += $times->site_time;
                        $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['total_service_time'] += $times->total_time;

                        $complement['brands'][$ticket_report->brand]['total_remote_time'] += $times->remote_time;
                        $complement['brands'][$ticket_report->brand]['total_site_time'] += $times->site_time;
                        $complement['brands'][$ticket_report->brand]['total_service_time'] += $times->total_time;
                    }

                    if (!empty($ticket_report->type)) {
                        $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['gear_types'][$ticket_report->type]['total_tickets_gear_type'] += 1;

                        if (isset($times->id)) {
                            $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['gear_types'][$ticket_report->type]['total_remote_time'] += $times->remote_time;
                            $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['gear_types'][$ticket_report->type]['total_site_time'] += $times->site_time;
                            $complement['companies'][$ticket_report->company]['brands'][$ticket_report->brand]['gear_types'][$ticket_report->type]['total_service_time'] += $times->total_time;
                        }
                    }
                }

                if (!empty($ticket_report->type)) {
                    $complement['companies'][$ticket_report->company]['gear_types'][$ticket_report->type]['total_tickets_gear_type'] += 1;
                    $complement['gear_types'][$ticket_report->type]['total_tickets_gear_type'] += 1;

                    if (isset($times->id)) {
                        $complement['companies'][$ticket_report->company]['gear_types'][$ticket_report->type]['total_remote_time'] += $times->remote_time;
                        $complement['companies'][$ticket_report->company]['gear_types'][$ticket_report->type]['total_site_time']         += $times->site_time;
                        $complement['companies'][$ticket_report->company]['gear_types'][$ticket_report->type]['total_service_time']        += $times->total_time;

                        $complement['gear_types'][$ticket_report->type]['total_remote_time'] += $times->remote_time;
                        $complement['gear_types'][$ticket_report->type]['total_site_time'] += $times->site_time;
                        $complement['gear_types'][$ticket_report->type]['total_service_time'] += $times->total_time;
                    }
                }

                if (!empty($ticket_report->department)) {
                    $complement['companies'][$ticket_report->company]['department'][$ticket_report->department]['total_tickets_department'] += 1;
                    $complement['department'][$ticket_report->department]['total_tickets_department'] += 1;

                    if (isset($times->id)) {

                        $complement['companies'][$ticket_report->company]['department'][$ticket_report->department]['total_remote_time']  += $times->remote_time;
                        $complement['companies'][$ticket_report->company]['department'][$ticket_report->department]['total_site_time']    += $times->site_time;
                        $complement['companies'][$ticket_report->company]['department'][$ticket_report->department]['total_service_time'] += $times->total_time;

                        $complement['department'][$ticket_report->department]['total_remote_time']  += $times->remote_time;
                        $complement['department'][$ticket_report->department]['total_site_time']    += $times->site_time;
                        $complement['department'][$ticket_report->department]['total_service_time'] += $times->total_time;
                    }
                }

                if (isset($times->id)) {
                    $complement['total_remote_time'] += $times->remote_time;
                    $complement['total_site_time'] += $times->site_time;
                    $complement['total_service_time'] += $times->total_time;

                    $complement['companies'][$ticket_report->company]['total_remote_time'] += $times->remote_time;
                    $complement['companies'][$ticket_report->company]['total_site_time'] += $times->site_time;
                    $complement['companies'][$ticket_report->company]['total_service_time'] += $times->total_time;
                }
            }

        }

        #parser format minutes
        // Ticket::format_minutes(  )
        $complement['total_remote_time']  = Ticket::format_minutes( $complement['total_remote_time'] );
        $complement['total_site_time']    = Ticket::format_minutes( $complement['total_site_time'] );
        $complement['total_service_time'] = Ticket::format_minutes( $complement['total_service_time'] );

        foreach ($departments as $department) {
            $complement['department'][$department->name]['total_remote_time']  = Ticket::format_minutes( $complement['department'][$department->name]['total_remote_time'] );
            $complement['department'][$department->name]['total_site_time']    = Ticket::format_minutes( $complement['department'][$department->name]['total_site_time'] );
            $complement['department'][$department->name]['total_service_time'] = Ticket::format_minutes( $complement['department'][$department->name]['total_service_time'] );
        }

        foreach ($gear_brands as $gear_brand) {
            #tickets por marca
            $complement['brands'][$gear_brand->name]['total_remote_time']  = Ticket::format_minutes( $complement['brands'][$gear_brand->name]['total_remote_time'] );
            $complement['brands'][$gear_brand->name]['total_site_time']    = Ticket::format_minutes( $complement['brands'][$gear_brand->name]['total_site_time'] );
            $complement['brands'][$gear_brand->name]['total_service_time'] = Ticket::format_minutes( $complement['brands'][$gear_brand->name]['total_service_time'] );
        }

        foreach ($gear_types as $gear_type) {
            #tickets por tipo
            $complement['gear_types'][$gear_type->name]['total_remote_time']  = Ticket::format_minutes( $complement['gear_types'][$gear_type->name]['total_remote_time'] );
            $complement['gear_types'][$gear_type->name]['total_site_time']    = Ticket::format_minutes( $complement['gear_types'][$gear_type->name]['total_site_time'] );
            $complement['gear_types'][$gear_type->name]['total_service_time'] = Ticket::format_minutes( $complement['gear_types'][$gear_type->name]['total_service_time'] );
        }

        foreach ($complement['companies'] as $company => $company_datas) {
            $complement['companies'][$company]['total_remote_time']  = Ticket::format_minutes( $company_datas['total_remote_time'] );
            $complement['companies'][$company]['total_site_time']    = Ticket::format_minutes( $company_datas['total_site_time'] );
            $complement['companies'][$company]['total_service_time'] = Ticket::format_minutes( $company_datas['total_service_time'] );

            foreach ($company_datas['department'] as $department => $department_datas) {
                $complement['companies'][$company]['department'][$department]['total_remote_time']  = Ticket::format_minutes( $department_datas['total_remote_time'] );
                $complement['companies'][$company]['department'][$department]['total_site_time']    = Ticket::format_minutes( $department_datas['total_site_time'] );
                $complement['companies'][$company]['department'][$department]['total_service_time'] = Ticket::format_minutes( $department_datas['total_service_time'] );
            }

            foreach ($company_datas['brands'] as $brand => $brand_datas) {
                $complement['companies'][$company]['brands'][$brand]['total_remote_time']  = Ticket::format_minutes( $brand_datas['total_remote_time'] );
                $complement['companies'][$company]['brands'][$brand]['total_site_time']    = Ticket::format_minutes( $brand_datas['total_site_time'] );
                $complement['companies'][$company]['brands'][$brand]['total_service_time'] = Ticket::format_minutes( $brand_datas['total_service_time'] );

                foreach ($brand_datas['gear_types'] as $type => $type_datas) {
                    $complement['companies'][$company]['brands'][$brand]['gear_types'][$type]['total_remote_time']  = Ticket::format_minutes( $type_datas['total_remote_time'] );
                    $complement['companies'][$company]['brands'][$brand]['gear_types'][$type]['total_site_time']    = Ticket::format_minutes( $type_datas['total_site_time'] );
                    $complement['companies'][$company]['brands'][$brand]['gear_types'][$type]['total_service_time'] = Ticket::format_minutes( $type_datas['total_service_time'] );
                }
            }

            foreach ($company_datas['gear_types'] as $type => $gear_types) {
                $complement['companies'][$company]['gear_types'][$type]['total_remote_time'] = Ticket::format_minutes( $gear_types['total_remote_time'] );
                $complement['companies'][$company]['gear_types'][$type]['total_site_time']   = Ticket::format_minutes( $gear_types['total_site_time'] );
                $complement['companies'][$company]['gear_types'][$type]['total_remote_time'] = Ticket::format_minutes( $gear_types['total_remote_time'] );
            }
        }

        return $complement;
    }
}