<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gears', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('type_id');
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->string('serial_number')->nullable();
            $table->text('description')->nullable();
            $table->text('comments')->nullable();
            $table->integer('coverage_id')->nullable();
            $table->boolean('master_coverage')->default(0);
            $table->integer('contract_id')->nullable();
            $table->boolean('master_contract')->default(0);
            $table->date('contract_start_at')->nullable();
            $table->string('status')->default('Funciona');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gears');
    }
}
