<style>
	body{
		font-family: 'Helvetica', sans-serif;
		font-size: 14px;
	}
	.text-danger {
	    color: #ed1c24!important;
	}
	h1, h2, h3, h4{
		font-weight: lighter;
	}
	.m-0{
		margin: 0 !important;
	}
	table{
		width: 100%;
		font-size: 12px;
	}
	table tr, table th, table td{
		vertical-align: top;
	}
	.bg-warning{
	background: #eef3f9 !important;
	}
</style>

<table cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
	<tr>
		<td style="vertical-align: middle;">
			<h2 style="margin: 0">
				{{$company->name}}
			</h2>
		</td>
		<td style="vertical-align: middle;" align="right">
			Fecha de exportación: {{ \Carbon\Carbon::now()->format('d M, Y - h:i a') }}
		</td>
	</tr>
</table>

@if(count($gears) > 0)
<table cellpadding="5" cellspacing="0" border="1">
	<thead>
		<tr>
			<th>Marca</th>
			<th>Modelo</th>
			<th>#Serie</th>
			<th>Contrato</th>
			<th>Estado</th>
			<th>Sitio</th>
		</tr>
	</thead>
	<tbody>
		@if(count($gears) > 0)
			@foreach($gears as $g)
				<tr class="@if(!$g->active) bg-danger-light @endif">
					<td style="vertical-align: middle;">
						{{ $g->brand->name }}
					</td>
					<td style="vertical-align: middle;">
						{{ $g->model->name }}
					</td>
					<td style="vertical-align: middle;" class="no-wrap" data-toggle="tooltip" data-placement="bottom" title="{{ str_limit($g->comments, 70) }}">
						<a href="{{ route('companies.gears.show', [$company, $g]) }}/">
							{{ $g->serial_number }}
						</a>
					</td>
					<td style="vertical-align: middle;">
						@if($g->contract)
							<a href="{{ route('contracts.show', $g->contract) }}/">
								{{ $g->contract->name }}
							</a>
						@else
							Sin Contrato
						@endif
					</td>
					<td style="vertical-align: middle;">
						{{ $g->status }}
					</td>
					<td style="vertical-align: middle;">
						{{ $g->company->name }}
					</td>
				</tr>
			@endforeach
		@else
		<tr>
			<td colspan="4">
				<p class="m-0">No se ha agregado equipo.</p>
			</td>
		</tr>
		@endif
	</tbody>
</table>
@endif


<p><small>©{{ \Carbon\Carbon::now()->format('Y') .' '. config('app.name') }}</small></p>