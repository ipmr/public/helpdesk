@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<a class="btn btn-link p-0" href="{{ route('agents.index') }}/">
				<i class="fa fa-angle-left fa-sm mr-1"></i>
				Regresar
			</a>
			<a class="btn btn-primary ml-auto" href="{{ route('agents.edit', $agent) }}/">
				<i class="fa fa-pencil-alt fa-xs"></i>
				Editar
			</a>
		</div>

		<table class="table table-striped m-0">
			<tr>
				<th width="30%">Usuario</th>
				<td>{{ $agent->user->username }}</td>
			</tr>
			<tr>
				<th width="30%">Nombre</th>
				<td>{{ $agent->user->first_name .' '. $agent->user->last_name }}</td>
			</tr>
			<tr>
				<th width="30%">E-mail</th>
				<td>{{ $agent->user->email }}</td>
			</tr>
			<tr>
				<th width="30%">Celular</th>
				<td>{{ ($agent->cellphone)?$agent->cellphone:'Sin Celular' }}</td>
			</tr>
			<tr>
				<th width="30%">Num Corto</th>
				<td>{{ ($agent->short_number)?$agent->short_number:'Sin Número Corto' }}</td>
			</tr>
			<tr>
				<th width="30%">Extensión</th>
				<td>{{ ($agent->extension)?$agent->extension:'Sin Extensión' }}</td>
			</tr>
			<tr>
				<th width="30%">Puesto</th>
				<td>{{ $agent->jobtitle }}</td>
			</tr>
			<tr>
				<th width="30%">Departamento</th>
				<td class="{{ $agent->department ? '' : 'text-danger' }}">{{ $agent->department->name or 'El agente no ha sido asignado a un departamento' }}</td>
			</tr>

			<tr>
				<th width="30%">Generar Tickets</th>
				<td>

					<i class="fa fa-{{ $agent->generate_tickets ? 'check' : 'times' }} text-{{ $agent->generate_tickets ? 'success' : 'danger' }}"></i>

				</td>
			</tr>
			<tr>
				<th width="30%">Permisos de manager</th>
				<td>

					<i class="fa fa-{{ $agent->as_manager ? 'check' : 'times' }} text-{{ $agent->as_manager ? 'success' : 'danger' }}"></i>

				</td>
			</tr>

		</table>
	</div>
</div>
@stop