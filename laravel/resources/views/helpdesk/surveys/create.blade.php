@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-link p-0" href="{{ route('surveys.index') }}/">
                        <i class="fa fa-angle-left"></i>
                        Regresar a lista de encuestas
                    </a>
                </div>
                <div class="card-body">
                    <div class="row form-row mb-3">
                        <div class="col-sm-8 ml-auto">
                            <label><b>Crear nueva encuesta</b></label>
                        </div>
                    </div>
                    <form action="{{ route('surveys.store') }}" method="post" id="newSurvey">
                        
                        {{ csrf_field() }}

                        @include('helpdesk.surveys.partials.form')
                        
                    </form>
                </div>
                <div class="card-footer">
                    <div class="row form-row">
                        <div class="col-sm-8 ml-auto">
                            <button type="submit" form="newSurvey" id="survey_form_btn" class="btn btn-success">
                                <i class="fa fa-check"></i>
                                Guardar Encuesta
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop