@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-link p-0" href="{{ route('surveys.index') }}/">
                        <i class="fa fa-angle-left"></i>
                        Regresar a lista de encuestas
                    </a>
                </div>
                <div class="card-body">
                    <div class="row form-row mb-3">
                        <div class="col-sm-8 ml-auto">
                            <label><b>Editar encuesta</b></label>
                        </div>
                    </div>
                    <form action="{{ route('surveys.update', $survey) }}" method="post" id="updateSurvey">
                        
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        @include('helpdesk.surveys.partials.form')
                        
                    </form>
                </div>
                <div class="card-footer">
                    <div class="row form-row">
                        <div class="col-sm-8 ml-auto">
                            <button type="submit" form="updateSurvey" id="survey_form_btn" class="btn btn-success">
                                <i class="fa fa-check"></i>
                                Actualizar Encuesta
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop