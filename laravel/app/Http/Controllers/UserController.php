<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class UserController extends Controller
{
    public function __construct()
    {

        $this->middleware('role:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::whereIn('role', ['admin', 'manager']);

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $users = $users->where(function ($q) use ($query_string, $request) {
                $q = $q->where('first_name', 'like', '%'.$request->q.'%')
                    ->orWhere('last_name', 'like', '%'.$request->q.'%')
                    ->orWhere('email', 'like', '%'.$request->q.'%')
                    ->orWhere('username', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%')
                    ->orWhere('username', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $users->toSql() );
            }

            if ($request->debug == 2) {
                dd( $users->get() );
            }
        }

        $users = $users->paginate(12);

        return view('helpdesk.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('helpdesk.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->validation($request);

        $user = new User();

        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->username   = $request->username;
        $user->email      = $request->email;
        $user->role       = $request->role;
        $user->password   = \Hash::make($request->password);

        $user->save();

        return redirect()
            ->route('users.index')
            ->with('msg', 'Se ha creado el usuario ' . $user->full_name . ' exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('helpdesk.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('helpdesk.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validation($request, $user);

        $except = ['_token', '_method', 'password'];

        if (!empty($request->password)) {

            $request->password = \Hash::make($request->password);
            $except            = ['_token', '_method'];

        }

        $user->update($request->except($except));

        return redirect()
            ->route('users.show', ['user' => $user->id])
            ->with('msg', 'La información del usuario ha sido actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validation(Request $request, $user = null)
    {
        $rules = [
            'first_name' => 'required|max:190|string',
            'last_name'  => 'required|max:190|string',
            'role'       => 'required|string',
            'email'      => [
                'required',
                'email',
                $user ? Rule::unique('users')->ignore($user->id) : 'unique:users',
            ],
            'username'   => [
                'required',
                $user ? Rule::unique('users')->ignore($user->id) : 'unique:users',
            ],
        ];

        $msgs = [
            'first_name.required' => 'El campo Nombre(s) es requerido',
            'first_name.max'      => 'El campo Nombre(s) es mayor a 190 caracteres',
            'first_name.string'   => 'El campo Nombre(s) debe ser alfanumérico',
            'last_name.required'  => 'El campo Apellido(s) es requerido',
            'last_name.max'       => 'El campo Apellido(s) es mayor a 190 caracteres',
            'last_name.string'    => 'El campo Apellido(s) debe ser alfanumérico',
            'role.required'       => 'El campo Tipo usuario es requerido',
            'role.string'         => 'El campo Tipo usuario debe ser alfanumérico',
            'email.required'      => 'El correo electrónico es requerido',
            'email.email'         => 'El correo electrónico no es válido',
            'email.unique'        => 'El correo electrónico no se encuentra disponible',
            'username.required'   => 'El nombre de usuario es requerido',
            'username.unique'     => 'El nombre de usuario no esta disponible',
        ];

        if (empty($user)) {

            $rules['password'] = 'required|min:6';

        } else if (!empty($request->password)) {

            $rules['password'] = 'required|min:6';

        }

        $validate = Validator::make($request->all(), $rules, $msgs)->validate();
    }
}
