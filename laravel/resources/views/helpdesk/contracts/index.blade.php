@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">Listado de contratos</p>
			<form action="{{ route('contracts.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
			<a href="#new_contract_collapse" data-toggle="collapse" class="btn btn-primary ml-2">
				<i class="fa fa-plus"></i>
				Nuevo Contrato
			</a>
		</div>
		<div class="card-body py-0">
			<div class="collapse" id="new_contract_collapse">
				<form action="{{ route('contracts.store') }}" method="post" class="py-3">
					@include('helpdesk.contracts.partials.form', [
						'btn' => 'Crear Contrato'
					])
					<div class="row form-row d-flex">
						<div class="col-sm-8 ml-auto">
							<button class="btn btn-success"><i class="fa fa-check"></i> Guardar Contrato</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@if(count($contracts) > 0)
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th width="30%">Nombre</th>
						<th>Descripción</th>
						<th>Vigencia</th>
					</tr>
				</thead>
				<tbody>
					@foreach($contracts as $contract)
					<tr>
						<td>
							<a href="{{ route('contracts.show', $contract) }}">
								{{ $contract->name }}
							</a>
						</td>
						<td>{{ $contract->description }}</td>
						<td>{{ $contract->life_time }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<div class="card-body">
				No se han agregado contratos
			</div>
		@endif
	</div>
</div>
@stop
