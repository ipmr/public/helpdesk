<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTicket extends Model
{
    public function project()
    {

        return $this->belongsTo('App\Project', 'project_id', 'id');
    }

    public function ticket()
    {

        return $this->belongsTo('App\Ticket', 'ticket_id', 'ticket_id');
    }
}