@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<p class="lead">
				Acuerdos De Servicio (SLA)
			</p>
			<form action="{{ route('sla.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                    value="{{ $_GET['q'] }}"
                @endisset>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
			<a class="btn btn-primary ml-2" href="{{ route('sla.create') }}/">
				<i class="fa fa-plus"></i>
				Nuevo acuerdo
			</a>
		</div>
		@if($slas->count() > 0)
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre de SLA</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($slas as $sla)
						<tr>
							<td><a href="{{ route('sla.show', $sla) }}/">{{ $sla->name }}</a></td>
							<td>{{ $sla->description }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<div class="card-body">
				No se han agregado acuerdos de servicio
			</div>
		@endif
	</div>
</div>
@stop