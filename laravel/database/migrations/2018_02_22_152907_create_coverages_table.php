<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coverages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('from_day')->nullable();
            $table->string('to_day')->nullable();
            $table->string('from_hour')->nullable();
            $table->string('to_hour')->nullable();
            $table->string('all_week')->default(0)->nullable();
            $table->string('all_day')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coverages');
    }
}
