<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\Rule;
use Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        return view('profile.index', compact( 'user' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ( $user->role == 'agent' ) {
            
            app('\App\Http\Controllers\AgentController')->update($request, $user->agent );

        } else if ( $user->role == 'company' ){


            $this->validate_profile_info($request, $user);


            #se modifica el nombre de la compañia
            $user->update([

                'first_name' => $request->contact_first_name,
                'last_name'  => $request->contact_last_name,
                'email'      => $request->email,

            ]);

            #se actualiza la contraseña en caso de ser requerido
            if ($request->password) {

                $user->update([
                    'password' => \Hash::make($request->password),
                ]);

            }

            $contact = $user->contact;

            $contact->update([

                'phone'  => $request->contact_phone,
                'ext'    => $request->contact_phone_ext,
                'mobile' => $request->contact_mobile,

            ]);

        } else if ( $user->role == 'admin' || $user->role == 'manager' ){
            $data = [

                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'username'   => $request->username,
                'email'      => $request->email,

            ];

            if (isset($request->password)) {
                
                $data['password'] = \Hash::make( $request->password );

            }

            #se modifica el nombre de la compañia
            $user->update($data);
        }

        return redirect()
            ->route('profile.index')
            ->with('msg', 'El perfil ha sido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validate_profile_info($request, $user)
    {
        $validations = [

            'username'           => [
                    'sometimes', 
                    'required', 
                    'string', 
                    'min:6', 
                    'max:15', 
                    'alpha_num', 
                    Rule::unique('users')->ignore($user ? $user->id : '')
                ],
            'email'              => [
                    'sometimes', 
                    'required', 
                    'email', 
                    Rule::unique('users')->ignore($user ? $user->id : '')
                ],
            'password'           => 'sometimes|string|nullable',

        ];

        $messages = [

            'username.required'           => 'El nombre de usuario es requerido',
            'username.unique'             => 'El nombre de usuario no esta disponible',
            'username.min'                => 'El nombre de usuario debe tener al menos 6 caracteres',
            'username.max'                => 'El nombre de usuario debe tener como maximo 15 caracteres',
            'username.alpha_num'          => 'El nombre de usuario solo puede contener números y letras',
            'email.required'              => 'El e-mail es requerido',
            'email.email'                 => 'El e-mail no es válido',
            'email.unique'                => 'El e-mail no esta disponible',
            'password'                    => 'La contraseña es requerida',

        ];

        if ($user->role == 'contact') {
            
            $validations['contact_first_name'] = 'sometimes|required|string';
            $validations['contact_last_name']  = 'sometimes|required|string';
            $validations['contact_phone']      = 'sometimes|string|nullable';
            $validations['contact_phone_ext']  = 'sometimes|string|nullable';
            $validations['contact_mobile']     = 'sometimes|string|nullable';

            $messages['contact_first_name.required'] = 'El nombre es requerido';
            $messages['contact_last_name.required']  = 'El apellido es requerido';

        } else if ( $user->role == 'admin' || $user->role == 'manager' ){

            $validations['first_name'] = 'sometimes|required|string';
            $validations['last_name']  = 'sometimes|required|string';

            $messages['first_name.required'] = 'El nombre es requerido';
            $messages['last_name.required']  = 'El apellido es requerido';

        }

        $validate = Validator::make($request->all(), $validations, $messages)->validate();

    }
}
