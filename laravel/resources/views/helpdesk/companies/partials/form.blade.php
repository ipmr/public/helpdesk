{{ csrf_field() }}

{{-- Titulo datos de la compañia --}}
<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<label><b>Datos de la compañia</b></label>
		<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
	</div>
</div>

{{-- Nombre de la compañia --}}
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">
		<span class="text-danger">*</span>
		@if($intention == 'new_company')
			Nombre de la compañia:
		@else
			Compañia:
		@endif
	</label>
	<div class="col-sm-3">
		<input type="text" name="name" class="form-control" autofocus required
		@if($intention == 'edit_company')
		value="{{ $company->name or old('name') }}" 
		@else
		value="{{ old('name') }}"
		@endif
		>
	</div>
</div>

{{-- Domicilio de la compañia --}}
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Domicilio:</label>
	<div class="col-sm-6">
		<div class="form-group">
			<input type="text" name="address" placeholder="Calle, Número, Colonia" class="form-control" required
			@if($intention == 'edit_company')
			value="{{ $company->address or old('address') }}" 
			@else
			value="{{ old('address') }}"
			@endif
			>
		</div>
		<div class="row form-row">
			<div class="col-sm-6">
				<input type="text" name="city" placeholder="Ciudad" class="form-control" required
				@if($intention == 'edit_company')
				value="{{ $company->city or old('city') }}" 
				@else
				value="{{ old('city') }}"
				@endif
				>
			</div>
			<div class="col-sm-4">
				<input type="text" name="state" placeholder="Estado" class="form-control" required
				@if($intention == 'edit_company')
				value="{{ $company->state or old('state') }}" 
				@else
				value="{{ old('state') }}"
				@endif
				>
			</div>
			<div class="col-sm-2">
				<input type="text" name="zip" class="form-control" placeholder="C.P." required
				@if($intention == 'edit_company')
				value="{{ $company->zip or old('zip') }}" 
				@else
				value="{{ old('zip') }}"
				@endif
				>
			</div>
		</div>
	</div>
</div>

{{-- Telefono de la compañia --}}
<div class="form-group row form-row">
	<label class="col-form-label col-sm-4 text-right">Teléfono:</label>
	<div class="col-sm-3">
		<input type="text" name="phone" class="form-control phone"
		@if($intention == 'edit_company')
		value="{{ $company->phone or old('phone') }}" 
		@else
		value="{{ old('phone') }}"
		@endif
		>
	</div>
</div>

{{-- RFC de la compañia --}}
<div class="form-group row form-row d-flex align-items-center">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> RFC:</label>
	<div class="col-sm-3">
		<input type="text" name="rfc" class="form-control text-uppercase" required
		@if($intention == 'edit_company')
			@if($company->master)
			value="{{ $company->rfc or old('rfc') }}"
			@else
			 	@if($company->master_rfc)
					value="{{ $company->master_parent->rfc or old('rfc') }}" 
					disabled
			 	@else
					value="{{ $company->rfc or old('rfc') }}"
			 	@endif
			@endif
		@elseif($intention == 'new_zone')
			value="{{ $company->master_parent->rfc or old('rfc') }}"
			disabled 
		@endif
		>
	</div>
	@if($intention == 'new_zone' || $intention == 'edit_company' && !$company->master)
	<div class="col-sm-5">
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input" id="master_rfc" @if($intention == 'new_zone' || $company->master_rfc) checked @endif @change="add_master_rfc">
			<label class="custom-control-label" for="master_rfc">Utilizar RFC global</label>
		</div>
	</div>
	@endif
</div>

{{-- Contrato de la compañia --}}
<div class="form-group row d-flex align-items-center form-row">
	<label class="col-form-label col-sm-4 text-right">Contrato:</label>
	<div class="col-sm-3">
		<select name="contract_id" class="form-control" @change="set_contract" 
			@if($intention == 'new_zone' || $intention == 'edit_company' && $company->master_contract) 
			disabled
			@endif
		>
			@if($intention == 'new_zone' || $intention == 'edit_company' && !$company->master)
			<option disabled selected value="null">Contrato global</option>
			@endif
			<option value>Sin Contrato</option>
			@foreach(App\Contract::latest()->get() as $contract)
				<option value="{{ $contract->id }}"
				@if($intention == 'edit_company' && !$company->master_contract)
					@if($company->contract && !$contract->master_contract && $company->contract->id == $contract->id)
						selected
					@endif
				@endif
				>
					{{ $contract->name }}
				</option>
			@endforeach
		</select>
	</div>
	@if($intention == 'new_zone' || $intention == 'edit_company' && !$company->master)
	<div class="col-sm-5">
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input" id="master_contract" @change="add_master_contract"
			@if($intention == 'new_zone' || $intention == 'edit_company' && $company->master_contract)
				checked
			@endif
			>
			<label class="custom-control-label" for="master_contract">Utilizar contrato global</label>
		</div>
	</div>
	@endif
</div>

{{-- Inicio de contrato --}}
<div class="form-group row form-row" v-if="contract">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Inicio De Contrato:</label>
	<div class="col-sm-3">
		<input type="text" name="contract_start_at" class="form-control datepicker"
		@if($intention == 'edit_company')
		value="{{ \Carbon\Carbon::parse($company->contract_start_at)->format('m/d/Y') }}" 
		@else
		value="{{ \Carbon\Carbon::now()->format('m/d/Y') }}" 
		@endif
		>
	</div>
</div>

{{-- Acuerdo de servicio --}}
<div class="form-group row form-row" v-if="contract">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> SLA:</label>
	<div class="col-sm-3" v-cloak>
		<select name="sla_id" class="form-control">
			@foreach(App\SLA::latest()->get() as $sla)
				<option 
					value="{{ $sla->id }}"
					@if($intention == 'edit_company')
						@if($company->sla && $company->sla->id == $sla->id)
							selected
						@endif
					@endif
				>
				{{ $sla->name }}
				</option>
			@endforeach
		</select>
	</div>
</div>

{{-- Tipo de cobertura --}}
<div class="form-group row form-row" v-if="contract">
	<label class="col-form-label col-sm-4 text-right"><span class="text-danger">*</span> Cobertura:</label>
	<div class="col-sm-3" v-cloak>
		<select name="coverage_id" class="form-control">
			@foreach(App\Coverage::latest()->get() as $coverage)
				<option 
					value="{{ $coverage->id }}"
					@if($intention == 'edit_company')
						@if($company->coverage && $company->coverage->id == $coverage->id)
							selected
						@endif
					@endif
				>
				{{ $coverage->name }}
				</option>
			@endforeach
		</select>
	</div>
</div>

{{-- Habilitar tickets urgentes --}}
<div class="form-group row form-row">
	<div class="col-sm-4 mx-auto">
		<div class="custom-control custom-checkbox">
		  <input type="checkbox" name="urgent_tickets" class="custom-control-input" id="urgentTickets"
			@if($intention == 'edit_company')
				@if($company->urgent_tickets)
					checked
				@endif
			@endif
		  >
		  <label class="custom-control-label" for="urgentTickets">Habilitar Tickets Urgentes</label>
		</div>
	</div>
</div>

<div class="form-row">
	<div class="col-sm-4 mx-auto">
		<hr class="m-0">
	</div>
</div>

{{-- Titulo para contacto principal --}}
<div class="row form-row mb-2 mt-3">
	<div class="col-sm-8 ml-auto">
		<label>
			<b>Contacto principal</b>
		</label>
	</div>
</div>

{{-- Formulario para contacto principal --}}
@if($intention != 'new_company')
	@include('helpdesk.contacts.partials.select_contact')
@endif

@if($intention != 'new_company')
<div id="createNewContactForm" v-if="new_contact">
@endif

@include('helpdesk.contacts.partials.form')

@if($intention != 'new_company')
</div>
@endif

