@extends('helpdesk.companies.layouts.tabs')
@section('tab_content')

	<div class="card-header d-flex align-items-center">
		<p class="lead">Lista de proyectos</p>
		<a href="#newProjectCollapse" data-toggle="collapse" class="btn btn-primary ml-auto"><i class="fa fa-plus"></i> Nuevo Proyecto</a>
	</div>

	

	<div class="collapse" id="newProjectCollapse">
		
		<div class="py-3 px-2" style="border-bottom: 1px solid #f2f2f2">
			
			
			<form action="{{ route('companies.projects.store', ['company' => $company]) }}" method="post">
				
				{{ csrf_field() }}


				@include('helpdesk.projects.partials.form')

				<div class="form-row">
					<div class="col-sm-4 mx-auto">
						<button type="submit" class="btn btn-success">
							<i class="fa fa-check"></i>
							Crear Proyecto
						</button>
					</div>
				</div>


			</form>


		</div>

	</div>


	@if ($company->projects()->count() > 0)
		<table class="table table-striped m-0">
			<thead>
				<tr>
					<th style="width: 15%"># Proyecto</th>
					<th>Título</th>
					<th>Descripción</th>
					<th  style="width: 15%" class="text-right">Estado</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($company->projects as $company_project)
					<tr>
						<td>
							<a href="{{ route('companies.projects.show', [$company, optional($company_project->project)->fup]) }}">
								{{ optional($company_project->project)->fup }}
							</a>
						</td>
						<td>{{ optional($company_project->project)->title }}</td>
						<td>{{ str_limit(optional($company_project->project)->description, 70) }}</td>
						<td class="text-right">{{ optional($company_project->project)->step }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@else


		<div class="card-body">
			No se han agregado proyectos
		</div>


	@endif

@stop