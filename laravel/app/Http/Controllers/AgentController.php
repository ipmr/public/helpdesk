<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Department;
use App\DepartmentGroupAgent;
use App\Notifications\NewAgentAccount;
use App\Notifications\TicketsSuspendedUser;
use App\Notifications\UpdateAgentData;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Notification;
use Validator;

class AgentController extends Controller
{

    public function __construct()
    {

        $this->middleware('role:admin,manager');

    }

    public function index(Request $request)
    {
        $columns = [
            "users.id",
            "users.first_name",
            "users.last_name",
            "users.email",
            "users.password",
            "users.username",
            "users.role",
            "agents.id as agent_id",
            "agents.department_id",
            "agents.jobtitle",
            "agents.active",
            "agents.cellphone",
            "agents.short_number",
            "agents.extension",
            "departments.name",
            "departments.description",
            "departments.supervisor_id",
        ];

        $agents = User::select($columns)
            ->join('agents', 'users.id', '=', 'agents.user_id')
            ->leftJoin('departments', 'agents.department_id', '=', 'departments.id');

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q);

            $agents = $agents->when('users.first_name', function ($query) use ($query_string, $request) {
                $query->orWhere('users.first_name', 'like', '%' . $request->q . '%');
                foreach ($query_string as $search) {
                    $query->orWhere('users.first_name', 'like', '%' . $search . '%');
                }
            })
                ->when('users.last_name', function ($query) use ($query_string, $request) {
                    $query->orWhere('users.last_name', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('users.last_name', 'like', '%' . $search . '%');
                    }
                })
                ->when('users.email', function ($query) use ($query_string, $request) {
                    $query->orWhere('users.email', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('users.email', 'like', '%' . $search . '%');
                    }
                })
                ->when('users.username', function ($query) use ($query_string, $request) {
                    $query->orWhere('users.username', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('users.username', 'like', '%' . $search . '%');
                    }
                })
                ->when('agents.jobtitle', function ($query) use ($query_string, $request) {
                    $query->orWhere('agents.jobtitle', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('agents.jobtitle', 'like', '%' . $search . '%');
                    }
                })
                ->when('agents.cellphone', function ($query) use ($query_string, $request) {
                    $query->orWhere('agents.cellphone', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('agents.cellphone', 'like', '%' . $search . '%');
                    }
                })
                ->when('agents.short_number', function ($query) use ($query_string, $request) {
                    $query->orWhere('agents.short_number', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('agents.short_number', 'like', '%' . $search . '%');
                    }
                })
                ->when('agents.extension', function ($query) use ($query_string, $request) {
                    $query->orWhere('agents.extension', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('agents.extension', 'like', '%' . $search . '%');
                    }
                })
                ->when('departments.name', function ($query) use ($query_string, $request) {
                    $query->orWhere('departments.name', 'like', '%' . $request->q . '%');
                    foreach ($query_string as $search) {
                        $query->orWhere('departments.name', 'like', '%' . $search . '%');
                    }
                });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd($agents->toSql());
            }

            if ($request->debug == 2) {
                dd($agents->get());
            }
        }

        $agents = $agents->orderBy('id', 'DESC')->paginate(12);

        return view('helpdesk.agents.index', compact('agents'));
    }

    public function create()
    {
        return view('helpdesk.agents.create');
    }

    public function store(Request $request)
    {

        #se valida la información del agente
        $this->validate_agent_data($request);

        #se crea el usuario del agente
        $user = $this->create_agent_user($request);

        #se crea el perfil del agente
        $agent = $this->create_agent_profile($request, $user);

        #si se creo el agente para completar la creacion de un
        #departamento, se redirecciona al formulario de creacion
        #de departamentos, de lo contrario se redirecciona al listado
        #de agentes
        $route = $route = route('agents.show', $agent->id);
        $msg   = 'El agente ' . $user->name . ' ha sido creado exitosamente.';

        return redirect($route)->with('msg', $msg);

    }

    public function create_agent_user($request)
    {

        $user             = new User();
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->username   = $request->username;
        $user->password   = \Hash::make($request->password);
        $user->save();

        return $user;

    }

    public function create_agent_profile($request, $user)
    {

        $agent                   = new Agent();
        $agent->user_id          = $user->id;
        $agent->jobtitle         = $request->jobtitle;
        $agent->generate_tickets = $request->generate_tickets || $request->as_manager ? 1 : 0;
        $agent->as_manager       = $request->as_manager ? 1 : 0;

        if ($request->has('department_id')) {
            $agent->department_id = $request->department_id;

            if ($request->has('group_id')) {

                $group_agent = new DepartmentGroupAgent();

                $group_agent->group_id = $request->group_id;

            }

        }

        if ($request->has('cellphone')) {
            $agent->cellphone = $request->cellphone;
        }

        if ($request->has('short_number')) {
            $agent->short_number = $request->short_number;
        }

        if ($request->has('extension')) {
            $agent->extension = $request->extension;
        }

        $agent->save();

        if ($request->has('group_id')) {

            $group_agent = new DepartmentGroupAgent();

            $group_agent->group_id = $request->group_id;
            $group_agent->agent_id = $agent->id;

            $group_agent->save();

        }

        #se le notifica al agente sobre su nueva cuenta
        $notification_data = [
            'password' => $request->password,
            'agent'    => $agent,
        ];

        $agent->user->notify(new NewAgentAccount($notification_data));

        return $agent;

    }

    public function show(Agent $agent)
    {
        return view('helpdesk.agents.show', compact('agent'));
    }

    public function edit(Agent $agent)
    {
        return view('helpdesk.agents.edit', compact('agent'));
    }

    public function update(Request $request, Agent $agent)
    {

        #se valida la informacion obtenida
        $this->validate_agent_data($request, $agent);

        #si el agente es supervisor de otro departamento
        #se retorna con la notificacion para delegar a otro supervisor
        if ($agent->supervisor && $agent->supervisor->id != $request->department_id) {
            return redirect()
                ->back()
                ->with('warning', 'El agente no puede ser asignado al departamento seleccionado, ya que actualmente es supervisor en ' . $agent->supervisor->name);
        }

        #se actualiza la informacion del usuario relacionado al agente
        $this->update_agent_user($request, $agent);

        #se actualiza la información del perfil del agente
        $this->update_agent_profile($request, $agent);

        if (auth()->user()->id != $agent->user->id) {

            $notification_data = [
                'user'  => $agent->user,
                'agent' => $agent,
            ];

            Notification::send($agent->user, new UpdateAgentData($notification_data));

        }

        return redirect()->route('agents.show', $agent)->with('msg', 'La información del agente ha sido actualizada');

    }

    public function update_agent_user($request, $agent)
    {

        $agent->user->update([

            'username'   => $request->username,
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,

        ]);
        if ($request->password) {

            $agent->user->update([

                'password' => \Hash::make($request->password),

            ]);

        }

    }

    public function update_agent_profile($request, $agent)
    {

        $attributes = [
            'jobtitle'         => $request->jobtitle,
            'generate_tickets' => $request->generate_tickets || $request->as_manager ? 1 : 0,
            'as_manager'       => $request->as_manager ? 1 : 0,
        ];

        if (isset($request->department_id)) {
            $attributes['department_id'] = $request->department_id;
        }

        if (isset($request->active)) {
            $attributes['active'] = $request->active;
        }

        if (isset($request->cellphone)) {
            $agent->cellphone = $request->cellphone;
        }

        if (isset($request->short_number)) {
            $agent->short_number = $request->short_number;
        }

        if (isset($request->extension)) {
            $agent->extension = $request->extension;
        }

        if ($agent->group) {

            if ($agent->group->supervisor->id == $agent->id) {

                $agent->group->supervisor_id = $agent->department
                    ->supervisor->id;

                $agent->group->save();

                $validate_if_supervisor_is_on_group = DepartmentGroupAgent::where('agent_id', $agent->department->supervisor->id)->
                    where('group_id', $agent->group->id)->
                    first();

                if (!$validate_if_supervisor_is_on_group) {

                    $add_department_supervisor_to_group = new DepartmentGroupAgent();

                    $add_department_supervisor_to_group->group_id = $agent->group->id;

                    $add_department_supervisor_to_group->agent_id = $agent->department->supervisor->id;

                    $add_department_supervisor_to_group->save();

                }

            }

            $remove_agent_from_group = DepartmentGroupAgent::where('agent_id', $agent->id)->first();

            $remove_agent_from_group->delete();

        }

        if ($request->has('group_id')) {

            $group_agent = new DepartmentGroupAgent();

            $group_agent->group_id = $request->group_id;
            $group_agent->agent_id = $agent->id;

            $group_agent->save();

        }

        $agent->update($attributes);

    }

    public function destroy(Request $request, Agent $agent)
    {
        // verificar si el usuario es un supervisor de departamento
        // si es supervisor, asignar un agente del departamento
        if ($agent->id == optional(optional($agent->department)->supervisor)->id) {

            app('App\Http\Controllers\DepartmentController')->assign_agent_as_supervisor($request, $agent->department);

        }

        // si es solo un agente
        // obtener todos los tickets (!cerrado)
        // asignar tickets a supervisor de departamentos
        $status = \App\Status::where('name', 'Cerrado')->first();

        $tickets = $agent->tickets()->whereNotIn('status_id', [$status->id])->get();

        $supervisor = optional($agent->department)->supervisor;

        if ($tickets->count() > 0) {
            foreach ($tickets as $ticket) {
                $ticket->agent_id = $supervisor->id;

                $ticket->save();

                $history = new \App\History();

                $history->ticket_id   = $ticket->ticket_id;
                $history->description = 'Usuario suspendido, '.optional($agent->user)->full_name.' se asigna a supervisor '.optional($supervisor->user)->full_name;

                $history->save();
            }

            $notification_data = [
                'title'   => 'Tickets de Agente Suspendido',
                'tickets' => $tickets,
            ];

            // enviar correos de asignaci'on de tickets
            // registrar movimiento en log de cada ticket
            Notification::send($supervisor->user, new TicketsSuspendedUser($notification_data));
        }

        $user = $agent->user;

        $msg = 'El agente '.$user->full_name.' ha sido suspendido';

        $agent->active = 0;

        $user->delete();

        return redirect()
            ->route('agents.index')
            ->with( 'msg', $msg );
    }

    public function validate_agent_data($request, $agent = null)
    {
        #se establecen los mensajes para las validaciones
        $msgs = [

            'username.required'       => 'El nombre de usuario es requerido',
            'username.alpha_num'      => 'El nombre de usuario solo puede contener letras y números',
            'username.min'            => 'El nombre de usuario debe contener mínimo 6 caracteres',
            'username.max'            => 'El nombre de usuario debe contener máximo 15 caracteres',
            'username.unique'         => 'El nombre de usuario no esta disponible',
            'first_name.required'     => 'Nombre(s) requerido(s)',
            'last_name.required'      => 'Apellido(s) requerido(s)',
            'email.required'          => 'El e-mail es requerido',
            'email.unique'            => 'El e-mail no esta disponible',
            'email.email'             => 'El e-mail no es válido',
            'jobtitle.required'       => 'El puesto es requerido',
            'password.required'       => 'La contraseña es requerida',
            'department_id.sometimes' => 'El departamento es requerido',
            'department_id.exists'    => 'El departamento seleccionado no es válido',

        ];

        #se valida la informacion del agente
        $validate = Validator::make($request->all(), [

            'username'      => ['required', 'alpha_num', 'min:6', 'max:15', Rule::unique('users')->ignore($agent ? $agent->user->id : '')],
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => ['required', 'email', Rule::unique('users')->ignore($agent ? $agent->user->id : '')],
            'jobtitle'      => 'required|string',
            'password'      => 'sometimes|string|nullable',
            'department_id' => 'sometimes|nullable|exists:departments,id',

        ], $msgs)->validate();
    }
}
