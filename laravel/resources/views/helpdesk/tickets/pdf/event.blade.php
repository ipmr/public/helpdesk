<style>
	body{
		font-family: 'Helvetica', sans-serif;
		font-size: 14px;
	}
	.text-danger {
	    color: #ed1c24!important;
	}
	h1, h2, h3, h4{
		font-weight: lighter;
	}
	.m-0{
		margin: 0 !important;
	}
	table{
		width: 100%;
		font-size: 12px;
	}
	table tr, table th, table td{
		vertical-align: top;
	}
	.bg-warning{
	background: #eef3f9 !important;
	}
</style>

<table cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
	<tr
>		<td style="vertical-align: middle;">
			<h2 style="margin: 0">
				Ticket <span class="text-danger">#{{ $ticket->ticket_id }}</span>
			</h2>
		</td>
		<td style="vertical-align: middle;" align="right">
			Fecha de exportación: {{ \Carbon\Carbon::now()->format('d M, Y - h:i a') }}
		</td>
	</tr>
</table>


{{-- <table cellpadding="0" cellspacing="0" style="margin-bottom: 20px">
	<tr>
		<td style="vertical-align: middle;">Status:</td>
		<td>{!! $ticket->status() !!}</td>
	</tr>
</table> --}}


<table cellpadding="0" cellspacing="0" style="margin-bottom: 20px; margin-top: 20px;">
	<tr>
		<td style="vertical-align: middle;">
			<h2 style="margin: 0">
				Comentarios al cliente
			</h2>
		</td>
	</tr>
</table>

<p><small>©{{ \Carbon\Carbon::now()->format('Y') .' '. config('app.name') }}</small></p>