<div class="d-flex">
    
    <form action="{{ route('tickets.update', $ticket->ticket_id) }}" method="post" id="updateStatusForm" style="width: 100%;">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="form-group m-0">
            <select name="status_id" class="form-control" @change="active_success_btn" style="border-radius: 3px 0 0 3px">
                @php
                    if ( in_array(auth()->user()->role, ['admin','manager']) ) {
                        $statuses = App\Status::get();
                    } else {
                        $statuses = App\Status::whereNotIn('name', ['Cerrado'])->get();
                    }
                @endphp
                @foreach($statuses as $status)
                <option {{ $ticket->status_id == $status->id ? 'selected' : '' }} value="{{ $status->id }}">
                    {{ $status->name }}
                </option>
                @endforeach
            </select>
        </div>
    </form>

    <button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Estado" form="updateStatusForm" class="btn btn-success" @click.prevent="change_status_ticket">
        <i class="fa fa-check" style="margin: 0 !important;"></i>
    </button>

</div>