var zoneApp = new Vue({

	el: '#zone_app',
	data: {
		new_contact: null,
		contract: false
	},
	mounted:function(){
		@if(isset($intention) && $intention == 'edit_company' && isset($company) && $company->contract && !$company->master_contract)
			this.contract = true;
		@endif
	},
	methods: {
		
		set_contract: function(e){

			var select = $(e.target);

			var value = select.val();

			if(value){
				this.contract = true;

				setTimeout(function(){
					$( ".datepicker" ).datepicker({
						monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
						monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
						dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
						dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
						dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					});
				}, 150);

			}else{
				this.contract = false;
			}

		},

		@isset($company)

		add_master_rfc: function(e){

			var checkbox = $(e.target);

			var master_parent_rfc = '{{ $company->master_parent->rfc }}';

			var input = $('[name="rfc"]');

			if(checkbox.is(':checked')){
				
				input.val(master_parent_rfc).attr('disabled', 'disabled');

			}else{
				
				input.val('').removeAttr('disabled').focus();

			}

		},

		add_master_contract: function(e){

			var t = this;
			var checkbox = $(e.target);
			var select = $('[name="contract_id"]');

			if(checkbox.is(':checked')){
				select.attr('disabled', 'disabled').val($('option:first', select).val());
				t.contract = false;
			}else{
				select.removeAttr('disabled');
				select.val($('option:eq(1)', select).val());
				t.contract = false;
			}

		},

		add_new_contact: function(e){

			var t = this;

			var link = $(e.target);
			var link_new = '<i class="fa fa-plus"></i> Nuevo Contacto';
			var link_cancel = '<i class="fa fa-times"></i> Cancelar';
			
			var select = $('#select_contact');
			
			if(select.attr('disabled')){
				select.removeAttr('disabled');
				t.new_contact = null;
				link.html(link_new);
			}else{
				select.attr('disabled', 'disabled').val('');
				t.new_contact = true;
				link.html(link_cancel);
				setTimeout(function(){
					$('html, body').animate({scrollTop: ($('#createNewContactForm').offset().top - 150)});
					$('[name="contact_first_name"]').focus();
					$('.phone').mask('(000) 000-0000', {placeholder: "(___) ___-____"});
				}, 100);
			}

		}
		@endisset
	}

})