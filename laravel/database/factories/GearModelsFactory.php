<?php

use Faker\Generator as Faker;

$factory->define(App\GearModel::class, function (Faker $faker) {
    return [
        'brand_type_id'  => 1,
        'name'     => strtoupper($faker->word),
    ];
});
