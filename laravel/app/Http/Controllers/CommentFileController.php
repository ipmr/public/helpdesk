<?php

namespace App\Http\Controllers;

use App\CommentFile;
use Illuminate\Http\Request;

class CommentFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CommentFile  $commentFile
     * @return \Illuminate\Http\Response
     */
    public function show(CommentFile $commentFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CommentFile  $commentFile
     * @return \Illuminate\Http\Response
     */
    public function edit(CommentFile $commentFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CommentFile  $commentFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommentFile $commentFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CommentFile  $commentFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentFile $commentFile)
    {
        //
    }
}
