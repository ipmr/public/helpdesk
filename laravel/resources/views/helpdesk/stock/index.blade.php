@extends('layouts.master')

@section('content')
    <div class="container" id="gears_app">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <p class="lead">
                    {{ $inventory->name }}
                </p>
                {{-- <form action="{{ route('inventories.stock.index',$inventory) }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                    <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                        value="{{ $_GET['q'] }}"
                    @endisset>
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </form> --}}
                <a class="btn btn-primary ml-auto" href="{{ route('inventories.stock.create',$inventory) }}">
                    <i class="fa fa-plus"></i>
                    Nuevo Equipo
                </a>
            </div>
            @if(count($stocks) > 0)
                <table class="table table-striped m-0">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>No. Serie</th>
                            <th>Estado</th>
                            <th>Activo</th>
                            <th>Compañia</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($stocks as $stock)
                            <tr>
                                <td>
                                    {{ $stock->gear->type->name }}
                                </td>
                                <td>
                                    {{ $stock->gear->brand->name }}
                                </td>
                                <td>
                                    {{ $stock->gear->model->name }}
                                </td>
                                <td>
                                    <a href="{{ route('inventories.stock.show', [$inventory, $stock]) }}">
                                        {{ $stock->gear->serial_number }}
                                    </a>
                                </td>
                                <td>
                                    {{ $stock->gear->status }}
                                </td>
                                @if ($stock->gear->active)
                                    <td class="text-success">
                                        Equipo activo
                                    </td>
                                @else
                                    <td class="text-danger">
                                        Equipo dado de baja
                                    </td>
                                @endif
                                <td>
                                    <button class="btn btn-link btn-sm" @click="open_modal({{ $stock->id }})">
                                        @if ($stock->assigned)
                                            {{ optional(optional($stock->gear)->company)->name}}
                                        @else
                                            [Asignar Compañia]
                                        @endif                                        
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="card-footer">
                    {{ $stocks->links() }}
                </div>
            @else
                <div class="card-body">No se han agregado elementos al inventario</div>
            @endif
        </div>

        @include('helpdesk.stock.html.modal_company_sites_assigned_to_gear')
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var gears = new Vue({
        el: '#gears_app',
        data: {
            form_url   : '',
            company_id : null,
            sites      : null,
        },
        mounted: function(){
        },
        methods: {
            open_modal: function(stock_id){
                var t = this
                t.form_url = '';
                var url = '{{ route('assign_gear_to_company', [$inventory, '#stock']) }}';
                t.form_url = url.replace('#stock', stock_id);

                t.company_id = company_id;

                $('#assign_company_to_gear').modal('show')
            },
            get_sites: function(e){
                var t = this;
                var select = $(e.target);
                var company = select.val();
                var url = '{{ route('companies.gears.index', '#company_id') }}';
                
                t.sites = null;
                if(select){
                    url = url.replace('#company_id', company);

                    $.get(url, function(response){
                        t.sites = response.zones;
                    });
                }
            }
        }
    });
</script>
@endsection