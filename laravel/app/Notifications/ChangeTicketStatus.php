<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ChangeTicketStatus extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->user   = $request['user'];
        $this->ticket = $request['ticket'];
        $this->status = $request['status'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text  = $this->status->description;

        if (in_array($this->user->role, ['agent','manager'])) {
            

            $text = $this->user->FullName . ' modificó el status del ticket a ' . $this->ticket->get_status->name;

            
        }

        return (new MailMessage)
            ->subject('Actualización Ticket ' . $this->status->name)
            ->greeting('Ticket #' . $this->ticket->ticket_id)
            ->line($text)
            ->action('Ver ticket', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $text  = $this->status->description;

        if (in_array($this->user->role, ['agent','manager'])) {
            $text = $this->user->FullName . ' modificó el status del ticket a ' . $this->ticket->get_status->name;
        }

        return [
            'msg'    => $text,
            'ticket' => $this->ticket,
        ];
    }
}
