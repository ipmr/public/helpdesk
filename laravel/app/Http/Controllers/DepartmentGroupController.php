<?php

namespace App\Http\Controllers;

use App\Department;
use App\Agent;
use App\DepartmentGroup;
use App\DepartmentGroupAgent;
use Illuminate\Http\Request;
use Validator;
use App\Notifications\AddAgentAsGroupSupervisor;

class DepartmentGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Department $department)
    {

        $groups = $department->groups;

        return response()->json($groups);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {

        $groups = json_encode($department->groups->pluck('name'));

        $groups = str_replace('[', '', $groups);
        $groups = str_replace(']', '', $groups);
        $groups = str_replace('"', '', $groups);

        $agents = json_encode($department->agents->pluck('id'));
        $agents = str_replace('[', '', $agents);
        $agents = str_replace(']', '', $agents);
        $agents = str_replace('"', '', $agents);

        $validate = Validator::make($request->all(), [

            'name'          => 'required|string|not_in:' . $groups,
            'supervisor_id' => 'required|in:' . $agents,

        ], [

            'name.not_in'            => 'El nombre del grupo es requerido',
            'name.not_in'            => 'El nombre del grupo ya se encuentra registrado',
            'supervisor_id.required' => 'El supervisor es requerido',
            'supervisor_id.in'       => 'El supervisor seleccionado no pertenece al departamento',

        ]);

        if ($validate->fails()) {

            $response = [

                'type'   => 'error',
                'errors' => $validate->errors()->all(),

            ];

            return response()->json($response);

        } else {


            $group                = new DepartmentGroup();
            $group->department_id = $department->id;
            $group->name          = $request->name;
            $group->supervisor_id = $request->supervisor_id;

            
            $group->save();


            $group_agent = new DepartmentGroupAgent();
            $group_agent->group_id = $group->id;
            $group_agent->agent_id = $request->supervisor_id;

            
            $group_agent->save();


            $notification_data = [

                'group' => $group,
                'agent' => $group->supervisor

            ];


            $group->supervisor->user->notify(new AddAgentAsGroupSupervisor($notification_data));


            return response()->json($group->department->groups()->with(['supervisor', 'supervisor.user', 'agents'])->latest()->get());

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentGroup  $group
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentGroup $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentGroup  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentGroup $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentGroup  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department,  DepartmentGroup $group)
    {
        

        
        $name_unavailable = $department
            ->groups()
            ->where('name', $request->name)
            ->where('name', '<>', $group->name)
            ->first();

        if($name_unavailable){

            $response = [

                'type' => 'error',
                'errors' => ['El nombre del grupo ya esta registrado']

            ];

            return response()->json($response);

        }


        $group->agents()->delete();

        $group->name = $request->name;
        $group->supervisor_id = $request->agents[0];
        $group->save();

        $add_agent_to_group = new DepartmentGroupAgent();
        $add_agent_to_group->group_id = $group->id;
        $add_agent_to_group->agent_id = $request->agents[0];
        $add_agent_to_group->save();



        $notification_data = [

            'group' => $group,
            'agent' => $group->supervisor

        ];


        $group->supervisor->user->notify(new AddAgentAsGroupSupervisor($notification_data));



        foreach($request->agents as $key => $agent){


            if($key > 0 && $agent !== $group->supervisor_id){


                $add_agent_to_group = new DepartmentGroupAgent();
                $add_agent_to_group->group_id = $group->id;
                $add_agent_to_group->agent_id = $agent;
                $add_agent_to_group->save();


            }


        }




        $response = [


            'groups' => $group->department->groups()->with(['supervisor', 'supervisor.user', 'agents'])->latest()->get()


        ];



        return response()->json($response);


    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentGroup  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentGroup $group)
    {
        //
    }

}
