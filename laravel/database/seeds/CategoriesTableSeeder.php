<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 1)->create([
            'name' => 'Servicio'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Mantenimiento'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Monitoreo'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Instalación'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Cableado'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Suministro'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Falla'
        ]);

        factory(Category::class, 1)->create([
            'name' => 'Otra'
        ]);
    }
}
