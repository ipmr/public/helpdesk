<?php
use App\Gear;
use App\GearBrand;
use App\GearType;
use App\GearModel;
use App\Coverage;
use Faker\Generator as Faker;

$factory->define(Gear::class, function (Faker $faker) {
    return [
        'type_id'           => function () {

            $rand_id = rand(1, GearType::count());

            $type_id = GearType::where('id', $rand_id)->first();

            return $type_id->id;
        },
        'brand_id'          => function () {

            $rand_id = rand(1, GearBrand::count());

            $brand_id = GearBrand::where('id', $rand_id)->first();

            return $brand_id->id;
        },
        'model_id'          => function () {

            $rand_id = rand(1, GearModel::count());

            $model_id = GearModel::where('id', $rand_id)->first();

            return $model_id->id;
        },
        'serial_number'     => $faker->phoneNumber,
        'description'       => $faker->text,
        'comments'          => $faker->text,
        'coverage_id'       => function () {

            $rand_id = rand(1, Coverage::count());

            $coverage_id = Coverage::where('id', $rand_id)->first();

            return $coverage_id->id;
        },
        'master_coverage'   => 1,
        'contract_id'       => null,
        'master_contract'   => 1,
        'contract_start_at' => date('Y-m-d'),
        'status'            => 'Funciona',
        'active'            => 1,
    ];
});
