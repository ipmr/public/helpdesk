@extends('layouts.master')

@section('content')
	
	<div class="container" id="app_notifications">
		<div class="card">
			<div class="card-header d-flex align-items-center card_header_fixed">
				<p class="lead mt-2 mb-2">
					Notificaciones
				</p>
				<a v-cloak class="btn btn-sm btn-light ml-auto" id="archive-notifications" v-if="checked" @click="archive_notifications">
					<i class="fa fa-archive"></i> 
					Archivar (@{{ elements_to_archive }})
				</a>
				<a href="{{ route('list-archive') }}" class="btn btn-sm btn-link" v-bind:class="[show_list]" id="list-archived-notifications">
					<i class="fa fa-list"></i>
					Lista Archivadas
				</a>
			</div>
			@include('helpdesk.notifications.partials.notifications_list', [
                'notifications' => $notifications,
            ])
		</div>
	</div>

@stop

@section('scripts')
	
	<script type="text/javascript">
		var app_notifications = new Vue({
			el: '#app_notifications',
			data: {
				show_list: 'btn btn-sm btn-default ml-auto',
				checked : false,
				elements_to_archive: 0,
				notifications : [],
			},
			mounted: function(){
				$('a.mark_notification_as_read').each(function(){

					var t = $(this);
					
					var id = t.data('notification');

					var href = t.attr('href');

					t.on('click', function(e){

						e.preventDefault();

						t.attr('disabled', 'disabled');

						var url = '{{ route('read-notification', '#notification_id') }}';

						url = url.replace('#notification_id', id);

						var data = {
							_token: '{{ csrf_token() }}',
						}

						$.post(url, data, function(response){
							
						}).done(function(){

							window.location = href;

						});

					});

				});
			},
			methods:{
				control_check: function(){
					var principal_is_check = $('.control-check').prop('checked')

					this.checked = principal_is_check;
					this.show_list = (principal_is_check)?'btn btn-sm btn-default ml-3':'btn btn-sm btn-default ml-auto';
					var count_elements = 0;

					$.each($('.check-notification'),function( index, check ){

						$(this).prop('checked', principal_is_check)
						if(principal_is_check){
							count_elements += 1;
						}
					})

					this.elements_to_archive = count_elements;
				},
				change_checked: function(){
					var principal_is_check = false;
					var count_elements = 0;
					$.each($('.check-notification'),function(){
						if ( $(this).prop('checked') ){
							principal_is_check = true;
							count_elements += 1;
						} else {
							$('.control-check').prop('checked', false)
						}

					});

					this.show_list = (principal_is_check)?'btn btn-sm btn-default ml-3':'btn btn-sm btn-default ml-auto';
					this.checked = principal_is_check;
					this.elements_to_archive = count_elements;
				},
				archive_notifications: function(){
					$('#archive-notifications').empty().append( 
						$('<i/>',{
							class: 'fa fa-spin fa-spinner',
						}),
						'Archivando Notificaciones'
					)

					var notifications_to_archive = [];

					$.each($('.check-notification'),function(){
						
						var value_id = $(this).val()

						if ( $(this).prop('checked') ){

							notifications_to_archive.push({
								name: 'notification-id',
								value: value_id
							})
						}
					});

					var url = '{{ route("archive-notification") }}';
					var data ={
						_token: '{{ csrf_token() }}',
						notifications: notifications_to_archive,
					}

					this.$http.post(url,data).then(function(response){
						window.location.href = response.body.redirect
					});
				}
			},
		});

	</script>

@stop