<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReopenTicketNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->ticket        = $request['ticket'];
        $this->reopen_ticket = $request['reopen_ticket'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reapertura Ticket: ' . $this->reopen_ticket->subject)
            ->greeting('Ticket #' . $this->ticket->ticket_id)
            ->line($this->reopen_ticket->message)
            ->action('Ver ticket', route('read_reopen_ticket', $this->reopen_ticket->id ) );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg'    => 'Reapertura ticket id #' . $this->ticket->ticket_id . ': ' .$this->reopen_ticket->subject.' "'.str_limit($this->reopen_ticket->message,140).'"',
            'ticket' => $this->ticket,
            'reopen_ticket' => $this->reopen_ticket,
        ];
    }
}
