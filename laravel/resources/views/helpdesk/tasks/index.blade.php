@extends('layouts.master')

@section('content')

	<div class="container" id="task_app">
		<div class="card">
			
			<div class="card-header">
				
				<p class="lead m-0">Mis Tareas</p>

			</div>

			
			<div class="card-body">
				
				@include('helpdesk.tasks.partials.list', [

					'create_new' => true,
					'tasks' => $tasks,
					'type' => 'personal'

				])

			</div>


		</div>
		@include('helpdesk.tickets.partials.modal-update-task')
		@include('helpdesk.tickets.partials.modal-add-event-to-task')
	</div>
	
@stop

@section('scripts')
	<script type="text/javascript">
		var task_app = new Vue({
			el: '#task_app',
			data: {
				task: [],
				update_url: null,
				task_id: null,
				ticket_id: null
			},
			methods: {
				get_task( task_id ){
					var t = this;

	                var show_url = '{{ route('tickets.tasks.show', ['ticket'=>'#ticket_id', 'task' => '#task_id']) }}';
	                var update_url = '{{ route('tickets.tasks.update', ['ticket'=>'#ticket_id', 'task' => '#task_id']) }}';

	                show_url = show_url.replace( '#ticket_id', $('.edit-task-'+task_id).attr('ticketid') );
	                show_url = show_url.replace( '#task_id', task_id );

	                update_url = update_url.replace( '#ticket_id', $('.edit-task-'+task_id).attr('ticketid') );
	                update_url = update_url.replace( '#task_id', task_id );

	                t.$http.get(show_url).then(function(response){
	                    t.task = response.body;

	                    t.update_url = update_url;

	                    $('#update_task_modal').modal('show')
	                });
				},

				add_event_to_task: function(task, ticket){

					var t = this;

					
					var modal = $('#add_event_to_task_modal');

					
					modal.modal('show');


					t.task_id = task;


					t.ticket_id = ticket;



				},


				comment_to_task_route: function(){


					var t = this;


					var route = "{{ route('tickets.tasks.events.store', ["#ticket_id", "#task_id"]) }}";

					
					route = route.replace('#ticket_id', t.ticket_id).replace('#task_id', t.task_id);


					return route;


				}
			}
		})
	</script>
@stop