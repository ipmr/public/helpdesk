@extends('layouts.master')
@section('content')
<div class="container">
	<div class="card">
		<div class="card-header d-flex align-items-center">
			<a href="{{ route('sla.show', $sla) }}/" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<div class="card-body">
			<form action="{{ route('sla.update', $sla) }}" method="post" id="newSLAForm">
				
				{{ method_field('PATCH') }}
				@include('helpdesk.sla.partials.form')

			</form>
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-sm-9 ml-auto">
					<button type="submit" class="btn btn-success" form="newSLAForm">
						<i class="fa fa-check"></i>
						Actualizar Acuerdo de Servicio
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop