<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		<span class="text-danger">*</span>
		Usuario:
	</label>
	<div class="col-sm-6">
		<div class="row form-row">
			<div class="col-sm-6">
				<input type="text" class="form-control" name="username" value="{{ $agent->user->username or old('username') }}" required autofocus>
			</div>
		</div>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		<span class="text-danger">*</span>
		Nombre Completo:
	</label>
	<div class="col-sm-6">
		<div class="row form-row">
			<div class="col-sm-6">
				<input type="text" class="form-control" placeholder="Nombre(s)" value="{{ $agent->user->first_name or old('first_name') }}" name="first_name" required>
			</div>
			<div class="col-sm-6">
				<input type="text" class="form-control" placeholder="Apellidos(s)" value="{{ $agent->user->last_name or old('last_name') }}" name="last_name" required>
			</div>
		</div>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		<span class="text-danger">*</span>
		E-mail:
	</label>
	<div class="col-sm-5">
		<input type="email" class="form-control" value="{{ $agent->user->email or old('email') }}" name="email" required>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		<span class="text-danger">*</span>
		Puesto:
	</label>
	<div class="col-sm-3">
		<input type="text" class="form-control" value="{{ $agent->jobtitle or old('jobtitle') }}" name="jobtitle" required>
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		Celular:
	</label>
	<div class="col-sm-3">
		<input type="text" class="form-control phone" value="{{ $agent->cellphone or old('cellphone') }}" name="cellphone">
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		Num Corto:
	</label>
	<div class="col-sm-3">
		<input type="text" class="form-control" value="{{ $agent->short_number or old('short_number') }}" name="short_number">
	</div>
</div>

<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		Extensión:
	</label>
	<div class="col-sm-2">
		<input type="text" class="form-control" value="{{ $agent->extension or old('extension') }}" name="extension">
	</div>
</div>

@if (auth()->user()->role == 'admin')
	@if(App\Department::count() > 0)
		
		
		<div class="form-row pt-3 mb-3">
			<div class="col-sm-4 mx-auto">
				<label><b>Asignar Departamento</b></label>
			</div>
		</div>


		<div class="form-group row form-row">
			<label class="col-sm-4 text-right col-form-label">
				Departamento:
			</label>
			<div class="col-sm-5">
				<div class="d-flex align-items-center">
					<select name="department_id" class="form-control" @change="get_department_groups">
						<option disabled selected value>Elige una opción</option>
						@foreach(App\Department::get() as $department)
						<option 
							@isset($agent) 
								{{ optional($agent->department)->id == $department->id ? 'selected' : '' }}  
							@endisset 
							value="{{ $department->id }}">
								{{ $department->name }}
						</option>
						@endforeach
					</select>
					<a class="btn btn-link ml-3 px-0" href="{{ route('departments.create') }}/">
						<i class="fa fa-plus"></i> Nuevo
					</a>
				</div>
			</div>
		</div>


		{{-- <div class="form-row" v-if="loading_groups == true">
			<div class="col-sm-4 mx-auto">
				<p><i class="fa fa-spinner fa-spin"></i> Cargando Grupos</p>
			</div>
		</div>


		<div class="form-group row form-row" 
			 v-if="groups && groups.length > 0">

			<label class="col-sm-4 text-right col-form-label">
				Grupo:
			</label>
			
			<div class="col-sm-3">
				<div class="d-flex align-items-center">
					<select name="group_id" 
							class="form-control" 
							required
							:value="current_group()" >

						<option disabled selected value>
							Elige una opción
						</option>
						
						<option v-for="group in groups" :value="group.id">
							@{{ group.name }}
						</option>

					</select>
				</div>
			</div>

		</div>

		<div class="form-row" v-if="groups && groups.length == 0">
			<div class="col-sm-4 mx-auto">
				<p>No hay grupos en el departamento</p>
			</div>
		</div> --}}

	@endif
@endif


<div class="form-row pt-3 mb-3">
	<div class="col-sm-4 mx-auto">
		<label><b>Seguridad</b></label>
	</div>
</div>


<div class="form-group row form-row">
	<label class="col-sm-4 text-right col-form-label">
		@isset($update) Restaurar @else <span class="text-danger">*</span> @endisset Contraseña:
	</label>
	<div class="col-sm-3">
		<input type="password" class="form-control" name="password" @isset($update) @else required @endisset>
	</div>
</div>

@role('admin')

@isset($agent)

<div class="form-group row form-row">
	<div class="col-sm-4 mx-auto">

		<p>Marca la casilla para permitir que el agente pueda generar tickets.</p>

		<div class="custom-control custom-checkbox">
		  	<input 	type="checkbox" 
			  		name="generate_tickets" 
			  		class="custom-control-input"

					@if($agent->generate_tickets)
						checked 
					@endif

			  		id="generateTickets">


		  	<label class="custom-control-label" for="generateTickets">Generar Tickets</label>
		</div>
	</div>
</div>


<div class="form-group row form-row">
	<div class="col-sm-4 mx-auto">

		<p>Marca la casilla para otorgar permisos de manager al agente.</p>

		<div class="custom-control custom-checkbox">
		  	<input 	type="checkbox" 
			  		name="as_manager" 
			  		class="custom-control-input"

					@if($agent->as_manager)
						checked 
					@endif

			  		id="managerPermissions">


		  	<label class="custom-control-label" for="managerPermissions">Permisos de manager</label>
		</div>
	</div>
</div>

@endisset

@endrole
