require_revision: function(e){
	

	var t = this;


	var modal = $('#requireRevisionModal');


	var form = $(e.target);


	var form_id = form.attr('id');


	var btn = $('button[form="'+form_id+'"]');


	var data = form.serialize();


	var url = form.attr('action');


	$.post(url, data, function(response){


		modal.modal('hide');

		btn.removeAttr('disabled');

		form[0].reset();


	}).done(function(response){


		t.revision_ticket = response.ticket;


		setTimeout(function(){

			
			t.show_notification(response.msg);


		}, 300);


	});




	$('#requireRevisionModal').on('hidden.bs.modal', function (e) {
	  


	});



},