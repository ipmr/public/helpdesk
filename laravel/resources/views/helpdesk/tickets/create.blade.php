@extends('layouts.master')
@section('content')

<div class="container">
	<div class="card">
		<div class="card-header">
			<div class="row">
				<div class="col-sm-9 ml-auto">
					<p class="lead">
					
						<img src="{{ asset('img/iconos/tickets.svg') }}" style="height: 40px;" class="mr-2" alt="">

						Nuevo Ticket

					</p>
				</div>
			</div>
		</div>
		
		<div class="card-body" id="tickets_app">
	
			<div class="form-row">
				<div class="col-sm-9 ml-auto">
					<p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
				</div>
			</div>


			<form action="{{ route('tickets.store') }}" method="post" id="newTicketForm">
				{{ csrf_field() }}
				
				<div v-cloak>
					<div class="row form-row">
						<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
							<span class="text-danger">*</span>
							Compañia:
						</label>
						<div class="form-group col-sm-4">
							<select name="company_id" id="company_master_select" class="form-control" @change="get_company_gears_models" required>
								<option disabled selected value>Elige una compañia</option>
								@foreach($companies as $company)
									
									<option value="{{ $company->id }}">{{ $company->name }}</option>

								@endforeach
							</select>
						</div>
					</div>
			
					
					@role('admin', 'manager', 'agent')


					<div class="row form-row" v-if=" zones ">
						<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
							
							Sitio:

						</label>
						<div class="form-group col-sm-4">
							<select name="zone_id" class="form-control" @change="get_zone_contacts">
								
								<option selected value>Elige un sitio (opcional)</option>

								<option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>


							</select>
						</div>
					</div>
			

					<div class="row form-row" v-if=" contacts.length > 0 ">
						<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
							
							<span class="text-danger">*</span>
							
							Contacto:

						</label>
						<div class="form-group col-sm-4">
							<select name="contact_id" class="form-control" required>
								<option disabled selected value>Elige un contacto</option>
								<option v-for="contact in contacts" :value="contact.contact.user.id">@{{ contact.contact.user.first_name + ' ' + contact.contact.user.last_name }}</option>
							</select>
						</div>
					</div>

					@endrole
					
					<div class="row form-row mb-3" v-if="search_models">
						<div class="col-sm-9 ml-auto d-flex align-items-center">
							<i class="fa fa-spin fa-spinner mr-2"></i> 
							<span>Cargando Información</span>
						</div>
					</div>
					
					<div class="form-row" v-if="models.length > 0">
						<div class="col-sm-9 ml-auto">
							<p class="text-info">Elige un equipo relacionado a tu requerimiento en caso de ser necesario</p>
						</div>
					</div>

					<div class="row form-row" v-if="models.length > 0">
						<label for="" class="col-form-label col-sm-3 text-right">Modelo:</label>
						<div class="form-group col-sm-4">
							<select name="model_id" class="form-control" @change="get_serial_numbers">
								<option selected value>Elige un modelo</option>
								<option v-for="model in models" :value="model.id">@{{ model.name }}</option>
							</select>
						</div>
					</div>

					<div class="row form-row mb-3" v-if="search_serial_numbers">
						<div class="col-sm-9 ml-auto d-flex align-items-center">
							<i class="fa fa-spin fa-spinner fa-2x mr-2"></i> 
							<span>Cargando Equipos</span>
						</div>
					</div>

					<div class="row form-row" v-if="gears.length > 0">
						<label for="" class="col-form-label col-sm-3 text-right"><span class="text-danger">*</span> Equipo:</label>
						<div class="form-group col-sm-6">
							<select name="gear_id" class="form-control" required>
								<option selected value>Elige un equipo</option>
								<option v-for="gear in gears" :value="gear.id">@{{ gear.serial_number }}</option>
							</select>
						</div>
					</div>

				</div>
				
				<div class="row form-row">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						<span class="text-danger">*</span> Asunto:
					</label>
					<div class="form-group col-sm-6">
						<input type="text" name="subject" class="form-control" value="{{ old('subject') }}" autofocus required>
					</div>
				</div>

				<div class="row form-row">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						<span class="text-danger">*</span> Descripción:
					</label>
					<div class="form-group col-sm-6">
						<textarea name="description" class="form-control" rows="5" required>{{ old('description') }}</textarea>
					</div>
				</div>
				
				<div class="row form-row">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						<span class="text-danger">*</span> Severidad:
					</label>
					<div class="form-group col-sm-4">
						<select v-if="urgent_tickets" name="severity_id" class="form-control" value="{{ old('severity_id') }}" required>
							@foreach(App\Severity::get() as $severity)
								<option value="{{ $severity->id }}" {{ $severity->name == 'Normal' ? 'selected' : '' }}>{{ $severity->name }}</option>
							@endforeach
						</select>
						<select v-else name="severity_id" class="form-control" value="{{ old('severity_id') }}" required>
							@foreach(App\Severity::where('name', '<>', 'Urgente')->get() as $severity)
								<option value="{{ $severity->id }}" {{ $severity->name == 'Normal' ? 'selected' : '' }}>{{ $severity->name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="row form-row">
				@role('admin','manager','agent')
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						<span class="text-danger">*</span> Categoría 1:
					</label>
					<div class="form-group col-sm-4 mb-3">
						<select name="category_id" id="parent" class="form-control" @change="get_category_son('a')" required>
				@else
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						<span class="text-danger">*</span> Categoría:
					</label>
					<div class="form-group col-sm-4 mb-3">
						<select name="category_id" id="parent" class="form-control" required>
				@endrole
							<option disabled selected value>Elige una opción</option>
							@foreach(App\Category::whereNull('parent_id')->get() as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="row form-row" v-if="sub_categories_a.length > 0">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						Categoría 2:
					</label>
					<div class="form-group col-sm-4 mb-3" v-cloak>
						<select name="category_id" id="sub_category_a" class="form-control" @change="get_category_son('b')">
							<option disabled selected value>Elige una opción</option>
							<option v-for="category in sub_categories_a" :value="category.id">@{{ category.name }}</option>
						</select>
					</div>
				</div>

				<div class="row form-row" v-if="sub_categories_b.length > 0">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						Categoría 3:
					</label>
					<div class="form-group col-sm-4 mb-3" v-cloak>
						<select name="category_id" id="sub_category_b" class="form-control" @change="get_category_son('c')">
							<option disabled selected value>Elige una opción</option>
							<option v-for="category in sub_categories_b" :value="category.id">@{{ category.name }}</option>
						</select>
					</div>
				</div>

				<div class="row form-row" v-if="sub_categories_c.length > 0">
					<label for="" class="col-form-label col-sm-3 text-left text-sm-right">
						Categoría 4:
					</label>
					<div class="form-group col-sm-4 mb-3" v-cloak>
						<select name="category_id" id="sub_category_c" class="form-control">
							<option disabled selected value>Elige una opción</option>
							<option v-for="category in sub_categories_c" :value="category.id">@{{ category.name }}</option>
						</select>
					</div>
				</div>

				<div class="row form-row mb-3" v-if="search_categories">
					<div class="col-sm-9 ml-auto d-flex align-items-center">
						<i class="fa fa-spin fa-spinner mr-2"></i> 
						<span>Cargando Información</span>
					</div>
				</div>
			</form>

		</div>

		<div class="card-footer">
			<div class="row form-row">
				<div class="col-sm-9 ml-auto">
					<button type="submit" form="newTicketForm" class="btn btn-success"><i class="fa fa-paper-plane"></i> Enviar Ticket</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('scripts')
	
	<script src="{{ asset('js/vue.js') }}"></script>
	<script>
		var ticketApp = new Vue({

			el: '#tickets_app',

			data: {

				search_models     	  : false,
				search_categories 	  : false,
				contacts 		  	  : [],
				zones 			  	  : null,
				models 			  	  : [],
				no_models 		  	  : false,
				search_serial_numbers : false,
				gears 				  : [],
				sub_categories_a 	  : [],
				sub_categories_b      : [],
				sub_categories_c 	  : [],
				urgent_tickets 		  : false
			},

			methods: {

				get_company_gears_models: function(e){

					var t = this;

					var select = $(e.target);

					var company = select.val();

					var url = '{{ route('companies.gears.index', '#company_id') }}';

					url = url.replace('#company_id', company);

					t.search_models = true;

					t.no_models = false;

					t.models = [];

					t.gears = [];

					t.contacts = [];

					t.zones = null;

					$.get(url, function(response){

						t.contacts = response.contacts;

						t.zones = response.zones;

						t.models = response.models;

						t.urgent_tickets = response.urgent_tickets ? true : false;

						t.gears = [];

						t.search_models = false;

						if(t.models.length == 0){
							t.no_models = true;
						}

					});

				},



				get_zone_contacts: function(e){


					var t = this;

					var select = $(e.target);

					var zone = select.val();

					var route = "{{ route('get_zone_contacts', '#zone_id') }}";


					if(zone){


						route = route.replace('#zone_id', zone);

						
					}else{


						var master_company = $('#company_master_select').val();


						route = route.replace('#zone_id', master_company);


					}


					t.contacts = [];

					t.models = [];

					t.gears = [];

					t.urgent_tickets = false;


					$.getJSON(route, function(response){

						t.contacts = response.contacts;

						t.models = response.models;

						t.urgent_tickets = response.urgent_tickets ? true : false;

					});


				},


				get_serial_numbers: function(){

					var t = this;

					var select = $(event.prevent);

					var value = select.val();

					var company_id = $('[name="company_id"]').val();

					var zone_id = $('[name="zone_id"]').val();

					var model_id = $('[name="model_id"]').val();

					var url = '{{ route("get_gears_by_model", ["#company_id", "#model_id"]) }}';


					if(zone_id){

						url = url.replace('#company_id', zone_id).replace('#model_id', model_id);
						
					}else{

						url = url.replace('#company_id', company_id).replace('#model_id', model_id);

					}


					t.search_serial_numbers = true;


					if(model_id){

						$.get(url, function(response){

							t.gears = response;

							t.search_serial_numbers = false;

							select.val($("option:first", select).val());

						});

					}else{

						t.search_serial_numbers = false;

						t.gears = [];

					}


				},

				get_category_son: function(level){

					var t      = this
					var url    = '{{ route('category.subcategory.index', ['category' => '#category']) }}'
					var parent = null

					
					if (level == 'a') {
						parent = $('#parent').val()

						t.sub_categories_a = []
						t.sub_categories_b = []

						$('#sub_category_a').val('')
						$('#sub_category_b').val('')
						$('#sub_category_c').val('')

						url = url.replace('#category', parent)

						t.search_categories = true;
					} else if ( level == 'b') {
						parent = $('#sub_category_a').val()

						t.sub_categories_c = []
						
						$('#sub_category_b').val('')
						$('#sub_category_c').val('')

						url = url.replace('#category', parent)

						t.search_categories = true;
					} else if ( level == 'c') {
						parent = $('#sub_category_b').val()
						
						$('#sub_category_c').val('')

						url = url.replace('#category', parent)
					}
					
					$.get(url, function(response){
						t.search_categories = false;
						if (level == 'a') {
							t.sub_categories_a = response
						} else if ( level == 'b') {
							t.sub_categories_b = response
						} else if ( level == 'c') {
							t.sub_categories_c = response
						}
					})
				}

			}

		});
	</script>

@stop