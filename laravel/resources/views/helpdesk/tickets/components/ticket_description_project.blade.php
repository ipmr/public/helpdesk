<form action="{{ route('assign_to_project', ['ticket' => $ticket->ticket_id]) }}" id="assignProject" method="post" style="width: 100%;">
    {{ csrf_field() }}
    <div class="form-group m-0">
        <select class="form-control" name="project_id"  @change="active_success_btn" style="border-radius: 3px 0 0 3px" required>
            <option value="">
                Selecciona un proyecto
            </option>
            @foreach (App\CompanyProject::where('company_id', $ticket->company_id)->get() as $company_project)
            <option value="{{ optional($company_project->project)->id }}" @isset ($ticket->project_ticket)
                {{ ($ticket->project_ticket->project->id == $company_project->project_id)?'selected':'' }}
                @endisset>
                {{ optional($company_project->project)->fup }} | {{ str_limit(optional($company_project->project)->title, 30) }}
            </option>
            @endforeach
        </select>
    </div>
</form>
<button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Proyecto" form="assignProject" class="btn btn-success">
    <i class="fa fa-check" style="margin: 0 !important;">
    </i>
</button>