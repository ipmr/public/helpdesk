<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name'    => $faker->company,
        'address' => $faker->address,
        'phone'   => $faker->tollFreePhoneNumber,
        'rfc'     => strtoupper(str_random(12)),
        'city'    => $faker->city,
        'state'   => $faker->state,
        'zip'     => rand(22000, 22200),
    ];
});
