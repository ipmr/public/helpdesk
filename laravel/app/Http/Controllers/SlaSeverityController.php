<?php

namespace App\Http\Controllers;

use App\SlaSeverity;
use Illuminate\Http\Request;

class SlaSeverityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SlaSeverity  $slaSeverity
     * @return \Illuminate\Http\Response
     */
    public function show(SlaSeverity $slaSeverity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SlaSeverity  $slaSeverity
     * @return \Illuminate\Http\Response
     */
    public function edit(SlaSeverity $slaSeverity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SlaSeverity  $slaSeverity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlaSeverity $slaSeverity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SlaSeverity  $slaSeverity
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlaSeverity $slaSeverity)
    {
        //
    }
}
