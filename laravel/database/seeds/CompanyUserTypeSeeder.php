<?php
use App\CompanyUserType;
use Illuminate\Database\Seeder;

class CompanyUserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CompanyUserType::class, 1)->create([
            'name' => 'Contacto Principal',
        ]);

        factory(CompanyUserType::class, 1)->create([
            'name' => 'Gerencial',
        ]);

        factory(CompanyUserType::class, 1)->create([
            'name' => 'Facturación',
        ]);

        factory(CompanyUserType::class, 1)->create([
            'name' => 'Mantenimiento',
        ]);

        factory(CompanyUserType::class, 1)->create([
            'name' => 'Operativo',
        ]);

        factory(CompanyUserType::class, 1)->create([
            'name' => 'Técnico',
        ]);
    }
}
