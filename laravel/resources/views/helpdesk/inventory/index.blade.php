@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <p class="lead">
                    Listado de almacenes
                </p>
                <form action="{{ route('inventories.index') }}" method="get" enctype="multipart/form-data" class="form-inline ml-auto">
                    <input class="form-control mr-sm-2" type="search" name="q" placeholder="Buscar..." aria-label="Buscar..." @isset ($_GET['q'])
                        value="{{ $_GET['q'] }}"
                    @endisset>
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </form>
                <a class="btn btn-primary ml-2" href="{{ route('inventories.create') }}">
                    <i class="fa fa-plus"></i>
                    Nuevo Almacen
                </a>
            </div>
            @if(count($inventories) > 0)
                <table class="table table-striped m-0">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Datos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($inventories as $inventory)
                        <tr>
                            <td>
                                <a href="{{ route('inventories.show', $inventory) }}">
                                    {{ $inventory->name }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('inventories.stock.index', $inventory) }}">
                                    {{ $inventory->stocks()->count() }} Equipos
                                </a>
                            </td>   
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="card-footer">
                    {{ $inventories->links() }}
                </div>
            @else
                <div class="card-body">No se han agregado inventarios</div>
            @endif
        </div>
    </div>
@stop
