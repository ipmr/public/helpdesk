<div class="card mt-3" id="tabs">
	<div class="card-header">
		
		@include('helpdesk.tickets.html.tabs_list')

	</div>
	<div class="card-body">

		<div class="tab-content" id="ticketTabs">
			
			@include('helpdesk.tickets.html.tab_comment')
			
			@include('helpdesk.tickets.html.tab_tasks')
			
			@include('helpdesk.tickets.html.tab_history')
			
		</div>
	</div>
</div>