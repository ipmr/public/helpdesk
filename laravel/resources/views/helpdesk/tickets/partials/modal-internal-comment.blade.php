@component('helpdesk.components.modal', [
	'id' 	=> 'new_internal_comment',
	'title' => 'Nuevo comentario interno'
])

	@slot('body')
		<form action="{{ route('internal-comment', $ticket->ticket_id) }}" method="post" id="new_internal_comment_form" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group">
				<textarea name="comment" class="form-control" placeholder="Escribe tu comentario..." required></textarea>
			</div>
			<div class="form-group m-0">
				<label for="">Adjuntar imágenes</label> <br>
				<input type="file" name="files[]" multiple>
			</div>
		</form>
	@endslot
	@slot('footer')
		<button type="submit" form="new_internal_comment_form" class="btn btn-success">
			<i class="fa fa-paper-plane"></i>
			Enviar comentario
		</button>
	@endslot
@endcomponent