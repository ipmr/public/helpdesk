@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-link p-0" href="{{ route('ticket-options.index') }}/">
                        <i class="fa fa-angle-left fa-sm mr-1"></i>
                        Regresar
                    </a>
                </div>
                <div class="card-body">
                    <form action="{{ route('severity.update', $severity) }}" method="post" id="newSeverityForm">
                        
                        {{ method_field('PATCH') }}

                        @include('helpdesk.severity.partials.form', ['btn'=>'Actualizar'])

                    </form>
                </div>
                @if(!$severity->static)
                <div class="card-footer">
                    <div class="row form-row">
                        <div class="col-sm-8 ml-auto">
                            <button class="btn btn-success"><i class="fa fa-check"></i> Actualizar Severidad</button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@stop