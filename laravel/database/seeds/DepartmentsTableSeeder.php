<?php

use Illuminate\Database\Seeder;

use App\Department;
use App\User;
use App\Agent;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Department::class, 1)->create([

        //     'name' => 'Desarrollo Web'

        // ])->each(function ($department) {

        //     factory(User::class, 1)->create([

        //         'name' => 'Ingeniero Uno',
        //         'email' => 'agent@helpdesk.com',
        //         'role' => 'agent',
        //         'username' => 'ingeniero.uno'

        //     ])->each(function ($user) use ($department) {

        //         factory(Agent::class, 1)->create([

        //             'user_id'       => $user->id,
        //             'department_id' => $department->id,
        //             'jobtitle' => 'Desarrollador Web'

        //         ]);

        //     });

        //     $supervisor = $department->agents[0];

        //     $department->update([
        //         'supervisor_id' => $supervisor->id
        //     ]);

        // });

        // Departmanetos random

        factory(Department::class, 7)->create()->each(function ($department) {

            factory(User::class, 5)->create([

                'role' => 'agent'

            ])->each(function ($user) use ($department) {

                factory(Agent::class, 1)->create([

                    'user_id'       => $user->id,
                    'department_id' => $department->id,

                ]);

            });

            $supervisor = $department->agents[rand(0,4)];

            $department->update([
                'supervisor_id' => $supervisor->id
            ]);

        });
    }
}
