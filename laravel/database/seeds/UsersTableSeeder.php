<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(User::class, 1)->create([

            'first_name' => 'Jonathan',
            'last_name'  => 'Velazquez',
            'email'      => 'admin@helpdesk.com',
            'role'       => 'admin',
            'username'   => 'admin',
            'password'   => \Hash::make( 'secret' )

        ]);

        // factory(User::class, 1)->create([

        //     'first_name' => 'Manager',
        //     'last_name'  => 'Manager',
        //     'email'      => 'manager@helpdesk.com',
        //     'role'       => 'manager',
        //     'username'   => 'manager',

        // ]);

    }
}
