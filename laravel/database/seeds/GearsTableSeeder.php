<?php

use App\GearBrand;
use App\GearModel;
use App\BrandType;
use App\GearType;
use Illuminate\Database\Seeder;

class GearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(GearType::class, 9)->create();
        factory(GearBrand::class, 5)->create()->each(function($brand){

            factory(BrandType::class, 3)->create([

                'brand_id' => $brand->id,

            ])->each(function($item) use ($brand) {

                $item->update([

                    'type_id' => rand(1,9),

                ]);

                factory(GearModel::class, 10)->create([
                    'brand_type_id' => $item->id
                ]);

            });

        });

    }
}
