@extends('layouts.master')

@section('css')

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')
<div class="container-fluid" id="reports">

    
    <div v-cloak class="form-row d-flex align-items-stretch" style="height: calc(100vh - 100px) !important; overflow: hidden;">
        
        <div class="col-sm-3">
            
            <form action="{{ route('get_report') }}" method="post" enctype="multipart/form-data" submit="false" id="form_report">
                
                {{ csrf_field() }}

                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <p class="lead">Filtros</p>
                        <button class="btn btn-success ml-auto" @click.prevent="get_report" id="load_data" data-btn="generate_report">
                            <i class="fa fa-check"></i> Generar
                        </button>
                    </div>
                    <div class="card-body" style="overflow: hidden; overflow-y: auto; height: calc(100vh - 158px)">

                            <div class="form-group">
                                
                                <div class="d-flex align-items-center mb-2">
                                    
                                    <label class="m-0" for="">Periodo</label>

                                    <a href="#adjustRange" class="ml-auto" data-toggle="collapse">Ajustar periodo manualmente</a>
                                    
                                </div>

                                <select name="report_range" class="form-control">
                                    
                                    <option disabled selected value="">
                                        Elige una opción
                                    </option>

                                    <option value="weekly">
                                        Reporte Semanal
                                    </option>

                                    <option value="monthly">
                                        Reporte Mensual
                                    </option>

                                    <option value="quarterly">
                                        Reporte Trimestral
                                    </option>

                                </select>

                            </div>         

                            <div id="adjustRange" class="collapse">
                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="form-group mb-0" v-show="report_type=='Tickets'">
                                            <label class="d-block">Fecha inicio</label>
                                            <input type="text" name="start_date" id="start_date" class="form-control datepicker" value="{{ date('Y-m-d', strtotime('-15 day')) }}" placeholder="0000/00/00" required></input>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group mb-0" v-show="report_type=='Tickets'">
                                            <label class="d-block">Fecha fin</label>
                                            <input type="text" name="end_date" id="end_date" class="form-control datepicker" value="{{ date('Y-m-d') }}" placeholder="0000/00/00" required></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                            <hr>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                <label>Status</label>
                                <select data-selectjs name="status[]" id="status" class="form-control" multiple="multiple">
                                    @foreach(App\Status::select('id', 'name')->get() as $status)
                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                <label>Severidad</label>
                                <select data-selectjs name="severity[]" id="severity" class="form-control" multiple="multiple">
                                    @foreach(App\Severity::select('id', 'name')->get() as $severity)
                                        <option value="{{ $severity->id }}">{{ $severity->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                <label>Categoría</label>
                                <select data-selectjs name="category[]" id="category" class="form-control" multiple="multiple">
                                    @foreach(App\Category::select('id', 'name')->get() as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                <label>Departamentos</label>
                                <select data-selectjs name="department[]" id="department" class="form-control" multiple="multiple">
                                    @foreach(App\Department::get() as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                @php
                                $agents = App\Agent::select('id', 'user_id')->with('user:id,first_name,last_name')->get();
                                @endphp
                                <label>Agente</label>
                                <select data-selectjs name="agent[]" id="agent" class="form-control" multiple="multiple">
                                    @foreach($agents as $agent)
                                    <option value="{{ $agent->id }}">{{ $agent->user->last_name .' '. $agent->user->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                @php
                                    $user = auth()->user();

                                    if ($user->role != 'company' ) {

                                        $companies = App\Company::select(['id','name'])->get();

                                    } else {
                                        $ids = [];
                                        foreach ($user->zones as $zone) {
                                            
                                            $ids[$zone->company->id] = $zone->company->id;
                                            #se obtendran las companias de las zonas
                                            if (optional($zone->company)->id) {
                                                #se obtendran los hijos de la compania
                                                if ($zone->role == 'Contacto Principal') {

                                                    $company_childs = $zone->company->all_childs();

                                                } else {

                                                    $company_childs = $zone->company->childs;

                                                }

                                                if (count($company_childs) > 0) {
                                                    
                                                    foreach ($company_childs as $company_child) {

                                                        $ids[$company_child->id] = $company_child->id;
                                                    }

                                                }
                                            }
                                        }
                                        $companies = App\Company::select(['id','name'])->whereIn('id',$ids)->get();
                                    }
                                @endphp
                                <label>Compañia</label>
                                <select data-selectjs name="company[]" id="company" class="form-control" multiple="multiple" @change="change_company">
                                    @foreach($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Marcas</label>
                                <select data-selectjs name="brand[]" id="brand" class="form-control" multiple="multiple">
                                    <option v-for="brand in select_brands" :value="brand.id">@{{ brand.name }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tipos</label>
                                <select data-selectjs name="type[]" id="type" class="form-control" multiple="multiple">
                                    <option v-for="type in select_types" :value="type.id">@{{ type.name }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Modelos</label>
                                <select data-selectjs name="model[]" id="model" class="form-control" multiple="multiple">
                                    <option v-for="model in select_models" :value="model.id">@{{ model.name }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Estado del equipo</label>
                                <select data-selectjs name="gear_status[]" id="gear_status" class="form-control" multiple="multiple">
                                    <option value="Funciona">Funciona</option>
                                    <option value="No Funciona">No Funciona</option>
                                    <option value="Tiene Fallas">Tiene Fallas</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Equipos</label>
                                <select data-selectjs name="gear[]" id="gear" class="form-control" multiple="multiple">
                                    <option v-for="gear in select_gears" :value="gear.id">@{{ gear.serial_number }}</option>
                                </select>
                            </div>

                            <div class="form-group" v-show="report_type=='Tickets'">
                                <label>Contactos</label>
                                <select data-selectjs name="contact[]" id="contact" class="form-control" multiple="multiple">
                                    <option v-for="contact in select_contacts" :value="contact.id">@{{ contact.first_name+' '+contact.last_name }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Contratos</label>
                                <select data-selectjs name="contract[]" id="contract" class="form-control" multiple="multiple">
                                    <option v-for="contract in select_contracts" :value="contract.id">@{{ contract.name }}</option>
                                </select>
                            </div>

                            <div class="form-group mb-0">
                                <label>Coberturas</label>
                                <select data-selectjs name="coverage[]" id="coverage" class="form-control" multiple="multiple">
                                    <option v-for="coverage in select_coverages" :value="coverage.id">@{{ coverage.name }}</option>
                                </select>
                            </div>
                    </div>
                </div>

            </form>

        </div>

        <div class="col-sm-9">
            <div class="card h-100">
                
                <div class="card-header d-flex align-items-center">
                    <p class="lead">Reporte</p>
                    
                    <div class="ml-auto d-flex align-items-center">
                        <a href="#" class="export_btn mx-2" @click="export_report('pdf')">
                            <i class="fa fa-file-pdf"></i>
                        </a>

                        <a href="#" class="export_btn" @click="export_report('excel')">
                            <i class="fa fa-file-excel"></i>
                        </a>

                    </div>
                </div>

                <div class="card-body" style="overflow: hidden; overflow-y: auto; height: calc(100vh - 158px)">
                    
                    <div v-if="report_ready">

                        <p v-if="loading_report" class="mt-5 text-center">
                            
                            <i class="fa fa-spin fa-spinner fa-lg mr-2"></i>
                            Generando Reporte (Esto puede tardar algunos segundos).

                        </p>

                        <div v-if="!loading_report">
                            
                            {{-- <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tickets-tab" data-toggle="tab" href="#tickets" role="tab" aria-controls="tickets" aria-selected="true">
                                        Tickets
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="gears-tab" data-toggle="tab" href="#gears" role="tab" aria-controls="gears" aria-selected="false">
                                        Equipos
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"></a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="tickets" role="tabpanel" aria-labelledby="tickets-tab">
                                    <div class="table-responsive">
                                        <table class="table m-0 table-striped data_table">
                                            <thead>
                                                <tr>
                                                    <th>#Ticket</th>
                                                    <th>Asunto</th>
                                                    <th>Severidad</th>
                                                    <th>Estado</th>
                                                    <th>Categoría</th>
                                                    <th>Departamento</th>
                                                    <th>Agente</th>
                                                    <th>Compañia</th>
                                                    <th>Equipo</th>
                                                    <th>Marca</th>
                                                    <th>Modelo</th>
                                                    <th>Tipo</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="ticket in tickets">
                                                    <td>#@{{ ticket.ticket_id }}</td>
                                                    <td>@{{ ticket.subject }}</td>
                                                    <td>
                                                        <span :style="{ color: ticket.severity_color }">
                                                            @{{ ticket.severity }}
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span :style="{ color: ticket.status_color }">
                                                            @{{ ticket.status }}        
                                                        </span>
                                                    </td>
                                                    <td>@{{ ticket.category }}</td>
                                                    <td>@{{ ticket.department }}</td>
                                                    <td>@{{ ticket.first_name +' '+ticket.last_name }}</td>
                                                    <td>@{{ ticket.company }}</td>
                                                    <td>@{{ (ticket.gear)?ticket.gear:'No asignado' }}</td>
                                                    <td>@{{ (ticket.gear)?ticket.brand:'No asignado' }}</td>
                                                    <td>@{{ (ticket.gear)?ticket.model:'No asignado' }}</td>
                                                    <td>@{{ (ticket.gear)?ticket.type:'No asignado' }}</td>
                                                    <td>@{{ ticket.created_at }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="gears" role="tabpanel" aria-labelledby="gears-tab">
                                    <div class="table-responsive">
                                        <table class="table m-0 table-striped data_table">
                                            <thead>
                                                <tr>
                                                    <th>Marca</th>
                                                    <th>Modelo</th>
                                                    <th>Tipo</th>
                                                    <th># Serie</th>
                                                    <th>Módulos</th>
                                                    <th>Contrato</th>
                                                    <th>Covertura</th>
                                                    <th>Estatus</th>
                                                    <th>Sitio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="gear in gears">
                                                    <td>@{{ gear.brand }}</td>
                                                    <td>@{{ gear.model }}</td>
                                                    <td>@{{ gear.type }}</td>
                                                    <td>@{{ gear.serial_number }}</td>
                                                    <td>@{{ gear.modules.length }}</td>
                                                    <td>@{{ (gear.contract)?gear.contract:'Sin Asignar' }}</td>
                                                    <td>@{{ (gear.coverage)?gear.coverage:'Sin Asignar' }}</td>
                                                    <td>@{{ gear.status }}</td>
                                                    <td>@{{ gear.company }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                            </div> --}}

                            
                            {{-- <table class="table m-0 table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#Ticket</th>
                                        <th>Asunto</th>
                                        <th>Severidad</th>
                                        <th>Estado</th>
                                        <th>Categoría</th>
                                        <th>Departamento</th>
                                        <th>Agente</th>
                                        <th>Compañia</th>
                                        <th>Fecha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="ticket in tickets">
                                        <td>#@{{ ticket.ticket_id }}</td>
                                        <td>@{{ ticket.subject }}</td>
                                        <td>
                                            <span :style="{ color: ticket.severity_color }">
                                                @{{ ticket.severity }}
                                            </span>
                                        </td>
                                        <td>
                                            <span :style="{ color: ticket.status_color }">
                                                @{{ ticket.status }}        
                                            </span>
                                        </td>
                                        <td>@{{ ticket.category }}</td>
                                        <td>@{{ ticket.department }}</td>
                                        <td>@{{ ticket.first_name +' '+ticket.last_name }}</td>
                                        <td>@{{ ticket.company }}</td>
                                        <td>@{{ ticket.created_at }}</td>
                                    </tr>
                                </tbody>
                            </table> --}}


                            <label><b>Tiempos de atención al cliente:</b></label>

                            <div class="form-row mb-4">
                                <div class="col-sm-4">
                                    
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-primary"><b>Atención Remota</b></p>
                                            <h2>@{{ complements.total_remote_time ? complements.total_remote_time : '0 mins'  }}</h2>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-danger"><b>Atención En Sitio</b></p>
                                            <h2>@{{ complements.total_site_time ? complements.total_site_time : '0 mins'  }}</h2>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-success"><b>Total de tiempo</b></p>
                                            <h2>@{{ complements.total_service_time ? complements.total_service_time : '0 mins'  }}</h2>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div style="padding: 1.5rem 1rem; margin: 0 -15px; background: #f8f9fa; border-top: 1px solid #eeeeee;">  
                                <label><b>Tickets por status:</b></label>

                                <div class="card-group m-0">
                                    <div v-for="(status, index) in complements.status" class="card w-100">
                                        <div class="card-body">
                                            <p class="m-0" :style="{color: status.color}">
                                                <b>(@{{ status.total_tickets_status }}) Tickets</b>
                                            </p>
                                            <small class="m-0">
                                                <b>@{{ index }}</b>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="padding: 1.5rem 1rem; margin: 0 -15px; border-top: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;" class="mb-4 bg-secondary">
                                <label><b>Tickets por severidad:</b></label>

                                <div class="card-group m-0">
                                    <div v-for="(severity, index) in complements.severity" class="card w-100">
                                        <div class="card-body">
                                            <p class="m-0" :style="{color: severity.color}">
                                                <b>(@{{ severity.total_tickets_severity }}) Tickets</b>
                                            </p>
                                            <small class="m-0">
                                                <b>@{{ index }}</b>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <label><b>(@{{ tickets.length }}) Tickets</b></label>
            

                            <ul class="list-group">
                                <li v-for="ticket in tickets" class="list-group-item">
                                    <small class="d-flex mb-2">
                                        <a :href="ticket_route(ticket.ticket_id)">
                                            #@{{ ticket.ticket_id }}
                                        </a>
                                        <span class="mx-3">|</span>
                                        <span>@{{ ticket.created_at }}</span>
                                    </small>
                                    <p class="mb-0">@{{ ticket.subject }}</p>
                                </li>
                            </ul>



                        </div>

                    </div>

                    <div v-else class="text-center mt-5">
                        
                        <h2 class="text-muted mb-5">No se ha generado un reporte aún</h2>

                        <div class="row">
                            <div class="col-sm-2 mx-auto">
                                <img src="{{ asset('img/iconos/REPORTES.svg') }}" class="img-fluid svg_illustration" alt="">
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
@endsection

@section('scripts')

    <script src="{{ asset('js/select2.min.js') }}"></script>

    <script type="text/javascript">
        var reports = new Vue({
            el: '#reports',
            data: {
                loading_report   : null,
                report_ready: null,
                report_type      : 'Tickets',
                select_gears     : @json(App\Gear::select('id','serial_number')->get()),
                select_brands    : @json(App\GearBrand::select('id','name')->get()),
                select_models    : @json(App\GearModel::select('id','name')->get()),
                select_types     : @json(App\GearType::select('id','name')->get()),
                select_contracts : @json(App\Contract::select('id','name')->get()),
                select_contacts  : @json(App\User::where('role','company')->get()),
                select_coverages : @json(App\Coverage::select('id','name')->get()),
                tickets   : [],
                complements: [],
                params    : [],
                gears     : [],
            },
            mounted(){
                var t = this
                t.load_plugin()

                $('.nav-link').click(function(){
                    t.report_type = $(this).text().trim()
                });
            },
            methods: {
                ticket_route: function(ticket_id){

                    var t = this;

                    var route = '{{ route('tickets.show', '#ticket_id') }}';

                    route = route.replace('#ticket_id', ticket_id);

                    return route;

                },
                load_plugin:function(){
                    $('select[data-selectjs]').select2({
                        placeholder: 'Seleccionar opción'
                    });
                },
                change_company: function(){

                    this.get_company_contacts()
                    this.get_company_contracts()
                    this.get_company_gears()
                    this.get_company_coverages()
                },
                get_company_contacts: function(){
                    var t = this
                    var company_id = $('#company_id').val()

                    var url = '{{ route('get_company_contacts', ['company' => '#company']) }}'
                    url = url.replace('#company', company_id)

                    t.$http.get(url).then(function(response){
                        
                        t.select_contacts = response.body
                    });
                },
                get_company_contracts: function(){
                    var t = this
                    var company_id = $('#company_id').val()

                    var url = '{{ route('get_company_contracts', ['company' => '#company']) }}'
                    url = url.replace('#company', company_id)

                    t.$http.get(url).then(function(response){
                        
                        t.select_contracts = response.body
                    });
                },
                get_company_gears: function(){
                    var t = this
                    var company_id = $('#company_id').val()

                    var url = '{{ route('get_company_gears', ['company' => '#company']) }}'
                    url = url.replace('#company', company_id)

                    t.$http.get(url).then(function(response){
                        
                        t.select_gears = response.body
                    });
                },
                get_company_coverages: function(){
                    var t = this
                    var company_id = $('#company_id').val()

                    var url = '{{ route('get_company_coverages', ['company' => '#company']) }}'
                    url = url.replace('#company', company_id)

                    t.$http.get(url).then(function(response){
                        
                        t.select_coverages = response.body;

                    });
                },
                get_report: function(){

                    var t    = this;
                    var url  = '{{ route('get_report') }}';
                    var data = $('form').serializeArray();

                    
                    var btn = $('[data-btn="generate_report"]');

                    btn.attr('disabled', 'disabled');


                    data.push({
                        name : 'report_type',
                        value: t.report_type, 
                    });

                    t.report_ready = true;

                    t.loading_report = true;


                    $.ajax({
                        type     : 'post',
                        url      : url,
                        data     : data,
                        dataType :'json'
                    }).done(function( response ){

                        btn.removeAttr('disabled');

                        t.loading_report = null;

                        if(t.report_type == 'Tickets'){

                            t.tickets     = response.tickets;
                            t.complements = response.complements;
                            t.params      = response.params;

                        } else if( t.report_type == 'Equipos'){

                            t.gears = response

                        }

                    });
                },
                export_report: function(format){
                    var url = '{{ route('get_report') }}'
                    url += '?report_type=Tickets&format_file='+format

                    $('#form_report').attr( 'action', url )

                    $('#form_report').submit()
                }
            }
        })
    </script>

@endsection