<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{
    public function read($notification){

    	$user = auth()->user();

    	$notification = $user->notifications()->find($notification);

        $notification->markAsRead();

    }

    public function index(){

    	$user = auth()->user();

    	$notifications = $user->notifications;
    	
    	return view('helpdesk.notifications.index', compact('notifications'));

    }

    public function list_archive()
    {
        $notifications = DatabaseNotification::onlyTrashed()->where('notifiable_id', auth()->user()->id )
            ->paginate(12);

        return view('helpdesk.notifications.archive', compact('notifications'));
    }


    public function destroy(Request $request)
    {
        if (isset($request->notifications)) {
            foreach ($request->notifications as $data_notification) {
                $notification = DatabaseNotification::where('id', $data_notification['value'])->first();

                $notification->delete();
            }
        }

        if (\Request::ajax()) {
            $result = [
                'redirect' => redirect()->back()->getTargetUrl(),
            ];

            return response()
                ->json($result);
        }

        return redirect()
            ->back()
            ->with('msg', count( $request->notifications ).' Notificaciones archivadas');
    }

}
