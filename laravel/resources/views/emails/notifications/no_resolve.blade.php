@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => url('/')])
            Tickets con SLA sin resolver 
        @endcomponent
    @endslot

    <table>
    @foreach ($tickets as $ticket)
        <tr>
            <td><h3 style="font-size: 20px; margin: 20px 0 20px 0 !important;">{!! '#'.$ticket->ticket_id !!}</h3></td>
        </tr>
        <tr>
            <td>
                {!! '<strong>'.str_limit($ticket->subject,140).'</strong>' !!}
            </td>
        </tr>
        <tr>
            <td>
                {!! str_limit($ticket->description,140) !!}
            </td>
        </tr>
        <tr>
            <td>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <a href="{{ route('tickets.show', ['ticket' => $ticket->ticket_id ]) }}" class="button button-blue">
                    <b>{!! 'Ver ticket' !!}</b>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <hr>
            </td>
        </tr>
    @endforeach
        <tr>
            <td>Te pedimos revises la información de manera prioritaria y corrobores los datos, esto para brindar un mejor servicio.</td>
        </tr>
    </table>

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y-m-d') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent