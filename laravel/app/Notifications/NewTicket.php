<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewTicket extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {

        $this->ticket = $request['ticket'];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        /*return (new MailMessage)
                    ->subject('Nuevo Ticket')
                    ->greeting('Ticket #' . $this->ticket->ticket_id)
                    ->line('Se ha recibido un nuevo ticket de ' . $this->ticket->company->name . ' con severidad ' . $this->ticket->get_severity->name)
                    ->action('Atender Ticket', route('tickets.show', $this->ticket->ticket_id));*/
        return (new MailMessage)
                    ->subject('Nuevo Ticket - #'.$this->ticket->ticket_id.' - '.str_limit($this->ticket->subject,40))
                    ->greeting("#{$this->ticket->ticket_id} - {$this->ticket->subject}")
                    ->line('"'.$this->ticket->description.'"')
                    ->action('Atender Ticket', route('tickets.show', $this->ticket->ticket_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg' => 'Se ha recibido un nuevo ticket de ' . $this->ticket->company->name . ' con severidad ' . $this->ticket->get_severity->name,
            'ticket' => $this->ticket
        ];
    }
}
