<script>
	
	var gearApp = new Vue({

		el: '#companyApp',
		data: {
			serial_numbers: @json($company->gears()->where('serial_number', '<>', $gear->serial_number)->pluck('serial_number')),
			gear_in_module: false,
			create_new_gear: false,
			brands: @json(App\GearBrand::get()),
			types: null,
			models: null,
			edit_module: null,
			edit_module_add_gear: false
		},
		mounted: function(){
			var t = this;
			var serial_numbers_input = $('.serial_numbers_input');	

			t.get_serial_numbers(serial_numbers_input);


			t.drag_drop_gear();

		},
		methods:{

			get_serial_numbers: function(el){

				var t = this;

			    el.autocomplete({
			      	source: t.serial_numbers
			    });

			},

			scroll_to_modules: function(){

				var t = this;
				var modules = $('#modules');

				setTimeout(function(){

					$('html, body').animate({scrollTop: modules.offset().top - 72});
					$('[name="slot"]').focus();

				}, 300);

			},

			add_gear_to_module: function(e){

				var t = this;

				var checkbox = $(e.target);

				var checked = checkbox.is(':checked');

				if(checked){

					t.gear_in_module = true;

					setTimeout(function(){

						var serial_numbers_input = $( ".serial_numbers_input" );

						t.get_serial_numbers(serial_numbers_input);

						var module_form = $('#gearInModule');

						$('html, body').animate({scrollTop: module_form.offset().top - 72});

						$('[name="existing_serial_number"]').focus();

					}, 300);

				}else{

					t.gear_in_module = false;

					t.create_new_gear = false;

					t.types = null;

					t.models = null;

				}

			},

			add_gear_to_module_in_modal: function(e){

				var t = this;

				var checkbox = $(e.target);

				var is_checked = checkbox.is(":checked");

				if(is_checked){
					t.edit_module_add_gear = true;
					setTimeout(function(){
						var serial_numbers_input = $( ".serial_numbers_input_in_modal" );

						t.get_serial_numbers(serial_numbers_input);
					}, 150);
				}else{
					t.edit_module_add_gear = false;
				}

			},

			go_to_create_new_gear: function(e){

				var t = this;

				t.create_new_gear = true;

				var serial_number_input = $('[name="serial_number"]');

				serial_number_input.attr('disabled', 'disabled');

				setTimeout(function(){

					var create_new_gear = $('#createNewGear');

					$('html, body').animate({scrollTop: create_new_gear.offset().top - 72});

				}, 150);

			},

			cancel_new_gear_creation: function(){

				var t = this;

				t.create_new_gear = false;

				$('[name="serial_number"]').removeAttr('disabled');

				setTimeout(function(){

					var serial_numbers_input = $('.serial_numbers_input');

					t.get_serial_numbers(serial_numbers_input);

					t.types = null;

					t.models = null;

				}, 150);

			},

			get_types: function(e){

				var t = this;
				var select = $(e.target);
				var value = select.val();
				var types_select = $('#types_select');
				var url = '{{ route("brands.types.index", "#brand_id") }}';
				url = url.replace('#brand_id', value);

				t.models = null;
				types_select.val($('option:first', types_select).val());

				t.$http.get(url).then(function(response){
					t.types = response.body;
				});

			},

			get_models: function(e){
				
				var t = this;
				var brand = $('#brands_select').val();
				var select = $(e.target);
				var value = select.val();
				var models_select = $('#models_select');
				var url = '{{ route('brands.types.models.index', ['#brand_id', '#type_id']) }}';

				url = url.replace('#brand_id', brand).replace('#type_id', value);

				models_select.val($('option:first', models_select).val());

				t.$http.get(url).then(function(response){

					t.models = response.body;

				});

			},

			@include('helpdesk.gears.components.scripts.drag-and-drop')

		}

	});

</script>