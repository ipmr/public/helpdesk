<?php

namespace App\Http\Controllers;

use App\Contract;
use Illuminate\Http\Request;
use App\Notifications\ChangeTicketStatus;
use Illuminate\Validation\Rule;
use Validator;
use Notification;

class ContractController extends Controller
{

    public function __construct(){


        $this->middleware('role:admin,manager');


    }


    public function index(Request $request)
    {

        $contracts = Contract::where('id', '>', 0);

        if (isset($request->q)) {
            $query_string = explode(' ', $request->q );

            $contracts = $contracts->where(function ($q) use ($query_string, $request) {
                $q = $q->where('name', 'like', '%'.$request->q.'%')
                    ->orWhere('description', 'like', '%'.$request->q.'%')
                    ->orWhere('period_number', 'like', '%'.$request->q.'%')
                    ->orWhere('period_format', 'like', '%'.$request->q.'%');
                    
                foreach ($query_string as $search) {
                    $q = $q->orWhere('name', 'like', '%'.$search.'%')
                    ->orWhere('description', 'like', '%'.$search.'%')
                    ->orWhere('period_number', 'like', '%'.$search.'%')
                    ->orWhere('period_format', 'like', '%'.$search.'%');
                }

                return $q;
            });
        }

        if (isset($request->debug)) {
            if ($request->debug == 0) {
                dd($query_string);
            }

            if ($request->debug == 1) {
                dd( $contracts->toSql() );
            }

            if ($request->debug == 2) {
                dd( $contracts->get() );
            }
        }

        $contracts = $contracts->orderBy('id', 'DESC')->get();

        return view('helpdesk.contracts.index', compact('contracts'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        #se valida la informacion del nuevo contrato
        $validate = $this->validate_contract_data($request);

        #se crea el nuevo contrato
        $contract = $this->create_contract($request);

        #se retorna con mensaje de confirmación
        return redirect()->back()->with("msg", "Se ha creado el contrato '{$contract->name}' exitosamente.");

    }

    public function validate_contract_data($request, $contract = null)
    {

        $validate = Validator::make($request->all(), [

            'name'          => ['required', 'string', Rule::unique('contracts')->ignore($contract ? $contract->id : '')],
            'description'   => 'string|nullable',
            'period_number' => 'required|numeric',
            'period_format' => 'required|in:d,w,m,y',

        ], [

            'name.required'          => 'El nombre del contrato es requerido',
            'name.unique'            => 'El nombre del contrato no esta disponible',
            'period_number.required' => 'El número de vigencia es requerido',
            'period_number.numeric'  => 'La vigencia debe ser un número',
            'period_format.required' => 'El formato de vigencia es requerido',
            'period_format.in'       => 'El formato de vigencia no es válido',

        ])->validate();

    }

    public function create_contract($request)
    {

        $contract                = new Contract();
        $contract->name          = $request->name;
        $contract->description   = $request->description;
        $contract->period_number = $request->period_number;
        $contract->period_format = $request->period_format;
        $contract->save();

        return $contract;

    }

    public function show(Contract $contract)
    {
        return view('helpdesk.contracts.show', compact('contract'));
    }

    public function edit(Contract $contract)
    {
        //
    }

    public function update(Request $request, Contract $contract)
    {
        #se valida la informacion del nuevo contrato
        $validate = $this->validate_contract_data($request, $contract);

        #se actualiza la informacion del contrato
        $contract->update($request->all());

        #se retorna con mensaje de confirmacion
        return redirect()->back()->with("msg", "El contrato '{$contract->name}' ha sido actualizado");
    }

    public function destroy(Contract $contract)
    {
        //
    }
}
