@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <a class="btn btn-link p-0" href="{{ route('contact-types.index') }}/">
                <i class="fa fa-angle-left fa-sm mr-1"></i>
                Regresar
            </a>
            <a class="btn btn-primary ml-auto" href="{{ route('contact-types.edit', $contact_type) }}/">
                <i class="fa fa-pencil-alt fa-xs"></i>
                Editar
            </a>
        </div>
        <table class="table table-striped m-0">
            <tr>
                <th width="30%">Nombre</th>
                <td>{{ $contact_type->name }}</td>
            </tr>
        </table>
    </div>
</div>
@stop