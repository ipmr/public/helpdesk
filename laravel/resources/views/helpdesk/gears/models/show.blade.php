@extends('layouts.master')
@section('content')

<div class="container" id="typeApp">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('brands.types.show', [$brand, $type]) }}" class="btn btn-link p-0">
				<i class="fa fa-angle-left"></i>
				Regresar
			</a>
		</div>
		<table class="table table-striped m-0">
			<thead>
				<tr style="border-bottom: 1px solid #f2f2f2" v-cloak class="bg-secondary">
					<td style="vertical-align: middle;">
						<div v-show="edit_model == true">
							<form action="{{ route('brands.types.models.update', [$brand, $type, $model]) }}" method="post" class="d-flex align-items-center">
								{{ csrf_field() }}
								{{ method_field('PATCH') }}
								<input type="text" class="form-control mr-3 text-uppercase" id="update_model_name_input" name="name" value="{{ $model->name }}">
								<button type="submit" class="btn btn-success ml-auto">
									<i class="fa fa-check"></i> Actualizar
								</button>
								<a href="#" class="btn btn-link text-danger px-0 ml-3" @click.prevent="cancel_model_name_edition">
									<i class="fa fa-times"></i> Cancelar
								</a>
							</form>
						</div>
						<div v-if="!edit_model" class="d-flex align-items-center">
							<b>
								<a href="{{ route('brands.show', $brand) }}">{{ $brand->name }}</a> / 
								<a href="{{ route('brands.types.show', [$brand, $type]) }}">{{ $type->name }}</a> / 
								{{ $model->name }}
							</b>
							<span class="ml-auto">
								<a href="#" class="btn btn-info m-0" @click.prevent="edit_model_name">
									<i class="fa fa-pencil-alt"></i> Editar
								</a>
							</span>
						</div>
					</td>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
@stop

@section('scripts')
	
	<script>

		var type_app = new Vue({

			el: '#typeApp',
			data: {
				edit_model: false
			},
			methods: {
				edit_model_name: function(){

					var t = this;
					t.edit_model = true;
					setTimeout(function(){

						$('#update_model_name_input').focus();

					}, 150);

				},

				cancel_model_name_edition: function(){

					var t = this;
					t.edit_model = false;

				}
			}

		})

	</script>

@stop