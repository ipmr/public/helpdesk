<ul class="nav nav-tabs nav-justified card-header-tabs">
	<li class="nav-item">
		<a class="nav-link {{ session()->get('tab') == 'public' ? 'active' : '' }} {{ session()->get('tab') ? '' : 'active' }}" id="public-comments-tab" data-toggle="tab" href="#public-comments" role="tab" aria-controls="public-comments" aria-selected="true">
			@role('company')

				({{ $ticket->comments()->where('type', 'public')->count() }})

			@else
			
				({{ $ticket->comments()->latest()->count() }})
			
			@endrole
			Eventos{{--  @role('company') @else al cliente @endrole --}}
		</a>
	</li>
	@role('company')
	@else
	<li class="nav-item">
		<a class="nav-link {{ session()->get('tab') == 'tasks' ? 'active' : '' }}" id="tasks-tab" data-toggle="tab" href="#tasks" role="tab" aria-controls="tasks" aria-selected="false">
			({{ $ticket->tasks()->where('status', 1)->count() }}/{{ $ticket->tasks->count() }})
			Tareas
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">
			({{ $ticket->histories->count() }})
			Historial de ticket
		</a>
	</li>
	{{-- <li class="nav-item">
		<a class="nav-link {{ session()->get('tab') == 'internal' ? 'active' : '' }}" id="internal-comments-tab" data-toggle="tab" href="#internal-comments" role="tab" aria-controls="internal-comments" aria-selected="false">
			({{ $ticket->comments()->where('type', 'internal')->count() }})
			Comentarios internos
		</a>
	</li> --}}
	@endrole
</ul>