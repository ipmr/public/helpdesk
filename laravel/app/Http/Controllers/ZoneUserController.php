<?php

namespace App\Http\Controllers;

use App\User;
use App\ZoneUser;
use App\CompanyUserType;
use App\ContactCatalog;
use Illuminate\Http\Request;

class ZoneUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ZoneUser  $zone_user
     * @return \Illuminate\Http\Response
     */
    public function show(ZoneUser $zone_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ZoneUser  $zone_user
     * @return \Illuminate\Http\Response
     */
    public function edit(ZoneUser $zone_user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ZoneUser  $zone_user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone_user)
    {
        #obtener asignacion
        $zone_user = ZoneUser::where('id', $zone_user)->first();

        $company   = $zone_user->company;

        $last_user = $zone_user->user;

        #obtener contacto nuevo
        $new_contact = User::where('id', $request->new_contact_id)->first();

        #eliminar asignacion a zona
        $zone_user->delete();

        #asignar nuevo contacto a la compania
        $company->contact_id = $new_contact->id;

        $company->save();

        $exist_zone_user = ZoneUser::where('user_id', $new_contact->id)->where('zone_id', $company->id)->first();

        $contact_type = CompanyUserType::where('name', 'Contacto Principal')->first();

        if (isset($exist_zone_user->id)) {

            $exist_zone_user->role = $contact_type->id;

            $exist_zone_user->save();

        } else {
            $new_assign = new ZoneUser();

            $new_assign->user_id = $new_contact->id;
            $new_assign->zone_id = $company->id;
            $new_assign->role    = $contact_type->id;

            $new_assign->save();
        }

        return redirect()
            ->back()
            ->with('msg', 'El usuario "'.$last_user->full_name.'" no podrá visualizar más, los tickets de la zona "'.$company->name.'"'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ZoneUser  $zone_user
     * @return \Illuminate\Http\Response
     */
    public function destroy(ZoneUser $zoneuser)
    {


        $company = $zoneuser->company;


        if(request()->has('contact_id')){


            $relief_contact_already_assigned = $company->contacts()->where('user_id', request()->contact_id)->first();

            if($relief_contact_already_assigned){


                $relief_contact_already_assigned->delete();


            }

            $company->contact_id = request()->contact_id;
            $company->save();

            $relief_contact = new ZoneUser();
            $relief_contact->user_id = request()->contact_id;
            $relief_contact->zone_id = $company->id;
            $relief_contact->role = 1;

            $relief_contact->save();


        }


        $zoneuser->delete();



        // $zone_user = ZoneUser::where('id', $zone_user)->first();

        // $company   = $zone_user->company;

        // $last_user = $zone_user->user;

        // #eliminar asignacion
        // $zone_user->delete();


        return redirect()
            ->back()
            ->with("msg", "El contacto ha sido desasignado de la zona {$company->name}");
    }
}
