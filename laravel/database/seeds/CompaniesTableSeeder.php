<?php

use App\Company;
use App\CompanyUser;
use App\Department;
use App\Ticket;
use App\User;
use App\ZoneUser;
use App\ContactCatalog;
use App\Project;
use App\ProjectHistory;
use App\CompanyProject;
use App\Gear;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Company::class, 4)->create()->each(function ($company) {

            factory(User::class, 1)->create([

                'role' => 'company',

            ])->each(function ($u) use ($company) {

                factory(CompanyUser::class, 1)->create([
                    'user_id' => $u->id,
                    'team_id' => $company->id,
                ]);

                factory(ZoneUser::class, 1)->create([
                    'user_id' => $u->id,
                    'zone_id' => $company->id,
                    'role'    => 1,
                ]);

                factory(ContactCatalog::class, 1)->create([
                    'user_id' => $u->id,
                    'master_zone_id' => $company->id,
                ]);

                $company->update([
                    'contact_id' => $u->id,
                ]);

            });

            $department = Department::find(rand(1, 7));

            factory(Gear::class, rand(15,25))->create([
                'company_id'    => $company->id,
            ]);

            $gears = Gear::where('company_id', $company->id)->get();

            factory(Ticket::class, 30)->create([

                'company_id'    => $company->id,
                'contact_id'    => $company->contact->id,
                'department_id' => $department->id,

            ])->each(function ($ticket) use ($department,$gears) {

                $agent = $department->agents[rand(0, 4)];

                $ticket->update([
                    'agent_id'    => $agent->id,
                    'severity_id' => rand(2, 4),
                    'status_id'   => rand(1, 5),
                    'category_id' => rand(1, 7),
                    'created_at'  => \Carbon\Carbon::now()->subDays(rand(0, 15)),
                ]);

                $ticket->update([

                    'ticket_id' => str_pad($ticket->company_id, 3, '0', STR_PAD_LEFT) . str_pad($ticket->id, 5, '0', STR_PAD_LEFT),

                ]);

                $gears_id = [];
                if ($gears->count() > 0) {
                    foreach ($gears as $gear) {
                        $gears_id[] = $gear->id;
                    }

                    if (rand(0,1)) {
                        $ticket->update([

                            'gear_id' => $gears_id[ rand( 0, (count($gears_id)-1) ) ],

                        ]);
                    }
                }

            });

            factory(Project::class, rand(3,10))->create()->each(function($project) use ($company){

                factory(CompanyProject::class, 1)->create([
                    'project_id' => $project->id,
                    'company_id' => $company->id,
                ]);

                factory(ProjectHistory::class, 1)->create([
                    'project_id'  => $project->id,
                    'description' => 'Proyecto fue creado por el sistema',
                    'status'      => 1,
                    'condition'   => 1,
                ]);

            });
        });

    }
}

