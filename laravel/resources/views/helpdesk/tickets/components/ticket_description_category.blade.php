<form action="{{ route('tickets.update', $ticket->ticket_id) }}" id="updateCategoryForm" method="post" style="width: 100%;">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="form-group m-0">
        <select name="category" class="form-control" @change="active_success_btn" style="border-radius: 3px 0 0 3px">
            @foreach(App\Category::get() as $category)
            <option value="{{ $category->id }}" {{ $ticket->category_id == $category->id ? 'selected' : '' }}>
                {{ $category->name }}
            </option>
            @endforeach
        </select>
    </div>
</form>

<button style="line-height: 0; border-radius: 0 3px 3px 0" disabled type="submit" data-toggle="tooltip" data-placement="top" title="Actualizar Categoría" form="updateCategoryForm" class="btn btn-success">
    <i class="fa fa-check" style="margin: 0 !important;"></i>
</button>