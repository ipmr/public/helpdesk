@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <a class="btn btn-link p-0" href="{{ route('surveys.index') }}/">
                        <i class="fa fa-angle-left fa-sm mr-1"></i>
                        Regresar a lista de encuestas
                    </a>
                    <a class="btn btn-primary ml-auto" href="{{ route('surveys.edit', $survey) }}/">
                        <i class="fa fa-pencil-alt fa-xs"></i>
                        Editar
                    </a>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered table-striped m-0">
                        <tr>
                            <th width="30%">Nombre</th>
                            <td>{{ $survey->name }}</td>
                        </tr>
                        <tr>
                            <th>Descripción</th>
                            <td>{{ $survey->description }}</td>
                        </tr>
                        <tr>
                            <th>Departamento</th>
                            <td>
                                @if($survey->department_id)
                                    <a href="{{ route('departments.show', $survey->department_id) }}/">
                                        {{ $survey->departament->name }}
                                    </a>
                                @else
                                    Sin departamento
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Total preguntas</th>
                            <td>
                                <a href="{{ route('surveys.questions.index', [ 'survey' => $survey->id ] ) }}">
                                    {{ $survey->questions->count() }}
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop