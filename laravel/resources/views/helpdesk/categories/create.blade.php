@extends('layouts.master')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <a class="btn btn-link p-0" href="{{ route('ticket-options.index') }}/">
                <i class="fa fa-angle-left fa-sm mr-1"></i>
                Regresar
            </a>
        </div>
        <div class="card-body">
            @if ( isset($parent) )
                <form action="{{ route('category.subcategory.store', ['category' => $parent->id]) }}" method="post" id="newCategoryForm">
            @else
                <form action="{{ route('category.store') }}" method="post" id="newCategoryForm">
            @endif
                
                @include('helpdesk.categories.partials.form', ['btn'=>'Crear'])
                
            </form>
        </div>
    </div>
</div>
@stop