<div class="tab-pane fade {{ session()->get('tab') == 'internal' ? 'show active' : '' }}" id="internal-comments" role="tabpanel" aria-labelledby="internal-comments-tab">
	<div class="mb-3">
		<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#new_internal_comment">
			<i class="far fa-comment"></i>
			Nuevo comentario
		</a>
	</div>
	
	@if(count($ticket->comments()->where('type', 'internal')->get()) > 0)
	<table class="table table-sm m-0 table-bordered table-striped">
		<thead>
			<tr>
				<td>Fecha y hora</td>
				<td>Autor</td>
				<td>Comentario</td>
				<td>Archivos</td>
			</tr>
		</thead>
		<tbody>
			@foreach($ticket->comments()->where('type', 'internal')->get() as $comment)
			<tr>
				<td class="no-wrap">{{ $comment->created_at->format('d M, Y - h:i a') }}</td>
				<td class="no-wrap">{{ $comment->user->first_name .' '. $comment->user->last_name }}</td>
				<td>{{ $comment->comment }}</td>
				<td class="no-wrap">
					@if(count($comment->files) > 0)
						@foreach($comment->files as $file)
						<a data-fancybox="internal_gallery" rel="internal" href="{{asset('uploads/'.$file->filename)}}">
							<i class="fa fa-paperclip"></i> 
							{{ $file->filename }}
						</a>
						@if( ! $loop->last)
						<br>
						@endif
						@endforeach
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p class="m-0">No se han agregado comentarios</p>
	@endif
</div>