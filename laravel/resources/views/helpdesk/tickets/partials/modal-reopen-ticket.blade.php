@component('helpdesk.components.modal', [
    'id'    => 'reopen_ticket',
    'title' => 'Reabrir ticket'
])

    @slot('body')
        <form action="{{ route('reopen_ticket', ['ticket' => $ticket->ticket_id ]) }}" method="post" id="reopen_ticket_form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">Asunto</label> <br>
                <input type="text" name="subject" class="form-control" placeholder="Asunto" required autofocus>
            </div>
            <div class="form-group">
                <label for="">Solicitud</label> <br>
                <textarea name="message" class="form-control" placeholder="Detalle solicitud..." required></textarea>
            </div>
        </form>
    @endslot
    @slot('footer')
        <button type="submit" form="reopen_ticket_form" class="btn btn-success">
            <i class="fa fa-paper-plane"></i>
            Enviar solicitud
        </button>
    @endslot
@endcomponent