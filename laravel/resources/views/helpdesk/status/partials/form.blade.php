<div class="form-row">
    <div class="col-sm-8 ml-auto">
        <label><b>Información del estado</b></label>
        <p>Campos requeridos marcados con <span class="text-danger">(*)</span></p>
    </div>
</div>
{{ csrf_field() }}
<div class="row form-row">
    <label class="col-sm-4 col-form-label text-right">
        <span class="text-danger">*</span>
        Nombre:
    </label>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="text" name="name" value="{{ $status->name or old('name') }}" class="form-control" autofocus autocomplete="off" required @isset($status){{ $status->static ? 'disabled' : '' }}@endisset placeholder="Nombre que verá el agente">
			@if(isset($status) && $status->static)
            <span class="help-block d-block mt-2 text-danger">El nombre del estado no puede ser modificado</span>
            @endif
        </div>
    </div>        
</div>
<div class="row form-row">
    <label class="col-sm-4 col-form-label text-right">Nombre para el cliente:</label>
    <div class="col-sm-6">
        <div class="form-group">
            <input type="text" name="description" value="{{ $status->description or old('description') }}" class="form-control" placeholder="Nombre que verá el cliente...">
        </div>
    </div>        
</div>